package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-10-25 15:38
 *
 * Saga的执行顺序有两种：
 * T:正向能力
 * C:补偿能力
 * T1, T2, T3, ..., Tn
 * T1, T2, ..., Tj, Cj,..., C2, C1，其中0 < j < n
 */
public enum SagaRecoverStrategy {
  /**
   * 向后恢复，补偿所有已完成的事务，如果任一子事务失败。
   * 这种做法的效果是撤销掉之前所有成功的sub-transation，使得整个Saga的执行结果撤销
   * 例：T1, T2, ..., Tj, Cj,..., C2, C1，其中0 < j < n
   */
  BACKWARD,
  /**
   * 向前恢复，重试失败的事务，假设每个子事务最终都会成功。
   * 适用于必须要成功的场景，执行顺序是类似于这样的：T1, T2, ..., Tj(失败), Tj(重试),..., Tn，
   * 其中j是发生错误的sub-transaction。该情况下不需要Ci
   *
   */
  FORWARD,
}
