package org.smartframework.saga.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author noah
 * @create 2021-09-30 16:18
 */
public class DefaultSagaDefinition<Data> implements SagaDefinition<Data> {

  private List<SagaStep<Data>> sagaSteps;
  private SagaRecoverStrategy sagaRecoverStrategy;
  private List<SagaStepCompletedObserver> sagaStepCompletedObservers;
  private Logger logger = LoggerFactory.getLogger(DefaultSagaDefinition.class);

  private Saga<Data> saga;


  private void onSagaStepCompleted(String sagaId, SagaStep<Data> sagaStep,
      SagaExecutionState currState,
      SagaExecutionState state,
      Data sagaData, boolean skip) {
    if (sagaStepCompletedObservers != null && sagaStepCompletedObservers.size() > 0) {
      for (SagaStepCompletedObserver observer : sagaStepCompletedObservers) {
        try {
          observer
              .apply(sagaId, (Saga<Object>) saga, (SagaStep<Object>) sagaStep, currState, state,
                  sagaData, skip);
        } catch (Exception ex) {
          logger.warn("onSagaStepCompleted Failure", ex.getMessage());
        }
      }
    }

  }

  public DefaultSagaDefinition(Saga<Data> saga, List<SagaStep<Data>> sagaSteps,
      SagaRecoverStrategy sagaRecoverStrategy) {
    this.saga = saga;
    this.sagaSteps = sagaSteps;
    this.sagaRecoverStrategy = sagaRecoverStrategy;
  }

  @Override
  public SagaActions<Data> start(String sagaId, Data sagaData,
      List<SagaStepCompletedObserver> sagaStepCompletedObservers) {
    this.sagaStepCompletedObservers = sagaStepCompletedObservers;
    SagaExecutionState initState = new SagaExecutionState(0, false);
    initState.setSagaRecoverType(sagaRecoverStrategy);
    return start(sagaId, initState, sagaData, sagaStepCompletedObservers);
//    if (stepToExecute.isEmpty()) {
//      return makeEndStateSagaActions(currentState);
//    } else
//      return stepToExecute.executeStep(sagaData, currentState);
  }

  @Override
  public SagaActions<Data> start(String sagaId, SagaExecutionState state, Data sagaData,
      List<SagaStepCompletedObserver> sagaStepCompletedObservers) {
    this.sagaStepCompletedObservers = sagaStepCompletedObservers;
    state.setSagaRecoverType(sagaRecoverStrategy);

    stepToExecute(sagaId, state, sagaData);
    stepToCompensating(sagaId, state, sagaData);

    final SagaActions<Data> sagaActions = new SagaActions<>();
    sagaActions.setData(sagaData);
    sagaActions.setExecutionState(state);
//    final SagaStep<Data> sagaStep = sagaSteps.get(state.getCurrentlyExecuting());
    sagaActions.setCurrentlyStepName(state.getCurrentlyStepName());
    return sagaActions;
  }


  private void stepToExecute(String sagaId, SagaExecutionState state, Data data) {
    if (state.isEndState()
        || state.isStop()
        || state.isCompensating()) {
      return;
    }
    if (state.getCurrentlyExecuting() >= sagaSteps.size()) {
      state.end();
      return;
    }
    final SagaStep<Data> step = sagaSteps.get(state.getCurrentlyExecuting());
    state.setCurrentlyStepName(step.getName());
    SagaExecutionState stateCopy = state.copy();
    final boolean hasAction = step.hasAction(data);
    if (!state.isCompensating() && hasAction) {
      final StepToExecute<Data> stepToExecute = new StepToExecute<>(step, state);
      stepToExecute.executeStep(data);
//      onSagaStepCompleted(step, state, data, false);
//      if (state.isCompensating() || state.isStop()) {
//        return;
//      }
    }
//    else {
//      onSagaStepCompleted(step, state, data, !hasAction);
//    }
//    if (state.getCurrentlyExecuting() == sagaSteps.size() - 1) {
//      state.end();
//      return;
//    }
    if (!state.isStop() && !state.isCompensating()
        && state.getCurrentlyExecuting() == sagaSteps.size() - 1) {
      state.end();
    }

    onSagaStepCompleted(sagaId, step, stateCopy, state, data, !hasAction);

    if (state.isEndState()
        || state.isStop()
        || state.isCompensating()) {
      return;
    }

    state.currentlyExecutingStepPlusOne();

    stepToExecute(sagaId, state, data);
  }


  private void stepToCompensating(String sagaId, SagaExecutionState state, Data data) {
    if (state.isEndState()
        || state.isStop()
        || !state.isCompensating()) {
      return;
    }
    if (state.getCurrentlyExecuting() < 0) {
      state.end();
      return;
    }
    final SagaStep<Data> step = sagaSteps.get(state.getCurrentlyExecuting());
    state.setCurrentlyStepName(step.getName());
    SagaExecutionState stateCopy = state.copy();
    final boolean hasCompensation = step.hasCompensation(data);
    if (state.isCompensating() && hasCompensation) {
      final StepToExecute<Data> stepToExecute = new StepToExecute<>(step, state);
      stepToExecute.executeStep(data);
//      onSagaStepCompleted(step, state, data, false);
//      if (state.isStop()) {
//        return;
//      }
    }
//    else {
//      onSagaStepCompleted(step, state, data, !hasCompensation);
//    }
    if (!state.isStop() && state.getCurrentlyExecuting() == 0) {
      state.end();
    }

    onSagaStepCompleted(sagaId, step, stateCopy, state, data, !hasCompensation);

    if (state.isEndState()
        || state.isStop()) {
      return;
    }
    state.currentlyExecutingStepMinusOne();
    stepToCompensating(sagaId, state, data);
  }


}
