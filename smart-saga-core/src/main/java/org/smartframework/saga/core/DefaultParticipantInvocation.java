package org.smartframework.saga.core;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author noah
 * @create 2021-09-30 18:12
 */
public class DefaultParticipantInvocation<Data> implements ParticipantInvocation<Data> {

  private Optional<Predicate<Data>> invocablePredicate;
  private Function<Data, SagaCommandResult> commandBuilder;

  public DefaultParticipantInvocation(Optional<Predicate<Data>> invocablePredicate,
                                      Function<Data, SagaCommandResult> commandBuilder) {
    this.invocablePredicate = invocablePredicate;
    this.commandBuilder = commandBuilder;
  }

  @Override
  public boolean isInvocable(Data data) {
    return invocablePredicate.map(p -> p.test(data)).orElse(true);
  }

  @Override
  public SagaCommandResult makeCommand(Data data) {
    return commandBuilder.apply(data);
  }
}
