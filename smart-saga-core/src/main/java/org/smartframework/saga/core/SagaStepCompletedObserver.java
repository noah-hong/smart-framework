package org.smartframework.saga.core;


/**
 * @author noah
 * @create 2021-10-26 15:55
 */
@FunctionalInterface
public interface SagaStepCompletedObserver {

  void apply(String sagaId, Saga<Object> saga, SagaStep<Object> sagaStep,
      SagaExecutionState currState,
      SagaExecutionState state,
      Object sagaData, boolean skip);
}
