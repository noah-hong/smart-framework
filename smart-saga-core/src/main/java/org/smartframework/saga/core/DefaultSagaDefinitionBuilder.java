package org.smartframework.saga.core;

import java.util.LinkedList;
import java.util.List;


/**
 * @author noah
 * @create 2021-09-30 16:55
 */
public class DefaultSagaDefinitionBuilder<Data> {

  private List<SagaStep<Data>> sagaSteps = new LinkedList<>();

  public void addStep(SagaStep<Data> sagaStep) {
    sagaSteps.add(sagaStep);
  }


  private Saga<Data> saga;

  public Saga<Data> getSaga() {
    return saga;
  }

  public DefaultSagaDefinitionBuilder(Saga<Data> saga) {
    this.saga = saga;

  }

  public SagaDefinition<Data> build(SagaRecoverStrategy recoverStrategy) {
    return new DefaultSagaDefinition<>(saga, sagaSteps, recoverStrategy);
  }

}
