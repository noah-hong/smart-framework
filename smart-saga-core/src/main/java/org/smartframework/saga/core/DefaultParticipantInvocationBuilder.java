package org.smartframework.saga.core;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author noah
 * @create 2021-09-30 17:09
 */
public class DefaultParticipantInvocationBuilder<Data> {

  private final DefaultSagaDefinitionBuilder<Data> sagaDefinitionBuilder;
  private final DefaultSagaStepBuilder<Data> stepBuilder;

  protected Optional<ParticipantInvocation<Data>> action = Optional.empty();
  protected Optional<ParticipantInvocation<Data>> compensation = Optional.empty();

  public DefaultParticipantInvocationBuilder(DefaultSagaStepBuilder stepBuilder,
      DefaultSagaDefinitionBuilder<Data> parent) {
    this.sagaDefinitionBuilder = parent;
    this.stepBuilder = stepBuilder;
  }

  public DefaultParticipantInvocationBuilder<Data> withCompensation(Optional<ParticipantInvocation<Data>> compensation) {
    this.compensation = compensation;
    return this;
  }
  public DefaultParticipantInvocationBuilder<Data> invokeParticipant(Optional<ParticipantInvocation<Data>> action) {
    this.action = action;
    return this;
  }


  public DefaultParticipantInvocationBuilder<Data> withCompensation(
      Function<Data, SagaCommandResult> compensation) {
    this.compensation = Optional
        .of(new DefaultParticipantInvocation<>(Optional.empty(), compensation));
    return this;
  }

  public DefaultParticipantInvocationBuilder<Data> withCompensation(
      Predicate<Data> compensationPredicate,
      Function<Data, SagaCommandResult> compensation) {
    this.compensation = Optional
        .of(new DefaultParticipantInvocation<>(Optional.of(compensationPredicate), compensation));
    return this;
  }

  public DefaultParticipantInvocationBuilder<Data> invokeParticipant(
      Function<Data, SagaCommandResult> action) {
    this.action = Optional.of(new DefaultParticipantInvocation<>(Optional.empty(), action));
    return this;
  }


  public DefaultParticipantInvocationBuilder<Data> invokeParticipant(
      Predicate<Data> invokeParticipantPredicate,
      Function<Data, SagaCommandResult> compensation) {
    this.action = Optional
        .of(new DefaultParticipantInvocation<>(Optional.of(invokeParticipantPredicate),
            compensation));
    return this;
  }

//    public DefaultSagaStepBuilder<Data> step() {
//        addStep();
//        return new DefaultSagaStepBuilder<>(sagaDefinitionBuilder);
//    }

  public DefaultSagaStepBuilder<Data> step(String setpName) {
    addStep();
    return new DefaultSagaStepBuilder<>(setpName, sagaDefinitionBuilder);
  }

  private void addStep() {
    sagaDefinitionBuilder
        .addStep(new DefaultSagaStep<>(stepBuilder.getName(), action, compensation));
  }

  public SagaDefinition<Data> build() {
    addStep();
    return sagaDefinitionBuilder.build(SagaRecoverStrategy.BACKWARD);
  }

  public SagaDefinition<Data> build(SagaRecoverStrategy recoverStrategy) {
    addStep();
    return sagaDefinitionBuilder.build(recoverStrategy);
  }
}
