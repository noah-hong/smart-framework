package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-10-12 11:52
 */
public class AppTest {


    public static void main(String[] args) {
        final SagaInstance sagaInstance = new SagaInstance();


        SagaManager<TestSagaData> sagaManager = new DefaultSagaManager<>(new SagaTest(), new SagaInstanceRepository() {
            @Override
            public void save(SagaInstance sagaInstance) {

            }

            @Override
            public void saveAsync(SagaInstance sagaInstance) {

            }

            @Override
            public SagaInstance find(String sagaId) {
                return sagaInstance;
            }

            @Override
            public void savaStep(String sagaId,SagaExecutionState currstate, SagaExecutionState state, Object sagaData,
                boolean skip) {

            }

            @Override
            public void savaStepAsync(String sagaId, SagaExecutionState currstate,
                SagaExecutionState state, Object sagaData, boolean skip) {

            }


        });
        final TestSagaData testSagaData = new TestSagaData();
        testSagaData.setCount(0);
        SagaInstance _sagaInstance = sagaManager.create("1", testSagaData);
        System.out.println(_sagaInstance);

        sagaInstance.setCurrentlyExecuting(_sagaInstance.getCurrentlyExecuting());
        sagaInstance.setCompensating(_sagaInstance.isCompensating());
        sagaInstance.setEndState(_sagaInstance.isEndState());
        sagaInstance.setExecutingException(_sagaInstance.getExecutingException());
        sagaInstance.setCompensatingException(_sagaInstance.getCompensatingException());
        sagaInstance.setLastMessage(_sagaInstance.getLastMessage());
        sagaInstance.setStop(_sagaInstance.isStop());
        sagaInstance.setCurrentlyStepName(_sagaInstance.getCurrentlyStepName());
        sagaInstance.setSagaData(_sagaInstance.getSagaData());
        sagaInstance.setExecutingMessages(_sagaInstance.getExecutingMessages());
        sagaInstance.setCompensatingMessages(_sagaInstance.getCompensatingMessages());
        sagaInstance.setSagaId(_sagaInstance.getSagaId());
        sagaInstance.setSagaName(_sagaInstance.getSagaName());
        sagaInstance.setSagaType(_sagaInstance.getSagaType());
        testSagaData.setCount(3);
        sagaInstance.setSagaData(testSagaData);

        final SagaInstance secondSagaInstance = sagaManager.recover("1");
        System.out.println(secondSagaInstance);
    }

    public static class TestSagaData {
        @Override
        public String toString() {
            return "TestSagaData{" +
                    "count=" + count +
                    '}';
        }

        private int count;

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }

    public static class SagaTest implements SagaDsl<TestSagaData> {

        private SagaDefinition<TestSagaData> sagaDefinition =
                step("step1")
                    .invokeParticipant(this::test1)
//                    .withCompensation(this::reject1)
                .step("step2")
                    .invokeParticipant(this::test2)
                    .withCompensation(this::reject2)
                .step("step3")
                    .invokeParticipant(this::predicate, this::test3)
                    .withCompensation(this::reject1)

                    .build(SagaRecoverStrategy.FORWARD);

        public boolean predicate(TestSagaData data) {
            return false;
        }
        public SagaCommandResult reject2(TestSagaData data){
//            throw new RuntimeException("");
            final SagaCommandResult participantInvocationResult = new SagaCommandResult();
            SagaCommandExceutionStatus status = SagaCommandExceutionStatus.SUCCESS;
            if (data.getCount() == 2) {
                status = SagaCommandExceutionStatus.FAILURE;
                participantInvocationResult.setMessage("情况异常，停止");
                System.out.println("reject2 情况异常，停止");
            }else{
                System.out.println("reject2 print " + data.getCount());
            }
            participantInvocationResult.setStatus(status);
            return participantInvocationResult;
        }


        public SagaCommandResult test4(TestSagaData data) {
            throw new RuntimeException("222");
        }

        public SagaCommandResult reject1(TestSagaData data) {
//            data.setCount(data.getCount() - 1);
            System.out.println("reject1 print " + data.getCount());
            final SagaCommandResult participantInvocationResult = new SagaCommandResult();
            SagaCommandExceutionStatus status = SagaCommandExceutionStatus.SUCCESS;
            participantInvocationResult.setStatus(status);
            participantInvocationResult.setMessage("回滚成功");
            return participantInvocationResult;
        }
        public SagaCommandResult test11(TestSagaData data) {
            data.setCount(11);
            System.out.println("test1 print " + data.getCount());
            final SagaCommandResult participantInvocationResult = new SagaCommandResult();
            SagaCommandExceutionStatus status = SagaCommandExceutionStatus.SUCCESS;
            participantInvocationResult.setStatus(status);
            return participantInvocationResult;
        }
        public SagaCommandResult test1(TestSagaData data) {
            data.setCount(2);
            System.out.println("test1 print " + data.getCount());
            final SagaCommandResult participantInvocationResult = new SagaCommandResult();
            SagaCommandExceutionStatus status = SagaCommandExceutionStatus.SUCCESS;
            participantInvocationResult.setStatus(status);
            return participantInvocationResult;
        }

        public SagaCommandResult test2(TestSagaData data) {
            final SagaCommandResult participantInvocationResult = new SagaCommandResult();
            SagaCommandExceutionStatus status = SagaCommandExceutionStatus.SUCCESS;
            if (data.getCount() == 2) {
                status = SagaCommandExceutionStatus.FAILURE;
                participantInvocationResult.setMessage("情况异常，停止");
            }
            participantInvocationResult.setStatus(status);
            return participantInvocationResult;
        }

        public SagaCommandResult test3(TestSagaData data) {
//            data.setCount(data.getCount() + 1);

            final SagaCommandResult participantInvocationResult = new SagaCommandResult();
            SagaCommandExceutionStatus status = SagaCommandExceutionStatus.FAILURE;
            participantInvocationResult.setStatus(status);
            participantInvocationResult.setMessage("情况异常，停止");
            System.out.println("test3 情况异常，停止");
            return participantInvocationResult;
        }

        @Override
        public String getSagaName() {
            return "SagaTest";
        }

        @Override
        public SagaDefinition<TestSagaData> getSagaDefinition() {
            return sagaDefinition;
        }

        @Override
        public void onStarting(String sagaId, TestSagaData testSagaData) {
            System.out.println("starting sagaid:"+sagaId+"data:"+testSagaData.toString());
        }

        @Override
        public void onSagaCompletedSuccessfully(SagaInstance sagaInstance) {
            System.out.println("success");
        }

        @Override
        public void onSagaRolledBack(SagaInstance sagaInstance) {
            System.out.println("rolledback");
        }
    }
}
