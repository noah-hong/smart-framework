package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-10-18 16:35
 */
public enum SagaStatus {

  /**
   * 启动中
   */
  STARTING("启动中"),
  /**
   * 执行中
   */
  EXECUTEING("执行中"),
  /**
   * 回滚中
   */
  ROLLBACKING("回滚中"),
  /**
   * 回滚完成
   */
  ROLLBACKED("回滚完成"),
  /**
   * 执行完成
   */
  EXECUTED("执行完成"),
  /**
   * 执行终止
   */
  STOPED("执行终止"),
  /**
   * 未知
   */
  UNKNOWN("未知"),

  ;

  private final String message;

  public String message() {
    return message;
  }


  SagaStatus(String msg) {
    this.message = msg;
  }
}
