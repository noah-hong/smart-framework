package org.smartframework.saga.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author noah
 * @create 2021-10-10 22:22
 */
public class StepToExecute<Data> {

  Logger logger = LoggerFactory.getLogger(StepToExecute.class);
  private final SagaStep<Data> step;
  //    private final int skipped;
  private final SagaExecutionState currentState;


  public StepToExecute(SagaStep<Data> step, SagaExecutionState currentState) {
    this.currentState = currentState;
    this.step = step;
  }

//    private int size() {
//        return step.map(x -> 1).orElse(0) + skipped;
//    }


  public void executeStep(Data data) {
    try {
      step.makeStep(currentState, data);
    } catch (Exception ex) {
      currentState.setLastMessage(ex.getMessage());
      logger.error("步骤:" + step.getName() + " 异常 " + ex.getMessage(), ex);
      if (currentState.isCompensating()) {
        currentState.getCompensatingMessages().put(step.getName(), ex.getMessage());
        currentState.setCompensatingException(ex);
        currentState.stop();
      } else {
        currentState.getExecutingMessages().put(step.getName(), ex.getMessage());
        if (currentState.getSagaRecoverType() == SagaRecoverStrategy.BACKWARD) {
          currentState.startCompensating();
        } else {
          currentState.stop();
        }
        currentState.setExecutingException(ex);
//                if()
      }
    }

//
//        return builder
//                .withUpdatedSagaData(data)
//                .withUpdatedState(encodeState(newState))
//                .withIsEndState(newState.isEndState())
//                .withIsCompensating(compensating)
//                .build();
  }

}
