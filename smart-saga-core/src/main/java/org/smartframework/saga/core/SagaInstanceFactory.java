package org.smartframework.saga.core;

import java.util.Optional;
import java.util.UUID;

/**
 * @author noah
 * @create 2021-10-13 16:49
 */
public class SagaInstanceFactory {

  private SagaManagerFactory defaultSagaManagerFactory;

  public SagaInstanceFactory(SagaManagerFactory defaultSagaManagerFactory) {
    this.defaultSagaManagerFactory = defaultSagaManagerFactory;
  }
  public <SagaData> SagaInstance create(Saga<SagaData> saga, SagaData sagaData, String sagaName) {
    SagaManager<SagaData> sagaManager = defaultSagaManagerFactory.create(saga);
    return sagaManager.create(UUID.randomUUID().toString(), Optional.ofNullable(sagaName), sagaData);
  }

  public <SagaData> SagaInstance create(Saga<SagaData> saga, SagaData sagaData) {
    SagaManager<SagaData> sagaManager = defaultSagaManagerFactory.create(saga);
    return sagaManager.create(UUID.randomUUID().toString(), Optional.empty(), sagaData);
  }

  public <SagaData> SagaInstance create(Saga<SagaData> saga, String sagaId, SagaData sagaData) {
    SagaManager<SagaData> sagaManager = defaultSagaManagerFactory.create(saga);
    return sagaManager.create(sagaId, Optional.empty(), sagaData);
  }

  public <SagaData> SagaInstance create(Saga<SagaData> saga, String sagaId, String sagaName,
      SagaData sagaData) {
    SagaManager<SagaData> sagaManager = defaultSagaManagerFactory.create(saga);
    return sagaManager.create(sagaId, Optional.of(sagaName), sagaData);
  }

  public <SagaData> SagaInstance recover(String sagaId) {
    SagaManager<SagaData> sagaManager = defaultSagaManagerFactory.create();
    return sagaManager.recover(sagaId);
  }

  public <SagaData> SagaInstance recover(String sagaId, SagaData sagaData) {
    SagaManager<SagaData> sagaManager = defaultSagaManagerFactory.create();
    return sagaManager.recover(sagaId, sagaData);
  }

  public <SagaData> SagaInstance restart(String sagaId) {
    SagaManager<SagaData> sagaManager = defaultSagaManagerFactory.create();
    return sagaManager.restart(sagaId);
  }
  public <SagaData> SagaInstance skipCurrent(String sagaId) {
    SagaManager<SagaData> sagaManager = defaultSagaManagerFactory.create();
    return sagaManager.skipCurrent(sagaId);
  }

}
