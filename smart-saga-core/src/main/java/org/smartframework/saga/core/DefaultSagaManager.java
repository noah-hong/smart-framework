package org.smartframework.saga.core;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.UnaryOperator;

/**
 * @author noah
 * @create 2021-10-10 22:03
 */
public class DefaultSagaManager<Data> implements SagaManager<Data> {

  private SagaInstanceRepository sagaInstanceRepository;
  private Saga<Data> saga;
  private Logger logger = LoggerFactory.getLogger(DefaultSagaManager.class);
  List<SagaStepCompletedObserver> observers = null;

  public DefaultSagaManager(SagaInstanceRepository sagaInstanceRepository) {
    this.sagaInstanceRepository = sagaInstanceRepository;
  }

  public DefaultSagaManager(SagaInstanceRepository sagaInstanceRepository,
      List<SagaStepCompletedObserver> observers) {
    this.sagaInstanceRepository = sagaInstanceRepository;
    this.observers = observers;
  }

  public DefaultSagaManager(Saga<Data> saga, SagaInstanceRepository sagaInstanceRepository) {
    this(sagaInstanceRepository);
    this.saga = saga;
  }

  public DefaultSagaManager(Saga<Data> saga, SagaInstanceRepository sagaInstanceRepository,
      List<SagaStepCompletedObserver> observers) {
    this(sagaInstanceRepository, observers);
    this.saga = saga;
  }

  private void processSagaAction(SagaActions<Data> sagaActions, SagaInstance sagaInstance) {
    sagaInstance.setCurrentlyExecuting(sagaActions.getExecutionState().getCurrentlyExecuting());
    sagaInstance.setCompensating(sagaActions.getExecutionState().isCompensating());
    sagaInstance.setEndState(sagaActions.getExecutionState().isEndState());
    sagaInstance.setExecutingException(sagaActions.getExecutionState().getExecutingException());
    sagaInstance
        .setCompensatingException(sagaActions.getExecutionState().getCompensatingException());
    sagaInstance.setLastMessage(sagaActions.getExecutionState().getLastMessage());
    sagaInstance.setStop(sagaActions.getExecutionState().isStop());
    sagaInstance.setCurrentlyStepName(sagaActions.getCurrentlyStepName());
    sagaInstance.setSagaData(sagaActions.getData());
    sagaInstance.setCompensatingMessages(sagaActions.getExecutionState().getCompensatingMessages());
    sagaInstance.setExecutingMessages(sagaActions.getExecutionState().getExecutingMessages());
    sagaInstance.setSagaStatus(sagaActions.getExecutionState().getSagaStatus());
    sagaInstance.setSagaRecoverStrategy(sagaActions.getExecutionState().getSagaRecoverType());
  }

  private void processSaga(SagaInstance sagaInstance) {

    if (sagaInstance.isEndState()) {
      if (sagaInstance.isCompensating()) {
        saga.onSagaRolledBack(sagaInstance);
      } else {
        saga.onSagaCompletedSuccessfully(sagaInstance);
      }
    } else {
      saga.onStoped(sagaInstance);
    }

  }

  @Override
  public SagaInstance create(String sagaId, Data sagaData) {
    return create(sagaId, Optional.empty(), sagaData);
  }


  @Override
  public SagaInstance create(String sagaId, Optional<String> sagaName, Data sagaData) {
    saga.onStarting(sagaId, sagaData);
    final SagaDefinition<Data> sagaDefinition = saga.getSagaDefinition();
    String defaultSagaName = saga.getSagaName();

    final SagaInstance sagaInstance = new SagaInstance();
    sagaInstance.setSagaId(sagaId);
    sagaInstance.setSagaName(sagaName.orElse(defaultSagaName));
    sagaInstance.setSagaType(saga.getClass());
    sagaInstance.setSagaRecoverStrategy(saga.getSagaDefinition().recoverStrategy());
    sagaInstance.setSagaStatus(SagaStatus.STARTING);
    sagaInstance.setSagaData(sagaData);
    sagaInstanceRepository.save(sagaInstance);
    final SagaActions<Data> sagaActions = sagaDefinition.start(sagaId, sagaData, observers);

    processSagaAction(sagaActions, sagaInstance);

    processSaga(sagaInstance);

    sagaInstanceRepository.saveAsync(sagaInstance);

    return sagaInstance;
  }

  protected Saga<Data> restoreSaga(Class<?> aSagaType) {
    try {
      return (Saga<Data>) aSagaType.newInstance();
    } catch (Exception e) {
      return null;
    }
  }

  private SagaInstance recoverWith(String sagaId,
      UnaryOperator<SagaExecutionState> stateFunc, UnaryOperator<Data> dataFunc) {
    final SagaInstance sagaInstance = sagaInstanceRepository.find(sagaId);
    if (dataFunc != null) {
      sagaInstance.setSagaData(dataFunc.apply((Data) sagaInstance.getSagaData()));
    }
    Objects.requireNonNull(sagaInstance);
    this.saga = restoreSaga(sagaInstance.getSagaType());
    Objects.requireNonNull(this.saga);
    sagaInstance.setSagaRecoverStrategy(saga.getSagaDefinition().recoverStrategy());

    SagaExecutionState state = new SagaExecutionState();
    state.setStop(false);
    state.setCompensating(sagaInstance.isCompensating());
    state.setEndState(sagaInstance.isEndState());
    state.setCurrentlyExecuting(sagaInstance.getCurrentlyExecuting());
    if (stateFunc != null) {
      state = stateFunc.apply(state);
    }

    final SagaActions<Data> newSagaActions = saga.getSagaDefinition()
        .start(sagaId, state, (Data) sagaInstance.getSagaData(), observers);

    processSagaAction(newSagaActions, sagaInstance);
    sagaInstanceRepository.save(sagaInstance);
    processSaga(sagaInstance);
    return sagaInstance;

  }

  @Override
  public SagaInstance recover(String sagaId) {
    return recoverWith(sagaId, UnaryOperator.identity(), UnaryOperator.identity());
  }

  @Override
  public SagaInstance recover(String sagaId, Data sagaData) {
    return recoverWith(sagaId, UnaryOperator.identity(), (data) -> sagaData);
//    final SagaInstance sagaInstance = sagaInstanceRepository.find(sagaId);
//    Objects.requireNonNull(sagaInstance);
//    sagaInstance.setSagaData(sagaData);
//    this.saga = restoreSaga(sagaInstance.getSagaType());
//    Objects.requireNonNull(this.saga);
//    sagaInstance.setSagaRecoverStrategy(saga.getSagaDefinition().recoverStrategy());
//
//    SagaActions<Data> sagaActions = new SagaActions<>();
//    SagaExecutionState state = new SagaExecutionState();
//    state.setStop(false);
//    state.setCompensating(sagaInstance.isCompensating());
//    state.setEndState(sagaInstance.isEndState());
//    state.setCurrentlyExecuting(sagaInstance.getCurrentlyExecuting());
//
//    sagaActions.setExecutionState(state);
//    sagaActions.setData((Data) sagaInstance.getSagaData());
//    sagaActions.setCurrentlyStepName(sagaInstance.getCurrentlyStepName());
//
//    final SagaActions<Data> newSagaActions = saga.getSagaDefinition()
//        .start(sagaId, sagaActions.getExecutionState(), sagaActions.getData(), observers);
//    processSagaAction(newSagaActions, sagaInstance);
//    sagaInstanceRepository.save(sagaInstance);
//
//    processSaga(sagaInstance);
//    return sagaInstance;
  }

  @Override
  public SagaInstance restart(String sagaId) {
    final SagaInstance sagaInstance = sagaInstanceRepository.find(sagaId);
    Objects.requireNonNull(sagaInstance);
    this.saga = restoreSaga(sagaInstance.getSagaType());
    Objects.requireNonNull(this.saga);
    return create(sagaId, Optional.ofNullable(sagaInstance.getSagaName()),
        (Data) sagaInstance.getSagaData());

  }

  @Override
  public SagaInstance skipCurrent(String sagaId) {
    return recoverWith(sagaId, (state) -> {
      if (state.isCompensating()) {
        state.setCurrentlyExecuting(state.getCurrentlyExecuting() - 1);
      } else {
        state.setCurrentlyExecuting(state.getCurrentlyExecuting() + 1);
      }
      return state;
    }, UnaryOperator.identity());
  }

}
