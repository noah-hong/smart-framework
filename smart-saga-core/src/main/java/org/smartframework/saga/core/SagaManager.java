package org.smartframework.saga.core;

import java.util.Optional;

/**
 * @author noah
 * @create 2021-09-30 15:32
 */
public interface SagaManager<Data> {


  SagaInstance create(String sagaId, Data sagaData);

  SagaInstance create(String sagaId, Optional<String> sagaName, Data sagaData);

  SagaInstance recover(String sagaId);
  SagaInstance recover(String sagaId, Data sagaData);
  SagaInstance restart(String sagaId);
  SagaInstance skipCurrent(String sagaId);
}
