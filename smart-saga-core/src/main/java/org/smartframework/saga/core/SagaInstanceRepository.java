package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-10-11 22:04
 */
public interface SagaInstanceRepository {

  void save(SagaInstance sagaInstance);

  void saveAsync(SagaInstance sagaInstance);

  SagaInstance find(String sagaId);

  void savaStep(String sagaId,SagaExecutionState currstate, SagaExecutionState state, Object sagaData, boolean skip);
  void savaStepAsync(String sagaId,SagaExecutionState currstate, SagaExecutionState state, Object sagaData, boolean skip);

}
