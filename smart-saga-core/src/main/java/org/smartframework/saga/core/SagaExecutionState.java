package org.smartframework.saga.core;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.smartframework.core.JsonUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author noah
 * @create 2021-10-11 8:34
 */
public class SagaExecutionState {

//  private String sagaId;
  private int currentlyExecuting;
  private String currentlyStepName;
  private boolean compensating;
  private boolean endState;
  private Throwable executingException;
  private String lastMessage;
  private SagaRecoverStrategy sagaRecoverType;

//  public String getSagaId() {
//    return sagaId;
//  }
//
//  public void setSagaId(String sagaId) {
//    this.sagaId = sagaId;
//  }

  public String getCurrentlyStepName() {
    return currentlyStepName;
  }

  public void setCurrentlyStepName(String currentlyStepName) {
    this.currentlyStepName = currentlyStepName;
  }

  public SagaRecoverStrategy getSagaRecoverType() {
    return sagaRecoverType;
  }

  public void setSagaRecoverType(SagaRecoverStrategy sagaRecoverType) {
    this.sagaRecoverType = sagaRecoverType;
  }

  public void setCurrentlyExecuting(int currentlyExecuting) {
    this.currentlyExecuting = currentlyExecuting;
  }

  public Map<String, String> getCompensatingMessages() {
    return compensatingMessages;
  }

  public void setCompensatingMessages(Map<String, String> compensatingMessages) {
    this.compensatingMessages = compensatingMessages;
  }

  private Map<String, String> compensatingMessages = new HashMap<>();

  public Map<String, String> getExecutingMessages() {
    return executingMessages;
  }

  public void setExecutingMessages(Map<String, String> executingMessages) {
    this.executingMessages = executingMessages;
  }

  private Map<String, String> executingMessages = new HashMap<>();

  public String getLastMessage() {
    return lastMessage;
  }

  public void setLastMessage(String lastMessage) {
    this.lastMessage = lastMessage;
  }
//    private Map<Integer, String> messages = new HashMap<>();
//
//    public Map<Integer, String> getMessages() {
//        return messages;
//    }
//
//    public void putMessage(Integer i, String message) {
//        messages.put(i, message);
//
//    }


  public boolean isStop() {
    return isStop;
  }

  public void setStop(boolean stop) {
    isStop = stop;
  }

  public void stop() {
    this.isStop = true;
  }

  private boolean isStop;

  public Throwable getCompensatingException() {
    return compensatingException;
  }

  public void setCompensatingException(Throwable compensatingException) {
    this.compensatingException = compensatingException;
  }

  private Throwable compensatingException;

//
//    public boolean isPause() {
//        return pause;
//    }
//
//    public void setPause(boolean pause) {
//        this.pause = pause;
//    }

//    private boolean pause;

  public Throwable getExecutingException() {
    return executingException;
  }

  public void setExecutingException(Throwable executingException) {
    this.executingException = executingException;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  public SagaExecutionState() {
  }

  public SagaExecutionState(int currentlyExecuting, boolean compensating) {
    this.currentlyExecuting = currentlyExecuting;
    this.compensating = compensating;
  }

  public int getCurrentlyExecuting() {
    return currentlyExecuting;
  }


  public void currentlyExecutingStepPlusOne() {
    this.currentlyExecuting += 1;
  }

  public void currentlyExecutingStepMinusOne() {
    this.currentlyExecuting -= 1;
  }

  public boolean isCompensating() {
    return compensating;
  }

  public void setCompensating(boolean compensating) {
    this.compensating = compensating;
  }

  public void startCompensating() {
    this.compensating = true;
  }

  public boolean isEndState() {
    return endState;
  }

  public void setEndState(boolean endState) {
    this.endState = endState;
  }

  public void end() {
    this.endState = true;
  }

  public SagaStatus getSagaStatus() {
    if (isStop()) {
      return SagaStatus.STOPED;
    }
    if (isEndState() && isCompensating()) {
      return SagaStatus.ROLLBACKED;
    }
    if (isEndState() && !isCompensating()) {
      return SagaStatus.EXECUTED;
    }
    if (!isEndState() && isCompensating()) {
      return SagaStatus.ROLLBACKING;
    }
    if (!isEndState() && !isCompensating()) {
      return SagaStatus.EXECUTEING;
    }

    return SagaStatus.UNKNOWN;
  }

  public SagaExecutionState copy() {
//    SagaExecutionState newState = new SagaExecutionState();
    return (SagaExecutionState) JsonUtils.parseObj(JsonUtils.toJsonString(this));
  }

}
