package org.smartframework.saga.core;

import java.util.Optional;

/**
 * @author noah
 * @create 2021-09-30 17:02
 */
public class DefaultSagaStep<Data> implements SagaStep<Data> {

  private Optional<ParticipantInvocation<Data>> participantInvocation;
  private Optional<ParticipantInvocation<Data>> compensation;

  public void setName(String name) {
    this.name = name;
  }

  private String name;


  public DefaultSagaStep(String name, Optional<ParticipantInvocation<Data>> participantInvocation,
      Optional<ParticipantInvocation<Data>> compensation) {
    this.participantInvocation = participantInvocation;
    this.compensation = compensation;
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public boolean hasAction(Data data) {
    return participantInvocation.isPresent() && participantInvocation.map(p -> p.isInvocable(data))
        .orElse(true);
  }

  @Override
  public boolean hasCompensation(Data data) {
    return compensation.isPresent() && compensation.map(p -> p.isInvocable(data)).orElse(true);
  }

  @Override
  public void makeStep(SagaExecutionState state, Data data) {
    final Optional<ParticipantInvocation<Data>> participantInvocation = getParticipantInvocation(
        state.isCompensating());
    if (participantInvocation.isPresent()) {
      final SagaCommandResult commandResult = participantInvocation.get().makeCommand(data);
      final SagaCommandExceutionStatus status = Optional.ofNullable(commandResult.getStatus())
          .orElse(SagaCommandExceutionStatus.FAILURE);
      final String message = Optional.ofNullable(commandResult.getMessage()).orElse("");
      state.setLastMessage(message);
      if (state.isCompensating()) {
        state.getCompensatingMessages().put(this.getName(), message);
      } else {
        state.getExecutingMessages().put(this.getName(), message);
      }
      switch (status) {
        case SUCCESS:
          break;
        default:
          if (state.isCompensating()) {
            state.stop();
          } else {
            if (state.getSagaRecoverType() == SagaRecoverStrategy.BACKWARD) {
              state.startCompensating();
            } else {
              state.stop();
            }
          }
          break;

      }

    }
  }

  private Optional<ParticipantInvocation<Data>> getParticipantInvocation(boolean compensating) {
    return compensating ? compensation : participantInvocation;
  }


}
