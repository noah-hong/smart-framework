package org.smartframework.saga.core;

import java.util.List;

/**
 * @author noah
 * @create 2021-10-18 11:21
 */
public abstract class SagaManagerFactory {

  public SagaInstanceRepository getSagaInstanceRepository() {
    return sagaInstanceRepository;
  }

  private SagaInstanceRepository sagaInstanceRepository;

  public List<SagaStepCompletedObserver> getObservers() {
    return observers;
  }

  public void setObservers(
      List<SagaStepCompletedObserver> observers) {
    this.observers = observers;
  }

  private List<SagaStepCompletedObserver> observers;

  public SagaManagerFactory(SagaInstanceRepository sagaInstanceRepository,
      List<SagaStepCompletedObserver> observers) {
    this.sagaInstanceRepository = sagaInstanceRepository;
    this.observers = observers;
  }

  public abstract <SagaData> SagaManager<SagaData> create(Saga<SagaData> saga);

  public abstract <SagaData> SagaManager<SagaData> create();
}
