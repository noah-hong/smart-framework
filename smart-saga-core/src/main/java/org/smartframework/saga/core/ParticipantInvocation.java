package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-09-30 18:11
 */
public interface ParticipantInvocation<Data> {

  boolean isInvocable(Data data);

  SagaCommandResult makeCommand(Data data);
}
