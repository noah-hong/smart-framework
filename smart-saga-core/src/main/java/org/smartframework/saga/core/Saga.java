package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-09-30 15:28
 */
public interface Saga<Data> {


//  default String getSagaCode() {
//    return this.getClass().getName();
//  }

  default String getSagaName() {
    return this.getClass().getSimpleName();
  }

  ;

  SagaDefinition<Data> getSagaDefinition();


  default void onStarting(String sagaId, Data data) {
  }

  default void onStoped(SagaInstance sagaInstance) {
  }

  default void onSagaCompletedSuccessfully(SagaInstance sagaInstance) {
  }

  default void onSagaRolledBack(SagaInstance sagaInstance) {
  }
}
