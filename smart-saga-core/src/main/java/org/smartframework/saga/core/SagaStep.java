package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-09-30 15:38
 */
public interface SagaStep<Data> {

  String getName();

  boolean hasAction(Data data);

  boolean hasCompensation(Data data);

  void makeStep(SagaExecutionState state, Data data);


}
