package org.smartframework.saga.core;

import java.util.List;

/**
 * @author noah
 * @create 2021-09-30 15:29
 */
public interface SagaDefinition<Data> {


  default SagaRecoverStrategy recoverStrategy() {
    return SagaRecoverStrategy.BACKWARD;
  }

  SagaActions<Data> start(String sagaId, Data sagaData,
      List<SagaStepCompletedObserver> sagaStepCompletedObservers);

  SagaActions<Data> start(String sagaId, SagaExecutionState state, Data data,
      List<SagaStepCompletedObserver> sagaStepCompletedObservers);


}
