package org.smartframework.saga.core;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author noah
 * @create 2021-09-30 17:07
 */
public class DefaultSagaStepBuilder<Data> {

  private final DefaultSagaDefinitionBuilder<Data> parent;

  public String getName() {
    return name;
  }

  private String name;

  public DefaultSagaStepBuilder(String name,
                                DefaultSagaDefinitionBuilder<Data> parent) {
    this.name = name;
    this.parent = parent;
  }

//  public DefaultSagaStepBuilder(
//          DefaultSagaDefinitionBuilder<Data> parent) {
//    this.parent = parent;
//  }

  public DefaultParticipantInvocationBuilder<Data> withCompensation(ParticipantInvocation<Data> compensation) {
    return new DefaultParticipantInvocationBuilder<>(this, parent).withCompensation(Optional.of(compensation));
  }

  public DefaultParticipantInvocationBuilder<Data> withCompensation(Function<Data, SagaCommandResult> compensation) {
    return new DefaultParticipantInvocationBuilder<>(this, parent).withCompensation(compensation);
  }

  public DefaultParticipantInvocationBuilder<Data> withCompensation(Predicate<Data> compensationPredicate,
                                                                    Function<Data, SagaCommandResult> compensation) {
    return new DefaultParticipantInvocationBuilder<>(this, parent).withCompensation(compensationPredicate, compensation);
  }
  public DefaultParticipantInvocationBuilder<Data> invokeOriginParticipant(ParticipantInvocation<Data> action) {
    return new DefaultParticipantInvocationBuilder<>(this, parent).invokeParticipant(Optional.of(action));
  }

  public DefaultParticipantInvocationBuilder<Data> invokeParticipant(Function<Data, SagaCommandResult> action) {
    return new DefaultParticipantInvocationBuilder<>(this, parent).invokeParticipant(action);
  }

  public DefaultParticipantInvocationBuilder<Data> invokeParticipant(Predicate<Data> invokeParticipantPredicate,
                                                                     Function<Data, SagaCommandResult> action) {

    return new DefaultParticipantInvocationBuilder<>(this, parent).invokeParticipant(invokeParticipantPredicate, action);
  }



}
