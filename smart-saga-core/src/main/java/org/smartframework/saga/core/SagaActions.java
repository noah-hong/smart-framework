package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-10-11 16:44
 */
public class SagaActions<Data> {

    public SagaExecutionState getExecutionState() {
        return executionState;
    }

    public void setExecutionState(SagaExecutionState executionState) {
        this.executionState = executionState;
    }

    private SagaExecutionState executionState;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    private Data data;

    public String getCurrentlyStepName() {
        return currentlyStepName;
    }

    public void setCurrentlyStepName(String currentlyStepName) {
        this.currentlyStepName = currentlyStepName;
    }

    private String currentlyStepName;


}
