package org.smartframework.saga.core;

import java.util.HashMap;
import java.util.Map;

/**
 * @author noah
 * @create 2021-10-11 16:37
 */
public class SagaInstance {

  private String sagaId;
  private String sagaName;
  private Class<?> sagaType;
  private SagaRecoverStrategy sagaRecoverStrategy;
  private SagaStatus sagaStatus;

  public void setSagaStatus(SagaStatus sagaStatus) {
    this.sagaStatus = sagaStatus;
  }

  public SagaRecoverStrategy getSagaRecoverStrategy() {
    return sagaRecoverStrategy;
  }

  public void setSagaRecoverStrategy(SagaRecoverStrategy sagaRecoverStrategy) {
    this.sagaRecoverStrategy = sagaRecoverStrategy;
  }

  public SagaStatus getSagaStatus() {
    return sagaStatus;
//    if (isStop() && isCompensating()) {
//      return SagaInstanceStatus.STOPED;
//    }
//    if (isEndState() && isCompensating()) {
//      return SagaInstanceStatus.ROLLBACKED;
//    }
//    if (isEndState() && !isCompensating()) {
//      return SagaInstanceStatus.EXECUTED;
//    }
//    if (!isEndState() && isCompensating()) {
//      return SagaInstanceStatus.ROLLBACKING;
//    }
//    if (!isEndState() && !isCompensating()) {
//      return SagaInstanceStatus.EXECUTEING;
//    }
//
//    return SagaInstanceStatus.UNKNOWN;
  }

  public Class<?> getSagaType() {
    return sagaType;
  }

  public void setSagaType(Class<?> sagaType) {
    this.sagaType = sagaType;
  }

  public int getCurrentlyExecuting() {
    return currentlyExecuting;
  }

  public void setCurrentlyExecuting(int currentlyExecuting) {
    this.currentlyExecuting = currentlyExecuting;
  }

  public boolean isCompensating() {
    return compensating;
  }

  public void setCompensating(boolean compensating) {
    this.compensating = compensating;
  }

  public boolean isEndState() {
    return endState;
  }

  public void setEndState(boolean endState) {
    this.endState = endState;
  }

  public Throwable getExecutingException() {
    return executingException;
  }

  public void setExecutingException(Throwable executingException) {
    this.executingException = executingException;
  }

  public Throwable getCompensatingException() {
    return compensatingException;
  }

  public void setCompensatingException(Throwable compensatingException) {
    this.compensatingException = compensatingException;
  }

  public String getLastMessage() {
    return lastMessage;
  }

  public void setLastMessage(String lastMessage) {
    this.lastMessage = lastMessage;
  }

  public boolean isStop() {
    return isStop;
  }

  public void setStop(boolean stop) {
    isStop = stop;
  }

  public String getCurrentlyStepName() {
    return currentlyStepName;
  }

  public void setCurrentlyStepName(String currentlyStepName) {
    this.currentlyStepName = currentlyStepName;
  }

  public Object getSagaData() {
    return sagaData;
  }

  public void setSagaData(Object sagaData) {
    this.sagaData = sagaData;
  }

  private int currentlyExecuting;
  private boolean compensating;
  private boolean endState;
  private Throwable executingException;
  private Throwable compensatingException;
  private String lastMessage;
  private boolean isStop;
  private String currentlyStepName;
  private Object sagaData;

  public Map<String, String> getExecutingMessages() {
    return executingMessages;
  }

  public void setExecutingMessages(Map<String, String> executingMessages) {
    this.executingMessages = executingMessages;
  }

  private Map<String, String> executingMessages = new HashMap<>();
  private Map<String, String> compensatingMessages = new HashMap<>();

  public Map<String, String> getCompensatingMessages() {
    return compensatingMessages;
  }

  public void setCompensatingMessages(Map<String, String> compensatingMessages) {
    this.compensatingMessages = compensatingMessages;
  }

  public String getSagaId() {
    return sagaId;
  }

  public void setSagaId(String sagaId) {
    this.sagaId = sagaId;
  }

  public String getSagaName() {
    return sagaName;
  }

  public void setSagaName(String sagaName) {
    this.sagaName = sagaName;
  }
}
