package org.smartframework.saga.core;


/**
 * @author noah
 * @create 2021-10-08 15:30
 */
public interface SagaDsl<Data> extends Saga<Data> {

//  default DefaultSagaStepBuilder<Data> step() {
//    DefaultSagaDefinitionBuilder<Data> builder = new DefaultSagaDefinitionBuilder<>();
//    return new DefaultSagaStepBuilder<>(builder);
//  }

  default DefaultSagaStepBuilder<Data> step(String stepName) {
    DefaultSagaDefinitionBuilder<Data> builder = new DefaultSagaDefinitionBuilder<>(this);
    return new DefaultSagaStepBuilder<>(stepName, builder);
  }




}
