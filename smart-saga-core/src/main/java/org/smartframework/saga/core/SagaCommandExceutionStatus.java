package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-10-11 14:35
 */
public enum SagaCommandExceutionStatus {
    SUCCESS,
    FAILURE,
}
