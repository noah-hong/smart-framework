package org.smartframework.saga.core;

/**
 * @author noah
 * @create 2021-10-11 14:14
 */
public class SagaCommandResult {

  public SagaCommandResult() {
  }

  public SagaCommandResult(SagaCommandExceutionStatus status) {
    this.status = status;
  }

  public SagaCommandExceutionStatus getStatus() {
    return status;
  }

  public void setStatus(SagaCommandExceutionStatus status) {
    this.status = status;
  }

  private SagaCommandExceutionStatus status;
  private String message;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  public static SagaCommandResult failure(String message) {
    return new SagaCommandResultBuilder(SagaCommandExceutionStatus.FAILURE).message(message)
        .build();
  }


  public static SagaCommandResult success(String message) {
    return new SagaCommandResultBuilder(SagaCommandExceutionStatus.SUCCESS).message(message)
        .build();
  }

  public static SagaCommandResult success() {
    return new SagaCommandResultBuilder(SagaCommandExceutionStatus.SUCCESS).build();
  }

  public static class SagaCommandResultBuilder {

    private SagaCommandExceutionStatus status;
    private String message;


    public SagaCommandResultBuilder(SagaCommandExceutionStatus status) {
      this.status = status;
    }

    public SagaCommandResultBuilder message(String message) {
      this.message = message;
      return this;
    }

    public <T> SagaCommandResult build() {
      SagaCommandResult responseEnvelope = new SagaCommandResult();
      responseEnvelope.setMessage(message);
      responseEnvelope.setStatus(status);
      return responseEnvelope;
    }

  }
}
