package org.smartframework.domain.driven.lame.woker;

import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IWorkerSiteService;

import java.util.List;

/**
 * @author noah
 * @create 2021-06-03 18:41
 */
public class CommandMessageListenerOrderly implements MessageListenerOrderly {

  private final MessagePipeStreamService messagePipeStreamService;

  public CommandMessageListenerOrderly(
      IWorkerSiteService invoker,
      IClientSiteResponderRegistrar clientSiteResponderRegistrar,
      MessagePipeStreamService messagePipeStreamService) {
    this.messagePipeStreamService = messagePipeStreamService;
    this.messagePipeStreamService.start();
  }

  private final static Logger log = LoggerFactory.getLogger(CommandMessageListenerOrderly.class);

  @Override
  public ConsumeOrderlyStatus consumeMessage(
      List<MessageExt> msgs, ConsumeOrderlyContext context) {
    messagePipeStreamService.put(msgs);
    return ConsumeOrderlyStatus.SUCCESS;
  }

}
