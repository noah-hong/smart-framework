package org.smartframework.domain.driven.lame.woker.refactor;

import org.apache.rocketmq.client.consumer.AllocateMessageQueueStrategy;
import org.apache.rocketmq.client.impl.consumer.DefaultMQPushConsumerImpl;
import org.apache.rocketmq.client.impl.consumer.ProcessQueue;
import org.apache.rocketmq.client.impl.consumer.RebalancePushImpl;
import org.apache.rocketmq.client.impl.factory.MQClientInstance;
import org.apache.rocketmq.common.MixAll;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.lame.woker.ChannelClosedEvent;

import java.util.List;
import java.util.function.Consumer;

/**
 * @author noah
 * @create 2021-06-23 14:34
 */
public class WorkerSiteRebalancePushImpl extends RebalancePushImpl {

  private static final Logger logger = LoggerFactory.getLogger(WorkerSiteRebalancePushImpl.class);
  private final DefaultMQPushConsumerImpl defaultMQPushConsumerImpl;

  public WorkerSiteRebalancePushImpl(
      DefaultMQPushConsumerImpl defaultMQPushConsumerImpl) {
    super(defaultMQPushConsumerImpl);
    this.defaultMQPushConsumerImpl = defaultMQPushConsumerImpl;
  }

  @Override
  public boolean lock(MessageQueue mq) {
    boolean result = super.lock(mq);
    if (result) {
      try {

        final WorkerSiteMQPushConsumer customDefaultMQPushConsumer = (WorkerSiteMQPushConsumer) defaultMQPushConsumerImpl
            .getDefaultMQPushConsumer();
        List<MessageQueue> messageQueues = defaultMQPushConsumerImpl.getmQClientFactory()
            .getMQAdminImpl().fetchPublishMessageQueues(mq.getTopic());
        int i = 0;
        for (MessageQueue messageQueue : messageQueues) {
          if (messageQueue.getQueueId() == mq.getQueueId()
              && messageQueue.getBrokerName().equalsIgnoreCase(mq.getBrokerName())) {
            break;
          }
          i++;
        }

        if (logger.isDebugEnabled() && !mq.getTopic().startsWith(MixAll.RETRY_GROUP_TOPIC_PREFIX)) {
          logger.debug("获取通道{}成功,工作区:{}", i,
              customDefaultMQPushConsumer.getServiceType().getSimpleName());
        }
      } catch (Exception ex) {
        log.error(ex.getMessage(), ex);
      }
    }
    return result;
  }

  public WorkerSiteRebalancePushImpl(String consumerGroup,
      MessageModel messageModel,
      AllocateMessageQueueStrategy allocateMessageQueueStrategy,
      MQClientInstance mQClientFactory,
      DefaultMQPushConsumerImpl defaultMQPushConsumerImpl) {
    super(consumerGroup, messageModel, allocateMessageQueueStrategy, mQClientFactory,
        defaultMQPushConsumerImpl);
    this.defaultMQPushConsumerImpl = defaultMQPushConsumerImpl;
  }

  @Override
  public boolean removeUnnecessaryMessageQueue(MessageQueue mq, ProcessQueue pq) {
    boolean result = super.removeUnnecessaryMessageQueue(mq, pq);
    try {
      final WorkerSiteMQPushConsumer customDefaultMQPushConsumer = (WorkerSiteMQPushConsumer) defaultMQPushConsumerImpl
          .getDefaultMQPushConsumer();

      Consumer<ChannelClosedEvent> consumer = customDefaultMQPushConsumer
          .OnCommandMessageChannelClosed();
      if (consumer != null) {
//        final Set<MessageQueue> messageQueues = this.topicSubscribeInfoTable.get(mq.getTopic());
//        int i = 0;
//        for (MessageQueue messageQueue : messageQueues) {
//          if (messageQueue == mq) {
//            break;
//          }
//          i++;
//        }
        List<MessageQueue> messageQueues = defaultMQPushConsumerImpl.getmQClientFactory()
            .getMQAdminImpl().fetchPublishMessageQueues(mq.getTopic());
        int i = 0;
        for (MessageQueue messageQueue : messageQueues) {
          if (messageQueue.getQueueId() == mq.getQueueId()
              && messageQueue.getBrokerName().equalsIgnoreCase(mq.getBrokerName())) {
            break;
          }
          i++;
        }
        logger.info("通道{}被转移,工作区:{}", i,
            customDefaultMQPushConsumer.getServiceType().getSimpleName());
        final ChannelClosedEvent commandMessageChannelClosedEvent = new ChannelClosedEvent();
        commandMessageChannelClosedEvent.setClosedChannelIndex(i);
        commandMessageChannelClosedEvent.setChannelTotalCount(messageQueues.size());
        commandMessageChannelClosedEvent
            .setServiceType(customDefaultMQPushConsumer.getServiceType());
        consumer.accept(commandMessageChannelClosedEvent);
      }
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
    }
    return result;
  }
}
