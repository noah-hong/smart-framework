package org.smartframework.domain.driven.lame.woker;


import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IWorkerSite;
import org.smartframework.domain.driven.core.communication.IWorkerSiteService;
import org.smartframework.domain.driven.lame.woker.refactor.WorkerSiteMQPushConsumer;

import java.util.HashSet;
import java.util.function.Consumer;

/**
 * @author noah
 * @create 2021-06-03 13:53
 */
public class LameWorkerSite implements IWorkerSite {

  private static final Logger log = LoggerFactory.getLogger(LameWorkerSite.class);
  private static final HashSet<Class<?>> SUBSCRIBED_TYPES = new HashSet<>();
  private String namespace;
  private String nameServer;

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  public String getNameServer() {
    return nameServer;
  }

  public void setNameServer(String nameServer) {
    this.nameServer = nameServer;
  }

  public Integer getCommandExecutorMinThread() {
    return commandExecutorMinThread;
  }

  public void setCommandExecutorMinThread(Integer commandExecutorMinThread) {
    this.commandExecutorMinThread = commandExecutorMinThread;
  }

  public Integer getCommandExecutorMaxThread() {
    return commandExecutorMaxThread;
  }

  public void setCommandExecutorMaxThread(Integer commandExecutorMaxThread) {
    this.commandExecutorMaxThread = commandExecutorMaxThread;
  }

  public Integer getCommandBackPressureThresholds() {
    return commandBackPressureThresholds;
  }

  public void setCommandBackPressureThresholds(Integer commandBackPressureThresholds) {
    this.commandBackPressureThresholds = commandBackPressureThresholds;
  }

  public Boolean isEnabledFastFail() {
    return enabledFastFail;
  }

  public void setEnabledFastFail(Boolean enabledFastFail) {
    this.enabledFastFail = enabledFastFail;
  }

  private final static int COMMAND_MESSAGE_BATCH_MAXSIZE = 100;
  private Integer commandExecutorMinThread = 10;
  private Integer commandExecutorMaxThread = 20;
  private Integer commandBackPressureThresholds = 20;
  private Boolean enabledFastFail = false;
  private final IWorkerSiteService invoker;
  private final IClientSiteResponderRegistrar clientSiteResponderRegistrar;
  private final Class<?> invokerType;
  private Consumer<ChannelClosedEvent> onCommandMessageChannelClosed;

  public void OnCommandMessageChannelClosed(
      Consumer<ChannelClosedEvent> onCommandMessageChannelClosed) {
    this.onCommandMessageChannelClosed = onCommandMessageChannelClosed;
  }

  public LameWorkerSite(IWorkerSiteService invoker,
      IClientSiteResponderRegistrar clientSiteResponderRegistrar) {
    this.invoker = invoker;
    this.clientSiteResponderRegistrar = clientSiteResponderRegistrar;
    this.invokerType = invoker.getHandlerType();
  }

  protected void buildConsumer(WorkerSiteMQPushConsumer workerSiteMQPushConsumer) {

  }

  @Override
  public void start() {

    String consumerGroup = RocketMqUtils.clazzDefaultName(invokerType) + "-lame-worker-group";
    WorkerSiteMQPushConsumer consumer = new WorkerSiteMQPushConsumer(this.getNamespace(),
        consumerGroup);
    buildConsumer(consumer);
    if (consumer.getTopicQueueNums() <= 0) {
      consumer.setTopicQueueNums(16);
    }
    consumer.setNamesrvAddr(this.getNameServer());
    consumer.setMessageModel(MessageModel.CLUSTERING);
    consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
    consumer.setServiceType(invokerType);
    MessagePipeStreamService messagePipeStreamService = new MessagePipeStreamService(
        clientSiteResponderRegistrar,
        invoker);
    messagePipeStreamService.setCommandExecutorMaxThread(this.getCommandExecutorMaxThread());
    messagePipeStreamService.setCommandExecutorMinThread(this.getCommandExecutorMinThread());
    messagePipeStreamService
        .setCommandBackPressureThresholds(this.getCommandBackPressureThresholds());
    messagePipeStreamService.setEnabledFastFail(this.isEnabledFastFail());

    consumer.setMessageListener(
        new CommandMessageListenerOrderly(invoker, clientSiteResponderRegistrar,
            messagePipeStreamService));
    consumer.setConsumeMessageBatchMaxSize(COMMAND_MESSAGE_BATCH_MAXSIZE);
    consumer.OnCommandMessageChannelClosed(onCommandMessageChannelClosed);

    try {
      final Class<?>[] interfaces = invokerType.getInterfaces();
      boolean allowRun = false;
      for (Class<?> interfaceClass : interfaces) {
        if (SUBSCRIBED_TYPES.add(interfaceClass)) {
          String topic = RocketMqUtils.generateTopic(interfaceClass);
          log.info("lame-worker-topic:{}", consumer.withNamespace(topic));
          consumer.subscribe(topic, "*");
          allowRun = true;
        }
      }
      if (allowRun) {
        consumer.start();
        log.info("工作组:{},已顺利启动", consumerGroup);
      } else {
        log.warn("工作组:{},未订阅任何Topic,无法启动", consumerGroup);
      }
    } catch (MQClientException e) {
      log.error(e.getErrorMessage(), e);
    }
  }

}
