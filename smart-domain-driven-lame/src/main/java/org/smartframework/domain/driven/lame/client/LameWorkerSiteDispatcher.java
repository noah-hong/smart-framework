package org.smartframework.domain.driven.lame.client;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.selector.SelectMessageQueueByHash;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.CommandMessageEnvelope;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.CommandStatus;
import org.smartframework.domain.driven.core.communication.CommandRequestEnvelope;
import org.smartframework.domain.driven.core.communication.IClientSiteProcessResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IDispatchingStrategy;
import org.smartframework.domain.driven.core.communication.IWorkerSiteDispatcher;
import org.smartframework.domain.driven.lame.common.formatter.DefaultMessageFormatter;
import org.smartframework.domain.driven.lame.common.formatter.IMessageRequestFormatter;
import org.smartframework.domain.driven.lame.woker.Properties4RocketMQ;
import org.smartframework.domain.driven.lame.woker.RocketMqUtils;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author noah
 * @create 2021-02-19 23:31
 */
public class LameWorkerSiteDispatcher implements IWorkerSiteDispatcher {

//
//  public Integer getWaitRespondTimeout() {
//    return waitRespondTimeout;
//  }
//
//  public void setWaitRespondTimeout(Integer waitRespondTimeout) {
//    this.waitRespondTimeout = waitRespondTimeout;
//  }
//
//  private Integer waitRespondTimeout = 60 * 1000;

  private static final Logger log =
      LoggerFactory.getLogger(LameWorkerSiteDispatcher.class);

  private final DefaultMQProducer producer;
  private final IClientSiteProcessResponderRegistrar processResponderRegistrar;
  private final IMessageRequestFormatter messageRequestFormatter = new DefaultMessageFormatter();


  public LameWorkerSiteDispatcher(
      DefaultMQProducer producer, IClientSiteProcessResponderRegistrar processResponderRegistrar) {
    this.producer = producer;
    this.processResponderRegistrar = processResponderRegistrar;

  }

  private Message toMessage(String topic, String tag, byte[] payload, int delayLevel) {
    //    String topic = RocketMQUtils.getTopic(event);
    if ("*".equals(tag)) {
      tag = "";
    }
    Message rocketMsg = new Message(topic, tag, payload);
    if (delayLevel > 0) {
      rocketMsg.setDelayTimeLevel(delayLevel);
    }
    return rocketMsg;
  }

//  protected IDispatchingStrategy buildDispatchingStrategy(
//      IDispatchingStrategy dispatchingStrategy) {
//    return dispatchingStrategy;
////    return new DefaultDispatchingStrategy(dispatchingStrategy, defaultRouteKey, defaultTimeout);
//  }

  @Override
  public CommandResponse<?> dispatch(
      Class<?> serviceProxy, CommandRequestEnvelope<?> envelope,
      IDispatchingStrategy dispatchingStrategy) {
    StopWatch stopWatch = StopWatch.createStarted();
//    long command_start = System.currentTimeMillis();

//    String commandId = Optional.ofNullable(envelope.getCommand().getPersistenceId())
//        .map(x -> x.getValue().toString())
//        .orElseGet(IPersistenceId.UNKNOWN::getValue);
//    dispatchingStrategy = buildDispatchingStrategy(dispatchingStrategy);
//    dispatchingStrategy = buildDispatchingStrategy(dispatchingStrategy, commandId, 60 * 1000);

    final Class<?> aCommandClass = envelope.getCommand().getClass();

    String topic = RocketMqUtils.generateTopic(serviceProxy);
    String tags = aCommandClass.getSimpleName();
//    final byte[] bytes = JSON.toJSONBytes(envelope);
    final byte[] bytes = messageRequestFormatter.serializeRequest(envelope);
    Message rocketMsg =
        toMessage(topic, tags, bytes, -1);

//    rocketMsg.getProperties()
//        .put(Properties4RocketMQ.COMMAND_NAME, aCommandClass.getSimpleName());
    rocketMsg.getProperties()
        .put(Properties4RocketMQ.CLIENT_ID, envelope.getClientId());
    rocketMsg.getProperties()
        .put(Properties4RocketMQ.CORRELATION_ID, envelope.getCorrelationId());
    rocketMsg.getProperties()
        .put(Properties4RocketMQ.WAIT_RESPOND_TIMEOUT, dispatchingStrategy.getTimeout().toString());
//    if (envelope.getProperties() != null) {
//      envelope.getProperties().forEach(rocketMsg.getProperties()
//          ::put);
//    }

    rocketMsg.setWaitStoreMsgOK(false);
    rocketMsg.setKeys(dispatchingStrategy.getRouteKey());

    try {

//      producer
//          .sendOneway(rocketMsg, new SelectMessageQueueByHash(), dispatchingStrategy.getRouteKey());
      producer
          .send(rocketMsg, new SelectMessageQueueByHash(), dispatchingStrategy.getRouteKey(),
              new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                  if (log.isDebugEnabled()) {
                    log.debug("sendResult:{}", JSON.toJSONString(sendResult));
                  }
                }

                @Override
                public void onException(Throwable e) {
                  log.error("命令发送失败", e);
                }
              });
      CommandResponse<?> commandResult =
          processResponderRegistrar.get(envelope.getCorrelationId())
              .waitRespond(dispatchingStrategy.getTimeout());

      if (log.isDebugEnabled()) {
        stopWatch.split();
        log.debug("命令请求成功，耗时：{}，请求信息：{}", stopWatch.getSplitTime(),
            JSON.toJSONString(envelope));
      }
      if (commandResult.getCommandStatus() == CommandStatus.ERROR) {
        final CommandMessageEnvelope[] messageEnvelopes = commandResult.getMessages();
        String messages;
        if (messageEnvelopes == null || messageEnvelopes.length == 0) {
          messages = "";
        } else {
          messages = Arrays.stream(messageEnvelopes).map(CommandMessageEnvelope::getMessage)
              .collect(
                  Collectors.joining(";"));
        }
        log.error("命令[{}]请求失败：{}", tags, messages);
      }
      return commandResult;
    } catch (Exception ex) {
      log.error(String.format("命令请求失败，请求信息：%s", JSON.toJSONString(envelope), ex));
      return CommandResponse.failure("请求失败，请重试");
    } finally {
      processResponderRegistrar.remove(envelope.getCorrelationId());
    }
  }
}
