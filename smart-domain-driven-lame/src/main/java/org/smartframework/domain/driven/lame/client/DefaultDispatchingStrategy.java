package org.smartframework.domain.driven.lame.client;

import org.apache.commons.lang3.StringUtils;
import org.smartframework.domain.driven.core.communication.IDispatchingStrategy;

/**
 * @author noah
 * @create 2021-08-24 16:14
 */
class DefaultDispatchingStrategy implements IDispatchingStrategy {

  private String routeKey;
  private Integer timeout;

  DefaultDispatchingStrategy(
      String defaultRouteKey, Integer defaultTimeout) {
    this.routeKey = defaultRouteKey;
    this.timeout = defaultTimeout == null ? 60 * 1000 : defaultTimeout;

  }

  DefaultDispatchingStrategy(IDispatchingStrategy dispatchingStrategy,
      String defaultRouteKey, Integer defaultTimeout) {
    if (dispatchingStrategy == null) {
      this.routeKey = defaultRouteKey;
      this.timeout = defaultTimeout;
    } else {
      if (!StringUtils.isEmpty(dispatchingStrategy.getRouteKey())) {
        this.routeKey = dispatchingStrategy.getRouteKey();
      } else {
        this.routeKey = defaultRouteKey;
      }
      if (dispatchingStrategy.getTimeout() != null) {
        this.timeout = dispatchingStrategy.getTimeout();
      } else {
        this.timeout = defaultTimeout;
      }

    }

  }

  @Override
  public String getRouteKey() {
    return routeKey;
  }

  @Override
  public Integer getTimeout() {
    return timeout;
  }
}
