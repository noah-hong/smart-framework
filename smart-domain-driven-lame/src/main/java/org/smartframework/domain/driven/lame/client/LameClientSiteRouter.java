package org.smartframework.domain.driven.lame.client;

import org.smartframework.domain.driven.core.communication.IClientSite;
import org.smartframework.domain.driven.core.communication.IClientSiteRouter;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author noah
 * @create 2021-06-04 15:34
 */
public class LameClientSiteRouter implements IClientSiteRouter {

  private static final ArrayList<IClientSite> RPC_CLIENT_REGISTRY = new ArrayList<>();
  private static final Random RANDOM = new Random();

  @Override
  public void register(IClientSite client) {
    RPC_CLIENT_REGISTRY.add(client);
  }

  @Override
  public IClientSite route() {
    int l = RPC_CLIENT_REGISTRY.size();
    final int i = RANDOM.nextInt(l);
    return RPC_CLIENT_REGISTRY.get(i);
  }
}
