package org.smartframework.domain.driven.lame.client;

import org.redisson.api.RedissonClient;
import org.smartframework.domain.driven.core.communication.IClientSite;
import org.smartframework.domain.driven.core.communication.IClientSiteProcessResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IClientSiteRouter;

/**
 * @author noah
 * @create 2021-06-05 22:21
 */
public class LameClientSiteFactory {

  private final IClientSiteProcessResponderRegistrar clientSiteProcessResponderRegistrar;
  private final RedissonClient redissonClient;
  private final IClientSiteRouter router;
  private final String namespace;
//  private final List<LameClientSite> sites = new ArrayList<>();


  public LameClientSiteFactory(
      RedissonClient redissonClient,
      IClientSiteRouter router,
      IClientSiteProcessResponderRegistrar clientSiteProcessResponderRegistrar,
      String namespace) {

    this.router = router;
    this.redissonClient = redissonClient;
    this.clientSiteProcessResponderRegistrar = clientSiteProcessResponderRegistrar;
    this.namespace = namespace;
  }

  public IClientSite create() {
    return new LameClientSite(redissonClient, router,
        clientSiteProcessResponderRegistrar,
        namespace);
  }
}
