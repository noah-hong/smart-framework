package org.smartframework.domain.driven.lame.common.formatter;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import org.redisson.codec.JsonJacksonCodec.ThrowableMixIn;
import org.smartframework.domain.driven.core.CommandResponse;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * @author noah
 * @create 2021-07-12 21:26
 */
public class DefaultMessageReplyFormatter implements IMessageReplyFormatter {

  private static ObjectMapper mapper;

  static{
    mapper = new ObjectMapper();
    mapper.setSerializationInclusion(Include.NON_NULL);
    mapper.setVisibility(mapper.getSerializationConfig()
        .getDefaultVisibilityChecker()
        .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
        .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
        .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
        .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    mapper.enable(Feature.WRITE_BIGDECIMAL_AS_PLAIN);
    mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    mapper.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
    mapper.addMixIn(Throwable.class, ThrowableMixIn.class);
    TypeResolverBuilder<?> mapTyper = new DefaultTypeResolverBuilder(DefaultTyping.NON_FINAL) {
      @Override
      public boolean useForType(JavaType t) {
        switch (_appliesFor) {
          case NON_CONCRETE_AND_ARRAYS:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
            // fall through
          case OBJECT_AND_NON_CONCRETE:
            return (t.getRawClass() == Object.class) || !t.isConcrete();
          case NON_FINAL:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
            // to fix problem with wrong long to int conversion
            if (t.getRawClass() == Long.class) {
              return true;
            }
            if (t.getRawClass() == XMLGregorianCalendar.class) {
              return false;
            }
            return !t.isFinal(); // includes Object.class
          default:
            // case JAVA_LANG_OBJECT:
            return t.getRawClass() == Object.class;
        }
      }
    };
    mapTyper.init(JsonTypeInfo.Id.CLASS, null);
    mapTyper.inclusion(JsonTypeInfo.As.PROPERTY);
    mapper.setDefaultTyping(mapTyper);
  }

  @Override
  public byte[] serializeReply(CommandResponse<?> command) {
    return new byte[0];
  }

  @Override
  public CommandResponse<?> deserializeReply(byte[] target) {
    return null;
  }
}
