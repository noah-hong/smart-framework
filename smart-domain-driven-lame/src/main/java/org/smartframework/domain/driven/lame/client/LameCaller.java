package org.smartframework.domain.driven.lame.client;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.CommandStatus;
import org.smartframework.domain.driven.core.communication.CommandRequest;
import org.smartframework.domain.driven.core.communication.CommandRequestEnvelope;
import org.smartframework.domain.driven.core.communication.ICaller;
import org.smartframework.domain.driven.core.communication.ICallerBarrier;
import org.smartframework.domain.driven.core.communication.ICallerBarrierFactory;
import org.smartframework.domain.driven.core.communication.ICallerInterceptor;
import org.smartframework.domain.driven.core.communication.IClientSiteRouter;
import org.smartframework.domain.driven.core.communication.IRoutableCommand;
import org.smartframework.domain.driven.core.communication.IWorkerSiteDispatcher;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

/**
 * @author noah
 * @create 2021-02-20 10:27
 */
public class LameCaller implements ICaller {

  private static final Logger log = LoggerFactory.getLogger(LameCaller.class);
  private final IWorkerSiteDispatcher dispatcher;
  private final IClientSiteRouter router;
  private final ICallerBarrierFactory callerBarrierFactory;
  private List<ICallerInterceptor> interceptors = new ArrayList<>();

  public void addInterceptors(ICallerInterceptor... interceptors) {
    if (!ObjectUtils.isEmpty(interceptors)) {
      CollectionUtils.addAll(this.interceptors, interceptors);
    }
  }

  public void addInterceptors(Collection<ICallerInterceptor> interceptors) {
    if (!ObjectUtils.isEmpty(interceptors)) {
      CollectionUtils.addAll(this.interceptors, interceptors);
    }
  }

  public List<ICallerInterceptor> getInterceptors() {
    return interceptors;
  }

  public LameCaller(
      IClientSiteRouter router,
      IWorkerSiteDispatcher dispatcher,
      ICallerBarrierFactory callerBarrierFactory) {
    this.router = router;
    this.dispatcher = dispatcher;
    this.callerBarrierFactory = callerBarrierFactory;
  }


  @Override
  public CommandResponse<?> call(Class<?> serviceType, IRoutableCommand command) {
    return this.call(serviceType, command, null);
  }

  @Override
  public CommandResponse<?> call(Class<?> serviceType, IRoutableCommand command, Integer timeout) {
    return this.call(serviceType, command, timeout, 0L);
  }


  @Override
  public CommandResponse<?> call(Class<?> serviceType, IRoutableCommand command, Integer timeout,
      Long barrierLockPeriod) {
    CommandRequest commandRequest = new CommandRequest();
    commandRequest.setServiceType(serviceType);
    commandRequest.setCommand(command);
    commandRequest.setTimeout(timeout);
    commandRequest.setBarrierLockPeriod(barrierLockPeriod);

    return this.call(commandRequest);
  }

  private void validCommandRequest(CommandRequest commandRequest) {
    Objects.requireNonNull(commandRequest.getCommand());
    Objects.requireNonNull(commandRequest.getServiceType());
//    if (commandRequest.getBarrierLockPeriod() == null) {
//      commandRequest.setBarrierLockPeriod(0L);
//    }
//    if (commandRequest.getTimeout() == 0) {
//      commandRequest.setTimeout(60 * 1000);
//    }

  }

  @Override
  public CommandResponse<?> call(CommandRequest commandRequest) {
    CommandResponse<?> commandResponse = new CommandResponse<>();
    try {
      validCommandRequest(commandRequest);
      if (!applyPreCall(commandRequest, commandResponse)) {
        return commandResponse;
      }

      if (commandRequest.getBarrierLockPeriod() > 0) {
        final ICallerBarrier callerBarrier = getCallerBarrier(commandRequest.getServiceType());
        final boolean entry = callerBarrier.tryEntry();
        if (!entry) {
          //        log.warn("系统繁忙，请稍后再试");
          return CommandResponse.failure("系统繁忙，请稍后再试");
        }
      }
      String routeKey = commandRequest.getCommand().getRouteKey();

      String clientId = Optional.of(router.route()).get().getClientId();
      CommandRequestEnvelope<IRoutableCommand> commandMessageEnvelope = new CommandRequestEnvelope<>();
      commandMessageEnvelope.setCommand(commandRequest.getCommand());
      commandMessageEnvelope.setCorrelationId(UUID.randomUUID().toString());
      commandMessageEnvelope.setClientId(clientId);
      log.debug(MessageFormat
          .format("远程调度开始,RouteKey:{0},ClientId:{1},CorrelationId:{2},Timestamp:{3}", routeKey, clientId,
              commandMessageEnvelope.getCorrelationId(), System.currentTimeMillis()));
      commandResponse = dispatcher
          .dispatch(commandRequest.getServiceType(), commandMessageEnvelope,
              new DefaultDispatchingStrategy(routeKey, commandRequest.getTimeout()));
      if (commandResponse.getCommandStatus() == CommandStatus.EXCEED_BACK_PRESSURE_THRESHOLD
          && commandRequest.getBarrierLockPeriod() > 0) {
        final ICallerBarrier callerBarrier = getCallerBarrier(commandRequest.getServiceType());
        callerBarrier.tryLock(commandRequest.getBarrierLockPeriod());
      }
      applyPostCall(commandRequest, commandResponse);
      return commandResponse;
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      commandResponse = CommandResponse.builder(CommandStatus.ERROR).withMessages("请求调度异常")
          .build();
      try {
        triggerAfterCompletion(commandRequest, commandResponse, e);
      } catch (Exception ex) {
        log.error(ex.getMessage(), ex);
      }
    }
    return commandResponse;
  }

  private ICallerBarrier getCallerBarrier(Class<?> serviceType) {
    return callerBarrierFactory.getBrarrier(serviceType);
  }

  boolean applyPreCall(CommandRequest request, CommandResponse<?> response) throws Exception {
    List<ICallerInterceptor> interceptors = getInterceptors();
    if (!ObjectUtils.isEmpty(interceptors)) {
      for (int i = 0; i < interceptors.size(); i++) {
        ICallerInterceptor interceptor = interceptors.get(i);
        if (!interceptor.preCall(request, response, this)) {
          triggerAfterCompletion(request, response, null, i - 1);
          return false;
        }
//        this.interceptorIndex = i;
      }
    }
    return true;
  }

  void applyPostCall(CommandRequest request, CommandResponse<?> response)
      throws Exception {

    List<ICallerInterceptor> interceptors = getInterceptors();
    if (!ObjectUtils.isEmpty(interceptors)) {
      for (int i = interceptors.size() - 1; i >= 0; i--) {
        ICallerInterceptor interceptor = interceptors.get(i);
        interceptor.postCall(request, response, this);
      }
    }

  }

  void triggerAfterCompletion(CommandRequest request, CommandResponse<?> response, Exception ex)
      throws Exception {

    triggerAfterCompletion(request, response, ex, getInterceptors().size() - 1);
  }

  void triggerAfterCompletion(CommandRequest request, CommandResponse<?> response, Exception ex,
      int interceptorIndex) throws Exception {

    List<ICallerInterceptor> interceptors = getInterceptors();
    if (!ObjectUtils.isEmpty(interceptors)) {
      for (int i = interceptorIndex; i >= 0; i--) {
        ICallerInterceptor interceptor = interceptors.get(i);
        try {
          interceptor.afterCompletion(request, response, this, ex);
        } catch (Throwable ex2) {
          log.error("ICallerInterceptor.afterCompletion threw exception", ex2);
        }
      }
    }
  }
//  private Optional<ICallerBarrier> getCallerBarrier(Class<?> serviceType) {
//    if (callerBarrierFactory != null) {
//      return Optional.ofNullable(callerBarrierFactory.getBrarrier(serviceType));
//    }
//    return Optional.empty();
//  }
}
