package org.smartframework.domain.driven.lame.client;

import org.smartframework.domain.driven.core.communication.ICallerBarrier;
import org.smartframework.domain.driven.core.communication.ICallerBarrierFactory;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author noah
 * @create 2021-07-22 12:55
 */
public class LameCallerBarrierFactory implements ICallerBarrierFactory {

  private final static Map<Type, ICallerBarrier> BARRIER_MAP = new ConcurrentHashMap<>();

  @Override
  public ICallerBarrier getBrarrier(Class<?> serviceType) {
    ICallerBarrier callerBarrier = BARRIER_MAP.get(serviceType);
    if (callerBarrier != null) {
      return callerBarrier;
    }
    synchronized (BARRIER_MAP) {
      callerBarrier = BARRIER_MAP.get(serviceType);
      if (callerBarrier != null) {
        return callerBarrier;
      }
      final LameCallerBarrier lameCallerBarrier = new LameCallerBarrier(serviceType);
      lameCallerBarrier.init();
      BARRIER_MAP.put(serviceType, lameCallerBarrier);
      return lameCallerBarrier;
    }
  }
}
