package org.smartframework.domain.driven.lame.woker;

import org.apache.rocketmq.common.ThreadFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.lame.common.ClientSiteResponderUtils;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author noah
 * @create 2021-06-27 11:06
 */
public class MessageProcessUnitExecutor {

  public boolean isFree() {
    return (messageExecutor.getMaximumPoolSize() - messageExecutor.getActiveCount()) > 0;
  }

  private final ThreadPoolExecutor messageExecutor;

  public MessageProcessUnitExecutor(int corePoolSize,
      int maximumPoolSize,
      MessagePipeStreamService messagePipeStreamService) {
    this.messageExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
        1000 * 60,
        TimeUnit.MILLISECONDS,
        new LinkedBlockingQueue<>(1),
        new ThreadFactoryImpl("LameServiceProcessThread_"),
        new InnerRejectedExecutionHandler(messagePipeStreamService));


  }

  public boolean execute(MessageProcessUnit messageProcessUnit) {
    try {
      messageExecutor.execute(messageProcessUnit);
      return true;
    } catch (RejectedExecutionException ex) {
      return false;
    }
  }

  private static class InnerRejectedExecutionHandler implements RejectedExecutionHandler {

    private static final Logger logger = LoggerFactory
        .getLogger(InnerRejectedExecutionHandler.class);
    final MessagePipeStreamService messagePipeStreamService;

    public InnerRejectedExecutionHandler(MessagePipeStreamService messagePipeStreamService) {
      this.messagePipeStreamService = messagePipeStreamService;
    }

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
      if (r instanceof MessageProcessUnit) {
        messagePipeStreamService.getMessagBackflowExecutor().execute(() -> {
          MessageProcessUnit messageProcessGroup = (MessageProcessUnit) r;
          logger.warn("当前处理请求已到达最大载荷");
          final BlockingQueue<MessagePipeInfo> innerProcessQueue = messageProcessGroup
              .getProcessQueue();
          while (!innerProcessQueue.isEmpty()) {
            final MessagePipeInfo messagePipeInfo = innerProcessQueue.poll();
            if (messagePipeStreamService.isEnabledFastFail()) {
              ClientSiteResponderUtils
                  .respond(messagePipeStreamService.getClientSiteResponderRegistrar(),
                      messagePipeInfo.getClientId(),
                      messagePipeInfo.getCorrelationId(),
                      CommandResponse.exceedBackPressureThreshold("系统繁忙，请稍后再试"));
            } else {
              messagePipeStreamService.put(messagePipeInfo);
            }
          }
        });
      }
      throw new RejectedExecutionException();
    }
  }


}
