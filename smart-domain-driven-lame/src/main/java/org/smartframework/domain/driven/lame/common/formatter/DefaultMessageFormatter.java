package org.smartframework.domain.driven.lame.common.formatter;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.communication.CommandRequestEnvelope;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;

/**
 * @author noah
 * @create 2021-07-12 21:26
 */
public class DefaultMessageFormatter implements IMessageReplyFormatter, IMessageRequestFormatter {

  private static final ObjectMapper MESSAGE_OBJECT_MAPPER;

  @JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
  @JsonAutoDetect(fieldVisibility = Visibility.ANY,
      getterVisibility = Visibility.PUBLIC_ONLY,
      setterVisibility = Visibility.NONE,
      isGetterVisibility = Visibility.NONE)
  public static class ThrowableMixIn {

  }


  static {
    MESSAGE_OBJECT_MAPPER = new ObjectMapper();
    MESSAGE_OBJECT_MAPPER.setSerializationInclusion(Include.NON_NULL);
    MESSAGE_OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    MESSAGE_OBJECT_MAPPER.registerModule(new JavaTimeModule());
    MESSAGE_OBJECT_MAPPER.setVisibility(MESSAGE_OBJECT_MAPPER.getSerializationConfig()
        .getDefaultVisibilityChecker()
        .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
        .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
        .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
        .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
    MESSAGE_OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    MESSAGE_OBJECT_MAPPER.enable(Feature.WRITE_BIGDECIMAL_AS_PLAIN);
    MESSAGE_OBJECT_MAPPER.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    MESSAGE_OBJECT_MAPPER.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
    MESSAGE_OBJECT_MAPPER.addMixIn(Throwable.class, ThrowableMixIn.class);
    TypeResolverBuilder<?> mapTyper = new DefaultTypeResolverBuilder(DefaultTyping.NON_FINAL,
        LaissezFaireSubTypeValidator.instance) {
      @Override
      public boolean useForType(JavaType t) {
        switch (_appliesFor) {
          case NON_CONCRETE_AND_ARRAYS:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
          case OBJECT_AND_NON_CONCRETE:
            return (t.getRawClass() == Object.class) || !t.isConcrete();
          case NON_FINAL:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
            if (t.getRawClass() == Long.class) {
              return true;
            }
            if (t.getRawClass() == XMLGregorianCalendar.class) {
              return false;
            }
            return !t.isFinal();
          default:
            return t.getRawClass() == Object.class;
        }
      }
    };
    mapTyper.init(JsonTypeInfo.Id.CLASS, null);
    mapTyper.inclusion(JsonTypeInfo.As.PROPERTY);
    MESSAGE_OBJECT_MAPPER.setDefaultTyping(mapTyper);
  }

  @Override
  public byte[] serializeReply(CommandResponse<?> command) {
    return new byte[0];
  }

  @Override
  public CommandResponse<?> deserializeReply(byte[] target) {
    return null;
  }

  private final static Logger logger = LoggerFactory.getLogger(DefaultMessageFormatter.class);

  @Override
  public byte[] serializeRequest(CommandRequestEnvelope<?> command) {
    try {
      return MESSAGE_OBJECT_MAPPER.writeValueAsBytes(command);
    } catch (JsonProcessingException e) {
      logger.error(e.getMessage(), e);
      return null;
    }
  }

  @Override
  public CommandRequestEnvelope<?> deserializeRequest(byte[] target) {
    try {
      return MESSAGE_OBJECT_MAPPER.readValue(target, CommandRequestEnvelope.class);
    } catch (IOException e) {
      logger.error("数据报文转换失败：" + e.getMessage(), e);
      return null;
    }
  }
}
