package org.smartframework.domain.driven.lame.common.formatter;

import org.smartframework.domain.driven.core.communication.CommandRequestEnvelope;

/**
 * @author noah
 * @create 2021-07-12 17:49
 */
public interface IMessageRequestFormatter {

  byte[] serializeRequest(CommandRequestEnvelope<?> command);

  CommandRequestEnvelope<?> deserializeRequest(byte[] target);
}
