package org.smartframework.domain.driven.lame.responder;

import org.smartframework.domain.driven.core.communication.IClientSiteProcessResponder;
import org.smartframework.domain.driven.core.communication.IClientSiteProcessResponderRegistrar;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author noah
 * @create 2021-02-20 16:15
 */
public class LameClientSiteProcessResponderRegistrar implements
    IClientSiteProcessResponderRegistrar {

  private static final Map<String, IClientSiteProcessResponder> RESPONDER_MAP = new ConcurrentHashMap<>();

  @Override
  public IClientSiteProcessResponder register(String correlationId,
      IClientSiteProcessResponder responder) {
     RESPONDER_MAP
        .putIfAbsent(correlationId, responder);
     return RESPONDER_MAP.get(correlationId);
  }

  @Override
  public void remove(String correlationId) {
    RESPONDER_MAP.remove(correlationId);
  }

  @Override
  public IClientSiteProcessResponder get(String corrlationId) {
    return Optional.ofNullable(RESPONDER_MAP.get(corrlationId))
        .orElse(this.register(corrlationId, new LameClientSiteProcessResponder()));
  }
}
