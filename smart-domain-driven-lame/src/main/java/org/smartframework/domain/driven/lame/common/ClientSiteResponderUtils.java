package org.smartframework.domain.driven.lame.common;

import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.communication.CommandResponseEnvelope;
import org.smartframework.domain.driven.core.communication.IClientSiteResponder;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;

/**
 * @author noah
 * @create 2021-07-13 11:52
 */
public final class ClientSiteResponderUtils {

  public static void respond(IClientSiteResponderRegistrar clientSiteResponderRegistrar,
      String clientId,
      String correlationId,
      CommandResponse<?> commandResponse) {
    final IClientSiteResponder clientSiteResponder = clientSiteResponderRegistrar
        .get(clientId);
    CommandResponseEnvelope commandResponseEnvelope = new CommandResponseEnvelope();
    commandResponseEnvelope.setCorrelationId(correlationId);
    commandResponseEnvelope
        .setCommandResult(commandResponse);
    clientSiteResponder.respond(commandResponseEnvelope);
  }
}
