package org.smartframework.domain.driven.lame.woker;

import org.smartframework.domain.driven.core.IBaseCommand;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author noah
 * @create 2021-02-10 17:51
 */
@Deprecated
public final class CommandTypeCollector {

  private static final Map<String, Class<? extends IBaseCommand<?>>> COMMAND_TYPE_MAP =
      new ConcurrentHashMap<>();

  public static Class<? extends IBaseCommand<?>> add(
      String className, Class<? extends IBaseCommand<?>> clazz) {
    COMMAND_TYPE_MAP.putIfAbsent(className, clazz);
    return COMMAND_TYPE_MAP.get(className);
  }

  public static Class<? extends IBaseCommand<?>> get(String className) {
    Class<? extends IBaseCommand<?>> aClass = COMMAND_TYPE_MAP.get(className);
    if (Objects.isNull(aClass)) {
      synchronized (COMMAND_TYPE_MAP) {
        aClass = COMMAND_TYPE_MAP.get(className);
        if (Objects.isNull(aClass)) {
          try {
            aClass = (Class<? extends IBaseCommand<?>>) Class.forName(className);
            COMMAND_TYPE_MAP
                .putIfAbsent(className, aClass);
            return COMMAND_TYPE_MAP.get(className);
          } catch (ClassNotFoundException e) {
            e.printStackTrace();
          }
        }
      }
    }
    return aClass;
  }
}
