package org.smartframework.domain.driven.lame.common.formatter;

import org.smartframework.domain.driven.core.CommandResponse;

/**
 * @author noah
 * @create 2021-07-12 17:49
 */
public interface IMessageReplyFormatter {

  byte[] serializeReply(CommandResponse<?> command);

  CommandResponse<?> deserializeReply(byte[] target);
}
