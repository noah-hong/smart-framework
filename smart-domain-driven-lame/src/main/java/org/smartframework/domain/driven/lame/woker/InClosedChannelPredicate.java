package org.smartframework.domain.driven.lame.woker;

import org.smartframework.domain.driven.lame.woker.InClosedChannelPredicate.PredicateData;

import java.util.function.Predicate;

/**
 * @author noah
 * @create 2021-06-23 17:02
 */
public interface InClosedChannelPredicate extends Predicate<PredicateData> {

  static boolean test(String commandId, Class<?> serviceType, ChannelClosedEvent event) {
    InnerPredicate innerPredicate = new InnerPredicate();
    final PredicateData predicateData = new PredicateData();
    predicateData.setEvent(event);
    predicateData.setServiceType(serviceType);
    predicateData.setCommandId(commandId);
    return innerPredicate.test(predicateData);
  }

  public static class PredicateData {

    private String commandId;
    private Class<?> serviceType;
    private ChannelClosedEvent event;

    public String getCommandId() {
      return commandId;
    }

    public void setCommandId(String commandId) {
      this.commandId = commandId;
    }

    public Class<?> getServiceType() {
      return serviceType;
    }

    public void setServiceType(Class<?> serviceType) {
      this.serviceType = serviceType;
    }

    public ChannelClosedEvent getEvent() {
      return event;
    }

    public void setEvent(ChannelClosedEvent event) {
      this.event = event;
    }
  }

  final class InnerPredicate implements InClosedChannelPredicate {

    @Override
    public boolean test(PredicateData predicateData) {
      try {
        if (predicateData.getServiceType()
            .isAssignableFrom(predicateData.getEvent().getServiceType())) {
          int hashCode = predicateData.getCommandId().hashCode();
          if (hashCode < 0) {
            hashCode = Math.abs(hashCode);
          }
          int index = hashCode % predicateData.getEvent().getChannelTotalCount();
          return predicateData.getEvent().getClosedChannelIndex() == index;
        } else {
          return false;
        }
      } catch (Exception ex) {
        return false;
      }
    }
  }

}
