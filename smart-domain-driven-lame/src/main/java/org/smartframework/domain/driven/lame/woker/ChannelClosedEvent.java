package org.smartframework.domain.driven.lame.woker;

/**
 * @author noah
 * @create 2021-06-23 15:29
 */
public class ChannelClosedEvent {


  /**
   * 工作区服务类，领域服务解释者类型
   */
  private Class<?> serviceType;
  /**
   * 被关闭的请求通道
   */
  private int closedChannelIndex;
  /**
   * 请求通道总数
   */
  private int channelTotalCount;

  public Class<?> getServiceType() {
    return serviceType;
  }

  public void setServiceType(Class<?> serviceType) {
    this.serviceType = serviceType;
  }

  public int getClosedChannelIndex() {
    return closedChannelIndex;
  }

  public void setClosedChannelIndex(int closedChannelIndex) {
    this.closedChannelIndex = closedChannelIndex;
  }

  public int getChannelTotalCount() {
    return channelTotalCount;
  }

  public void setChannelTotalCount(int channelTotalCount) {
    this.channelTotalCount = channelTotalCount;
  }
}
