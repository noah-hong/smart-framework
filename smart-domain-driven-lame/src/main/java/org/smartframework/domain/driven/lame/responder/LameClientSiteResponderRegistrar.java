package org.smartframework.domain.driven.lame.responder;

import org.redisson.api.RedissonClient;
import org.smartframework.domain.driven.core.communication.IClientSiteResponder;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


/**
 * @author noah
 * @create 2021-02-22 13:42
 */
public class LameClientSiteResponderRegistrar implements IClientSiteResponderRegistrar {


  private final RedissonClient redissonClient;

  public LameClientSiteResponderRegistrar(RedissonClient redissonClient) {
    this.redissonClient = redissonClient;
  }

  private static final Map<String, IClientSiteResponder> RPC_CLIENT_RESPONDER_MAP = new HashMap<>();

  @Override
  public IClientSiteResponder register(String clientId, IClientSiteResponder responder) {
    RPC_CLIENT_RESPONDER_MAP.putIfAbsent(clientId, responder);
    return RPC_CLIENT_RESPONDER_MAP.get(clientId);
  }

  @Override
  public IClientSiteResponder get(String clientId) {

    return Optional.ofNullable(RPC_CLIENT_RESPONDER_MAP.get(clientId))
        .orElseGet(
            () -> {
              synchronized (RPC_CLIENT_RESPONDER_MAP) {
                return Optional.ofNullable(RPC_CLIENT_RESPONDER_MAP.get(clientId))
                    .orElseGet(
                        () -> {
                          IClientSiteResponder rpcClientResponder =
                              new LameClientSiteResponder(clientId, redissonClient);
                          return this.register(clientId, rpcClientResponder);
                        });
              }
            });
  }
}
