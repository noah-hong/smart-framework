package org.smartframework.domain.driven.lame.woker;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author noah
 * @create 2021-02-20 14:02
 */
public final class RocketMqUtils {

  private static final Map<Class<?>, String> TOPIC_MAP = new ConcurrentHashMap<>();

  //  private static final Map<Class<? extends ICommand<?>>, String> SUB_TOPIC_MAP = new ConcurrentHashMap<>();
  public static String generateTopic(Class<?> serviceClazz) {
    return Optional.ofNullable(TOPIC_MAP.get(serviceClazz)).orElseGet(() -> {
      synchronized (TOPIC_MAP) {
        return Optional.ofNullable(TOPIC_MAP.get(serviceClazz))
            .orElseGet(() -> {
              String s = clazzDefaultName(serviceClazz);
              TOPIC_MAP.putIfAbsent(serviceClazz, s);
              return TOPIC_MAP.get(serviceClazz);
            });
      }
    });
  }


  public static String clazzDefaultName(Class<?> clazz) {
    return "lame-worker%" + clazz.getName().replace(".", "-").replace("$", "-");
  }
}
