package org.smartframework.domain.driven.lame.responder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.communication.IClientSiteProcessResponder;
import org.smartframework.domain.driven.core.communication.exceptions.DispatchingException;

import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author noah
 * @create 2021-02-20 15:44
 */
public class LameClientSiteProcessResponder implements IClientSiteProcessResponder {

  private static final Logger log = LoggerFactory
      .getLogger(LameClientSiteProcessResponder.class);
  private final ArrayBlockingQueue<CommandResponse<?>> queue = new ArrayBlockingQueue<>(1);

  public LameClientSiteProcessResponder() {
  }

  @Override
  public <T> CommandResponse<T> waitRespond(int timeout) {
    try {
      CommandResponse<T> commandResult = (CommandResponse<T>) queue
          .poll(timeout, TimeUnit.MILLISECONDS);
      return Optional.ofNullable(commandResult)
          .orElseGet(() -> CommandResponse.timeout("系统繁忙，请稍后再试"));
    } catch (InterruptedException e) {
      log.warn("命令执行中断", e);
      return CommandResponse.error(new DispatchingException("发了一点意外，命令执行中断，请重试"));
    }
  }

  @Override
  public <T> void respond(CommandResponse<T> commandResult) {
    queue.offer(commandResult);
  }
}
