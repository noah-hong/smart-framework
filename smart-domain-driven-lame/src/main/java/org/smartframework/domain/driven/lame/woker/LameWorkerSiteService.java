package org.smartframework.domain.driven.lame.woker;


import com.alibaba.fastjson.JSON;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.DomainException;
import org.smartframework.domain.driven.core.ICommand;
import org.smartframework.domain.driven.core.communication.CommandHandlerInterceptor;
import org.smartframework.domain.driven.core.communication.CommandRequestEnvelope;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IWorkerSiteService;
import org.smartframework.domain.driven.lame.Constants;
import org.smartframework.domain.driven.lame.common.ClientSiteResponderUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author noah
 * @create 2021-02-22 0:11
 */
public class LameWorkerSiteService implements IWorkerSiteService {

  private static final Logger log = LoggerFactory.getLogger(LameWorkerSiteService.class);
  private final Object handler;
  private final IClientSiteResponderRegistrar clientSiteResponderRegistrar;
  private final Map<Class<?>, Method> handleMethods = new HashMap<>();
  private final Class<?> handlerType;
  private List<CommandHandlerInterceptor> interceptors = new ArrayList<>();

  public void addInterceptors(CommandHandlerInterceptor... interceptors) {
    if (!ObjectUtils.isEmpty(interceptors)) {
      CollectionUtils.addAll(this.interceptors, interceptors);
    }
  }

  public void addInterceptors(Collection<CommandHandlerInterceptor> interceptors) {
    if (!ObjectUtils.isEmpty(interceptors)) {
      CollectionUtils.addAll(this.interceptors, interceptors);
    }
  }

  public List<CommandHandlerInterceptor> getInterceptors() {
    return interceptors;
  }
//  public LameWorkerSiteInvoker(
//      Object invoker, IClientSiteResponderRegistrar clientSiteResponderRegistrar) {
//    this.invoker = invoker;
//    cacheInvokeMethodsFor(invoker);
//    this.clientSiteResponderRegistrar = clientSiteResponderRegistrar;
//  }

  public LameWorkerSiteService(
      Object invoker, Class<?> invokerType,
      IClientSiteResponderRegistrar clientSiteResponderRegistrar) {
    this.handler = invoker;
    this.handlerType = invokerType;
    cacheInvokeMethodsFor(invoker);
    this.clientSiteResponderRegistrar = clientSiteResponderRegistrar;
  }

  public void cacheInvokeMethodsFor(Object invoker) {
    final Class<?> aClass = invoker.getClass();
    final Method[] methods = aClass.getMethods();
    for (Method method : methods) {
      if (method.getName().equalsIgnoreCase(Constants.COMMAND_METHOD_NAME)) {
        final Class<?>[] parameterTypes = method.getParameterTypes();
        final Class<?> returnType = method.getReturnType();
        if (ICommand.class.isAssignableFrom(parameterTypes[0])
            && CommandResponse.class.isAssignableFrom(returnType)) {
          handleMethods.put(parameterTypes[0], method);
        }
      }
    }
  }

  boolean applyPreHandle(CommandRequestEnvelope<?> request, CommandResponse<?> response) throws Exception {
    List<CommandHandlerInterceptor> interceptors = getInterceptors();
    if (!ObjectUtils.isEmpty(interceptors)) {
      for (int i = 0; i < interceptors.size(); i++) {
        CommandHandlerInterceptor interceptor = interceptors.get(i);
        if (!interceptor.preHandle(request, response, this.handler)) {
          triggerAfterCompletion(request, response, null, i - 1);
          return false;
        }
//        this.interceptorIndex = i;
      }
    }
    return true;
  }

  void applyPostHandle(CommandRequestEnvelope<?> request, CommandResponse<?> response)
      throws Exception {

    List<CommandHandlerInterceptor> interceptors = getInterceptors();
    if (!ObjectUtils.isEmpty(interceptors)) {
      for (int i = interceptors.size() - 1; i >= 0; i--) {
        CommandHandlerInterceptor interceptor = interceptors.get(i);
        interceptor.postHandle(request, response, this.handler);
      }
    }

  }


  void triggerAfterCompletion(CommandRequestEnvelope<?> request, CommandResponse<?> response, Exception ex,
      int interceptorIndex) throws Exception {

    List<CommandHandlerInterceptor> interceptors = getInterceptors();
    if (!ObjectUtils.isEmpty(interceptors)) {
      for (int i = interceptorIndex; i >= 0; i--) {
        CommandHandlerInterceptor interceptor = interceptors.get(i);
        try {
          interceptor.afterCompletion(request, response, this.handler, ex);
        } catch (Throwable ex2) {
          log.error("CommandHandlerInterceptor.afterCompletion threw exception", ex2);
        }
      }
    }
  }

  void triggerAfterCompletion(CommandRequestEnvelope<?> request, CommandResponse<?> response, Exception ex)
      throws Exception {

    triggerAfterCompletion(request, response, ex, getInterceptors().size() - 1);
  }


  private CommandResponse<?> doInvoke(CommandRequestEnvelope<?> envelope) {
    ICommand command = envelope.getCommand();
    final Optional<Method> method = Optional.ofNullable(handleMethods.get(command.getClass()));
    if (method.isPresent()) {
      final Method x = method.get();
      CommandResponse<?> response = new CommandResponse<>();
      try {
        if (!applyPreHandle(envelope, response)) {
          return response;
        }
        response = (CommandResponse<?>) x.invoke(handler, command);
        applyPostHandle(envelope, response);
        return response;
      } catch (Exception e) {
        final Throwable rootCause = ExceptionUtils.getRootCause(e);
        if (rootCause instanceof DomainException) {
          log.warn(rootCause.getMessage(), rootCause);
          response = CommandResponse.failure(rootCause);
        } else {
          log.error(rootCause.getMessage(), rootCause);
          response = CommandResponse.error(rootCause);
        }
        try {
          triggerAfterCompletion(envelope, response, e);
        } catch (Exception ex) {
          log.error(ex.getMessage(), ex);
        }
        return response;
      }
    } else {
      log.error("未找到合适的实现：" + command.getClass().getName());
      return CommandResponse.error("未找到合适的实现");
    }


  }

  @Override
  public CommandResponse<?> invoke(CommandRequestEnvelope<?> envelope) {
    Objects.requireNonNull(envelope);
    long invokeStart = System.currentTimeMillis();
    CommandResponse<?> commandResponse = doInvoke(envelope);
    long invokeEnd = System.currentTimeMillis();
    if (log.isDebugEnabled()) {
      log.debug("执行命令耗时：{}，请求信息：{}", (invokeEnd - invokeStart), JSON.toJSONString(envelope));
    }
//    IClientSiteResponder clientSiteResponder =
//        clientSiteResponderRegistrar.get(envelope.getClientId());

//    CommandResponseEnvelope commandReponseEnvelope = new CommandResponseEnvelope();
//    commandReponseEnvelope.setCommandResult(commandResponse);
//    commandReponseEnvelope.setCorrelationId(envelope.getCorrelationId());
    long respondStart = System.currentTimeMillis();
    ClientSiteResponderUtils
        .respond(clientSiteResponderRegistrar, envelope.getClientId(),
            envelope.getCorrelationId(), commandResponse);
//    clientSiteResponder.respond(commandReponseEnvelope);
    long respondEnd = System.currentTimeMillis();
    if (log.isDebugEnabled()) {
      log.debug("回传结果耗时：{}，CorrelationId：{}", (respondEnd - respondStart),
          envelope.getCorrelationId());
    }
    return commandResponse;
  }

  @Override
  public Class<?> getHandlerType() {
    return handlerType;
  }
}
