package org.smartframework.domain.driven.lame.woker;

import java.util.Map;

/**
 * @author noah
 * @create 2021-06-15 11:36
 */
public class MessagePipeInfo {

  public MessageProcessBehavior getBehavior() {
    return behavior;
  }

  public void setBehavior(
      MessageProcessBehavior behavior) {
    this.behavior = behavior;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getCorrelationId() {
    return correlationId;
  }

  public void setCorrelationId(String correlationId) {
    this.correlationId = correlationId;
  }

  public Integer getWaitRespondTimeout() {
    return waitRespondTimeout;
  }

  public void setWaitRespondTimeout(Integer waitRespondTimeout) {
    this.waitRespondTimeout = waitRespondTimeout;
  }

  public Long getMessageBornTimestamp() {
    return messageBornTimestamp;
  }

  public void setMessageBornTimestamp(Long messageBornTimestamp) {
    this.messageBornTimestamp = messageBornTimestamp;
  }

  public String getKey() {
    return key;
  }

  public byte[] getBody() {
    return body;
  }

  public void setBody(byte[] body) {
    this.body = body;
  }

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getTags() {
    return tags;
  }

  public void setTags(String tags) {
    this.tags = tags;
  }

  public void setKey(String key) {
    this.key = key;
  }

  //  public String getCommandName() {
//    return commandName;
//  }
//  public void setCommandName(String commandName) {
//    this.commandName = commandName;
//  }
  public Class<?> getServiceType() {
    return serviceType;
  }

  public void setServiceType(Class<?> serviceType) {
    this.serviceType = serviceType;
  }

  private String key;
  private MessageProcessBehavior behavior;
  private String clientId;
  private String correlationId;
  private Integer waitRespondTimeout;
  private Long messageBornTimestamp;
  //  private String commandName;
  private byte[] body;
  private String messageId;
  private String topic;
  private String tags;
  private Class<?> serviceType;

  public Map<String, String> getProperties() {
    return properties;
  }

  public void setProperties(Map<String, String> properties) {
    this.properties = properties;
  }

  private Map<String, String> properties;

  public boolean isTimeout() {
    return (messageBornTimestamp + waitRespondTimeout) <= System.currentTimeMillis();
  }

  @Override
  public String toString() {
    return "MessagePipeInfo{" +
        "key='" + key + '\'' +
        ", behavior=" + behavior +
        ", bodyLength=" + body.length +
        ", clientId='" + clientId + '\'' +
        ", correlationId='" + correlationId + '\'' +
        ", waitRespondTimeout=" + waitRespondTimeout +
        ", messageBornTimestamp=" + messageBornTimestamp +
//        ", commandName='" + commandName + '\'' +
        ", messageId='" + messageId + '\'' +
        ", topic='" + topic + '\'' +
        ", tags='" + tags + '\'' +
        ", serviceType=" + serviceType.getSimpleName() +
        '}';
  }

  enum MessageProcessBehavior {
    PROCESS,
    DELETE
  }
}
