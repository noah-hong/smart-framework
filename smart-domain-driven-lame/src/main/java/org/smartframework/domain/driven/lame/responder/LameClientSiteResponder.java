package org.smartframework.domain.driven.lame.responder;

import org.redisson.api.RBlockingQueue;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.communication.CommandResponseEnvelope;
import org.smartframework.domain.driven.core.communication.IClientSiteResponder;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

/**
 * @author noah
 * @create 2021-02-22 14:43
 */
public class LameClientSiteResponder implements IClientSiteResponder {

  private static final Logger log = LoggerFactory.getLogger(LameClientSiteResponder.class);
  private final RBlockingQueue<CommandResponseEnvelope> blockingQueue;
  private String clientId;

  public LameClientSiteResponder(String clientId, RedissonClient redissonClient) {
    this.clientId = clientId;
    this.blockingQueue = redissonClient.getBlockingQueue(clientId);

  }

  @Override
  public void respond(CommandResponseEnvelope envelope) {
    log.debug(MessageFormat
        .format("远程调度执行结束,开始回传结果,ClientId:{0},CorrelationId:{1},Timestamp:{2}", clientId,
            envelope.getCorrelationId(), System.currentTimeMillis()));
    blockingQueue.offerAsync(envelope);
    log.debug(MessageFormat
        .format("远程调度执行结束,回传结果结束,ClientId:{0},CorrelationId:{1},Timestamp:{2}", clientId,
            envelope.getCorrelationId(), System.currentTimeMillis()));
    //如果1天还未消费返回结果则销毁队列
    blockingQueue.expireAsync(1, TimeUnit.DAYS);
  }

}
