package org.smartframework.domain.driven.lame.woker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.communication.CommandRequestEnvelope;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IWorkerSiteService;
import org.smartframework.domain.driven.lame.common.ClientSiteResponderUtils;
import org.smartframework.domain.driven.lame.common.formatter.DefaultMessageFormatter;
import org.smartframework.domain.driven.lame.common.formatter.IMessageRequestFormatter;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

/**
 * @author noah
 * @create 2021-06-14 18:02
 */
public class MessageProcessUnit implements Runnable {

  private final static Logger log = LoggerFactory.getLogger(MessageProcessUnit.class);

  public String getUnitName() {
    return unitName;
  }

  private final String unitName;
//  private final String tags;

  public BlockingQueue<MessagePipeInfo> getProcessQueue() {
    return processQueue;
  }


  private final BlockingQueue<MessagePipeInfo> processQueue;
  private volatile boolean isRunning = false;
  private final IMessageRequestFormatter messageRequestFormatter = new DefaultMessageFormatter();
  private final Object objLock = new Object();
  private IClientSiteResponderRegistrar clientSiteResponderRegistrar;

  public IWorkerSiteService getInvoker() {
    return invoker;
  }

  private final IWorkerSiteService invoker;
  private final MessageProcessUnitExecutor messageExecutor;

  public void onStoped(
      Consumer<MessageProcessUnit> onStoped) {
    this.onStoped = onStoped;
  }

  private Consumer<MessageProcessUnit> onStoped;

  public MessageProcessUnit(String unitName, IWorkerSiteService invoker,
      MessageProcessUnitExecutor messageExecutor,
      IClientSiteResponderRegistrar clientSiteResponderRegistrar) {
    this.unitName = unitName;
    this.invoker = invoker;
    this.messageExecutor = messageExecutor;
    this.processQueue = new LinkedBlockingQueue<>();
    this.clientSiteResponderRegistrar = clientSiteResponderRegistrar;
  }

  public void put(MessagePipeInfo messagePipeInfo) {
    processQueue.offer(messagePipeInfo);
  }

  public void process() {
    if (!isRunning()) {
      try {
        isRunning = messageExecutor.execute(this);
      } catch (Exception ex) {
        log.error(ex.getMessage(), ex);
        isRunning = false;
      }
    }
  }

  public boolean isRunning() {
    return isRunning;
  }

  public void stop() {
    isRunning = false;
    if (onStoped != null) {
      onStoped.accept(this);
    }
  }

  public boolean isActive() {
    return isRunning() || !processQueue.isEmpty();
  }

  protected void exec(MessagePipeInfo messagePipeInfo) {
    if (messagePipeInfo.isTimeout()) {
      log.warn("等待执行超时,请求信息：{}", messagePipeInfo);
      return;
    }
    if (log.isDebugEnabled()) {
      log.debug("接收到请求，开始执行，请求信息：{}", messagePipeInfo.toString());
    }
    final CommandRequestEnvelope<?> commandMessageEnvelope = messageRequestFormatter
        .deserializeRequest(messagePipeInfo.getBody());
    if (commandMessageEnvelope == null) {
      ClientSiteResponderUtils
          .respond(clientSiteResponderRegistrar, messagePipeInfo.getClientId(),
              messagePipeInfo.getCorrelationId(), CommandResponse.error("数据报文转换失败，请联系管理员"));
    } else {
      invoker.invoke(commandMessageEnvelope);
    }
  }

  protected void doRun() {
    try {
      synchronized (objLock) {
        while (!processQueue.isEmpty()) {
          final MessagePipeInfo messagePipeInfo = processQueue.poll();
          try {
            exec(messagePipeInfo);
          } catch (Exception e) {
            //NOTE: 返回错误结果到客户端(√)
            log.warn(
                String.format("执行失败，请求信息：%s", messagePipeInfo.toString()), e);
            try {
              ClientSiteResponderUtils
                  .respond(clientSiteResponderRegistrar, messagePipeInfo.getClientId(),
                      messagePipeInfo.getCorrelationId(), CommandResponse.error("执行失败，请联系管理员"));
            } catch (Exception ex) {
              log.error(ex.getMessage(), ex);
            }
          }
        }
      }
    } catch (Exception ex) {
      log.warn(ex.getMessage(), ex);
    } finally {
      stop();
    }

  }

  @Override
  public void run() {
    doRun();
  }
}
