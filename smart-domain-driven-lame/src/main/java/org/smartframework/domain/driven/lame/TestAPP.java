package org.smartframework.domain.driven.lame;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.rocketmq.common.ThreadFactoryImpl;
import org.smartframework.domain.driven.core.communication.ICallerBarrier;
import org.smartframework.domain.driven.lame.client.LameCallerBarrierFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author noah
 * @create 2021-06-13 21:06
 */
public class TestAPP {

  public static void testStopwatch() throws InterruptedException {
    StopWatch stopWatch = StopWatch.createStarted();
    Thread.sleep(1000);
    stopWatch.split();
    System.out.println(stopWatch.getSplitTime());
  }

  public static void testThreadPool() {
    ReentrantLock lock = new ReentrantLock();
//    lock.tryLock(1999,TimeUnit.MILLISECONDS);
    ThreadPoolExecutor pool = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
//    pool.execute();
//    System.out.println(pool.getActiveCount());
  }


  public static void testSyscQueue() throws InterruptedException {
    final long l = System.currentTimeMillis();

//    LameCallerBarrier lameCallerBarrier = new LameCallerBarrier(TestAPP.class);
    LameCallerBarrierFactory lameCallerBarrierFactory = new LameCallerBarrierFactory();
    final ICallerBarrier brarrier = lameCallerBarrierFactory.getBrarrier(TestAPP.class);
    brarrier.tryLock(100L);
    System.out.println(brarrier.tryEntry());
//    lameCallerBarrier.tryLock(300L);
//    lameCallerBarrier.tryEntry();
//    lameCallerBarrier.tryEntry();
//    lameCallerBarrier.tryEntry();
//    final long l2 = System.currentTimeMillis();
//    System.out.println("end+" + (l2 - l));
//    Runnable target;
//    Thread t = new Thread(() -> {
//      try {
//        for (int i = 0; i < 3; i++) {
//          Thread.sleep(100);
//          System.out.println(lameCallerBarrier.tryEntry());
//        }
//      } catch (InterruptedException e) {
//        e.printStackTrace();
//      }
//
//    });
//    t.setDaemon(false);
//    t.start();

//    BlockingQueue<String> blockingQueue = new LinkedBlockingQueue<>(1);
//    blockingQueue.offer("1");
//    blockingQueue.offer("2");
//    final Object poll = blockingQueue.poll();
//    final Object poll2 = blockingQueue.poll();
//    System.out.println(poll);
//    System.out.println(poll2);
//    SynchronousQueue queue = new SynchronousQueue<Runnable>();
//    queue(1);
//    queue.offer(2);
//    final Object poll = queue.poll();
//    final Object poll2 = queue.poll();
//    System.out.println(poll);
//    System.out.println(poll2);
  }

  public static void testLock() throws InterruptedException {
    ReentrantLock lock = new ReentrantLock();
    lock.tryLock();

    System.out.println(lock.isLocked());
    Thread t = new Thread(() -> {
      try {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        System.out.println("t:" + lock.tryLock(200, TimeUnit.MILLISECONDS));
        stopWatch.split();
        System.out.println("t-time:" + stopWatch.getSplitTime());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    });
    t.start();
    Thread.sleep(220);
    System.out.println("1:" + lock.isLocked());
    lock.unlock();
    System.out.println("2:" + lock.isLocked());
    Thread.sleep(301);
    System.out.println("3:" + lock.isLocked());
  }

  static class InnerRejectedExecutionHandler implements RejectedExecutionHandler {

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {

//      System.out.println(111);
//      InnerThread messageProcessGroup = (InnerThread) r;
//      System.out.println("name:" + ((InnerThread) r).getName());

    }
  }

  static class InnerThread implements Runnable {

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    private String name;

    @Override
    public void run() {
      System.out.println("thread-ud:" + Thread.currentThread().getId());


    }
  }

  public static void testSynchronousQueue() throws InterruptedException {
    SynchronousQueue queue = new SynchronousQueue();
    Runnable target;
    Thread thread =
        new Thread(
            () -> {
              while (true) {
                try {
                  System.out.println(queue.take());
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            });
    thread.setDaemon(true);

    queue.add("s");
    queue.put("s2");
    System.out.println("1");
    thread.start();
    Thread.sleep(1000);

  }


  public static void testMessageProcessGroup() throws ExecutionException, InterruptedException {

    ThreadPoolExecutor messageExecutor =
        new ThreadPoolExecutor(
            1,
            1,
            1000 * 60,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>(1),
            new ThreadFactoryImpl("ProcessMessageThread_"),
            new InnerRejectedExecutionHandler());
    for (int i = 0; i < 50; i++) {
      final InnerThread innerThread = new InnerThread();
      innerThread.setName("name" + String.valueOf(i));
      messageExecutor.execute(innerThread);
//      System.out.println(submit.get());

    }
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("end");
  }

  public static void listTest() {
    List<String> l = new LinkedList<>();
    l.add("1");
    l.add("2");

    l.forEach(
        x -> {
          if (x.equalsIgnoreCase("1")) {
            return;
          }
          System.out.println(x);
        });
  }

  public static void testCapacity() {
    BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(3);
    try {
      queue.offer(1);
      queue.offer(2);
      System.out.println(queue.offer(3));
      System.out.println(queue.offer(4));

      queue.offer(5);
      while (!queue.isEmpty()) {
        System.out.println(queue.poll());
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void test() {
    for (int i = 0; i < 100; i++) {
      int finalI = i;
      ForkJoinPool.commonPool().submit(() -> {
        try {
          Thread.sleep(500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println(String.valueOf(finalI));
      });
    }
    try {
      Thread.sleep(10000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public static void testThread() {
    ThreadPoolExecutor executor =
        new ThreadPoolExecutor(
            1,
            1,
            1000 * 60,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>(5),
            Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy());
    for (int i = 0; i < 10; i++) {
      try {
        int finalI = i;
        executor.execute(
            () -> {
              try {
                Thread.sleep(1000);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              System.out.println(String.valueOf(finalI));
            });
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }

  }

  public static void main(String[] args) throws Exception {
    testStopwatch();
    //noah-ns:lame-client-site:20210709:ece8293a-9e9b-4371-aec5-4d9821d2d7ba
//    LocalDateTime now = LocalDateTime.now();
//    String dateFormat = now.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
//    System.out.println(dateFormat);
//    System.out.println(ClassUtils(ICaller.class));
//    testMessageProcessGroup();
//    System.out.println(ForkJoinPool.commonPool().submit(() -> System.out.println("1")));
//    listTest();
//    List<String> list = new ArrayList<>();
//    list.add("123");
//    list.add("456");

//    String s1 ="testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";
//    String s2 ="te1sttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";
//    System.out.println(s1.hashCode() % 12);
//    System.out.println(Math.abs(s2.hashCode()) %12);

//    Set<String> set = new HashSet<>();
//    final boolean s = set.add("s");
//    final boolean s2 = set.add("s");
//    System.out.println(s2);
//    ConcurrentHashMap<String, ConcurrentLinkedQueue<String>> map = new ConcurrentHashMap<>();
//    final ConcurrentLinkedQueue<String> test = Optional.ofNullable(map
//        .putIfAbsent("test", new ConcurrentLinkedQueue<>())).orElseGet(() -> map.get("test"));
//    test.add("tt");
//    test.add("tt1");
//
//    test.remove("tt1");
//
//    while (!test.isEmpty()) {
//      System.out.println(test.poll());
//    }
    //add()和remove()方法在失败的时候会抛出异常(不推荐)
//    Queue<String> queue = new ConcurrentLinkedQueue<>();
//    //添加元素
//    queue.offer("a");
//    queue.offer("b");
//    queue.offer("c");
//    queue.offer("d");
//    queue.offer("e");
//    Runnable target;
//    Thread t = new Thread(() -> {
//      Integer i = 0;
//      while (true) {
//        queue.offer((i++).toString());
//        try {
//          Thread.sleep(100);
//        } catch (InterruptedException e) {
//          e.printStackTrace();
//        }
//      }
//    });
//    t.setDaemon(true);
//    t.start();
//    for (String q : queue) {
//      System.out.println(q);
//      try {
//        Thread.sleep(100);
//      } catch (InterruptedException e) {
//        e.printStackTrace();
//      }
//    }

  }
}
