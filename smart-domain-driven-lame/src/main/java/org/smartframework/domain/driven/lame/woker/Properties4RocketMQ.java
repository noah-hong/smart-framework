package org.smartframework.domain.driven.lame.woker;

/**
 * @author noah
 * @create 2021-02-10 17:47
 */
public class Properties4RocketMQ {

  public static final String COMMAND_NAME = "LAME_COMMAND_NAME";
  public static final String CLIENT_ID = "LAME_CLIENT_ID";
  public static final String CORRELATION_ID = "LAME_CORRELATION_ID";
  public static final String WAIT_RESPOND_TIMEOUT = "LAME_WAIT_RESPOND_TIMEOUT";
  public static final String LOCATION = "LAME_LOCATION";
}
