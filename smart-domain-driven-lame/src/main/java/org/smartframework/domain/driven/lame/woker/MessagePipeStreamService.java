package org.smartframework.domain.driven.lame.woker;

import org.apache.rocketmq.common.ThreadFactoryImpl;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IWorkerSiteService;
import org.smartframework.domain.driven.lame.common.ClientSiteResponderUtils;
import org.smartframework.domain.driven.lame.woker.MessagePipeInfo.MessageProcessBehavior;

import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author noah
 * @create 2021-06-14 16:48
 */
public class MessagePipeStreamService extends Thread {

  private final static Logger log = LoggerFactory.getLogger(MessagePipeStreamService.class);
  private final ConcurrentHashMap<String, MessageProcessUnit> messageProcessUnitMap = new ConcurrentHashMap<>();

  private BlockingQueue<MessagePipeInfo> processMessageQueue;

  public IClientSiteResponderRegistrar getClientSiteResponderRegistrar() {
    return clientSiteResponderRegistrar;
  }

  private final IClientSiteResponderRegistrar clientSiteResponderRegistrar;
  private final IWorkerSiteService invoker;
  private ThreadPoolExecutor messagePressureOverloadExecutor;

  public ThreadPoolExecutor getMessagBackflowExecutor() {
    return messagBackflowExecutor;
  }

  private ThreadPoolExecutor messagBackflowExecutor;
  private MessageProcessUnitExecutor messageProcessUnitExecutor;

  public Integer getCommandExecutorMinThread() {
    return Optional.ofNullable(commandExecutorMinThread).orElse(10);
  }

  public void setCommandExecutorMinThread(Integer commandExecutorMinThread) {
    this.commandExecutorMinThread = commandExecutorMinThread;
  }

  public Integer getCommandExecutorMaxThread() {
    return Optional.ofNullable(commandExecutorMaxThread).orElse(10);
  }

  public void setCommandExecutorMaxThread(Integer commandExecutorMaxThread) {
    this.commandExecutorMaxThread = commandExecutorMaxThread;
  }

  public Integer getCommandBackPressureThresholds() {
    return Optional.ofNullable(commandBackPressureThresholds).orElse(10);
  }

  public void setCommandBackPressureThresholds(Integer commandBackPressureThresholds) {
    this.commandBackPressureThresholds = commandBackPressureThresholds;
  }

  public Boolean isEnabledFastFail() {
    return Optional.ofNullable(enabledFastFail).orElse(false);
  }

  public void setEnabledFastFail(Boolean enabledFastFail) {
    this.enabledFastFail = enabledFastFail;
  }

  private Integer commandExecutorMinThread = 10;
  private Integer commandExecutorMaxThread = 20;
  private Integer commandBackPressureThresholds = 300;
  private Boolean enabledFastFail;


  public MessagePipeStreamService(IClientSiteResponderRegistrar clientSiteResponderRegistrar,
      IWorkerSiteService invoker) {
    this.clientSiteResponderRegistrar = clientSiteResponderRegistrar;
    this.invoker = invoker;

    ScheduledExecutorService scheduledService = Executors
        .newSingleThreadScheduledExecutor(
            new ThreadFactoryImpl("LameClearMessageScheduledThread_"));

    scheduledService.scheduleAtFixedRate(
        () -> {
          final Enumeration<String> keys = messageProcessUnitMap.keys();
          while (keys.hasMoreElements()) {
            final String key = keys.nextElement();
            if (isRemoveMessageUnit(key)) {
              if (log.isDebugEnabled()) {
                log.debug("释放工作区:{}-{}",
                    invoker.getHandlerType().getSimpleName(), key);
              }
              delete(key);
            }
          }
        },
        1000 * 60,
        1000 * 60,
        TimeUnit.MILLISECONDS);
  }

  public void delete(String key) {
    MessagePipeInfo messagePipeInfo = new MessagePipeInfo();
    messagePipeInfo.setKey(key);
    messagePipeInfo.setBehavior(MessageProcessBehavior.DELETE);
    messagePipeInfo.setServiceType(this.invoker.getHandlerType());
    try {
      processMessageQueue.offer(messagePipeInfo, 3 * 1000, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
      log.warn("移除消息处理组操作被中断", e);
    }
  }

  public void put(MessagePipeInfo messagePipeInfo) {
    if (messagePipeInfo.getBehavior() == MessageProcessBehavior.PROCESS
        && messagePipeInfo
        .isTimeout()) {
      log.warn("请求已超时，请求信息：{}", messagePipeInfo.toString());
      return;
    }
    final boolean offer = processMessageQueue.offer(messagePipeInfo);
    // 到达处理上限
    if (!offer) {
      log.warn("等待处理的请求已超出最大阈值，请求开始回流。请求信息：{}", messagePipeInfo.toString());
      messagePressureOverloadExecutor.execute(() -> {

        ClientSiteResponderUtils
            .respond(clientSiteResponderRegistrar, messagePipeInfo.getClientId(),
                messagePipeInfo.getCorrelationId(),
                CommandResponse.exceedBackPressureThreshold("系统繁忙，请稍后再试"));
//        final IClientSiteResponder clientSiteResponder = clientSiteResponderRegistrar
//            .get(clientId);
//        CommandResponseEnvelope commandResponseEnvelope = new CommandResponseEnvelope();
//        commandResponseEnvelope.setCorrelationId(correlationId);
//        commandResponseEnvelope
//            .setCommandResult(CommandResponse.failure("等待处理的请求已超出最大阈值，请稍后再试"));
//        clientSiteResponder.respond(commandResponseEnvelope);
      });
    }
  }

  public void put(List<MessageExt> exts) {
    exts.forEach(
        messageExt -> {
          try {
            MessagePipeInfo messagePipeInfo = new MessagePipeInfo();
            messagePipeInfo.setKey(messageExt.getKeys());
            messagePipeInfo.setBody(messageExt.getBody());
            messagePipeInfo.setMessageId(messageExt.getMsgId());
            messagePipeInfo.setMessageBornTimestamp(messageExt.getBornTimestamp());
//            messagePipeInfo.setCommandName(
//                messageExt.getProperty(Properties4RocketMQ.COMMAND_NAME));
            messagePipeInfo.setWaitRespondTimeout(
                Integer.valueOf(messageExt.getProperty(Properties4RocketMQ.WAIT_RESPOND_TIMEOUT)));
            messagePipeInfo.setBehavior(MessageProcessBehavior.PROCESS);
            messagePipeInfo.setClientId(messageExt.getProperty(Properties4RocketMQ.CLIENT_ID));
            messagePipeInfo.setCorrelationId(
                messageExt.getProperty(Properties4RocketMQ.CORRELATION_ID));
            messagePipeInfo.setServiceType(invoker.getHandlerType());
            messagePipeInfo.setTopic(messageExt.getTopic());
            messagePipeInfo.setTags(messageExt.getTags());
            messagePipeInfo.setProperties(messageExt.getProperties());
            if (log.isDebugEnabled()) {
              log.debug("请求进入通道，请求消息：{}", messagePipeInfo.toString());
            }
            put(messagePipeInfo);
          } catch (Exception ex) {
            log.warn("消息请求未进入管道");
            log.warn(ex.getMessage(), ex);
          }
        });
  }

  private Boolean isInit = false;

  @Override
  public synchronized void start() {
    if (!isInit) {
      if (this.getCommandBackPressureThresholds() > 0) {
        processMessageQueue = new LinkedBlockingQueue<>(this.getCommandBackPressureThresholds());
      } else {
        processMessageQueue = new LinkedBlockingQueue<>(1);
      }
      this.messagBackflowExecutor =
          new ThreadPoolExecutor(
              1,
              10,
              1000 * 60,
              TimeUnit.MILLISECONDS,
              new LinkedBlockingQueue<>(500),
              new ThreadFactoryImpl("LameProcessBackflowThread_"),
              new ThreadPoolExecutor.DiscardPolicy());
      this.messagePressureOverloadExecutor =
          new ThreadPoolExecutor(
              1,
              10,
              1000 * 60,
              TimeUnit.MILLISECONDS,
              new LinkedBlockingQueue<>(500),
              new ThreadFactoryImpl("LameProcessOverloadThread_"),
              new ThreadPoolExecutor.DiscardPolicy());
      this.messageProcessUnitExecutor =
          new MessageProcessUnitExecutor(
              this.getCommandExecutorMinThread(), this.getCommandExecutorMaxThread(), this);
      this.setDaemon(false);
      this.setName(
          String.format("LameProcessMainThread_%s", invoker.getHandlerType().getSimpleName()));
      isInit = true;
    }
//    super.setName("MessagePipeStreamServiceThread_" + invoker.getInvokerType().getSimpleName());
    super.start();
  }

  @Override
  public void run() {
    while (true) {
      try {
        final MessagePipeInfo messagePipeInfo = processMessageQueue.take();
        switch (messagePipeInfo.getBehavior()) {
          case PROCESS:
            process(messagePipeInfo);
            break;
          case DELETE:
            remove(messagePipeInfo);
            break;
          default:
            break;
        }
      } catch (Exception e) {
        log.warn(e.getMessage(), e);
      }
    }
  }


  private void process(MessagePipeInfo messagePipeInfo) {
    final MessageProcessUnit processUnit = getMessageProcessUnit(
        messagePipeInfo.getKey());
    processUnit.put(messagePipeInfo);
    processUnit.process();
  }

  private void remove(MessagePipeInfo messagePipeInfo) {
    if (isRemoveMessageUnit(messagePipeInfo.getKey())) {
      messageProcessUnitMap.remove(messagePipeInfo.getKey());
    }
  }

  private boolean isRemoveMessageUnit(String unitName) {
    final MessageProcessUnit messageProcessUnit = messageProcessUnitMap
        .get(unitName);
    return !messageProcessUnit.isActive();
  }


  public MessageProcessUnit getMessageProcessUnit(String unitName) {
    final MessageProcessUnit currMessageProcessUnit = messageProcessUnitMap.get(unitName);
    if (Objects.isNull(currMessageProcessUnit)) {
      final MessageProcessUnit newMessageProcessUnit = new MessageProcessUnit(unitName,
          invoker, messageProcessUnitExecutor, clientSiteResponderRegistrar);
      newMessageProcessUnit.onStoped(m -> {
        messagBackflowExecutor.execute(() -> {
          while (!m.isRunning() && !m.getProcessQueue().isEmpty()) {
            final MessagePipeInfo messagePipeInfo = m.getProcessQueue().poll();
            if (enabledFastFail) {
              ClientSiteResponderUtils
                  .respond(clientSiteResponderRegistrar, messagePipeInfo.getClientId(),
                      messagePipeInfo.getCorrelationId(),
                      CommandResponse.exceedBackPressureThreshold("系统繁忙，请稍后再试"));
            } else {
              if (log.isDebugEnabled()) {
                log.debug("消息处理单元停止后存在未执行消息，开始重推请求，请求信息：{}", messagePipeInfo.toString());
              }
              this.put(messagePipeInfo);
            }

          }
        });

      });
      messageProcessUnitMap.putIfAbsent(unitName, newMessageProcessUnit);
      return newMessageProcessUnit;
    } else {
      return currMessageProcessUnit;
    }
  }


}
