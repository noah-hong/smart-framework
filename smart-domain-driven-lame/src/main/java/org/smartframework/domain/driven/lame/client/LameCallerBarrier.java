package org.smartframework.domain.driven.lame.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.communication.ICallerBarrier;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author noah
 * @create 2021-07-22 11:06
 */
public class LameCallerBarrier implements ICallerBarrier {

  private final Logger logger = LoggerFactory.getLogger(LameCallerBarrier.class);

  private boolean entry = true;
  private final BlockingQueue<Long> blockingQueue = new LinkedBlockingQueue<>(1);
  private boolean isInit = false;
  private final Class<?> serviceType;

  public LameCallerBarrier(Class<?> serviceType) {
    this.serviceType = serviceType;
  }

  public void init() {
    if (!isInit) {
      isInit = true;
      Thread thread = new Thread(() -> {
        while (true) {
          try {
            final Long period = blockingQueue.take();
            entry = false;
            Thread.sleep(period);
          } catch (InterruptedException e) {
            logger.warn(e.getMessage(), e);
          } finally {
            entry = true;
          }
        }
      });

      thread.setName(
          "LameCallerBarrierThread-" + serviceType.getSimpleName());

      thread.setDaemon(true);
      thread.start();
    }
  }


  @Override
  public boolean tryEntry() {
    return entry;
  }

  @Override
  public void tryLock(Long period) {
    if (period > 0) {
      blockingQueue.offer(period);
    }
  }


}
