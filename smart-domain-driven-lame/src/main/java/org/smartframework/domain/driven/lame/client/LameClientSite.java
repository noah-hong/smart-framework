package org.smartframework.domain.driven.lame.client;

import org.redisson.api.RBlockingQueue;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.communication.CommandResponseEnvelope;
import org.smartframework.domain.driven.core.communication.IClientSite;
import org.smartframework.domain.driven.core.communication.IClientSiteProcessResponder;
import org.smartframework.domain.driven.core.communication.IClientSiteProcessResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IClientSiteRouter;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

/**
 * @author noah
 * @create 2021-02-22 10:20
 */
public class LameClientSite implements IClientSite {

  private final IClientSiteProcessResponderRegistrar clientSiteProcessResponderRegistrar;
  private final RedissonClient redissonClient;
  private final IClientSiteRouter router;
  private final static Logger logger = LoggerFactory.getLogger(LameClientSite.class);

  private Boolean isRunning = false;

  public LameClientSite(
      RedissonClient redissonClient,
      IClientSiteRouter router,
      IClientSiteProcessResponderRegistrar clientSiteProcessResponderRegistrar,
      String namespace) {
    this.router = router;
    this.redissonClient = redissonClient;
    this.clientSiteProcessResponderRegistrar = clientSiteProcessResponderRegistrar;
    LocalDateTime now = LocalDateTime.now();
    String dateFormat = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    String flag = UUID.randomUUID().toString();
    String suffix = String.format("%s:lame-client-site", namespace);
    clientId = String.format("%s:%s:%s", suffix, dateFormat, flag);

  }

  @Override
  public void start() {
    if (!isRunning) {

      RBlockingQueue<CommandResponseEnvelope> blockingQueue =
          redissonClient.getBlockingQueue(getClientId());
      CommandResponseConsumer consumer =
          new CommandResponseConsumer(blockingQueue, clientSiteProcessResponderRegistrar);

      Thread thread = new Thread(consumer);
      thread.setName("LameClientSite_" + getClientId());
      thread.setDaemon(true);
      thread.start();
      isRunning = true;
      router.register(this);
    }
  }

//  @Override
//  public void stop() {
//    if (isRunning) {
//      redissonClient.getBlockingQueue(getClientId()).delete();
//    }
//  }


  private final String clientId;

  @Override
  public String getClientId() {
    return clientId;
  }

  private static class CommandResponseConsumer implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(CommandResponseConsumer.class);
    private final BlockingQueue<CommandResponseEnvelope> queue;
    private final IClientSiteProcessResponderRegistrar clientSiteProcessResponderRegistrar;

    CommandResponseConsumer(
        BlockingQueue<CommandResponseEnvelope> q,
        IClientSiteProcessResponderRegistrar clientSiteProcessResponderRegistrar) {
      this.queue = q;
      this.clientSiteProcessResponderRegistrar = clientSiteProcessResponderRegistrar;
    }

    @Override
    public void run() {
      try {
        while (true) {
          try {
            consume(queue.take());
          } catch (Exception ex) {
            log.warn("远程调度结果消费时发生异常", ex);
          }
        }
      } catch (Exception ex) {
        log.warn("远程调度结果消费线程发生异常", ex);
      }
    }

    void consume(CommandResponseEnvelope x) {
      log.debug(MessageFormat
          .format("远程调度结束,已接收到回传结果,CorrelationId:{0},Timestamp:{1}",
              x.getCorrelationId(), System.currentTimeMillis()));
      IClientSiteProcessResponder clientSiteProcessResponder =
          clientSiteProcessResponderRegistrar.get(x.getCorrelationId());
      try {
        if (clientSiteProcessResponder == null) {
          log.warn(MessageFormat
              .format("远程调度结束,响应回传结果的对象已不存在,CorrelationId:{0},Timestamp:{1}",
                  x.getCorrelationId(), System.currentTimeMillis()));
          return;
        }
        clientSiteProcessResponder.respond(x.getCommandResult());
      } catch (Exception ex) {
        log.error(MessageFormat
            .format("远程调度结束,回传结果时发生异常,CorrelationId:{0},Timestamp:{1}", x.getCorrelationId(),
                System.currentTimeMillis()), ex);
        if (clientSiteProcessResponder != null) {
          clientSiteProcessResponder.respond(CommandResponse.failure(ex));
        }
      }
    }
  }
}
