package org.smartframework.saga.examples.saga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.saga.core.SagaCommandResult;
import org.smartframework.saga.core.SagaDefinition;
import org.smartframework.saga.core.SagaDsl;
import org.smartframework.saga.core.SagaInstance;
import org.smartframework.saga.core.SagaRecoverStrategy;
import org.smartframework.spring.boot.autoconfigure.saga.annotation.Saga;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author noah
 * @create 2021-10-15 20:30
 */
@Saga
public class TestSuccessSagaDsl implements SagaDsl<TestSagaData> {

  @Autowired
  TestFailureSagaDsl sagaDsl;

  private Logger logger = LoggerFactory.getLogger(TestSuccessSagaDsl.class);
  private SagaDefinition<TestSagaData> sagaDefinition =
      step("try")
          .invokeParticipant(this::invoker1)
//          .withCompensation(this::compensator1)
          .step("confirm")
          .invokeParticipant(this::predicate, this::invoker2)
          .withCompensation(this::compensator2)
          .step("step3")
          .invokeParticipant(this::predicate3, this::invoker3)
          //定义Saga恢复策略
          .build(SagaRecoverStrategy.FORWARD);

  public boolean predicate3(TestSagaData data) {
    //返回false则不会执行对应的正向能力或者补偿能力
    return true;
  }

  public boolean predicate(TestSagaData data) {
    //返回false则不会执行对应的正向能力或者补偿能力
    return false;
  }

  private SagaCommandResult invoker1(TestSagaData data) {
    //TODO 正向能力1处理
    return SagaCommandResult.success("执行invoker1成功");
  }

  private SagaCommandResult invoker2(TestSagaData data) {
    //TODO 正向能力2处理
    return SagaCommandResult.success("执行invoker2成功");
  }

  private SagaCommandResult invoker3(TestSagaData data) {
    //TODO 正向能力3处理
    return SagaCommandResult.success("执行invoker3成功");
  }

  private SagaCommandResult compensator1(TestSagaData data) {
    //TODO 正向能力1对应的补偿能力处理
    return SagaCommandResult.success();
  }

  private SagaCommandResult compensator2(TestSagaData data) {
    //TODO 正向能力2对应的补偿能力处理，如果未执行正向能力2则不会执行该补偿
    return SagaCommandResult.success();
  }


//  @Override
//  public String getSagaCode() {
//    return "test-saga-dsl";
//  }

  @Override
  public String getSagaName() {
    return "test-saga-dsl";
  }

  @Override
  public SagaDefinition<TestSagaData> getSagaDefinition() {
    return sagaDefinition;
  }

  @Override
  public void onStarting(String sagaId, TestSagaData testSagaData) {
    //TODO saga开始前
    logger.info("saga开始前");
  }

  @Override
  public void onSagaCompletedSuccessfully(SagaInstance sagaInstance) {
    //TODO saga执行成功
    logger.info("saga执行成功");
  }

  @Override
  public void onSagaRolledBack(SagaInstance sagaInstance) {
    //TODO saga补偿成功
    logger.info("saga补偿成功");
  }
}
