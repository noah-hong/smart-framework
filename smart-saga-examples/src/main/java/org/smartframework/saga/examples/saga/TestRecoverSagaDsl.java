package org.smartframework.saga.examples.saga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.saga.core.SagaCommandResult;
import org.smartframework.saga.core.SagaDefinition;
import org.smartframework.saga.core.SagaDsl;
import org.smartframework.saga.core.SagaInstance;
import org.smartframework.spring.boot.autoconfigure.saga.annotation.Saga;

/**
 * @author noah
 * @create 2021-10-15 20:30
 */
@Saga
public class TestRecoverSagaDsl implements SagaDsl<TestSagaData> {

  private Logger logger = LoggerFactory.getLogger(TestRecoverSagaDsl.class);
  private SagaDefinition<TestSagaData> sagaDefinition =
      step("step1")
          .invokeParticipant(this::invoker1)
          .withCompensation(this::compensator1)
          .step("step2")
          .invokeParticipant(this::invoker2)
          .withCompensation(this::compensator2)
          .build();

  private SagaCommandResult invoker1(TestSagaData data) {

    return SagaCommandResult.success("执行invoker1成功");
  }

  private SagaCommandResult invoker2(TestSagaData data) {
//    final SagaCommandResult sagaCommandResult = new SagaCommandResult(
//        SagaCommandExceutionStatus.FAILURE);
//    sagaCommandResult.setMessage("执行invoker2失败");
//    return sagaCommandResult;
    throw new RuntimeException("执行invoker2失败-ex");
  }

  private SagaCommandResult compensator1(TestSagaData data) {
    if (data.getCount() == 3) {
      return SagaCommandResult.success("回滚compensator1成功");
    }
    return SagaCommandResult.failure("回滚compensator1失败");
  }

  private SagaCommandResult compensator2(TestSagaData data) {
    return SagaCommandResult.success("回滚compensator2成功");
  }


  @Override
  public String getSagaName() {
    return "test-saga-dsl";
  }

  @Override
  public SagaDefinition<TestSagaData> getSagaDefinition() {
    return sagaDefinition;
  }

  @Override
  public void onStarting(String sagaId, TestSagaData testSagaData) {
    //TODO saga开始前
    logger.info("saga开始前");
  }

  @Override
  public void onSagaCompletedSuccessfully(SagaInstance sagaInstance) {
    //TODO saga执行成功
    logger.info("saga执行成功");
  }

  @Override
  public void onSagaRolledBack(SagaInstance sagaInstance) {
    //TODO saga补偿成功
    logger.info("saga补偿成功");
  }
}
