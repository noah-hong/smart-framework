package org.smartframework.saga.examples.saga.spring;

/**
 * @author noah
 * @create 2022-04-15 11:20
 */

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SpringUtil implements BeanFactoryPostProcessor, ApplicationContextAware {
  private static ConfigurableListableBeanFactory beanFactory;
  private static ApplicationContext applicationContext;

  public SpringUtil() {
  }

  @Override
  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
    SpringUtil.beanFactory = beanFactory;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) {
    SpringUtil.applicationContext = applicationContext;
  }

  public static ApplicationContext getApplicationContext() {
    return applicationContext;
  }

  public static ListableBeanFactory getBeanFactory() {
    return (ListableBeanFactory)(null == beanFactory ? applicationContext : beanFactory);
  }

//  public static <T> T getBean(String name) {
//    return getBeanFactory().getBean(name);
//  }

  public static <T> T getBean(Class<T> clazz) {
    return getBeanFactory().getBean(clazz);
  }

  public static <T> T getBean(String name, Class<T> clazz) {
    return getBeanFactory().getBean(name, clazz);
  }

//  public static <T> T getBean(TypeReference<T> reference) {
//    ParameterizedType parameterizedType = (ParameterizedType)reference.getType();
//    Class<T> rawType = (Class)parameterizedType.getRawType();
//    Class<?>[] genericTypes = (Class[]) Arrays.stream(parameterizedType.getActualTypeArguments()).map((type) -> {
//      return (Class)type;
//    }).toArray((x$0) -> {
//      return new Class[x$0];
//    });
//    String[] beanNames = getBeanFactory().getBeanNamesForType(
//        ResolvableType.forClassWithGenerics(rawType, genericTypes));
//    return getBean(beanNames[0], rawType);
//  }

  public static <T> Map<String, T> getBeansOfType(Class<T> type) {
    return getBeanFactory().getBeansOfType(type);
  }

  public static String[] getBeanNamesForType(Class<?> type) {
    return getBeanFactory().getBeanNamesForType(type);
  }

  public static String getProperty(String key) {
    return null == applicationContext ? null : applicationContext.getEnvironment().getProperty(key);
  }

  public static String[] getActiveProfiles() {
    return null == applicationContext ? null : applicationContext.getEnvironment().getActiveProfiles();
  }

//  public static String getActiveProfile() {
//    String[] activeProfiles = getActiveProfiles();
//    return ArrayUtil.isNotEmpty(activeProfiles) ? activeProfiles[0] : null;
//  }

  public static <T> void registerBean(String beanName, T bean) {
    if (null != beanFactory) {
      beanFactory.registerSingleton(beanName, bean);
    } else if (applicationContext instanceof ConfigurableApplicationContext) {
      ConfigurableApplicationContext context = (ConfigurableApplicationContext)applicationContext;
      context.getBeanFactory().registerSingleton(beanName, bean);
    }

  }
}

