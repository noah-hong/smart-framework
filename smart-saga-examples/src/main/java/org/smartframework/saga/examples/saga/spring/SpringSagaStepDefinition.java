package org.smartframework.saga.examples.saga.spring;

import org.smartframework.saga.core.ParticipantInvocation;
import org.smartframework.saga.core.SagaCommandResult;
import org.smartframework.saga.examples.saga.TestSagaData;
import org.springframework.stereotype.Component;

/**
 * @author noah
 * @create 2022-04-15 10:43
 */

public class SpringSagaStepDefinition {

  @Component
  public static class Participart implements ParticipantInvocation<TestSagaData>{

    @Override
    public boolean isInvocable(TestSagaData testSagaData) {
      return true;
    }

    @Override
    public SagaCommandResult makeCommand(TestSagaData testSagaData) {
      return SagaCommandResult.success();
    }
  }

  @Component
  public static class Compersation implements ParticipantInvocation<TestSagaData> {

    @Override
    public boolean isInvocable(TestSagaData testSagaData) {
      return false;
    }

    @Override
    public SagaCommandResult makeCommand(TestSagaData testSagaData) {
      return null;
    }
  }
}
