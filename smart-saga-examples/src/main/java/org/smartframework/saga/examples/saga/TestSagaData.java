package org.smartframework.saga.examples.saga;

/**
 * @author noah
 * @create 2021-10-15 20:30
 */
public class TestSagaData {
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
