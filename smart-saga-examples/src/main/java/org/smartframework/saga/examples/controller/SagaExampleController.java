package org.smartframework.saga.examples.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.smartframework.saga.core.SagaInstance;
import org.smartframework.saga.examples.saga.TestFailureForwardSagaDsl;
import org.smartframework.saga.examples.saga.TestFailureSagaDsl;
import org.smartframework.saga.examples.saga.TestRecoverSagaDsl;
import org.smartframework.saga.examples.saga.TestSagaData;
import org.smartframework.saga.examples.saga.TestSpringSagaDsl;
import org.smartframework.saga.examples.saga.TestSuccessSagaDsl;
import org.smartframework.spring.boot.autoconfigure.saga.core.SpringSagaInstanceFactory;
import org.smartframework.spring.boot.autoconfigure.saga.web.SagaApiResponseEnvelope;
import org.smartframework.spring.boot.autoconfigure.saga.web.SagaInstanceVO;
import org.smartframework.spring.boot.autoconfigure.saga.web.SagaResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author noah
 * @create 2021-05-27 23:15
 */
@RestController
@RequestMapping(value = "/saga/examples")
@Api(value = "Saga样例")
public class SagaExampleController {

  @Autowired
  private SpringSagaInstanceFactory sagaInstanceFactory;
  @Autowired
  private TestSuccessSagaDsl testSuccessSagaDsl;
  @Autowired
  private TestFailureSagaDsl testFailureSagaDsl;
  @Autowired
  private TestFailureForwardSagaDsl testFailureForwardSagaDsl;
  @Autowired
  private TestSpringSagaDsl testSpringSagaDsl;
  @Autowired
  private TestRecoverSagaDsl testRecoverSagaDsl;

  @ApiOperation("Spring-Saga执行成功样例")
  @PostMapping("spring-exec-success")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> springExecSuccess() {
    String sagaId = UUID.randomUUID().toString();
    final SagaInstance sagaInstance = sagaInstanceFactory
        .create(TestSpringSagaDsl.class, new TestSagaData());

    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }


  @ApiOperation("Saga执行成功样例")
  @PostMapping("exec-success")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> execSuccess() {
    String sagaId = UUID.randomUUID().toString();
    final SagaInstance sagaInstance = sagaInstanceFactory
        .create(testSuccessSagaDsl, sagaId, new TestSagaData());

    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }

  @ApiOperation("Saga执行失败样例-向前恢复")
  @PostMapping("exec-failure-forward")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> execFailureForward() {
    String sagaId = UUID.randomUUID().toString();
    final SagaInstance sagaInstance = sagaInstanceFactory
        .create(testFailureForwardSagaDsl, sagaId, new TestSagaData());
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }

  @ApiOperation("Saga执行失败样例")
  @PostMapping("exec-failure")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> execFailure() {
    String sagaId = UUID.randomUUID().toString();
    final SagaInstance sagaInstance = sagaInstanceFactory
        .create(testFailureSagaDsl, sagaId, new TestSagaData());
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }

  @ApiOperation("Saga执行还原样例")
  @PostMapping("exec-recover-fr")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> execRecoverFr() {
    String sagaId = UUID.randomUUID().toString();
    final SagaInstance sagaInstance = sagaInstanceFactory
        .create(testRecoverSagaDsl, sagaId, new TestSagaData());
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }

  @ApiOperation("Saga执行还原样例")
  @PostMapping("exec-recover-create")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> execRecoverCreate(String sagaId) {
    final SagaInstance sagaInstance = sagaInstanceFactory
        .recoverToReStart(sagaId);
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }
  @ApiOperation("Saga执行还原样例-跳过当前步骤")
  @PostMapping("exec-recover-to-next")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> execRecoverToNext(String sagaId) {
    final SagaInstance sagaInstance = sagaInstanceFactory
        .recoverToNextStep(sagaId);
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }

  @ApiOperation("Saga执行还原样例")
  @PostMapping("exec-recover")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> execRecover(String sagaId) {
    final SagaInstance sagaInstance = sagaInstanceFactory
        .recover(sagaId);
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }

  @ApiOperation("Saga执行还原样例")
  @PostMapping("exec-recover-with-data")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> execRecoverWithData(String sagaId) {
    final TestSagaData testSagaData = new TestSagaData();
    testSagaData.setCount(3);
    final SagaInstance sagaInstance = sagaInstanceFactory
        .recover(sagaId, testSagaData);
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }

}
