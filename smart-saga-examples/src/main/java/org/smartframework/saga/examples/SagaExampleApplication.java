package org.smartframework.saga.examples;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.smartframework.spring.boot.autoconfigure.saga.EnableSaga;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author noah
 * @create 2021-05-28 15:17
 */
@SpringBootApplication
@EnableSwagger2Doc
@EnableSaga
public class SagaExampleApplication {

  public static void main(String[] args) {

    SpringApplication.run(SagaExampleApplication.class, args);
  }


}
