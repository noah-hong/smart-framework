package org.smartframework.saga.examples.saga;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.saga.core.SagaCommandResult;
import org.smartframework.saga.core.SagaDefinition;
import org.smartframework.saga.core.SagaInstance;
import org.smartframework.saga.examples.saga.spring.SpringSagaStepCompensation;
import org.smartframework.saga.examples.saga.spring.SpringSagaStepDefinition;
import org.smartframework.saga.examples.saga.spring.SpringSagaStepParticipart;
import org.smartframework.spring.boot.autoconfigure.saga.annotation.Saga;
import org.smartframework.spring.boot.autoconfigure.saga.core.SpringSagaDsl;

/**
 * @author noah
 * @create 2022-04-14 16:24
 */
@Saga
public class TestSpringSagaDsl extends SpringSagaDsl<TestSagaData> {

  private Logger logger = LoggerFactory.getLogger(TestSpringSagaDsl.class);
//  private SagaDefinition<TestSagaData> sagaDefinition =
//      step("step1")
//          .context(SpringUtil.getApplicationContext())
//          .invokeParticipant(SpringSagaStepParticipart.class)
//          .withCompensation(SpringSagaStepCompensation.class)
//          .step("step2")
//          .invokeParticipant(SpringSagaStepDefinition.Participart.class)
//          .withCompensation(SpringSagaStepDefinition.Compersation.class)
//          .build();

  private SagaCommandResult invoker1(TestSagaData data) {

    return SagaCommandResult.success("执行invoker1成功");
  }

  @Override
  public String getSagaName() {
    return "test-saga-dsl";
  }

  @Override
  public SagaDefinition<TestSagaData> getSagaDefinition() {
    return
        step("step1")
            .invokeParticipant(SpringSagaStepParticipart.class)
            .withCompensation(SpringSagaStepCompensation.class)
            .step("step2")
            .invokeParticipant(SpringSagaStepDefinition.Participart.class)
            .withCompensation(SpringSagaStepDefinition.Compersation.class)
            .build();
  }

  @Override
  public void onStarting(String sagaId, TestSagaData testSagaData) {
    //TODO saga开始前
    logger.info("saga开始前");
  }

  @Override
  public void onSagaCompletedSuccessfully(SagaInstance sagaInstance) {
    //TODO saga执行成功
    logger.info("saga执行成功");
  }

  @Override
  public void onSagaRolledBack(SagaInstance sagaInstance) {
    //TODO saga补偿成功
    logger.info("saga补偿成功");
  }
}
