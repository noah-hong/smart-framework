# 能力编排框架
一种基于Saga理念进行设计的轻量级能力编排框架

## 前言

能力编排框架为多种能力组合编排提供了一个基本框架。它的核心编排理念基于Saga模型，以便于进行长活事务的相关业务设计。
Saga模型是把一个分布式事务拆分成多个本地事务，每个本地事务都有对应的执行模块和补偿模块(可选),当任何一个事务失败时,可以通过调用补偿模块来进行恢复,达到事务的最终一致性。


## 特性说明

- [x] 能力编排
  + [x] 正向能力编排
  + [x] 补偿能力编排
- [x] 执行状态持久化
- [x] 数据一致性保证
  + [x] 向后恢复：正向能力异常时自动执行正向能力所对应的补偿能力进行数据补偿、回滚
  + [x] 向前恢复：正向能力异常时自动挂起,直到正向能力恢复正常时继续执行,最终保证每个正向能力都会成功
- [x] 能力编排管理界面
  + [x] 执行日志查看
  + [x] 能力一键重试
  + [x] 能力中断恢复
    + [x] 正向能力异常重试
    + [x] 补偿能力异常重试

## 流程图
### 正向能力执行时序图
![正向能力.png](docs/正向能力时序图.png)

### 补偿能力执行时序图
![补偿能力.png](docs/补偿能力时序图.png)

### 状态机
![状态机.png](docs/状态机.png)

### 示例
![示例.png](docs/示例.png)

## 注意事项
### 幂等性
正向能力和补偿能力均需要支持幂等性

### 防悬挂控制
#### 可能造成的问题
补偿能力操作早于正向能力，而导致的数据问题

#### 解决方案
能力编排框架在调用正向能力时，可能会出现因网络拥堵而导致的超时，此时能力编排框架会执行补偿能力；在此之后，拥堵在网络上的正向能力被所属服务收到，出现了补偿能力比正向能力先执行的情况。**所以用户在编排能力时，每个能力应当允许空补偿，但是要拒绝执行空补偿之后到来的正向能力请求**。

### 隔离性
#### 可能造成的问题
- 丢失更新：覆盖了其他人更新的数据
- 脏读：读取了尚未完成的数据
- 模糊或者不可重复读：不同的步骤读取相同的数据却得到了不同的结果

#### 解决方案
能力编排框架本身不提供隔离性保证,需要在长活事务设计过程中进行考虑
可以考虑使用如下方案解决隔离性带来的问题
- 语义锁：应用程序级的锁（如状态机中定义的状态）
- 交换式更新：把更新操作设计成可以按任何顺序执行
- 悲观视图：重新编排能力顺序，最大限度降低业务风险

## 快速开始
- 在POM文件中引入如下依赖
``` maven
<dependency>
    <groupId>org.smartframework</groupId>
    <artifactId>smart-saga-spring-boot-starter</artifactId>
    <version>1.2-SMART-SNAPSHOT</version>
</dependency>
```

## 如何使用

- 使用smart-spring-boot-autoconfigure项目中资源目录下的mysql-schema.sql文件创建相关表
  - saga_instance_info：saga执行结果信息
  - saga_step_info：saga执行步骤信息

- 在Spring Boot Application的上添加`@EnableSaga`，表示允许Saga定义注入

``` Java
@SpringBootApplication
@EnableSaga
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
```

- Saga定义样例

``` java
@Saga
public class TestSuccessSagaDsl implements SagaDsl<TestSagaData> {

  private Logger logger = LoggerFactory.getLogger(TestSuccessSagaDsl.class);
  private SagaDefinition<TestSagaData> sagaDefinition =
      step("step1")
          .invokeParticipant(this::invoker1)
          .withCompensation(this::compensator1)
      .step("step2")
          .invokeParticipant(this::predicate, this::invoker2)
          .withCompensation(this::compensator2)
          //定义Saga恢复策略
          .build(SagaRecoverStrategy.FORWARD);


  public boolean predicate(TestSagaData data) {
    //返回false则不会执行对应的正向能力或者补偿能力
    return false;
  }

  private SagaCommandResult invoker1(TestSagaData data) {
    //TODO 正向能力1处理
    return SagaCommandResult.success("执行invoker1成功");
  }

  private SagaCommandResult invoker2(TestSagaData data) {
    //TODO 正向能力2处理
    return SagaCommandResult.success("执行invoker2成功");
  }

  private SagaCommandResult compensator1(TestSagaData data) {
    //TODO 正向能力1对应的补偿能力处理
    return SagaCommandResult.success();
  }

  private SagaCommandResult compensator2(TestSagaData data) {
    //TODO 正向能力2对应的补偿能力处理，如果未执行正向能力2则不会执行该补偿
    return SagaCommandResult.success();
  }


  @Override
  public String getSagaName() {
    return "test-saga-dsl";
  }

  @Override
  public SagaDefinition<TestSagaData> getSagaDefinition() {
    return sagaDefinition;
  }

  @Override
  public void onStarting(String sagaId, TestSagaData testSagaData) {
    //TODO saga开始前
    logger.info("saga开始前");
  }

  @Override
  public void onSagaCompletedSuccessfully(SagaInstance sagaInstance) {
    //TODO saga执行成功
    logger.info("saga执行成功");
  }

  @Override
  public void onSagaRolledBack(SagaInstance sagaInstance) {
    //TODO saga补偿成功
    logger.info("saga补偿成功");
  }
}

```

- Saga定义后的使用样例
``` java
@RestController
@RequestMapping(value = "/saga/examples")
@Api(value = "Saga样例")
public class SagaExampleController {

  @Autowired
  private SagaInstanceFactory sagaInstanceFactory;
  @Autowired
  private TestSuccessSagaDsl testSuccessSagaDsl;

  @ApiOperation("Saga执行成功样例")
  @PostMapping("exec-success")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> execSuccess() {
    String sagaId = UUID.randomUUID().toString();
    final SagaInstance sagaInstance = sagaInstanceFactory
        .create(testSuccessSagaDsl, sagaId, new TestSagaData());

    return ResponseEntity.ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }
  ```
  - Saga恢复策略说明
  ``` java
  /**
  * @author noah
  * @create 2021-10-25 15:38
  *
  * Saga的执行顺序有两种：
  * T:正向能力
  * C:补偿能力
  * T1, T2, T3, ..., Tn
  * T1, T2, ..., Tj, Cj,..., C2, C1，其中0 < j < n
  */
  public enum SagaRecoverStrategy {
    /**
    * 向后恢复，补偿所有已完成的事务，如果任一子事务失败。
    * 这种做法的效果是撤销掉之前所有成功的sub-transation，使得整个Saga的执行结果撤销
    * 例：T1, T2, ..., Tj, Cj,..., C2, C1，其中0 < j < n
    */
    BACKWARD,
    /**
    * 向前恢复，重试失败的事务，假设每个子事务最终都会成功。
    * 适用于必须要成功的场景，执行顺序是类似于这样的：T1, T2, ..., Tj(失败), Tj(重试),..., Tn，
    * 其中j是发生错误的sub-transaction。该情况下不需要Ci
    *
    */
    FORWARD,
  }

  ```
  