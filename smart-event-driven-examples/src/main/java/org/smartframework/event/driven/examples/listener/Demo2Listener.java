package org.smartframework.event.driven.examples.listener;

import com.alibaba.fastjson.JSON;
import org.smartframework.event.driven.core.SubscribeResult;
import org.smartframework.event.driven.core.context.IMessageContext;
import org.smartframework.event.driven.core.context.ISubscribeResult;
import org.smartframework.event.driven.examples.event.DemoEvent;
import org.smartframework.event.driven.examples.event.DemoRootEvent;
import org.smartframework.event.driven.examples.service.DemoService;
import org.smartframework.event.driven.rocketmq.annotation.RocketMQEventListener;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author noah
 * @create 2021-05-09 20:19
 * @RocketMQEventListener 声明该类为订阅类, 每个订阅类为一个消费者组, 每个方法为该订阅者组订阅的方法，并可指定消息名称空间,消息队列的服务器地址等
 * @ExactlyOnce 开启消息消费精确一次功能
 * @ConsumedLogger 开启消息消费后日志记录功能
 */
@RocketMQEventListener
public class Demo2Listener {

  @Autowired
  private DemoService demoService;

  /**
   * 消息订阅方法
   *
   * @param event          订阅的消息
   * @param messageContext 消息上下文
   * @return 消息订阅结果
   */
  public ISubscribeResult subscribe(DemoEvent event, IMessageContext messageContext) {
    System.out.println("Demo2Listener:DemoEvent:" + demoService.get());
    return SubscribeResult.SUCCESS;
  }

  /**
   * 可订阅由DemoRootEvent派生的所有子类,如Demo1Event,Demo2Event
   */
  public ISubscribeResult subscribe(DemoRootEvent event, IMessageContext messageContext) {
    System.out.println("Demo2Listener:DemoRootEvent:" + JSON.toJSON(event));
    return SubscribeResult.SUCCESS;
  }

}
