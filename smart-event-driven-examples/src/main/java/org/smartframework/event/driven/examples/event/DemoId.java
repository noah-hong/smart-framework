package org.smartframework.event.driven.examples.event;


import org.smartframework.core.IPersistenceId;

/**
 * @author noah
 * @create 2021-05-09 20:17
 */
public class DemoId implements IPersistenceId<Integer> {

  public DemoId(Integer id) {
    this.id = id;
  }

  public DemoId() {
    this.id = 0;
  }

  public void setValue(Integer id) {
    this.id = id;
  }

  private Integer id;

  @Override
  public Integer getValue() {
    return id;
  }
}
