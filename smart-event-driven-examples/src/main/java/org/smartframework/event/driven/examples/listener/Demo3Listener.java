package org.smartframework.event.driven.examples.listener;

import com.alibaba.fastjson.JSON;
import org.smartframework.event.driven.core.SubscribeResult;
import org.smartframework.event.driven.core.context.IMessageContext;
import org.smartframework.event.driven.core.context.ISubscribeResult;
import org.smartframework.event.driven.examples.event.Demo2Event;
import org.smartframework.event.driven.rocketmq.annotation.RocketMQEventListener;

/**
 * @author noah
 * @create 2021-05-09 20:19
 */
@RocketMQEventListener
//@ExactlyOnce
//@ConsumedLogger
public class Demo3Listener {

  public ISubscribeResult subscribe(Demo2Event event, IMessageContext messageContext) {
    System.out.println("Demo3Listener:Demo2Event:" + JSON.toJSON(event));
    return SubscribeResult.SUCCESS;
  }

}
