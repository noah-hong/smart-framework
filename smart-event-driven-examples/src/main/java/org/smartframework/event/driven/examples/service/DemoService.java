package org.smartframework.event.driven.examples.service;

import org.springframework.stereotype.Service;

/**
 * @author noah
 * @create 2021-05-30 23:04
 */
@Service
public class DemoService {

  public Integer get() {
    return 1;
  }
}
