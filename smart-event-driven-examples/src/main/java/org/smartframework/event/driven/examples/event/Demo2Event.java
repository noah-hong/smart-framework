package org.smartframework.event.driven.examples.event;


import org.smartframework.event.driven.core.annotation.SubTopic;

/**
 * @author noah
 * @create 2021-05-09 20:17
 */
@SubTopic
public class Demo2Event extends DemoRootEvent {

    public void setPersistenceId(DemoId demoId) {
        this.persistenceId = demoId;
    }

    private DemoId persistenceId;

    @Override
    public DemoId getPersistenceId() {
        return persistenceId;
    }
}
