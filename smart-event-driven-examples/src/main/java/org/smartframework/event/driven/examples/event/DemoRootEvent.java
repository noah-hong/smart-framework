package org.smartframework.event.driven.examples.event;


import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.annotation.Topic;

/**
 * @author noah
 * @create 2021-05-09 20:13
 */
@Topic
public abstract class DemoRootEvent implements IEvent<DemoId> {

  @Override
  public String getTagName() {
    return "DEMO2";
  }

  @Override
  public Integer getVersion() {
    return 1;
  }
}
