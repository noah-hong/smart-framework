package org.smartframework.event.driven.examples.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.skywalking.apm.toolkit.trace.TraceContext;
import org.smartframework.event.driven.core.EventEnvelope;
import org.smartframework.event.driven.core.annotation.Publishable;
import org.smartframework.event.driven.core.store.IEventEnvelopStore;
import org.smartframework.event.driven.examples.event.Demo2Event;
import org.smartframework.event.driven.examples.event.DemoEvent;
import org.smartframework.event.driven.examples.event.DemoId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author noah
 * @create 2021-05-27 23:15
 */
@RestController
@RequestMapping(value = "/event-driven/examples")
@Api(value = "事件样例")
public class EventDrivenExampleController {

  //事件保存对象
  @Autowired
  private IEventEnvelopStore eventStore;

  /**
   * @return
   * @Publishable 用于消息保存后通知消息中继者按顺序发送所保存的消息
   */
  @ApiOperation("存储事件")
  @PostMapping
  @Transactional
  public ResponseEntity<String> storeEvent() {
    
    //TODO 业务逻辑执行，业务数据入库操作
//    final DemoEvent demo1Event = new DemoEvent();
//    demo1Event.setPersistenceId(new DemoId(1));
//    eventStore.append(new EventEnvelope(demo1Event));
    final Demo2Event demo2Event = new Demo2Event();
    demo2Event.setPersistenceId(new DemoId(2));
    eventStore.append(new EventEnvelope(demo2Event));
    return ResponseEntity.ok(TraceContext.traceId());
  }

  @ApiOperation("发送事件")
  @GetMapping
  @Publishable
  public ResponseEntity<String> publishEvent() {

    return ResponseEntity.ok(TraceContext.traceId());
  }

  @ApiOperation("保存并发送事件")
  @PostMapping("/save-then-publish")
  @Publishable
  public ResponseEntity<String> saveAndPublishEvent(){
    final DemoEvent demoEvent = new DemoEvent();
    demoEvent.setPersistenceId(new DemoId(2));
    eventStore.append(new EventEnvelope(demoEvent));
    return ResponseEntity.ok(TraceContext.traceId());
  }
}
