package org.smartframework.event.driven.examples;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.apache.rocketmq.client.log.ClientLogger;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.EnableRocketMQEventListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author noah
 * @create 2021-05-28 15:17
 */
@SpringBootApplication
@EnableSwagger2Doc
@EnableRocketMQEventListener
public class Application  {

  //NOTE: 测试JACKJSON(√)
  //NOTE: 测试REDISSON回复通道只有工作组时会不会积压消息(√)
  //NOTE: 测试TOPIC增加broker或者broker增加队列时的情况(√)(备注：在控制台选择一个broker添加topic后，其他所有broker也会添加该topic)
  //NOTE: 客户区配置细粒度华到每个代理(√)
  //NOTE: JVM监测(√)
  //NOTE: 测试订阅重试次数到达最大时，通过控制台进行重消费是否可以（√） 不可以，之前的消息还在内存中
  //NOTE: 增加消费失败时，客户机的ip打印
  //NOTE: 在rocket-console直接重试的消息通过consumeMessageDirectly方法进行重试
  public static void main(String[] args) {
//    System.setProperty("rocketmq.client.logUseSlf4j", "true");
//    System.setProperty("rocketmq.client.logLevel", "warn");
    System.setProperty(ClientLogger.CLIENT_LOG_USESLF4J, "false");
    SpringApplication.run(Application.class, args);
  }


}
