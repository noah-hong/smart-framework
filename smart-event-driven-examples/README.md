# 事件驱动框架

## 前言

``` txt
    事件驱动框架旨在提供一种解决方案，用于简化分布式应用服务信息通讯的复杂性。
    它是一个简易的事件总线，能够帮助开发人员快速方便安全的治理消息，以能够提升应用服务整体可用性、可维护性和可扩展性
    它通过本地消息表的形式实现最终一致性
```

## 特性说明

- [x] 消息端到端顺序性
- [x] 消息持久化&溯源
- [x] 消息订阅管理
- [x] 消息透明发送
- [x] 消息便捷重发
- [x] 消息消费精确一次
- [x] 消息消费日志
- [ ] 消息管理界面


## 架构图

![architecture.png](docs/ReliablePublication.png)

## 支持说明
### 消息队列
- [x] RocketMQ

### 数据库
- [x] MySql

## 快速开始
- 在POM文件中引入如下依赖
``` maven
<dependency>
    <groupId>org.smartframework</groupId>
    <artifactId>smart-event-driven-spring-boot-starter</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

## 如何使用

- 执行smart-spring-boot-autoconfigure项目中资源目录下的mysql-schema.sql文件创建相关表
    - event_sourcing_info：事件信息(OUTBOX)，可根据保存的事件信息回溯更改历史
    - event_tracker_info：事件发布追踪者，创建事件后需要增加对应的TAG记录，事件才能正常发送
    - event_transaction_record：消息队列消费记录，幂等，用于保证消息消费精确一次
    - event_consumed_log：消息消费日志，无论成功还是失败都会记录

- 在application.yml添加相关配置信息，样例配置如下:

```yml
smart:
  event-driven:
    rocketmq:
      name-server: 10.251.64.105:9876;10.251.64.130:9876;10.251.64.80:9876;10.251.64.53:9876
      #name-server: 10.251.76.2:9876;10.251.76.3:9876;10.251.76.4:9876;10.251.76.5:9876
      namespace: noah-ns
      publishable-notifier-topic: ${spring.application.name}-publishable-notifier
      consumer:
        enabled: true
      producer:
        group: ${spring.application.name}-producer-group

```

- 在Spring Boot Application的上添加`@EnableRocketMQEventListener`，表示要开启消息订阅服务

``` Java
@SpringBootApplication
@EnableRocketMQEventListener
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
```

- 消息生产端使用样例

``` java
@RestController
@RequestMapping(value = "/event-driven/examples")
@Api(value = "事件样例")
public class EventDrivenExampleController {

  //事件保存对象
  @Autowired
  private IEventEnvelopStore eventStore;

  /**
   * @return
   * @Publishable 用于消息保存后通知消息中继者按顺序发送所保存的消息
   */
  @ApiOperation("存储事件并发送")
  @PostMapping
  @Transactional
  @Publishable
  public ResponseEntity<Void> storeEvent() {
    //TODO 业务逻辑执行，业务数据入库操作
    //定义消息Demo1Event
    final Demo1Event demo1Event = new Demo1Event();
    demo1Event.setPersistenceId(new DemoId(1));
    eventStore.append(new EventEnvelope(demo1Event));
    //定义消息Demo2Event
    final Demo2Event demo2Event = new Demo2Event();
    demo2Event.setPersistenceId(new DemoId(2));
    eventStore.append(new EventEnvelope(demo2Event));
    return ResponseEntity.ok().build();
  }
}
```

- 消息订阅管理

``` java
/**
 * 声明该类为订阅类，每个订阅类为一个消费者组，每个方法为该消费者组订阅的TOPIC.
 * 并可指定名称空间隔离环境便于调试，也可指定消息队列的服务器地址等
 * @RocketMQEventListener 
 * @ExactlyOnce 开启消息消费精确一次功能
 * @ConsumedLogger 开启消息消费后日志记录功能
 */
@RocketMQEventListener
@ExactlyOnce
@ConsumedLogger
public class Demo2Listener {

  @Autowired
  private DemoService demoService;

  /**
   * 消息订阅方法
   * @param event 订阅的消息
   * @param messageContext 消息上下文
   * @return 消息订阅结果
   */
  public ISubscribeResult subscribe(Demo1Event event, IMessageContext messageContext) {
    System.out.println("DemoService:" + demoService.get());
    //SubscribeResult.FALIURE(new RuntimeException("消息执行失败,于30秒后会重试"));
    //SubscribeResult.FALIURE_NOT_RECONSUME(new RuntimeException("消息执行失败,但不会重试"));
    return SubscribeResult.SUCCESS;
  }

   /**
   * 可订阅由DemoRootEvent派生的所有子类,如Demo1Event,Demo2Event
   */
  public ISubscribeResult subscribe(DemoRootEvent event, IMessageContext messageContext) {
    System.out.println("Demo2Listener:DemoRootEvent:" + JSON.toJSON(event));
    return SubscribeResult.SUCCESS;
  }
}
```