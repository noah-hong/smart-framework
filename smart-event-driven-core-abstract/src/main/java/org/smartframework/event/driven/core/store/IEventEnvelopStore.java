package org.smartframework.event.driven.core.store;

import org.smartframework.event.driven.core.EventEnvelope;

/**
 * @author noah
 * @create 2021-05-24 16:33
 */
public interface IEventEnvelopStore {

  void append(EventEnvelope event);
  void append(EventEnvelope... events);
}
