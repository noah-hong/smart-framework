package org.smartframework.event.driven.core;

import java.io.Serializable;
import org.smartframework.core.IAccount;

/**
 * @author noah
 * @create 2021-04-26 15:08
 */
public class EventSender implements Serializable, IAccount<String> {

  private static final long serialVersionUID = 1L;

  public static EventSender ANONYMOUS = new EventSender();

  public EventSender() {
    this.id = "-1";
    this.name = "anonymous";
    this.scope = "none";
    this.mobilePhone = "";
    this.workerNumber = "";
  }

  public EventSender(IAccount account) {
    this.id = account.getId().toString();
    this.name = account.getName();
    this.scope = account.getScope();
    this.mobilePhone = account.getMobilePhone();
    this.workerNumber = account.getWorkerNumber();
  }

  @Override
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public String getScope() {
    return scope;
  }

  @Override
  public String getWorkerNumber() {
    return workerNumber;
  }

  @Override
  public String getMobilePhone() {
    return mobilePhone;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  private String name;
  private String id;
  private String scope;
  private String mobilePhone;
  private String workerNumber;

  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }

  public void setWorkerNumber(String workerNumber) {
    this.workerNumber = workerNumber;
  }
}
