package org.smartframework.event.driven.core.store;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * @author noah
 * @create 2021-04-26 16:34
 */
public class StoredEvent {

  public byte[] getPayload() {
    return payload;
  }

  public void setPayload(byte[] payload) {
    this.payload = payload;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getSubTopic() {
    return subTopic;
  }

  public void setSubTopic(String subTopic) {
    this.subTopic = subTopic;
  }

  public String getTagName() {
    return tagName;
  }

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }

  public String getEventId() {
    return eventId;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public String getPersistenceId() {
    return persistenceId;
  }

  public void setPersistenceId(String persistenceId) {
    this.persistenceId = persistenceId;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getEventType() {
    return eventType;
  }

  public void setEventType(String eventType) {
    this.eventType = eventType;
  }

  public String getSenderName() {
    return senderName;
  }

  public void setSenderName(String senderName) {
    this.senderName = senderName;
  }

  public String getSenderId() {
    return senderId;
  }

  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }

  public String getSenderScope() {
    return senderScope;
  }

  public void setSenderScope(String senderScope) {
    this.senderScope = senderScope;
  }

  public LocalDateTime getStoredTime() {
    return storedTime;
  }

  public void setStoredTime(LocalDateTime storedTime) {
    this.storedTime = storedTime;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public String getSerializer() {
    return serializer;
  }

  public void setSerializer(String serializer) {
    this.serializer = serializer;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Map<String, String> getProperties() {
    return properties;
  }

  public void setProperties(Map<String, String> properties) {
    this.properties = properties;
  }

  private Long id;
  private byte[] payload;
  private String topic;
  private String subTopic;
  private String tagName;
  private String eventId;
  private String persistenceId;
  private String location;
  private String eventType;
  private String senderName;
  private String senderId;
  private String senderScope;
  private LocalDateTime storedTime;
  private Integer version;
  private Map<String, String> properties;
  private String serializer;

}
