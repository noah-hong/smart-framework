package org.smartframework.event.driven.core;

import org.smartframework.core.IPersistenceId;

/**
 * @author noah
 * @create 2021-07-03 16:22
 */
public abstract class BaseEvent<PersistenceId extends IPersistenceId<?>> implements
    IEvent<PersistenceId> {

  private PersistenceId persistenceId;
  private Integer version = 1;


  @Override
  public Integer getVersion() {
    return version;
  }

  public void setPersistenceId(PersistenceId persistenceId) {
    this.persistenceId = persistenceId;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  @Override
  public PersistenceId getPersistenceId() {
    return persistenceId;
  }


}
