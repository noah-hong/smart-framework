package org.smartframework.event.driven.core;

import org.smartframework.event.driven.core.context.IMessageContext;
import org.smartframework.event.driven.core.context.ISubscribeResult;

/**
 * @author noah
 * @create 2021-04-30 14:37
 */
public interface IEventListenerProxy {

  ISubscribeResult subscribe(IMessageContext context);

  Class<?> getProxyTargetType();
}
