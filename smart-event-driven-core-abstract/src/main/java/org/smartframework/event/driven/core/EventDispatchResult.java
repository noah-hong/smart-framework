package org.smartframework.event.driven.core;

/**
 * @author noah
 * @create 2021-04-26 15:48
 */
public class EventDispatchResult {

  private DispatchStatus dispatchStatus;

  public DispatchStatus getDispatchStatus() {
    return dispatchStatus;
  }

  public void setDispatchStatus(DispatchStatus dispatchStatus) {
    this.dispatchStatus = dispatchStatus;
  }
}
