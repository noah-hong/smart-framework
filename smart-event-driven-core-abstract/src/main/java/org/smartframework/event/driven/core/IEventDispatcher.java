package org.smartframework.event.driven.core;

import org.smartframework.event.driven.core.store.StoredEvent;

/**
 * @author noah
 * @create 2021-04-26 15:43
 */
public interface IEventDispatcher {

  EventDispatchResult dispatch(StoredEvent storedEvent);
}
