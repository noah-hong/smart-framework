package org.smartframework.event.driven.core;

/**
 * @author noah
 * @create 2020-04-19 23:45
 */
public interface IEventPublisher {

  void publish(PublishableEventContext context);

  void publish(String tagName, PublishableEventContext context);
}
