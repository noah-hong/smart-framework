package org.smartframework.event.driven.core.store;

/**
 * @author noah
 * @create 2020-08-25 14:43
 */
@FunctionalInterface
public interface ITransactionExecutor {

  void execute(ITransactionRunnable run);
}
