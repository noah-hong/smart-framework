package org.smartframework.event.driven.core.store;


/**
 * @author noah
 * @create 2020-08-25 15:03
 */
@FunctionalInterface
public interface ITransactionRunnable {

  void run();
}
