package org.smartframework.event.driven.core;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * @author noah
 * @create 2021-04-26 14:43
 */
public class EventEnvelope implements Serializable {

  private static final long serialVersionUID = 1L;

  public String getEventId() {
    return eventId;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public IEvent<?> getEvent() {
    return event;
  }

  public void setEvent(IEvent<?> event) {
    this.event = event;
  }

  public LocalDateTime getOccurredOn() {
    return occurredOn;
  }

  public void setOccurredOn(LocalDateTime occurredOn) {
    this.occurredOn = occurredOn;
  }

  public EventSender getSender() {
    return sender;
  }

  public void setSender(EventSender sender) {
    this.sender = sender;
  }

  private String eventId;
  private IEvent<?> event;
  private LocalDateTime occurredOn;
  private EventSender sender;

  public static Builder builder(IEvent<?> event) {

    return new Builder(event);
  }

  public EventEnvelope(IEvent<?> event) {
    this();
    this.event = event;
  }

  public EventEnvelope() {
    this.occurredOn = LocalDateTime.now();
    this.eventId = UUID.randomUUID().toString();
    this.sender = EventSender.ANONYMOUS;
  }

  public static class Builder {

    private String eventId;
    private LocalDateTime occurredOn;
    private EventSender sender;
    private final IEvent<?> event;

    public Builder(IEvent<?> event) {
      this.event = event;
      this.occurredOn = LocalDateTime.now();
      this.eventId = UUID.randomUUID().toString();
      this.sender = EventSender.ANONYMOUS;
    }

    public Builder eventId(String eventId) {
      this.eventId = eventId;
      return this;
    }

    public Builder occurredOn(LocalDateTime occurredOn) {
      this.occurredOn = occurredOn;
      return this;
    }

    public Builder sender(EventSender sender) {
      this.sender = sender;
      return this;
    }


    public EventEnvelope build() {
      Objects.requireNonNull(event);
      EventEnvelope eventEnvelope = new EventEnvelope();
      eventEnvelope.setEventId(eventId);
      eventEnvelope.setSender(sender);
      eventEnvelope.setOccurredOn(occurredOn);
      eventEnvelope.setEvent(event);
      return eventEnvelope;
    }

  }
}
