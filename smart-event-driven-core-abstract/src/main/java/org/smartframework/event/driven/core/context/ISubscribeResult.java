package org.smartframework.event.driven.core.context;

/**
 * @author noah
 * @create 2021-01-28 15:30
 */
public interface ISubscribeResult {

  ExecutionStatus getExecutionStatus();

  Throwable getException();
}
