package org.smartframework.event.driven.core;

import org.smartframework.event.driven.core.context.IMessageContext;

/**
 * @author noah
 * @create 2021-05-25 13:04
 */
public interface ISubscribeRequest {

  IMessageContext getMessageContext();
  Object getHandler();
}
