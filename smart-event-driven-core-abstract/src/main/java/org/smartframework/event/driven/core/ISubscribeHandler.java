package org.smartframework.event.driven.core;

/**
 * @author noah
 * @create 2021-05-25 11:01
 */
public interface ISubscribeHandler {

  void handle(ISubscribeRequest request, ISubscribeResponse response,
      ISubscribeHandlerChain chain);
}
