package org.smartframework.event.driven.core.store;

import org.smartframework.event.driven.core.EventEnvelope;

/**
 * @author noah
 * @create 2020-07-24 14:08
 */
public interface IEventEnvelopeFormatter {

  byte[] serialize(EventEnvelope target);

  EventEnvelope deserialize(byte[] target);

  String serializer();
}
