package org.smartframework.event.driven.core.context;

/**
 * @author noah
 * @create 2021-01-28 15:20
 */
public enum ExecutionStatus {
  UNKNOWN,
  SUCCESS,
  FALIURE_NOT_RECONSUME,
  FALIURE,
}
