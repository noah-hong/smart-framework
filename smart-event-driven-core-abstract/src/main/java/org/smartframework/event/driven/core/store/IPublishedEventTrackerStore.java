package org.smartframework.event.driven.core.store;

import java.util.List;

/**
 * @author noah
 * @create 2020-05-04 22:55
 */
public interface IPublishedEventTrackerStore {

  List<PublishedEventTracker> getTrackers();

  PublishedEventTracker getTrackerByTagName(String aTagName);

  void track(PublishedEventTracker tracker);
}
