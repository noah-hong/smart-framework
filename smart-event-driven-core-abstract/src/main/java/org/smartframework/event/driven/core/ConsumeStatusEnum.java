package org.smartframework.event.driven.core;

/**
 * @author noah
 * @create 2020-05-04 13:17
 */
public enum ConsumeStatusEnum {
  CONSUMED,
  UNCONSUMED,
  CONSUMING
}
