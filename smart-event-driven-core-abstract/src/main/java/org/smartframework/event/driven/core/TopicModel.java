package org.smartframework.event.driven.core;

/**
 * @author noah
 * @create 2020-09-13 16:18
 */
public class TopicModel {

  public TopicModel(){

  }

  public TopicModel(String topic, String subTopic) {
    this.topic = topic;
    this.subTopic = subTopic;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getSubTopic() {
    return subTopic;
  }

  public void setSubTopic(String subTopic) {
    this.subTopic = subTopic;
  }

  String topic;
  String subTopic;

}