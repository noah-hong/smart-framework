package org.smartframework.event.driven.core;

/**
 * @author noah
 * @create 2020-09-13 16:18
 */
public interface ITopicGenerator {

  TopicModel generate(Class<? extends IEvent> aClass);
}
