package org.smartframework.event.driven.core.store;

/**
 * @author noah
 * @create 2020-05-04 22:56
 */
public class PublishedEventTracker {

  private long mostRecentStoredId;
  private String tagName;

  public long getMostRecentStoredId() {
    return mostRecentStoredId;
  }

  public void setMostRecentStoredId(long mostRecentStoredId) {
    this.mostRecentStoredId = mostRecentStoredId;
  }

  public String getTagName() {
    return tagName;
  }

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }
}
