package org.smartframework.event.driven.core.store;

import java.util.List;

/**
 * @author noah
 * @create 2021-05-23 21:42
 */
public interface IEventQueryStore {

  List<StoredEvent> allStoredEventsSinceByTag(String tagName, Long aStoredId);

  List<StoredEvent> allStoredEventsSinceByTag(String tagName, Long aStoredId, Integer limit);
}
