package org.smartframework.event.driven.core.store;

/**
 * @author noah
 * @create 2021-04-26 16:09
 */
public interface IEventStore  {

  void append(StoredEvent event);
  void appends(StoredEvent... events);
}
