package org.smartframework.event.driven.core;

import java.io.Serializable;
import org.smartframework.core.IPersistenceId;
/**
 * @author noah
 * @create 2021-04-26 14:06
 */
public interface IEvent<PersistenceId extends IPersistenceId<?>> extends Serializable {

  PersistenceId getPersistenceId();

  String getTagName();

  default Integer getVersion() {
    return 1;
  }

}
