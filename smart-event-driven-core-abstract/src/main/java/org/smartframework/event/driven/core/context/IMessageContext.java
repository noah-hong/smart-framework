package org.smartframework.event.driven.core.context;

import org.smartframework.event.driven.core.EventEnvelope;

/**
 * @author noah
 * @create 2020-04-19 16:36
 */
public interface IMessageContext {

  MessageInfo getMessageInfo();

  EventEnvelope getEventEnvelop();
}
