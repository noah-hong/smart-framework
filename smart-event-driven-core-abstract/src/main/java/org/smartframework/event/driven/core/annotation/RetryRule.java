package org.smartframework.event.driven.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author noah
 * @create 2021-10-22 16:00
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RetryRule {

    /**
     * 重试次数，默认3次
     *
     * @return
     */
    int retryCount() default 3;

    /**
     * 重试类型
     * ALWAYS：总是重试，retryCount不生效
     * CUSTOM：自定义重试次数，retryCount生效
     *
     * @return
     */
    RetryType retryType() default RetryType.CUSTOM;

    /**
     * 重试间隔周期，30秒为一个周期
     * 当为1时，30秒后再进行下一次重试；为2时，60秒后再进行一下次重试，以此类推
     *
     * @return
     */
    int retryInterval() default 1;

}
