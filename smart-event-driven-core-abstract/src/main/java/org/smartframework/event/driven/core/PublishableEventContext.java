package org.smartframework.event.driven.core;

import java.util.HashMap;
import java.util.Map;

/**
 * @author noah
 * @create 2021-09-14 13:43
 */
public class PublishableEventContext {

  public static String PROPERTIES_PREFIX = "PublishableEventContext_";

  Map<String, String> properties = new HashMap<>();

  public Map<String, String> getProperties() {
    return properties;
  }

  public String getProperty(String key) {
    return properties.get(PROPERTIES_PREFIX + key);
  }

  public void putProperty(String key, String value) {
    properties.put(PROPERTIES_PREFIX + key, value);
  }

  public void putPropertyWithPrefix(String key, String value) {
    if (key.startsWith(PROPERTIES_PREFIX)) {
      properties.put(key, value);
    }
  }

}
