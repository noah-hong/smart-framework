package org.smartframework.event.driven.core.store;

/**
 * @author noah
 * @create 2021-09-23 16:24
 */
public interface IStoredEventCustomizer {

  void customize(StoredEvent event);
}
