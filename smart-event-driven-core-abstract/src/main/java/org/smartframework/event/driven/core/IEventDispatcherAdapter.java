package org.smartframework.event.driven.core;

import org.smartframework.event.driven.core.EventDispatchResult;
import org.smartframework.event.driven.core.EventEnvelope;
import org.smartframework.event.driven.core.IEventDispatcher;

/**
 * @author noah
 * @create 2022-03-21 13:43
 */
public interface IEventDispatcherAdapter {

  EventDispatchResult dispatch(EventEnvelope eventEnvelope);

}
