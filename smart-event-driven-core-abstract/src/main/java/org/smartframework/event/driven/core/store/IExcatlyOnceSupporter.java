package org.smartframework.event.driven.core.store;

import org.smartframework.event.driven.core.ConsumeStatusEnum;
import org.smartframework.event.driven.core.context.IMessageContext;

/**
 * @author noah
 * @create 2020-04-21 13:48
 */
@FunctionalInterface
public interface IExcatlyOnceSupporter {

  ConsumeStatusEnum support(IMessageContext messageContext);
}
