package org.smartframework.event.driven.core;

/**
 * @author noah
 * @create 2021-04-29 13:56
 */
public interface IEventReceiver {

  void start();
}
