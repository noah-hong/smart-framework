package org.smartframework.event.driven.core;

/**
 * @author noah
 * @create 2021-09-14 13:53
 */
public interface IPublishableEventContextCustomizer {

  void customize(PublishableEventContext publishContext);
}
