package org.smartframework.event.driven.core.store;

import org.smartframework.event.driven.core.context.IMessageContext;
import org.smartframework.event.driven.core.context.ISubscribeResult;

/**
 * @author noah
 * @create 2020-05-04 17:39
 */
public interface IConsumedMessageLogger {

//  void log(IMessageContext messageContext, Throwable ex);

  default void log(IMessageContext messageContext, ISubscribeResult subscribeResult,
      Throwable throwable) {
  }



}
