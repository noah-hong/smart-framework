package org.smartframework.event.driven.core;

import org.smartframework.event.driven.core.context.ISubscribeResult;

/**
 * @author noah
 * @create 2021-05-25 13:06
 */
public interface ISubscribeResponse {
  
  ISubscribeResult getSubscribeResult();

  void setSubscribeResult(ISubscribeResult subscribeResult);
}
