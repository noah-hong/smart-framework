DROP TABLE IF EXISTS `event_sourcing_info`;
-- 事件溯源，保存数据历史信息并回溯显示更改历史
CREATE TABLE `event_sourcing_info`
(
    `id`             BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
    `event_id`       VARCHAR(64)   NOT NULL default '' COMMENT '事件标识',
    `tag_name`       VARCHAR(20)   NOT NULL DEFAULT '' COMMENT '标记名称-一般为表名或者领域名称',
    `persistence_id` VARCHAR(64)   NOT NULL DEFAULT '' COMMENT '持久化编号-每张表的主键或者领域标识',
    `location`       VARCHAR(255)  NOT NULL DEFAULT '' COMMENT '',
    `event_type`     VARCHAR(200)  NOT NULL DEFAULT '' COMMENT '事件类型',
    `event_body`     BLOB          NOT NULL COMMENT '事件实体',
    `event_version`  INT           NOT NULL DEFAULT 0 COMMENT '事件版本',
    `sender`         VARCHAR(20)   NOT NULL DEFAULT 'sys',
    `sender_id`      VARCHAR(64)   NOT NULL DEFAULT '',
    `sender_scope`   VARCHAR(10)   NOT NULL DEFAULT '',
    `stored_time`    TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `serializer`     VARCHAR(20)   NOT NULL DEFAULT 'FAST-JSON',
    `topic`          VARCHAR(255)  NOT NULL DEFAULT '' COMMENT '主题',
    `sub_topic`      VARCHAR(255)  NOT NULL DEFAULT '*' COMMENT '子主题',
    `properties`     VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '用户属性',
    `created_time`   datetime      not null default CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_key` (`event_id`),
    INDEX            `I_TAG_NAME` (`tag_name`),
    INDEX            `I_TAG_NAME_PERSISTENCE_ID` (`tag_name`, `persistence_id`)
) COMMENT ='事件溯源';


DROP TABLE IF EXISTS `event_tracker_info`;
CREATE TABLE `event_tracker_info`
(
    `most_recent_stored_id` BIGINT      NOT NULL DEFAULT 0 COMMENT '事件溯源编号',
--     `most_recent_event_id` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '事件溯源event_id',
    `tag_name`              VARCHAR(50) NOT NULL DEFAULT '标记名称' COMMENT '以标记名称作为事件发送分割标记',
    PRIMARY KEY (`tag_name`)
) COMMENT ='事件发布追踪者';


DROP TABLE IF EXISTS `event_transaction_record`;
CREATE TABLE `event_transaction_record`
(
    `id`              bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `consumer_group`  varchar(128) NOT NULL DEFAULT '' COMMENT '消费者组',
    `message_id`      varchar(255) NOT NULL DEFAULT '' COMMENT '消息编号',
    `message_key`     varchar(255) NULL COMMENT '消息自定义key',
    `topic`           varchar(255) NOT NULL DEFAULT '' COMMENT '主题',
    `sub_topic`       varchar(255) NOT NULL DEFAULT '' COMMENT '子主题',
    `sender`          VARCHAR(20)  NOT NULL DEFAULT 'sys',
    `sender_id`       VARCHAR(64)  NOT NULL DEFAULT '',
    `sender_scope`    VARCHAR(10)  NOT NULL DEFAULT '',
    `reconsume_times` int          not null DEFAULT 0 COMMENT '重消费次数',
    `stored_time`     datetime     not null DEFAULT CURRENT_TIMESTAMP COMMENT '消息存储时间',
    `created_time`    datetime     not null default CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    # UNIQUE KEY `uk_message_id` (`consumer_group`, `message_id`),
    UNIQUE KEY `uk_message_key` (`consumer_group`, `message_key`)
) COMMENT ='消息队列消费记录，幂等';


DROP TABLE IF EXISTS `event_consumed_log`;
CREATE TABLE `event_consumed_log`
(
    `id`              bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `consumer_group`  varchar(128) NOT NULL DEFAULT '' COMMENT '消费者组',
    `message_id`      varchar(255) NOT NULL DEFAULT '' COMMENT '消息编号',
    `message_key`     varchar(255) NULL COMMENT '消息自定义key',
    `topic`           varchar(255) NOT NULL DEFAULT '' COMMENT '主题',
    `sub_topic`       varchar(255) NOT NULL DEFAULT '' COMMENT '子主题',
    `queue_id`        int(11) NOT NULL DEFAULT 0 COMMENT '队列编号',
    `broker`          varchar(255) NOT NULL DEFAULT '' COMMENT '',
    `message_offset`  bigint(20) NOT NULL DEFAULT 0 COMMENT '偏移量',
    `reconsume_times` int          not null DEFAULT 0 COMMENT '重消费次数',
    `stored_time`     datetime     not null DEFAULT CURRENT_TIMESTAMP COMMENT '消息存储时间',
    `born_time_stamp` bigint       not null default 0 COMMENT '',
    `properties`      text null COMMENT '属性集合',
    `payload`         text null COMMENT '消息载荷',
    `exception_stack` text null comment '异常简讯',
    `exec_status`     varchar(50)  NOT NULL default '' comment '执行状态',
    `sender`          VARCHAR(20)  NOT NULL DEFAULT 'sys',
    `sender_id`       VARCHAR(64)  NOT NULL DEFAULT '',
    `sender_scope`    VARCHAR(10)  NOT NULL DEFAULT '',
    `created_time`    datetime     not null default CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) COMMENT ='消息消费日志';



DROP TABLE IF EXISTS `saga_instance_info`;
CREATE TABLE `saga_instance_info`
(
    `saga_id`                varchar(100) NOT NULL COMMENT 'saga编号',
    `saga_name`              varchar(200) NULL DEFAULT '' COMMENT 'saga名称',
    `saga_recover_strategy`  varchar(100) NULL DEFAULT '' COMMENT 'saga恢复策略:BACKWARD,FORWARD',
    `saga_type`              varchar(200) NULL DEFAULT '' COMMENT 'saga类型',
    `saga_status`            varchar(50) NULL DEFAULT '' COMMENT 'saga状态',
    `currently_executing`    int NULL DEFAULT 0 COMMENT '当前执行步骤位置',
    `compensating`           bit NULL DEFAULT 0 COMMENT '是否进入补偿操作',
    `end_state`              bit NULL DEFAULT 0 COMMENT '正常执行/补偿执行是否已结束',
    `executing_exception`    TEXT NULL  COMMENT '执行正常操作时发生的异常信息',
    `compensating_exception` TEXT NULL COMMENT '执行补充时发生的异常信息',
    `last_message`           varchar(500) NULL DEFAULT '' COMMENT '最后反馈的消息',
    `stop_state`             bit NULL DEFAULT 0 COMMENT '执行是否已停止，只有补偿失败时才会停止',
    `currently_step_name`    varchar(100) NULL DEFAULT '' COMMENT '当前执行步骤名称',
    `saga_data`              MEDIUMTEXT NULL COMMENT '执行数据',
    `executing_messages`     TEXT NULL COMMENT '执行正常调用时反馈的消息',
    `compensating_messages`  TEXT NULL COMMENT '执行补偿时反馈的消息',
    `created_time`           datetime NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`saga_id`)
) COMMENT ='saga执行结果信息';


DROP TABLE IF EXISTS `saga_step_info`;
CREATE TABLE `saga_step_info`
(
    `id`                     bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `saga_id`                varchar(100) NOT NULL COMMENT 'saga编号',
    `currently_step_name`    varchar(100) NULL DEFAULT '' COMMENT '当前执行步骤名称',
    `skip`                   bit NULL DEFAULT 0 COMMENT '当前执行步骤是否已跳过',
    `saga_status`            varchar(50) NULL DEFAULT '' COMMENT 'saga状态',
    `currently_executing`    int NULL DEFAULT 0 COMMENT '当前执行步骤位置',
    `compensating`           bit NULL DEFAULT 0 COMMENT '是否进入补偿操作',
    `end_state`              bit NULL DEFAULT 0 COMMENT '正常执行/补偿执行是否已结束',
    `executing_exception`    TEXT NULL COMMENT '执行正常操作时发生的异常信息',
    `compensating_exception` TEXT NULL COMMENT '执行补充时发生的异常信息',
    `last_message`           varchar(500) NULL DEFAULT '' COMMENT '最后反馈的消息',
    `stop_state`             bit NULL DEFAULT 0 COMMENT '执行是否已停止，只有补偿失败时才会停止',
    `saga_data`              MEDIUMTEXT NULL COMMENT '执行数据',
    `executing_messages`     TEXT NULL COMMENT '执行正常调用时反馈的消息',
    `compensating_messages`  TEXT NULL COMMENT '执行补偿时反馈的消息',
    `created_time`           datetime NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) COMMENT ='saga执行步骤信息';
