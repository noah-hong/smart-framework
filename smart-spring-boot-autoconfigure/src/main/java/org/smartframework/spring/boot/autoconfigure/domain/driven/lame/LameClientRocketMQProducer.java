package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.UUID;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.SmartInitializingSingleton;

/**
 * @author noah
 * @create 2021-06-05 17:24
 */
public class LameClientRocketMQProducer implements SmartInitializingSingleton {

  private final static Logger log = LoggerFactory.getLogger(LameClientRocketMQProducer.class);

  public DefaultMQProducer getInstance() {
    return producer;
  }

  private DefaultMQProducer producer;

  public LameClientRocketMQProducer(DefaultMQProducer producer) {
    this.producer = producer;
  }

  public LameClientRocketMQProducer(LameProperties properties) {
    producer = new DefaultMQProducer(properties.getClient().getRocketmq().getProducerGroup());
    producer.setNamesrvAddr(properties.getClient().getRocketmq().getNameServer());
    producer.setNamespace(properties.getNamespace());
    producer.setInstanceName(UUID.randomUUID().toString());
  }

  @Override
  public void afterSingletonsInstantiated() {
    try {
      producer.start();
    } catch (MQClientException e) {
      throw new UndeclaredThrowableException(e);
    }
  }
}
