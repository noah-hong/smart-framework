package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.context.annotation.Import;

/**
 * @author noah
 * @create 2021-06-06 17:33
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
//@Import(LameServiceRegistrar.class)
public @interface EnableLameService {
//  String[] value() default {};
}
