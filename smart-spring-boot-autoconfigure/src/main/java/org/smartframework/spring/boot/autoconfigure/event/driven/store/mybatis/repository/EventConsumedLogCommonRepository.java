package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventConsumedLogDO;

/**
* <p>
 * 消息消费日志 服务类
 * </p>
*
* @author System
* @since 2022-03-24
*/
public interface EventConsumedLogCommonRepository extends IService<EventConsumedLogDO> {

}
