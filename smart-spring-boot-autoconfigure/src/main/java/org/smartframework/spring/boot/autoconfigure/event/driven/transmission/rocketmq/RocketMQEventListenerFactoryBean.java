package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq;

import org.springframework.beans.factory.FactoryBean;

/**
 * @author noah
 * @create 2021-05-08 15:36
 */
@Deprecated
public class RocketMQEventListenerFactoryBean implements FactoryBean<Object> {

  @Override
  public Object getObject() throws Exception {
    return null;
  }

  @Override
  public Class<?> getObjectType() {
    return null;
  }
}
