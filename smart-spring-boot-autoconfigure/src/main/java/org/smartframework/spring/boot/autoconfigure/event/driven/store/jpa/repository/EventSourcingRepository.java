package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository;

import java.util.List;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventSourcingInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 * @author noah
 * @create 2020-05-10 16:23
 */
public interface EventSourcingRepository
    extends JpaSpecificationExecutor<EventSourcingInfo>, CrudRepository<EventSourcingInfo, Long>,
    QueryByExampleExecutor<EventSourcingInfo> {

  List<EventSourcingInfo> findByTagNameEqualsAndIdGreaterThan(String tagName, Long id);

  List<EventSourcingInfo> findByTagNameEqualsAndPersistenceIdEqualsAndIdGreaterThan(
      String tagName, String persistenceId, Long id);

  List<EventSourcingInfo> findByIdGreaterThanAndIdLessThanEqual(Long fromId, Long toId);

  @Query("select id from event_sourcing_info where eventId =:eventId")
  Long findIdByEventId(@Param("eventId") String eventId);
}
