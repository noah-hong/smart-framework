package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support;

import org.smartframework.event.driven.core.store.ITransactionExecutor;
import org.smartframework.event.driven.core.store.ITransactionRunnable;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * @author noah
 * @create 2020-08-25 15:21
 */
public class MybatisTransactionExecutor implements ITransactionExecutor {

  public TransactionTemplate transactionTemplate;

  public MybatisTransactionExecutor(TransactionTemplate transactionTemplate) {
    this.transactionTemplate = transactionTemplate;
  }

  @Override
  public void execute(ITransactionRunnable run) {

    transactionTemplate.execute(
        (status) -> {
          run.run();
          return null;
        });
  }
}
