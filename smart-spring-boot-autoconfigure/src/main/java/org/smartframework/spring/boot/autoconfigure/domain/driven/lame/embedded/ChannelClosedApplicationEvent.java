package org.smartframework.spring.boot.autoconfigure.domain.driven.lame.embedded;

import org.smartframework.domain.driven.lame.woker.ChannelClosedEvent;
import org.springframework.context.ApplicationEvent;

/**
 * @author noah
 * @create 2021-06-23 16:27
 */
public class ChannelClosedApplicationEvent extends ApplicationEvent {

  public ChannelClosedApplicationEvent(ChannelClosedEvent source) {
    super(source);
  }

}
