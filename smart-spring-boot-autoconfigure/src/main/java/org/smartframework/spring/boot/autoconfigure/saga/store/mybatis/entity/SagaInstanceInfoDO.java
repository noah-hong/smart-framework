package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
* <p>
  * saga执行结果信息
  * </p>
*
* @author System
* @since 2022-04-05
*/
@TableName("saga_instance_info")
public class SagaInstanceInfoDO implements Serializable {

private static final long serialVersionUID=1L;

        /**
        * saga编号
        */
                @TableId(value = "saga_id", type = IdType.ASSIGN_ID)
                private String sagaId;

        /**
        * saga名称
        */
        private String sagaName;

        /**
        * saga恢复策略:BACKWARD,FORWARD
        */
        private String sagaRecoverStrategy;

        /**
        * saga类型
        */
        private String sagaType;

        /**
        * saga状态
        */
        private String sagaStatus;

        /**
        * 当前执行步骤位置
        */
        private Integer currentlyExecuting;

        /**
        * 是否进入补偿操作
        */
        private Boolean compensating;

        /**
        * 正常执行/补偿执行是否已结束
        */
        private Boolean endState;

        /**
        * 执行正常操作时发生的异常信息
        */
        private String executingException;

        /**
        * 执行补充时发生的异常信息
        */
        private String compensatingException;

        /**
        * 最后反馈的消息
        */
        private String lastMessage;

        /**
        * 执行是否已停止，只有补偿失败时才会停止
        */
        private Boolean stopState;

        /**
        * 当前执行步骤名称
        */
        private String currentlyStepName;

        /**
        * 执行数据
        */
        private String sagaData;

        /**
        * 执行正常调用时反馈的消息
        */
        private String executingMessages;

        /**
        * 执行补偿时反馈的消息
        */
        private String compensatingMessages;

        private LocalDateTime createdTime;


    public String getSagaId() {
    return sagaId;
    }

        public void setSagaId(String sagaId) {
    this.sagaId = sagaId;
    }

    public String getSagaName() {
    return sagaName;
    }

        public void setSagaName(String sagaName) {
    this.sagaName = sagaName;
    }

    public String getSagaRecoverStrategy() {
    return sagaRecoverStrategy;
    }

        public void setSagaRecoverStrategy(String sagaRecoverStrategy) {
    this.sagaRecoverStrategy = sagaRecoverStrategy;
    }

    public String getSagaType() {
    return sagaType;
    }

        public void setSagaType(String sagaType) {
    this.sagaType = sagaType;
    }

    public String getSagaStatus() {
    return sagaStatus;
    }

        public void setSagaStatus(String sagaStatus) {
    this.sagaStatus = sagaStatus;
    }

    public Integer getCurrentlyExecuting() {
    return currentlyExecuting;
    }

        public void setCurrentlyExecuting(Integer currentlyExecuting) {
    this.currentlyExecuting = currentlyExecuting;
    }

    public Boolean getCompensating() {
    return compensating;
    }

        public void setCompensating(Boolean compensating) {
    this.compensating = compensating;
    }

    public Boolean getEndState() {
    return endState;
    }

        public void setEndState(Boolean endState) {
    this.endState = endState;
    }

    public String getExecutingException() {
    return executingException;
    }

        public void setExecutingException(String executingException) {
    this.executingException = executingException;
    }

    public String getCompensatingException() {
    return compensatingException;
    }

        public void setCompensatingException(String compensatingException) {
    this.compensatingException = compensatingException;
    }

    public String getLastMessage() {
    return lastMessage;
    }

        public void setLastMessage(String lastMessage) {
    this.lastMessage = lastMessage;
    }

    public Boolean getStopState() {
    return stopState;
    }

        public void setStopState(Boolean stopState) {
    this.stopState = stopState;
    }

    public String getCurrentlyStepName() {
    return currentlyStepName;
    }

        public void setCurrentlyStepName(String currentlyStepName) {
    this.currentlyStepName = currentlyStepName;
    }

    public String getSagaData() {
    return sagaData;
    }

        public void setSagaData(String sagaData) {
    this.sagaData = sagaData;
    }

    public String getExecutingMessages() {
    return executingMessages;
    }

        public void setExecutingMessages(String executingMessages) {
    this.executingMessages = executingMessages;
    }

    public String getCompensatingMessages() {
    return compensatingMessages;
    }

        public void setCompensatingMessages(String compensatingMessages) {
    this.compensatingMessages = compensatingMessages;
    }

    public LocalDateTime getCreatedTime() {
    return createdTime;
    }

        public void setCreatedTime(LocalDateTime createdTime) {
    this.createdTime = createdTime;
    }
    
    public static final String SAGA_ID = "saga_id";

    public static final String SAGA_NAME = "saga_name";

    public static final String SAGA_RECOVER_STRATEGY = "saga_recover_strategy";

    public static final String SAGA_TYPE = "saga_type";

    public static final String SAGA_STATUS = "saga_status";

    public static final String CURRENTLY_EXECUTING = "currently_executing";

    public static final String COMPENSATING = "compensating";

    public static final String END_STATE = "end_state";

    public static final String EXECUTING_EXCEPTION = "executing_exception";

    public static final String COMPENSATING_EXCEPTION = "compensating_exception";

    public static final String LAST_MESSAGE = "last_message";

    public static final String STOP_STATE = "stop_state";

    public static final String CURRENTLY_STEP_NAME = "currently_step_name";

    public static final String SAGA_DATA = "saga_data";

    public static final String EXECUTING_MESSAGES = "executing_messages";

    public static final String COMPENSATING_MESSAGES = "compensating_messages";

    public static final String CREATED_TIME = "created_time";

@Override
public String toString() {
return "SagaInstanceInfoDO{" +
        "sagaId=" + sagaId +
        ", sagaName=" + sagaName +
        ", sagaRecoverStrategy=" + sagaRecoverStrategy +
        ", sagaType=" + sagaType +
        ", sagaStatus=" + sagaStatus +
        ", currentlyExecuting=" + currentlyExecuting +
        ", compensating=" + compensating +
        ", endState=" + endState +
        ", executingException=" + executingException +
        ", compensatingException=" + compensatingException +
        ", lastMessage=" + lastMessage +
        ", stopState=" + stopState +
        ", currentlyStepName=" + currentlyStepName +
        ", sagaData=" + sagaData +
        ", executingMessages=" + executingMessages +
        ", compensatingMessages=" + compensatingMessages +
        ", createdTime=" + createdTime +
"}";
}
}
