package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.util.Arrays;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.redisson.config.SentinelServersConfig;
import org.redisson.config.SingleServerConfig;
import org.smartframework.domain.driven.core.communication.IClientSiteProcessResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.lame.responder.LameClientSiteProcessResponderRegistrar;
import org.smartframework.domain.driven.lame.responder.LameClientSiteResponderRegistrar;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.LameProperties.RedissonNodeType;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.LameProperties.RedissonResonderConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

/**
 * @author noah
 * @create 2021-06-03 13:41
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(LameProperties.class)
@ConditionalOnProperty(prefix = "smart.domain-driven.lame.responder.redisson", value = "nodes", matchIfMissing = false)
public class LameResponderAutoConfigure {

  @Autowired
  public LameProperties properties;
  @Autowired
  public ApplicationContext applicationContext;

  @Bean
  @ConditionalOnMissingBean(LameResponderRedissonClient.class)
  public LameResponderRedissonClient lameResponderRedissonClient() throws Exception {

    final RedissonResonderConfiguration redissonProperties = properties.getResponder().getRedisson();
    String password = redissonProperties.getPassword();
    String[] nodes = StringUtils.tokenizeToStringArray(
        redissonProperties
            .getNodes(), ",;");

    Config configuration = new Config();
    ObjectMapper om = new ObjectMapper();
    om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    om.registerModule(new JavaTimeModule());
    configuration.setCodec(new JsonJacksonCodec(om));

    if (redissonProperties.getNodeType() == RedissonNodeType.Single) {
      final SingleServerConfig singleServerConfig = configuration
          .useSingleServer()
          .setAddress(nodes[0]);
      if (!StringUtils.isEmpty(password)) {
        singleServerConfig.setPassword(password);
      }
    } else if (redissonProperties.getNodeType() == RedissonNodeType.Cluster) {
      ClusterServersConfig clusterServersConfig =
          configuration
              .useClusterServers()
              .addNodeAddress(nodes)
              .setScanInterval(2000);
      if (!StringUtils.isEmpty(password)) {
        clusterServersConfig.setPassword(password);
      }
    } else if (redissonProperties.getNodeType() == RedissonNodeType.Sentinel) {
      SentinelServersConfig sentinelServersConfig = configuration
          .useSentinelServers()
          .setCheckSentinelsList(false)
          .setMasterName(redissonProperties.getMasterName());
      sentinelServersConfig.setSentinelAddresses(Arrays.asList(nodes));
      if (!StringUtils.isEmpty(password)) {
        sentinelServersConfig.setPassword(password);
      }
    }

//    final ArrayList<String> strings = new ArrayList<>();
//    strings.add("redis://10.0.38.155:26379");
//    strings.add("redis://10.0.38.156:26380");
//    strings.add("redis://10.0.38.157:26381");
//    configuration
//        .useSentinelServers()
////        .setPassword("123456")
//        .setMasterName("redis1")
//        .setCheckSentinelsList(false)
//        .setSentinelAddresses(strings);

//    configuration.useSingleServer().setPassword("123456")
//        .setAddress(String.format("redis://%s", "10.0.91.249:7001"));

    return new LameResponderRedissonClient(configuration);
  }


  @Bean
  @ConditionalOnMissingBean(IClientSiteProcessResponderRegistrar.class)
  public IClientSiteProcessResponderRegistrar clientSiteProcessResponderRegistrar() {
    return new LameClientSiteProcessResponderRegistrar();
  }

  @Bean
  @ConditionalOnMissingBean(IClientSiteResponderRegistrar.class)
  public IClientSiteResponderRegistrar clientSiteResponderRegistrar(
      LameResponderRedissonClient rpcWorkerRedissonClient) {
    return new LameClientSiteResponderRegistrar(rpcWorkerRedissonClient.getInstance());
  }

}
