package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded.EmbeddedRocketMQProducer;

/**
 * @author noah
 * @create 2021-05-06 13:42
 */
public class RocketMQProducerFactory {

//  public DefaultMQProducer getRocketMQProducer() {
//    return defaultMQProducer;
//  }
//
//  private final DefaultMQProducer defaultMQProducer;
//
//  public EventDrivenRocketMQProducerFactory(DefaultMQProducer defaultMQProducer) {
//    this.defaultMQProducer = defaultMQProducer;
//  }

  public static DefaultMQProducer create(EventDrivenRocketMQProperties properties) {
    return new EmbeddedRocketMQProducer(properties).getInstance();
  }


}
