package org.smartframework.spring.boot.autoconfigure;

import com.esotericsoftware.kryo.serializers.TimeSerializers.LocalDateSerializer;
import com.esotericsoftware.kryo.serializers.TimeSerializers.LocalDateTimeSerializer;
import com.esotericsoftware.kryo.serializers.TimeSerializers.LocalTimeSerializer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javax.xml.datatype.XMLGregorianCalendar;
import org.redisson.codec.JsonJacksonCodec.ThrowableMixIn;
import org.smartframework.core.IAccount;
import org.smartframework.domain.driven.core.Commander;

/**
 * @author noah
 * @create 2021-07-12 17:17
 */
public class TestApp {

  public static void main(String[] args) {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.setSerializationInclusion(Include.NON_NULL);
    objectMapper.setVisibility(objectMapper.getSerializationConfig()
        .getDefaultVisibilityChecker()
        .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
        .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
        .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
        .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
    objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    objectMapper.enable(Feature.WRITE_BIGDECIMAL_AS_PLAIN);
    objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    objectMapper.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
    objectMapper.addMixIn(Throwable.class, ThrowableMixIn.class);
    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    objectMapper.registerModule(new JavaTimeModule());
    TypeResolverBuilder<?> mapTyper = new DefaultTypeResolverBuilder(DefaultTyping.NON_FINAL) {
      @Override
      public boolean useForType(JavaType t) {
        switch (_appliesFor) {
          case NON_CONCRETE_AND_ARRAYS:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
            // fall through
          case OBJECT_AND_NON_CONCRETE:
            return (t.getRawClass() == Object.class) || !t.isConcrete();
          case NON_FINAL:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
            // to fix problem with wrong long to int conversion
            if (t.getRawClass() == Long.class) {
              return true;
            }
            if (t.getRawClass() == XMLGregorianCalendar.class) {
              return false;
            }
            return !t.isFinal(); // includes Object.class
          default:
            // case JAVA_LANG_OBJECT:
            return t.getRawClass() == Object.class;
        }
      }
    };
    mapTyper.init(JsonTypeInfo.Id.CLASS, null);
    mapTyper.inclusion(JsonTypeInfo.As.PROPERTY);
    objectMapper.setDefaultTyping(mapTyper);

    Commander account = new Commander();
    account.setId("test-id");
    account.setName("test-name");
    account.setScope("test-scope");
    Model<Commander> mo = new Model<Commander>(account);
    mo.setDate(LocalDateTime.now());
//    mo.setAccount(account);
    try {
      final byte[] s = objectMapper.writeValueAsBytes(mo);
      final IModel<Commander> model = objectMapper.readValue(s, IModel.class);
      System.out.println(model.getAccount().getId());
      System.out.println(model.getAccount().getScope());
      System.out.println(model.getDate().getYear());
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }


  }

  interface IModel<Account extends IAccount<?>> {

    Account getAccount();

    LocalDateTime getDate();
  }

  static class Model<Account extends IAccount<?>> implements IModel<Account> {

    private Model() {
    }

    public Model(Account account) {
      this.account = account;
    }

    @Override
    public Account getAccount() {
      return account;
    }

    Account account;

    @Override
    public LocalDateTime getDate() {
      return date;
    }

    public void setDate(LocalDateTime date) {
      this.date = date;
    }

    LocalDateTime date;
  }
}
