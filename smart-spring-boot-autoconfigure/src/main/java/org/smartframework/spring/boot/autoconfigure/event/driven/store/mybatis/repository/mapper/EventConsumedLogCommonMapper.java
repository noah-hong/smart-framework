package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventConsumedLogDO;

/**
* <p>
 * 消息消费日志 Mapper 接口
 * </p>
*
* @author System
* @since 2022-03-24
*/
 @Mapper
public interface EventConsumedLogCommonMapper extends BaseMapper<EventConsumedLogDO> {

}
