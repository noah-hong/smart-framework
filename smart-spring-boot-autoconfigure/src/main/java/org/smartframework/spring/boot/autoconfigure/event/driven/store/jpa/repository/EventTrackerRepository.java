package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository;

import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventTrackerInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 * @author noah
 * @create 2020-05-10 16:38
 */
public interface EventTrackerRepository
    extends CrudRepository<EventTrackerInfo, Long>, QueryByExampleExecutor<EventTrackerInfo> {
  EventTrackerInfo findByTagName(String tagName);
}
