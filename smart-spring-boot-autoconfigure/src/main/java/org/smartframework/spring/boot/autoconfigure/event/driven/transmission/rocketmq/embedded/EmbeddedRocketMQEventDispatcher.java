package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.smartframework.event.driven.rocketmq.RocketMQEventDispatcher;
import org.springframework.beans.factory.InitializingBean;

/**
 * @author noah
 * @create 2021-05-30 14:24
 */
public class EmbeddedRocketMQEventDispatcher extends RocketMQEventDispatcher implements
    InitializingBean {

  public EmbeddedRocketMQEventDispatcher(
      DefaultMQProducer producer) {
    super(producer);
  }


  @Override
  public void afterPropertiesSet() throws Exception {
    super.producer.start();
  }
}
