package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventTransactionRecordDO;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventTransactionRecordCommonRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.mapper.EventTransactionRecordCommonMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
* <p>
 * 消息队列消费记录，幂等 服务实现类
 * </p>
*
* @author System
* @since 2022-03-24
*/
@Repository
@Primary
public class EventTransactionRecordCommonRepositoryImpl extends ServiceImpl<EventTransactionRecordCommonMapper, EventTransactionRecordDO> implements
    EventTransactionRecordCommonRepository {

}
