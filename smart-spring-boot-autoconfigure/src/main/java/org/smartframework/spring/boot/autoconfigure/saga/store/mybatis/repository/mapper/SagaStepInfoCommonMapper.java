package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.entity.SagaStepInfoDO;

/**
* <p>
 * saga执行步骤信息 Mapper 接口
 * </p>
*
* @author System
* @since 2022-04-05
*/
 @Mapper
public interface SagaStepInfoCommonMapper extends BaseMapper<SagaStepInfoDO> {

}
