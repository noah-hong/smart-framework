package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author noah
 * @create 2020-05-10 15:56
 */
@Entity(name = "event_sourcing_info")
public class EventSourcingInfo implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "event_id")
  private String eventId;
  @Column(name = "tag_name")
  private String tagName;
  @Column(name = "persistence_id")
  private String persistenceId;
  @Column(name = "location")
  private String location;
  @Column(name = "event_type")
  private String eventType;
  @Column(name = "event_Body")
  private byte[] eventBody;
  @Column(name = "event_version")
  private Integer eventVersion;
  @Column(name = "sender")
  private String sender;
  @Column(name = "sender_id")
  private String senderId;
  @Column(name = "sender_scope")
  private String senderScope;
  @Column(name = "stored_time")
  private LocalDateTime storedTime;
  @Column(name = "serializer")
  private String serializer;
  @Column(name = "topic")
  private String topic;
  @Column(name = "sub_topic")
  private String subTopic;

  @Column(name = "properties")
  private String properties;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEventId() {
    return eventId;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public String getTagName() {
    return tagName;
  }

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }

  public String getPersistenceId() {
    return persistenceId;
  }

  public void setPersistenceId(String persistenceId) {
    this.persistenceId = persistenceId;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getEventType() {
    return eventType;
  }

  public void setEventType(String eventType) {
    this.eventType = eventType;
  }

  public byte[] getEventBody() {
    return eventBody;
  }

  public void setEventBody(byte[] eventBody) {
    this.eventBody = eventBody;
  }

  public Integer getEventVersion() {
    return eventVersion;
  }

  public void setEventVersion(Integer eventVersion) {
    this.eventVersion = eventVersion;
  }

  public String getSender() {
    return sender;
  }

  public void setSender(String sender) {
    this.sender = sender;
  }

  public String getSenderId() {
    return senderId;
  }

  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }

  public String getSenderScope() {
    return senderScope;
  }

  public void setSenderScope(String senderScope) {
    this.senderScope = senderScope;
  }

  public LocalDateTime getStoredTime() {
    return storedTime;
  }

  public void setStoredTime(LocalDateTime storedTime) {
    this.storedTime = storedTime;
  }

  public String getSerializer() {
    return serializer;
  }

  public void setSerializer(String serializer) {
    this.serializer = serializer;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getSubTopic() {
    return subTopic;
  }

  public void setSubTopic(String subTopic) {
    this.subTopic = subTopic;
  }

  public String getProperties() {
    return properties;
  }

  public void setProperties(String properties) {
    this.properties = properties;
  }
}
