package org.smartframework.spring.boot.autoconfigure.saga;

import java.util.List;
import org.smartframework.saga.core.Saga;
import org.smartframework.saga.core.SagaManagerFactory;
import org.smartframework.saga.core.SagaInstanceFactory;
import org.smartframework.saga.core.SagaInstanceRepository;
import org.smartframework.saga.core.SagaStepCompletedObserver;
import org.smartframework.spring.boot.autoconfigure.saga.core.SpringSagaInstanceFactory;
import org.smartframework.spring.boot.autoconfigure.saga.core.SpringSagaManagerFactory;
import org.smartframework.spring.boot.autoconfigure.saga.store.jpa.JpaSagaStoreAutoConfiguration;
import org.smartframework.spring.boot.autoconfigure.saga.web.SagaController;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author noah
 * @create 2021-10-13 17:59
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(Saga.class)
public class SagaAutoConfiguration implements InitializingBean, ApplicationContextAware {


  @Autowired(required = false)
  List<SagaStepCompletedObserver> observers;

  @Bean
  public SagaManagerFactory sagaManagerFactory(SagaInstanceRepository sagaInstanceRepository) {
    return new SpringSagaManagerFactory(sagaInstanceRepository, observers);
  }

  @Bean
  public SagaInstanceFactory sagaInstanceFactory(SagaManagerFactory sagaManagerFactory) {
    return new SagaInstanceFactory(sagaManagerFactory);
  }

  @Bean
  public SpringSagaInstanceFactory springSagaInstanceFactory(
      SagaInstanceFactory sagaInstanceFactory) {
    return new SpringSagaInstanceFactory(sagaInstanceFactory);
  }

  @Bean
  @ConditionalOnProperty(prefix = "smart.saga.web", value = "enable", havingValue = "true", matchIfMissing = false)
  public SagaController sagaController() {
    return new SagaController();
  }


  @Override
  public void afterPropertiesSet() throws Exception {

//    final Map<String, SagaDsl> sagaDslMap = applicationContext.getBeansOfType(SagaDsl.class);
//    HashSet<String> sagaCodes = new HashSet<String>();
//    for (SagaDsl sagaDsl : sagaDslMap.values()) {
//      if (sagaCodes.contains(sagaDsl.getSagaCode())) {
//        throw new RuntimeException("saga code:" + sagaDsl.getSagaCode() + "已存在");
//      } else {
//        sagaCodes.add(sagaDsl.getSagaCode());
//      }
//
//    }

  }

  ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
}
