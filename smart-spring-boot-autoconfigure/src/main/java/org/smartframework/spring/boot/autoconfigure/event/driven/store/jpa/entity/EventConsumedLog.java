package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author noah
 * @create 2021-01-28 14:26
 */
@Entity(name = "event_consumed_log")
public class EventConsumedLog {

  private static final long serialVersionUID = 1L;

  /**
   * 主键
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "consumer_group")
  private String consumerGroup;
  @Column(name = "message_id")
  private String messageId;
  @Column(name = "message_key")
  private String messageKey;
  @Column(name = "topic")
  private String topic;
  @Column(name = "sub_topic")
  private String subTopic;
  @Column(name = "reconsume_times")
  private Integer reconsumeTimes;
  @Column(name = "stored_time")
  private LocalDateTime storedTime;
  @Column(name = "exec_status")
  private String execStatus;
  @Column(name = "message_offset")
  private Long messageOffset;
  @Column(name = "properties")
  private String properties;
  @Column(name = "payload")
  private String payload;
  @Column(name = "broker")
  private String broker;
  @Column(name = "queue_id")
  private int queueId;
  @Column(name = "born_time_stamp")
  private Long bornTimeStamp;
  @Column(name = "sender")
  private String sender;
  @Column(name = "sender_id")
  private String senderId;
  @Column(name = "sender_scope")
  private String senderScope;
  @Column(name = "exception_stack")
  private String exceptionStack;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getConsumerGroup() {
    return consumerGroup;
  }

  public void setConsumerGroup(String consumerGroup) {
    this.consumerGroup = consumerGroup;
  }

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }

  public String getMessageKey() {
    return messageKey;
  }

  public void setMessageKey(String messageKey) {
    this.messageKey = messageKey;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getSubTopic() {
    return subTopic;
  }

  public void setSubTopic(String subTopic) {
    this.subTopic = subTopic;
  }

  public Integer getReconsumeTimes() {
    return reconsumeTimes;
  }

  public void setReconsumeTimes(Integer reconsumeTimes) {
    this.reconsumeTimes = reconsumeTimes;
  }

  public LocalDateTime getStoredTime() {
    return storedTime;
  }

  public void setStoredTime(LocalDateTime storedTime) {
    this.storedTime = storedTime;
  }

  public String getExecStatus() {
    return execStatus;
  }

  public void setExecStatus(String execStatus) {
    this.execStatus = execStatus;
  }

  public Long getMessageOffset() {
    return messageOffset;
  }

  public void setMessageOffset(Long messageOffset) {
    this.messageOffset = messageOffset;
  }

  public String getProperties() {
    return properties;
  }

  public void setProperties(String properties) {
    this.properties = properties;
  }

  public String getPayload() {
    return payload;
  }

  public void setPayload(String payload) {
    this.payload = payload;
  }

  public String getBroker() {
    return broker;
  }

  public void setBroker(String broker) {
    this.broker = broker;
  }

  public int getQueueId() {
    return queueId;
  }

  public void setQueueId(int queueId) {
    this.queueId = queueId;
  }

  public Long getBornTimeStamp() {
    return bornTimeStamp;
  }

  public void setBornTimeStamp(Long bornTimeStamp) {
    this.bornTimeStamp = bornTimeStamp;
  }

  public String getSender() {
    return sender;
  }

  public void setSender(String sender) {
    this.sender = sender;
  }

  public String getSenderId() {
    return senderId;
  }

  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }

  public String getSenderScope() {
    return senderScope;
  }

  public void setSenderScope(String senderScope) {
    this.senderScope = senderScope;
  }

  public String getExceptionStack() {
    return exceptionStack;
  }

  public void setExceptionStack(String exceptionStack) {
    this.exceptionStack = exceptionStack;
  }
}
