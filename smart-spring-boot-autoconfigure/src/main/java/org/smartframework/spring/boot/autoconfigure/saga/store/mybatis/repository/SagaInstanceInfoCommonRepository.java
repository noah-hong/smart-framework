package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.entity.SagaInstanceInfoDO;

/**
* <p>
 * saga执行结果信息 服务类
 * </p>
*
* @author System
* @since 2022-04-05
*/
public interface SagaInstanceInfoCommonRepository extends IService<SagaInstanceInfoDO> {

}
