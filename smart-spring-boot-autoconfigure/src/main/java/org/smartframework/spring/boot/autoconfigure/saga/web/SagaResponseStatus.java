package org.smartframework.spring.boot.autoconfigure.saga.web;

/**
 * @author noah
 * @create 2021-03-23 16:53
 */
public enum SagaResponseStatus {

  SUCCESS("200000", "成功"),

  // =================400请求异常=====================
  ERROR_REQUEST_EXCEPTION("400000", "请求异常"),
  ERROR_BIZ_EXCEPTION("400001", "业务逻辑错误"),
  ERROR_PARAMETER_VALIDATION_EXCEPTION("400002", "参数错误"),
  ERROR_HYSTRIX_EXCEPTION("400003", "Hystrix异常"),
  ERROR_QUERY_NON_DATA_EXCEPTION("400004", "查询无记录"),
  ERROR_DATA_HAD_EXISTS_EXCEPTION("400005", "数据已存在"),

  // =================401认证异常=====================
  ERROR_AUTHENTICATION_EXCEPTION("401000", "用户认证信息异常，请重新登录后再试"),
  ERROR_AUTHENTICATION_TOKEN_EXPIRED_EXCEPTION("401002", "用户认证信息已过期，请重新登录后再试"),
  ERROR_UNAUTHENTICATION_EXCEPTION("401003", "用户尚未登录，请登录后再试"),

  ERROR_INTERNAL_SERVER_EXCEPTION("500000", "服务内部错误"),
  ;

  private final String code;
  private final String message;

  SagaResponseStatus(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public <T> SagaApiResponseEnvelope<T> toEnvelope() {
    return SagaApiResponseEnvelope.status(this).build();
  }

  public <T> SagaApiResponseEnvelope<T> toEnvelope(T response) {
    return SagaApiResponseEnvelope.status(this).build(response);
  }

  public String getCode() {
    return this.code;
  }

  public String getMessage() {
    return this.message;
  }

}
