package org.smartframework.spring.boot.autoconfigure.domain.driven.lame.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author noah
 * @create 2021-06-02 14:13
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface LameClient {

  String name() default "";

  String qualifier() default "";

  boolean primary() default true;
}
