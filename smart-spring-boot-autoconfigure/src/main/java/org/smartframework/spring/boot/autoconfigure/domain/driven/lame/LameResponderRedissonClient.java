package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;


import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 * @author noah
 * @create 2021-02-22 16:47
 */
public class LameResponderRedissonClient {

  public RedissonClient getInstance() {
    return redissonClient;
  }

  private final RedissonClient redissonClient;

  public LameResponderRedissonClient(Config config) {
    redissonClient = Redisson.create(config);
  }

}
