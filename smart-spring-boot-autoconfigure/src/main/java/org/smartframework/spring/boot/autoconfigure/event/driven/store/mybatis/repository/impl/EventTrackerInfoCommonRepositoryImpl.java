package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventTrackerInfoDO;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventTrackerInfoCommonRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.mapper.EventTrackerInfoCommonMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
* <p>
 * 事件发布追踪者 服务实现类
 * </p>
*
* @author System
* @since 2022-03-24
*/
@Repository
@Primary
public class EventTrackerInfoCommonRepositoryImpl extends ServiceImpl<EventTrackerInfoCommonMapper, EventTrackerInfoDO> implements
    EventTrackerInfoCommonRepository {

}
