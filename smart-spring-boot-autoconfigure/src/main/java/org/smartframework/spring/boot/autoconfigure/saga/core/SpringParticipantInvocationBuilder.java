package org.smartframework.spring.boot.autoconfigure.saga.core;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import org.smartframework.saga.core.DefaultParticipantInvocation;
import org.smartframework.saga.core.DefaultParticipantInvocationBuilder;
import org.smartframework.saga.core.DefaultSagaDefinitionBuilder;
import org.smartframework.saga.core.DefaultSagaStep;
import org.smartframework.saga.core.DefaultSagaStepBuilder;
import org.smartframework.saga.core.ParticipantInvocation;
import org.smartframework.saga.core.SagaCommandResult;
import org.smartframework.saga.core.SagaDefinition;
import org.smartframework.saga.core.SagaRecoverStrategy;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author noah
 * @create 2022-04-14 17:59
 */
public class SpringParticipantInvocationBuilder<Data> {

  ApplicationContext applicationContext;

  private final DefaultSagaDefinitionBuilder<Data> sagaDefinitionBuilder;
  private final SpringSagaStepBuilder<Data> stepBuilder;

  private Optional<ParticipantInvocation<Data>> action = Optional.empty();
  private Optional<ParticipantInvocation<Data>> compensation = Optional.empty();


  public SpringParticipantInvocationBuilder(SpringSagaStepBuilder<Data> stepBuilder,
      DefaultSagaDefinitionBuilder<Data> parent, ApplicationContext applicationContext) {
    this.sagaDefinitionBuilder = parent;
    this.stepBuilder = stepBuilder;
    this.applicationContext = applicationContext;
  }


  public SpringParticipantInvocationBuilder<Data> withCompensation(
      Class<? extends ParticipantInvocation<Data>> clazz) {
    final ParticipantInvocation<Data> compensationPredicate = applicationContext.getBean(clazz);
    this.compensation = Optional
        .of(compensationPredicate);
    return this;
  }

  public SpringParticipantInvocationBuilder<Data> invokeParticipant(
      Class<? extends ParticipantInvocation<Data>> clazz) {
    final ParticipantInvocation<Data> participantInvocation = applicationContext.getBean(clazz);
    this.action = Optional.of(participantInvocation);
    return this;
  }
  public SpringParticipantInvocationBuilder<Data> withCompensation(Optional<ParticipantInvocation<Data>> compensation) {
    this.compensation = compensation;
    return this;
  }
  public SpringParticipantInvocationBuilder<Data> invokeParticipant(Optional<ParticipantInvocation<Data>> action) {
    this.action = action;
    return this;
  }

  public SpringParticipantInvocationBuilder<Data> withCompensation(
      Function<Data, SagaCommandResult> compensation) {
    this.compensation = Optional
        .of(new DefaultParticipantInvocation<>(Optional.empty(), compensation));
    return this;
  }

  public SpringParticipantInvocationBuilder<Data> withCompensation(
      Predicate<Data> compensationPredicate,
      Function<Data, SagaCommandResult> compensation) {
    this.compensation = Optional
        .of(new DefaultParticipantInvocation<>(Optional.of(compensationPredicate), compensation));
    return this;
  }

  public SpringParticipantInvocationBuilder<Data> invokeParticipant(
      Function<Data, SagaCommandResult> action) {
    this.action = Optional.of(new DefaultParticipantInvocation<>(Optional.empty(), action));
    return this;
  }


  public SpringParticipantInvocationBuilder<Data> invokeParticipant(
      Predicate<Data> invokeParticipantPredicate,
      Function<Data, SagaCommandResult> compensation) {
    this.action = Optional
        .of(new DefaultParticipantInvocation<>(Optional.of(invokeParticipantPredicate),
            compensation));
    return this;
  }

//    public DefaultSagaStepBuilder<Data> step() {
//        addStep();
//        return new DefaultSagaStepBuilder<>(sagaDefinitionBuilder);
//    }

  public SpringSagaStepBuilder<Data> step(String setpName) {
    addStep();
    return new SpringSagaStepBuilder<>(setpName, sagaDefinitionBuilder, applicationContext);
  }

  private void addStep() {
    sagaDefinitionBuilder
        .addStep(new DefaultSagaStep<>(stepBuilder.getName(), action, compensation));
  }

  public SagaDefinition<Data> build() {
    addStep();
    return sagaDefinitionBuilder.build(SagaRecoverStrategy.BACKWARD);
  }

  public SagaDefinition<Data> build(SagaRecoverStrategy recoverStrategy) {
    addStep();
    return sagaDefinitionBuilder.build(recoverStrategy);
  }


}
