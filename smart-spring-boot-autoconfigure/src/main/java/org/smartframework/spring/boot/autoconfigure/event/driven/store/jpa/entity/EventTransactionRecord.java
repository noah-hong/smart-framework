package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author noah
 * @create 2020-05-21 18:13
 */
@Entity(name = "event_transaction_record")
public class EventTransactionRecord {

  private static final long serialVersionUID = 1L;
  /**
   * 主键
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(name = "consumer_group")
  private String consumerGroup;
  @Column(name = "message_id")
  private String messageId;
  @Column(name = "message_key")
  private String messageKey;
  @Column(name = "topic")
  private String topic;
  @Column(name = "sub_topic")
  private String subTopic;
  @Column(name = "reconsume_times")
  private Integer reconsumeTimes;
  @Column(name = "stored_time")
  private LocalDateTime storedTime;
  @Column(name = "sender")
  private String sender;
  @Column(name = "sender_id")
  private String senderId;
  @Column(name = "sender_scope")
  private String senderScope;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getConsumerGroup() {
    return consumerGroup;
  }

  public void setConsumerGroup(String consumerGroup) {
    this.consumerGroup = consumerGroup;
  }

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }

  public String getMessageKey() {
    return messageKey;
  }

  public void setMessageKey(String messageKey) {
    this.messageKey = messageKey;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getSubTopic() {
    return subTopic;
  }

  public void setSubTopic(String subTopic) {
    this.subTopic = subTopic;
  }

  public Integer getReconsumeTimes() {
    return reconsumeTimes;
  }

  public void setReconsumeTimes(Integer reconsumeTimes) {
    this.reconsumeTimes = reconsumeTimes;
  }

  public LocalDateTime getStoredTime() {
    return storedTime;
  }

  public void setStoredTime(LocalDateTime storedTime) {
    this.storedTime = storedTime;
  }

  public String getSender() {
    return sender;
  }

  public void setSender(String sender) {
    this.sender = sender;
  }

  public String getSenderId() {
    return senderId;
  }

  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }

  public String getSenderScope() {
    return senderScope;
  }

  public void setSenderScope(String senderScope) {
    this.senderScope = senderScope;
  }

}
