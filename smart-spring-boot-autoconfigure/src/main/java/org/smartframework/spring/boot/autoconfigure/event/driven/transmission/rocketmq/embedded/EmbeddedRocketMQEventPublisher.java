package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded;

import org.smartframework.event.driven.core.IEventDispatcher;
import org.smartframework.event.driven.core.PublishableEventContext;
import org.smartframework.event.driven.core.store.IEventQueryStore;
import org.smartframework.event.driven.core.store.IPublishedEventTrackerStore;
import org.smartframework.event.driven.rocketmq.RocketMQEventPublisher;
import org.smartframework.event.driven.rocketmq.exceptions.PublishingEventException;
import org.springframework.retry.RetryException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

/**
 * @author noah
 * @create 2021-05-28 17:21
 */
public class EmbeddedRocketMQEventPublisher extends RocketMQEventPublisher {

  private static final long TIME_UNIT = 1000;
  // 重试次数
  private static final int MAX_ATTEMPTS = 3;

  public EmbeddedRocketMQEventPublisher(
      IEventQueryStore eventQueryStore,
      IPublishedEventTrackerStore trackerStore,
      IEventDispatcher dispatcher) {
    super(eventQueryStore, trackerStore, dispatcher);
  }

  @Override
  @Retryable(
      value = {RetryException.class, PublishingEventException.class},
      maxAttempts = MAX_ATTEMPTS,
      backoff = @Backoff(delay = TIME_UNIT, maxDelay = TIME_UNIT * 6, multiplier = 2))
  public synchronized void publish(PublishableEventContext context) {
    super.publish(context);
  }


  @Override
  @Retryable(
      value = {RetryException.class, PublishingEventException.class},
      maxAttempts = MAX_ATTEMPTS,
      backoff = @Backoff(delay = TIME_UNIT, maxDelay = TIME_UNIT * 6, multiplier = 2))
  public void publish(String tagName, PublishableEventContext context) {
    super.publish(tagName, context);
  }
}
