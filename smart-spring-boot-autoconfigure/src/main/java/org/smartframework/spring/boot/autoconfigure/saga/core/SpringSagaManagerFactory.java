package org.smartframework.spring.boot.autoconfigure.saga.core;

import java.util.List;
import org.smartframework.saga.core.SagaManagerFactory;
import org.smartframework.saga.core.Saga;
import org.smartframework.saga.core.SagaInstanceRepository;
import org.smartframework.saga.core.SagaManager;
import org.smartframework.saga.core.SagaStepCompletedObserver;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author noah
 * @create 2021-10-18 14:04
 */
public class SpringSagaManagerFactory extends SagaManagerFactory implements
    ApplicationContextAware {

  public SpringSagaManagerFactory(
      SagaInstanceRepository sagaInstanceRepository,
      List<SagaStepCompletedObserver> observers) {
    super(sagaInstanceRepository, observers);
  }

  @Override
  public <SagaData> SagaManager<SagaData> create(Saga<SagaData> saga) {
    return new SpringSagaManager<>(saga, getSagaInstanceRepository(), getObservers());
  }

  @Override
  public <SagaData> SagaManager<SagaData> create() {
    return new SpringSagaManager<>(applicationContext, getSagaInstanceRepository(), getObservers());
  }

  private ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
}
