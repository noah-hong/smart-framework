package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
* <p>
  * 事件发布追踪者
  * </p>
*
* @author System
* @since 2022-03-24
*/
@TableName("event_tracker_info")
public class EventTrackerInfoDO implements Serializable {

private static final long serialVersionUID=1L;

        /**
        * 事件溯源编号
        */
        private Long mostRecentStoredId;

        /**
        * 以标记名称作为事件发送分割标记
        */
                @TableId(value = "tag_name", type = IdType.ASSIGN_ID)
                private String tagName;


    public Long getMostRecentStoredId() {
    return mostRecentStoredId;
    }

        public void setMostRecentStoredId(Long mostRecentStoredId) {
    this.mostRecentStoredId = mostRecentStoredId;
    }

    public String getTagName() {
    return tagName;
    }

        public void setTagName(String tagName) {
    this.tagName = tagName;
    }
    
    public static final String MOST_RECENT_STORED_ID = "most_recent_stored_id";

    public static final String TAG_NAME = "tag_name";

@Override
public String toString() {
return "EventTrackerInfoDO{" +
        "mostRecentStoredId=" + mostRecentStoredId +
        ", tagName=" + tagName +
"}";
}
}
