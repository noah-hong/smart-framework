package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq;

import org.apache.rocketmq.client.MQAdmin;
import org.apache.rocketmq.client.producer.MQProducer;
import org.smartframework.event.driven.core.IEventDispatcher;
import org.smartframework.event.driven.core.IEventDispatcherAdapter;
import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
import org.smartframework.event.driven.rocketmq.RocketMQEventDispatcherAdapter;
import org.smartframework.spring.boot.autoconfigure.event.driven.EventDrivenAutoConfigure;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded.EmbeddedRocketMQEventDispatcher;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded.EmbeddedRocketMQProducer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author noah
 * @create 2021-05-06 13:11
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventDrivenRocketMQProperties.class)
@ConditionalOnClass(MQAdmin.class)
@AutoConfigureAfter(EventDrivenAutoConfigure.class)
@ConditionalOnProperty(prefix = "smart.event-driven.rocketmq", value = "name-server", matchIfMissing = false)
@ConditionalOnBean({EventDrivenAutoConfigure.class})
public class RocketMQAutoConfiguration {

  private final EventDrivenRocketMQProperties properties;

  public RocketMQAutoConfiguration(EventDrivenRocketMQProperties properties) {
    this.properties = properties;
  }

  @Bean
  @ConditionalOnMissingBean(IEventDispatcher.class)
  public IEventDispatcher eventDispatcher(EmbeddedRocketMQProducer producer) {
    return new EmbeddedRocketMQEventDispatcher(producer.getInstance());
  }

  @Bean
  @ConditionalOnMissingBean(IEventDispatcherAdapter.class)
  public IEventDispatcherAdapter eventDispatcherAdapter(ITopicGenerator topicGenerator,
      IEventEnvelopeFormatter eventEnvelopeFormatter, IEventDispatcher eventDispatcher) {
    return new RocketMQEventDispatcherAdapter(topicGenerator, eventEnvelopeFormatter,
        eventDispatcher);
  }

  @Bean
  public EmbeddedRocketMQProducer embeddedRocketMQProducer() {
    return new EmbeddedRocketMQProducer(properties);
  }

  @Bean
  @ConditionalOnMissingBean(MQProducer.class)
  public MQProducer defaultMQProducer(EmbeddedRocketMQProducer producer) {
    return producer.getInstance();
  }


}
