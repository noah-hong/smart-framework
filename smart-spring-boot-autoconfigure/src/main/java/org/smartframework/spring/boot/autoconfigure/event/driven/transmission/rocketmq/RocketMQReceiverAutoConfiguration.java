package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq;

import org.apache.rocketmq.client.log.ClientLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.IEventReceiver;
import org.smartframework.event.driven.core.ISubscribeHandler;
import org.smartframework.event.driven.core.handler.EmbeddedConsumedMessageLoggerSubscribeHandler;
import org.smartframework.event.driven.core.handler.EmbeddedExcatlyOnceSubscribeHandler;
import org.smartframework.event.driven.core.store.IConsumedMessageLogger;
import org.smartframework.event.driven.core.store.IExcatlyOnceSupporter;
import org.smartframework.event.driven.core.store.ITransactionExecutor;
import org.smartframework.event.driven.rocketmq.annotation.RocketMQEventListener;
import org.smartframework.event.driven.rocketmq.support.RocketMQEventListenerProxy;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded.EmbeddedRocketMQEventListenerProxy;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded.EmbeddedRocketMQEventReceiver;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.GenericApplicationContext;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

/**
 * @author noah
 * @create 2021-053:52
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventDrivenRocketMQProperties.class)
@ConditionalOnBean(value = RocketMQAutoConfiguration.class, annotation = EnableRocketMQEventListener.class)
@AutoConfigureAfter({RocketMQAutoConfiguration.class})
@Import({RocketMQAutoConfiguration.class})
@ConditionalOnProperty(prefix = "smart.event-driven.rocketmq.consumer", value = "enabled", havingValue = "true", matchIfMissing = true)
public class RocketMQReceiverAutoConfiguration implements ApplicationContextAware,
    ApplicationRunner {

  @PostConstruct
  public void init() throws Exception {
    //限制rocketmq-client.log日志文件200MB大小
    System.getProperty(ClientLogger.CLIENT_LOG_FILESIZE, String.valueOf(1024 * 200));
    final Map<String, Object> map = this.applicationContext
            .getBeansWithAnnotation(RocketMQEventListener.class);
    for (Object listener : map.values()) {
      this.initReceiver(listener);
    }

  }

  private void initReceiver(Object listener) {
    RocketMQEventListenerProxy proxy = new EmbeddedRocketMQEventListenerProxy(listener);
    final Map<String, ISubscribeHandler> subscribeHandlerMap = applicationContext
        .getBeansOfType(ISubscribeHandler.class);
    for (ISubscribeHandler subscribeHandler : subscribeHandlerMap.values()) {
      proxy.getAdditionalHandlers().add(subscribeHandler);
    }
    IEventReceiver receiver = new EmbeddedRocketMQEventReceiver(proxy,
        applicationContext);

    applicationContext.registerBean(
        String.format("%s_%s", receiver.getClass().getSimpleName(),
            listener.getClass().getSimpleName()),
        IEventReceiver.class, () -> receiver);
  }

  @Configuration(proxyBeanMethods = false)
  public static class EmbeddedSubscribeHandlerConfiguration {

    @Bean
    @ConditionalOnBean(IConsumedMessageLogger.class)
    @ConditionalOnMissingBean(name = "consumedMessageLoggerSubscribeHandler")
    public ISubscribeHandler consumedMessageLoggerSubscribeHandler(
        ObjectProvider<IConsumedMessageLogger> logger) {

      return new EmbeddedConsumedMessageLoggerSubscribeHandler(logger.getIfAvailable());
    }

    @Bean
    @ConditionalOnBean({IExcatlyOnceSupporter.class, ITransactionExecutor.class})
    @ConditionalOnMissingBean(name = "excatlyOnceSubscribeHandler")
    public ISubscribeHandler excatlyOnceSubscribeHandler(
            ObjectProvider<IExcatlyOnceSupporter> supporter,
            ObjectProvider<ITransactionExecutor> executors) {
      return new EmbeddedExcatlyOnceSubscribeHandler(supporter.getIfAvailable(),
              executors.getIfAvailable());
    }

//    @Bean
//    @ConditionalOnBean(IConsumedMessageLogger.class)
//    @ConditionalOnMissingBean(name = "retryRuleSubscribeHandler")
//    public ISubscribeHandler retryRuleSubscribeHandler() {
//      return new EmbeddedRetryRuleSubscribeHandler();
//    }

  }


  private GenericApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = (GenericApplicationContext) applicationContext;
  }

  @Override
  public void run(ApplicationArguments args) {
    ForkJoinPool.commonPool().submit(this::createEventReceiver);

  }

  private final static Logger log = LoggerFactory
      .getLogger(RocketMQReceiverAutoConfiguration.class);

  public void createEventReceiver() {
    try {
      EventDrivenRocketMQProperties properties = applicationContext
          .getBean(EventDrivenRocketMQProperties.class);
      if (properties.getConsumer().isEnabled()) {
        final Map<String, IEventReceiver> receiverMap = applicationContext
            .getBeansOfType(IEventReceiver.class);
        for (IEventReceiver receiver : receiverMap.values()) {
          receiver.start();
        }
      }
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
    }
  }
}
