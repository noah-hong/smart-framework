package org.smartframework.spring.boot.autoconfigure.saga.store.jpa.repository;

import org.smartframework.spring.boot.autoconfigure.saga.store.jpa.entity.SagaInstanceInfo;
import org.smartframework.spring.boot.autoconfigure.saga.store.jpa.entity.SagaStepInfo;
import org.springframework.data.repository.CrudRepository;

/**
 * @author noah
 * @create 2021-10-13 21:45
 */
public interface SagaStepInfoRepository extends CrudRepository<SagaStepInfo, Integer> {
}
