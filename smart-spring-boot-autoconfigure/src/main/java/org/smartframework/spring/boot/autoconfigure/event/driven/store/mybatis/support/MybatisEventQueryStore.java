package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.smartframework.event.driven.core.store.IEventQueryStore;
import org.smartframework.event.driven.core.store.StoredEvent;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventSourcingInfo;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventSourcingRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventSourcingInfoDO;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventSourcingInfoCommonRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

/**
 * @author noah
 * @create 2021-05-24 17:49
 */
public class MybatisEventQueryStore implements IEventQueryStore {

  public final EventSourcingInfoCommonRepository eventSourcingRepository;

  public MybatisEventQueryStore(
      EventSourcingInfoCommonRepository eventSourcingRepository) {

    this.eventSourcingRepository = eventSourcingRepository;
  }

  private StoredEvent toModel(EventSourcingInfoDO eventSourcing) {
    StoredEvent storedEvent = new StoredEvent();
    storedEvent.setId(eventSourcing.getId());
    storedEvent.setPayload(eventSourcing.getEventBody());

    storedEvent.setTopic(eventSourcing.getTopic());
    storedEvent.setSubTopic(eventSourcing.getSubTopic());
    storedEvent.setTagName(eventSourcing.getTagName());
    storedEvent.setEventId(eventSourcing.getEventId());
    storedEvent.setPersistenceId(eventSourcing.getPersistenceId());
    storedEvent.setLocation(eventSourcing.getLocation());
    storedEvent.setEventType(eventSourcing.getEventType());
    storedEvent.setSenderName(eventSourcing.getSender());
    storedEvent.setSenderId(eventSourcing.getSenderId());
    storedEvent.setSenderScope(eventSourcing.getSenderScope());
    storedEvent.setStoredTime(eventSourcing.getStoredTime());
    storedEvent.setVersion(eventSourcing.getEventVersion());
    storedEvent.setSerializer(eventSourcing.getSerializer());
    if (!StringUtils.isEmpty(eventSourcing.getProperties())) {
      try {
        storedEvent.setProperties(JSON.parseObject(eventSourcing.getProperties(),
            new TypeReference<HashMap<String, String>>() {
            }));
      } catch (Exception ex) {

      }
    }
    return storedEvent;
  }

  @Override
  public List<StoredEvent> allStoredEventsSinceByTag(String tagName, Long aStoredId) {
    QueryWrapper<EventSourcingInfoDO> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq(EventSourcingInfoDO.TAG_NAME, tagName);
    queryWrapper.gt(EventSourcingInfoDO.ID, aStoredId);
    List<EventSourcingInfoDO> eventSourcings = eventSourcingRepository.list(queryWrapper);

    if (CollectionUtils.isEmpty(eventSourcings)) {
      return new ArrayList<>(0);
    }

    return eventSourcings.stream().map(this::toModel).collect(Collectors.toList());
  }

  @Override
  public List<StoredEvent> allStoredEventsSinceByTag(String tagName, Long aStoredId,
      Integer limit) {
    QueryWrapper<EventSourcingInfoDO> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq(EventSourcingInfoDO.TAG_NAME, tagName);
    queryWrapper.gt(EventSourcingInfoDO.ID, aStoredId);
    queryWrapper.last(MessageFormat.format("limit {0} lock in share mode", limit));
//    lock in share mode
    final List<EventSourcingInfoDO> list = eventSourcingRepository.list(queryWrapper);

    return list.stream().map(this::toModel).collect(Collectors.toList());
  }

  private EventSourcingInfo toDO(StoredEvent event) {
    EventSourcingInfo eventSourcing = new EventSourcingInfo();
    eventSourcing.setEventId(event.getEventId());
    eventSourcing.setLocation(event.getLocation());
    eventSourcing.setTagName(event.getTagName());
    eventSourcing.setPersistenceId(event.getPersistenceId());
    eventSourcing.setEventType(event.getEventType());
    eventSourcing.setEventBody(event.getPayload());
    eventSourcing.setEventVersion(event.getVersion());
    eventSourcing.setSender(event.getSenderName());
    eventSourcing.setSenderId(event.getSenderId());
    eventSourcing.setSenderScope(event.getSenderScope());
    eventSourcing.setStoredTime(event.getStoredTime());
    eventSourcing.setSerializer(event.getSerializer());
    eventSourcing.setTopic(event.getTopic());
    eventSourcing.setSubTopic(event.getSubTopic());
    return eventSourcing;
  }

}
