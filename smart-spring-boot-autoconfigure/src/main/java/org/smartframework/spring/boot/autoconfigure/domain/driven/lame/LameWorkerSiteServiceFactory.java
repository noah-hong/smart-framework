package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import org.smartframework.domain.driven.core.communication.CommandHandlerInterceptor;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IWorkerSiteService;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.embedded.EmbeddedLameWorkerSiteService;

import java.util.Collection;

/**
 * @author noah
 * @create 2021-06-07 12:02
 */
public class LameWorkerSiteServiceFactory {

  private final IClientSiteResponderRegistrar clientSiteResponderRegistrar;

  public LameWorkerSiteServiceFactory(IClientSiteResponderRegistrar clientSiteResponderRegistrar) {
    this.clientSiteResponderRegistrar = clientSiteResponderRegistrar;
  }

  public IWorkerSiteService create(Object invoker) {
    return new EmbeddedLameWorkerSiteService(invoker, clientSiteResponderRegistrar);
  }

  public IWorkerSiteService create(Object invoker, Collection<CommandHandlerInterceptor> interceptors) {
    final EmbeddedLameWorkerSiteService embeddedLameWorkerSiteService = new EmbeddedLameWorkerSiteService(invoker, clientSiteResponderRegistrar);
    embeddedLameWorkerSiteService.addInterceptors(interceptors);
    return embeddedLameWorkerSiteService;
  }
}
