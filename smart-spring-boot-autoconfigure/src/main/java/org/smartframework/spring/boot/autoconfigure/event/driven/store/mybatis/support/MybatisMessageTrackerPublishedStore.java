package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.smartframework.event.driven.core.store.IPublishedEventTrackerStore;
import org.smartframework.event.driven.core.store.PublishedEventTracker;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventTrackerInfo;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventTrackerRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventTrackerInfoDO;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventTrackerInfoCommonRepository;


/**
 * @author noah
 * @create 2020-05-10 20:51
 */
public class MybatisMessageTrackerPublishedStore implements IPublishedEventTrackerStore {

  private final EventTrackerInfoCommonRepository eventTrackerRepository;

  public MybatisMessageTrackerPublishedStore(
      EventTrackerInfoCommonRepository eventTrackerRepository) {
    this.eventTrackerRepository = eventTrackerRepository;
  }

  private PublishedEventTracker toModel(EventTrackerInfoDO eventTrackerDO) {
    PublishedEventTracker tracker = new PublishedEventTracker();
    tracker.setMostRecentStoredId(eventTrackerDO.getMostRecentStoredId());
    tracker.setTagName(eventTrackerDO.getTagName());
    return tracker;
  }

  private EventTrackerInfoDO toDO(PublishedEventTracker tracker) {
    EventTrackerInfoDO eventTrackerDO = new EventTrackerInfoDO();
    eventTrackerDO.setMostRecentStoredId(tracker.getMostRecentStoredId());
    eventTrackerDO.setTagName(tracker.getTagName());
    return eventTrackerDO;
  }

  @Override
  public List<PublishedEventTracker> getTrackers() {
    final List<EventTrackerInfoDO> list1 = eventTrackerRepository.list();
    List<PublishedEventTracker> list = new ArrayList<>();
    for (EventTrackerInfoDO eventTrackerInfoDO : list1) {
      list.add(toModel(eventTrackerInfoDO));
    }
    return list;
  }

  @Override
  public PublishedEventTracker getTrackerByTagName(String aTagName) {
    QueryWrapper<EventTrackerInfoDO> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq(EventTrackerInfoDO.TAG_NAME,aTagName);
    final EventTrackerInfoDO eventTrackerDO = eventTrackerRepository.getOne(queryWrapper);

    return toModel(eventTrackerDO);
  }

  @Override
  public void track(PublishedEventTracker tracker) {
    final EventTrackerInfoDO eventTrackerDO = toDO(tracker);
    eventTrackerRepository.saveOrUpdate(eventTrackerDO);
  }
}
