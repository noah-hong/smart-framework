package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
* <p>
  * 消息队列消费记录，幂等
  * </p>
*
* @author System
* @since 2022-03-24
*/
@TableName("event_transaction_record")
public class EventTransactionRecordDO implements Serializable {

private static final long serialVersionUID=1L;

        /**
        * 主键
        */
                @TableId(value = "id", type = IdType.AUTO)
                private Long id;

        /**
        * 消费者组
        */
        private String consumerGroup;

        /**
        * 消息编号
        */
        private String messageId;

        /**
        * 消息自定义key
        */
        private String messageKey;

        /**
        * 主题
        */
        private String topic;

        /**
        * 子主题
        */
        private String subTopic;

        private String sender;

        private String senderId;

        private String senderScope;

        /**
        * 重消费次数
        */
        private Integer reconsumeTimes;

        /**
        * 消息存储时间
        */
        private LocalDateTime storedTime;

        private LocalDateTime createdTime;


    public Long getId() {
    return id;
    }

        public void setId(Long id) {
    this.id = id;
    }

    public String getConsumerGroup() {
    return consumerGroup;
    }

        public void setConsumerGroup(String consumerGroup) {
    this.consumerGroup = consumerGroup;
    }

    public String getMessageId() {
    return messageId;
    }

        public void setMessageId(String messageId) {
    this.messageId = messageId;
    }

    public String getMessageKey() {
    return messageKey;
    }

        public void setMessageKey(String messageKey) {
    this.messageKey = messageKey;
    }

    public String getTopic() {
    return topic;
    }

        public void setTopic(String topic) {
    this.topic = topic;
    }

    public String getSubTopic() {
    return subTopic;
    }

        public void setSubTopic(String subTopic) {
    this.subTopic = subTopic;
    }

    public String getSender() {
    return sender;
    }

        public void setSender(String sender) {
    this.sender = sender;
    }

    public String getSenderId() {
    return senderId;
    }

        public void setSenderId(String senderId) {
    this.senderId = senderId;
    }

    public String getSenderScope() {
    return senderScope;
    }

        public void setSenderScope(String senderScope) {
    this.senderScope = senderScope;
    }

    public Integer getReconsumeTimes() {
    return reconsumeTimes;
    }

        public void setReconsumeTimes(Integer reconsumeTimes) {
    this.reconsumeTimes = reconsumeTimes;
    }

    public LocalDateTime getStoredTime() {
    return storedTime;
    }

        public void setStoredTime(LocalDateTime storedTime) {
    this.storedTime = storedTime;
    }

    public LocalDateTime getCreatedTime() {
    return createdTime;
    }

        public void setCreatedTime(LocalDateTime createdTime) {
    this.createdTime = createdTime;
    }
    
    public static final String ID = "id";

    public static final String CONSUMER_GROUP = "consumer_group";

    public static final String MESSAGE_ID = "message_id";

    public static final String MESSAGE_KEY = "message_key";

    public static final String TOPIC = "topic";

    public static final String SUB_TOPIC = "sub_topic";

    public static final String SENDER = "sender";

    public static final String SENDER_ID = "sender_id";

    public static final String SENDER_SCOPE = "sender_scope";

    public static final String RECONSUME_TIMES = "reconsume_times";

    public static final String STORED_TIME = "stored_time";

    public static final String CREATED_TIME = "created_time";

@Override
public String toString() {
return "EventTransactionRecordDO{" +
        "id=" + id +
        ", consumerGroup=" + consumerGroup +
        ", messageId=" + messageId +
        ", messageKey=" + messageKey +
        ", topic=" + topic +
        ", subTopic=" + subTopic +
        ", sender=" + sender +
        ", senderId=" + senderId +
        ", senderScope=" + senderScope +
        ", reconsumeTimes=" + reconsumeTimes +
        ", storedTime=" + storedTime +
        ", createdTime=" + createdTime +
"}";
}
}
