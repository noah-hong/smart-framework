package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository;

import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventConsumedLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * @author noah
 * @create 2020-05-21 18:20
 */
public interface EventConsumedLogRepository
    extends CrudRepository<EventConsumedLog, Long> {}
