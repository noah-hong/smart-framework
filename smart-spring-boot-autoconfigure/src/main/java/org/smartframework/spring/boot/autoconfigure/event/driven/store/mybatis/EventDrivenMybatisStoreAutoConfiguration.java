package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis;

import javax.sql.DataSource;
import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.store.IConsumedMessageLogger;
import org.smartframework.event.driven.core.store.IEventEnvelopStore;
import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
import org.smartframework.event.driven.core.store.IEventQueryStore;
import org.smartframework.event.driven.core.store.IEventStore;
import org.smartframework.event.driven.core.store.IExcatlyOnceSupporter;
import org.smartframework.event.driven.core.store.IPublishedEventTrackerStore;
import org.smartframework.event.driven.core.store.IStoredEventCustomizer;
import org.smartframework.event.driven.core.store.ITransactionExecutor;
import org.smartframework.spring.boot.autoconfigure.event.driven.EventDrivenAutoConfigure;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventConsumedLogCommonRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventSourcingInfoCommonRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventTrackerInfoCommonRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventTransactionRecordCommonRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.MybatisRepositoryEventDrivenConfiguration;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support.MyBatisComsumedMessageLogger;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support.MybatisEventEnvelopStoreAdapter;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support.MybatisEventQueryStore;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support.MybatisEventStore;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support.MybatisExcatlyOnceSupporter;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support.MybatisMessageTrackerPublishedStore;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support.MybatisTransactionExecutor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * @author noah
 * @create 2021-05-06 11:18
 */
@Configuration(proxyBeanMethods = false)
@AutoConfigureAfter({EventDrivenAutoConfigure.class})
@ConditionalOnBean({EventDrivenAutoConfigure.class, DataSource.class})
@Import(MybatisRepositoryEventDrivenConfiguration.class)
public class EventDrivenMybatisStoreAutoConfiguration {

  @Bean
  @ConditionalOnMissingBean(IExcatlyOnceSupporter.class)
  public IExcatlyOnceSupporter mybatisExcatlyOnceSupport(
      EventTransactionRecordCommonRepository eventTransactionRecordCommonRepository) {
    return new MybatisExcatlyOnceSupporter(eventTransactionRecordCommonRepository);
  }

  @Bean
  @ConditionalOnMissingBean(IConsumedMessageLogger.class)
  public IConsumedMessageLogger mybatisComsumedMessageLogger(
      EventConsumedLogCommonRepository recordRepository) {
    return new MyBatisComsumedMessageLogger(recordRepository);
  }

  @Bean
  @ConditionalOnMissingBean(IEventStore.class)
  public IEventStore mybatisEventStore(EventSourcingInfoCommonRepository eventSourcingRepository,
      ObjectProvider<IStoredEventCustomizer> storedEventCustomizer) {
    final MybatisEventStore eventStore = new MybatisEventStore(eventSourcingRepository);
    eventStore.setStoredEventCustomizer(storedEventCustomizer.getIfAvailable(() -> event -> {
    }));
    return eventStore;
  }

  @Bean
  @ConditionalOnMissingBean(IEventQueryStore.class)
  public IEventQueryStore mybatisEventQueryStore(
      EventSourcingInfoCommonRepository eventSourcingRepository) {
    return new MybatisEventQueryStore(eventSourcingRepository);
  }

  @Bean
  @ConditionalOnMissingBean(IEventEnvelopStore.class)
  public IEventEnvelopStore mybatisEventEnvelopStore(IEventStore eventStore,
      ITopicGenerator topicGenerator,
      IEventEnvelopeFormatter serializer) {
    return new MybatisEventEnvelopStoreAdapter(eventStore, topicGenerator, serializer);
  }

  @Bean
  @ConditionalOnMissingBean(ITransactionExecutor.class)
  public ITransactionExecutor mybatisTransactionExecutor(TransactionTemplate transactionTemplate) {
    return new MybatisTransactionExecutor(transactionTemplate);
  }

  @Bean
  @ConditionalOnMissingBean(IPublishedEventTrackerStore.class)
  public IPublishedEventTrackerStore mybatisPublishedEventTrackerStore(
      EventTrackerInfoCommonRepository eventTrackerRepository) {
    return new MybatisMessageTrackerPublishedStore(eventTrackerRepository);
  }


}
