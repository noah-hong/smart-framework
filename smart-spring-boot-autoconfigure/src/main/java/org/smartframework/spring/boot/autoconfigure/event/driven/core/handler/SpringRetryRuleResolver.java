package org.smartframework.spring.boot.autoconfigure.event.driven.core.handler;

import org.smartframework.event.driven.core.annotation.RetryRule;
import org.smartframework.event.driven.core.handler.IRetryRuleResolver;
import org.smartframework.event.driven.core.handler.RetryRuleInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.StandardEnvironment;

/**
 * @author noah
 * @create 2021-10-22 23:21
 */
@Deprecated
public class SpringRetryRuleResolver implements IRetryRuleResolver {


    @Autowired(required = false)
    public StandardEnvironment environment;

    @Override
    public RetryRuleInfo resolve(RetryRule retryRule) {

//        String nameServer = environment.resolvePlaceholders(retryRule.retryCount());
        return null;
    }
}
