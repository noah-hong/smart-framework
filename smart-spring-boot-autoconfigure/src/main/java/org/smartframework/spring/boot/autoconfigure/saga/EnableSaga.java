package org.smartframework.spring.boot.autoconfigure.saga;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author noah
 * @create 2021-10-14 11:58
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(SagaRegistrar.class)
public @interface EnableSaga {
    String[] value() default {};
}
