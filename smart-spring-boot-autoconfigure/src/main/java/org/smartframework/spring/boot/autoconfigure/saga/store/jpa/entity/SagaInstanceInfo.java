package org.smartframework.spring.boot.autoconfigure.saga.store.jpa.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author noah
 * @create 2021-10-13 18:14
 */
@Entity(name = "saga_instance_info")
public class SagaInstanceInfo implements Serializable {

  private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

  @Id
  @Column(name = "saga_id")
  private String sagaId;
  @Column(name = "saga_name")
  private String sagaName;
  @Column(name = "saga_type")
  private String sagaType;
  @Column(name = "saga_recover_strategy")
  private String sagaRecoverStrategy;
  @Column(name = "saga_status")
  private String sagaStatus;
  @Column(name = "currently_executing")
  private int currentlyExecuting;
  @Column(name = "compensating")
  private boolean compensating;
  @Column(name = "end_state")
  private boolean endState;
  @Column(name = "executing_exception")
  private String executingException;
  @Column(name = "compensating_exception")
  private String compensatingException;
  @Column(name = "last_message")
  private String lastMessage;
  @Column(name = "stop_state")
  private boolean isStop;
  @Column(name = "currently_step_name")
  private String currentlyStepName;
  @Column(name = "saga_data")
  private String sagaData;
  @Column(name = "executing_messages")
  private String executingMessages;
  @Column(name = "compensating_messages")
  private String compensatingMessages;

  public String getSagaId() {
    return sagaId;
  }

  public void setSagaId(String sagaId) {
    this.sagaId = sagaId;
  }

  public String getSagaName() {
    return sagaName;
  }

  public void setSagaName(String sagaName) {
    this.sagaName = sagaName;
  }

  public String getSagaStatus() {
    return sagaStatus;
  }

  public void setSagaStatus(String sagaStatus) {
    this.sagaStatus = sagaStatus;
  }

  public String getSagaType() {
    return sagaType;
  }

  public void setSagaType(String sagaType) {
    this.sagaType = sagaType;
  }

  public String getSagaRecoverStrategy() {
    return sagaRecoverStrategy;
  }

  public void setSagaRecoverStrategy(String sagaRecoverStrategy) {
    this.sagaRecoverStrategy = sagaRecoverStrategy;
  }

  public int getCurrentlyExecuting() {
    return currentlyExecuting;
  }

  public void setCurrentlyExecuting(int currentlyExecuting) {
    this.currentlyExecuting = currentlyExecuting;
  }

  public boolean isCompensating() {
    return compensating;
  }

  public void setCompensating(boolean compensating) {
    this.compensating = compensating;
  }

  public boolean isEndState() {
    return endState;
  }

  public void setEndState(boolean endState) {
    this.endState = endState;
  }

  public String getExecutingException() {
    return executingException;
  }

  public void setExecutingException(String executingException) {
    this.executingException = executingException;
  }

  public String getCompensatingException() {
    return compensatingException;
  }

  public void setCompensatingException(String compensatingException) {
    this.compensatingException = compensatingException;
  }

  public String getLastMessage() {
    return lastMessage;
  }

  public void setLastMessage(String lastMessage) {
    this.lastMessage = lastMessage;
  }

  public boolean isStop() {
    return isStop;
  }

  public void setStop(boolean stop) {
    isStop = stop;
  }

  public String getCurrentlyStepName() {
    return currentlyStepName;
  }

  public void setCurrentlyStepName(String currentlyStepName) {
    this.currentlyStepName = currentlyStepName;
  }

  public String getSagaData() {
    return sagaData;
  }

  public void setSagaData(String data) {
    this.sagaData = data;
  }

  public String getExecutingMessages() {
    return executingMessages;
  }

  public void setExecutingMessages(String executingMessages) {
    this.executingMessages = executingMessages;
  }

  public String getCompensatingMessages() {
    return compensatingMessages;
  }

  public void setCompensatingMessages(String compensatingMessages) {
    this.compensatingMessages = compensatingMessages;
  }
}
