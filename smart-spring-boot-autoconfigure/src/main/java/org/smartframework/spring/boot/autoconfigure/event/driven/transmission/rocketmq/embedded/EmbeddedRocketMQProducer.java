package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded;

import java.util.UUID;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.smartframework.rocketmq.SmartMQProducer;
import org.smartframework.rocketmq.SmartTopicValidator;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.EventDrivenRocketMQProperties;

/**
 * @author noah
 * @create 2021-05-23 21:44
 */
public class EmbeddedRocketMQProducer {

  public SmartMQProducer getInstance() {
    return producer;
  }

  private SmartMQProducer producer;

  public EmbeddedRocketMQProducer(SmartMQProducer producer) {
    this.producer = producer;
  }

  public EmbeddedRocketMQProducer(EventDrivenRocketMQProperties properties) {
    this.producer = new SmartMQProducer(properties.getProducer().getGroup());
    producer.setDefaultTopicQueueNums(properties.getProducer().getDefaultTopicQueueNums());
    producer.setEnabledAutoCreateTopic(properties.getProducer().isEnabledAutoCreateTopic());
    producer.setNamesrvAddr(properties.getNameServer());
    producer.setNamespace(properties.getNamespace());
    producer.setInstanceName(UUID.randomUUID().toString());
    producer.setSendMsgTimeout(properties.getProducer().getSendMessageTimeout());
    producer.setCreateTopicKey(SmartTopicValidator.AUTO_CREATE_TOPIC_KEY_TOPIC_BY_VERSION4_3);
//    producer.setDefaultTopicQueueNums(10);
    producer
        .setRetryTimesWhenSendFailed(properties.getProducer().getRetryTimesWhenSendFailed());
    producer.setRetryTimesWhenSendAsyncFailed(
        properties.getProducer().getRetryTimesWhenSendAsyncFailed());
    producer.setMaxMessageSize(properties.getProducer().getMaxMessageSize());
    producer.setCompressMsgBodyOverHowmuch(
        properties.getProducer().getCompressMessageBodyThreshold());
    producer.setRetryAnotherBrokerWhenNotStoreOK(properties.getProducer().isRetryNextServer());
  }
}
