package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventTrackerInfoDO;

/**
* <p>
 * 事件发布追踪者 服务类
 * </p>
*
* @author System
* @since 2022-03-24
*/
public interface EventTrackerInfoCommonRepository extends IService<EventTrackerInfoDO> {

}
