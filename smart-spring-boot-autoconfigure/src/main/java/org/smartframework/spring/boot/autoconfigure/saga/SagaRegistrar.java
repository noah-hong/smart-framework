package org.smartframework.spring.boot.autoconfigure.saga;

import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.EnableRocketMQEventListener;
import org.smartframework.spring.boot.autoconfigure.saga.annotation.Saga;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * @author noah
 * @create 2021-10-14 11:58
 */
public class SagaRegistrar implements ImportBeanDefinitionRegistrar {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
                                        BeanDefinitionRegistry registry, BeanNameGenerator importBeanNameGenerator) {

        AnnotationAttributes eventListenerAnnoAttrs =
                AnnotationAttributes.fromMap(
                        importingClassMetadata
                                .getAnnotationAttributes(EnableRocketMQEventListener.class.getName()));

        Optional.ofNullable(eventListenerAnnoAttrs)
                .ifPresent(annoAttrs -> {
                    Set<String> basePackages = new HashSet<>();
                    for (String pkg : (String[]) annoAttrs.get("value")) {
                        if (StringUtils.hasText(pkg)) {
                            basePackages.add(pkg);
                        }
                    }

                    if (basePackages.isEmpty()) {
                        basePackages.add(
                                ClassUtils.getPackageName(importingClassMetadata.getClassName()));
                    }

                    BeanDefinitionBuilder builder =
                            BeanDefinitionBuilder
                                    .genericBeanDefinition(SagaDefinitionBeanDefinitionRegistryPostProcessor.class);
                    builder
                            .addConstructorArgValue(StringUtils.collectionToCommaDelimitedString(basePackages));
                    registry.registerBeanDefinition(SagaRegistrar.class.getSimpleName(),
                            builder.getBeanDefinition());
                });
    }

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
                                        BeanDefinitionRegistry registry) {

    }

    static class SagaDefinitionBeanDefinitionRegistryPostProcessor implements
            BeanDefinitionRegistryPostProcessor, ApplicationContextAware {

        private final String basePackage;

        public SagaDefinitionBeanDefinitionRegistryPostProcessor(String basePackage) {
            this.basePackage = basePackage;
        }


        @Override
        public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry)
                throws BeansException {
            ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry);
            scanner.setResourceLoader(this.applicationContext);
            scanner.addIncludeFilter(
                    new AnnotationTypeFilter(Saga.class));

            scanner.scan(
                    StringUtils.tokenizeToStringArray(
                            this.basePackage, ",; \t\n"));

        }

        @Override
        public void postProcessBeanFactory(
                ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

        }

        private ApplicationContext applicationContext;

        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
            this.applicationContext = applicationContext;
        }
    }

}
