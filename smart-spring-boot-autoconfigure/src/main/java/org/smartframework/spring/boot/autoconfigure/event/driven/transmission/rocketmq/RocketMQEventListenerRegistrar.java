package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.smartframework.event.driven.rocketmq.annotation.RocketMQEventListener;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

/**
 * @author noah
 * @create 2021-05-12 17:32
 */
public class RocketMQEventListenerRegistrar implements ImportBeanDefinitionRegistrar {

  @Override
  public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
      BeanDefinitionRegistry registry, BeanNameGenerator importBeanNameGenerator) {

    AnnotationAttributes eventListenerAnnoAttrs =
        AnnotationAttributes.fromMap(
            importingClassMetadata
                .getAnnotationAttributes(EnableRocketMQEventListener.class.getName()));

    Optional.ofNullable(eventListenerAnnoAttrs)
        .ifPresent(annoAttrs -> {
          Set<String> basePackages = new HashSet<>();
          for (String pkg : (String[]) annoAttrs.get("value")) {
            if (StringUtils.hasText(pkg)) {
              basePackages.add(pkg);
            }
          }

          if (basePackages.isEmpty()) {
            basePackages.add(
                ClassUtils.getPackageName(importingClassMetadata.getClassName()));
          }

          BeanDefinitionBuilder builder =
              BeanDefinitionBuilder
                  .genericBeanDefinition(EventListenerBeanDefinitionRegistryPostProcessor.class);
          builder
              .addConstructorArgValue(StringUtils.collectionToCommaDelimitedString(basePackages));
          registry.registerBeanDefinition(RocketMQEventListenerRegistrar.class.getSimpleName(),
              builder.getBeanDefinition());
        });
  }

  @Override
  public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
      BeanDefinitionRegistry registry) {

  }

  static class EventListenerBeanDefinitionRegistryPostProcessor implements
      BeanDefinitionRegistryPostProcessor, ApplicationContextAware {

    private final String basePackage;

    public EventListenerBeanDefinitionRegistryPostProcessor(String basePackage) {
      this.basePackage = basePackage;
    }


    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry)
        throws BeansException {
      ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry);
      scanner.setResourceLoader(this.applicationContext);
      scanner.addIncludeFilter(
          new AnnotationTypeFilter(RocketMQEventListener.class));

      scanner.scan(
          StringUtils.tokenizeToStringArray(
              this.basePackage, ",; \t\n"));

    }

    @Override
    public void postProcessBeanFactory(
        ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

    }

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
      this.applicationContext = applicationContext;
    }
  }

}

