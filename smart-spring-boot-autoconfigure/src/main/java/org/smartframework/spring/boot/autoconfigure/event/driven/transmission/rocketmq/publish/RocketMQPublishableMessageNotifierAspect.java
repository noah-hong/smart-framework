package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.publish;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.ThreadFactoryImpl;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.smartframework.event.driven.core.IPublishableEventContextCustomizer;
import org.smartframework.event.driven.core.PublishableEventContext;

/**
 * @author noah
 * @create 2020-05-05 15:14
 */
@Aspect
public class RocketMQPublishableMessageNotifierAspect {

  private final DefaultMQProducer producer;
  private final ThreadPoolExecutor executor;
  Logger log = LoggerFactory.getLogger(RocketMQPublishableMessageNotifierAspect.class);
  private final String publishableMessageNotifierTopic;
  private List<IPublishableEventContextCustomizer> publishContextCustomizers = new ArrayList<>();

  public List<IPublishableEventContextCustomizer> getPublishContextCustomizers() {
    return publishContextCustomizers;
  }

  public void setPublishContextCustomizers(
      List<IPublishableEventContextCustomizer> publishContextCustomizers) {
    this.publishContextCustomizers = publishContextCustomizers;
  }

  public RocketMQPublishableMessageNotifierAspect(
      String publishableMessageNotifierTopic, DefaultMQProducer producer) {
    this.publishableMessageNotifierTopic = publishableMessageNotifierTopic;
    this.producer = producer;
    this.executor =
        new ThreadPoolExecutor(
            1,
            1,
            1000 * 60,
            TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>(1),
            new ThreadFactoryImpl("EventPublishableNotifierThread_"),
            new ThreadPoolExecutor.DiscardPolicy());
  }

  @Pointcut(
      "@annotation(org.smartframework.event.driven.core.annotation.Publishable) || @within(org.smartframework.event.driven.core.annotation.Publishable)")
  public void RocketMQPublishableAspect() {
  }

  @After("RocketMQPublishableAspect()")
  public void after(JoinPoint joinPoint) {
    executor.execute(
        () -> {
          try {
            final PublishableEventContext publishContext = new PublishableEventContext();
            final List<IPublishableEventContextCustomizer> publishContextCustomizers = getPublishContextCustomizers();
            publishContextCustomizers.forEach(x -> x.customize(publishContext));
            Message msg =
                new Message(
                    publishableMessageNotifierTopic,
                    ("All").getBytes(RemotingHelper.DEFAULT_CHARSET));
            final Map<String, String> properties = publishContext.getProperties();
            if (Objects.nonNull(properties)) {
              for (String key : properties.keySet()) {

                msg.getProperties().put(key, properties.get(key));
              }
            }
            producer.sendOneway(
                msg,
                (mqs, msg1, arg) -> {
                  Integer id = (Integer) arg;
                  int index = Math.abs(id % mqs.size());
                  return mqs.get(index);
                },
                publishableMessageNotifierTopic.hashCode());
          } catch (Exception e) {
            log.error(e.getMessage(), e);
          }
        });
  }
}
