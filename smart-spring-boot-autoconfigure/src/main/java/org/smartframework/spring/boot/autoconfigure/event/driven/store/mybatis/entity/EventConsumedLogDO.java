package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
* <p>
  * 消息消费日志
  * </p>
*
* @author System
* @since 2022-03-24
*/
@TableName("event_consumed_log")
public class EventConsumedLogDO implements Serializable {

private static final long serialVersionUID=1L;

        /**
        * 主键
        */
                @TableId(value = "id", type = IdType.AUTO)
                private Long id;

        /**
        * 消费者组
        */
        private String consumerGroup;

        /**
        * 消息编号
        */
        private String messageId;

        /**
        * 消息自定义key
        */
        private String messageKey;

        /**
        * 主题
        */
        private String topic;

        /**
        * 子主题
        */
        private String subTopic;

        /**
        * 队列编号
        */
        private Integer queueId;

        private String broker;

        /**
        * 偏移量
        */
        private Long messageOffset;

        /**
        * 重消费次数
        */
        private Integer reconsumeTimes;

        /**
        * 消息存储时间
        */
        private LocalDateTime storedTime;

        private Long bornTimeStamp;

        /**
        * 属性集合
        */
        private String properties;

        /**
        * 消息载荷
        */
        private String payload;

        /**
        * 异常简讯
        */
        private String exceptionStack;

        /**
        * 执行状态
        */
        private String execStatus;

        private String sender;

        private String senderId;

        private String senderScope;

        private LocalDateTime createdTime;


    public Long getId() {
    return id;
    }

        public void setId(Long id) {
    this.id = id;
    }

    public String getConsumerGroup() {
    return consumerGroup;
    }

        public void setConsumerGroup(String consumerGroup) {
    this.consumerGroup = consumerGroup;
    }

    public String getMessageId() {
    return messageId;
    }

        public void setMessageId(String messageId) {
    this.messageId = messageId;
    }

    public String getMessageKey() {
    return messageKey;
    }

        public void setMessageKey(String messageKey) {
    this.messageKey = messageKey;
    }

    public String getTopic() {
    return topic;
    }

        public void setTopic(String topic) {
    this.topic = topic;
    }

    public String getSubTopic() {
    return subTopic;
    }

        public void setSubTopic(String subTopic) {
    this.subTopic = subTopic;
    }

    public Integer getQueueId() {
    return queueId;
    }

        public void setQueueId(Integer queueId) {
    this.queueId = queueId;
    }

    public String getBroker() {
    return broker;
    }

        public void setBroker(String broker) {
    this.broker = broker;
    }

    public Long getMessageOffset() {
    return messageOffset;
    }

        public void setMessageOffset(Long messageOffset) {
    this.messageOffset = messageOffset;
    }

    public Integer getReconsumeTimes() {
    return reconsumeTimes;
    }

        public void setReconsumeTimes(Integer reconsumeTimes) {
    this.reconsumeTimes = reconsumeTimes;
    }

    public LocalDateTime getStoredTime() {
    return storedTime;
    }

        public void setStoredTime(LocalDateTime storedTime) {
    this.storedTime = storedTime;
    }

    public Long getBornTimeStamp() {
    return bornTimeStamp;
    }

        public void setBornTimeStamp(Long bornTimeStamp) {
    this.bornTimeStamp = bornTimeStamp;
    }

    public String getProperties() {
    return properties;
    }

        public void setProperties(String properties) {
    this.properties = properties;
    }

    public String getPayload() {
    return payload;
    }

        public void setPayload(String payload) {
    this.payload = payload;
    }

    public String getExceptionStack() {
    return exceptionStack;
    }

        public void setExceptionStack(String exceptionStack) {
    this.exceptionStack = exceptionStack;
    }

    public String getExecStatus() {
    return execStatus;
    }

        public void setExecStatus(String execStatus) {
    this.execStatus = execStatus;
    }

    public String getSender() {
    return sender;
    }

        public void setSender(String sender) {
    this.sender = sender;
    }

    public String getSenderId() {
    return senderId;
    }

        public void setSenderId(String senderId) {
    this.senderId = senderId;
    }

    public String getSenderScope() {
    return senderScope;
    }

        public void setSenderScope(String senderScope) {
    this.senderScope = senderScope;
    }

    public LocalDateTime getCreatedTime() {
    return createdTime;
    }

        public void setCreatedTime(LocalDateTime createdTime) {
    this.createdTime = createdTime;
    }
    
    public static final String ID = "id";

    public static final String CONSUMER_GROUP = "consumer_group";

    public static final String MESSAGE_ID = "message_id";

    public static final String MESSAGE_KEY = "message_key";

    public static final String TOPIC = "topic";

    public static final String SUB_TOPIC = "sub_topic";

    public static final String QUEUE_ID = "queue_id";

    public static final String BROKER = "broker";

    public static final String MESSAGE_OFFSET = "message_offset";

    public static final String RECONSUME_TIMES = "reconsume_times";

    public static final String STORED_TIME = "stored_time";

    public static final String BORN_TIME_STAMP = "born_time_stamp";

    public static final String PROPERTIES = "properties";

    public static final String PAYLOAD = "payload";

    public static final String EXCEPTION_STACK = "exception_stack";

    public static final String EXEC_STATUS = "exec_status";

    public static final String SENDER = "sender";

    public static final String SENDER_ID = "sender_id";

    public static final String SENDER_SCOPE = "sender_scope";

    public static final String CREATED_TIME = "created_time";

@Override
public String toString() {
return "EventConsumedLogDO{" +
        "id=" + id +
        ", consumerGroup=" + consumerGroup +
        ", messageId=" + messageId +
        ", messageKey=" + messageKey +
        ", topic=" + topic +
        ", subTopic=" + subTopic +
        ", queueId=" + queueId +
        ", broker=" + broker +
        ", messageOffset=" + messageOffset +
        ", reconsumeTimes=" + reconsumeTimes +
        ", storedTime=" + storedTime +
        ", bornTimeStamp=" + bornTimeStamp +
        ", properties=" + properties +
        ", payload=" + payload +
        ", exceptionStack=" + exceptionStack +
        ", execStatus=" + execStatus +
        ", sender=" + sender +
        ", senderId=" + senderId +
        ", senderScope=" + senderScope +
        ", createdTime=" + createdTime +
"}";
}
}
