package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventSourcingInfoDO;

/**
* <p>
 * 事件溯源 服务类
 * </p>
*
* @author System
* @since 2022-03-24
*/
public interface EventSourcingInfoCommonRepository extends IService<EventSourcingInfoDO> {

}
