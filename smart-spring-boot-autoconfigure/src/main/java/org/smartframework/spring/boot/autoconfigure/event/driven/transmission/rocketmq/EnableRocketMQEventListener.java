package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.context.annotation.Import;

/**
 * @author noah
 * @create 2021-05-12 18:05
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(RocketMQEventListenerRegistrar.class)
public @interface EnableRocketMQEventListener {
  String[] value() default {};
}
