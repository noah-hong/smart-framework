package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.entity.SagaInstanceInfoDO;

/**
* <p>
 * saga执行结果信息 Mapper 接口
 * </p>
*
* @author System
* @since 2022-04-05
*/
 @Mapper
public interface SagaInstanceInfoCommonMapper extends BaseMapper<SagaInstanceInfoDO> {

}
