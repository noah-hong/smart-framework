package org.smartframework.spring.boot.autoconfigure.domain.driven.lame.embedded;

import java.util.Optional;
import org.smartframework.core.SmartUtils;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IWorkerSiteService;
import org.smartframework.domain.driven.lame.woker.LameWorkerSite;
import org.smartframework.domain.driven.lame.woker.refactor.WorkerSiteMQPushConsumer;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.LameProperties;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.LameProperties.WorkerServiceConfiguration;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.LameProperties.WorkerSiteConfiguration;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.annotation.LameService;
import org.springframework.context.ApplicationContext;

/**
 * @author noah
 * @create 2021-06-07 11:41
 */
public class EmbeddedRocketMQLameWorkerSite extends LameWorkerSite {

  private LameProperties properties;

  public void configureUsingProperties(WorkerServiceConfiguration configuration) {
    if (configuration == null) {
      return;
    }

    this.setCommandExecutorMaxThread(
        SmartUtils.value(configuration.getCommandExecutorMaxThread(),
            this.getCommandExecutorMaxThread()));
    this.setCommandExecutorMinThread(
        SmartUtils.value(configuration.getCommandExecutorMinThread(),
            this.getCommandExecutorMinThread()));

    this.setCommandBackPressureThresholds(
        SmartUtils.value(configuration.getCommandBackPressureThresholds(),
            this.getCommandBackPressureThresholds()));
    this.setEnabledFastFail(
        SmartUtils.value(configuration.isEnabledFastFail(), this.isEnabledFastFail()));
  }

  private WorkerServiceConfiguration getWorkerInvokerConfiguration(IWorkerSiteService invoker) {
    final Class<?> invokerType = invoker.getHandlerType();
    final LameService lameServiceAnno = invokerType.getAnnotation(LameService.class);
    final WorkerSiteConfiguration workerProperties = properties.getWorker();
    if (workerProperties.getServiceConfig() != null) {
      return Optional
          .ofNullable(workerProperties.getServiceConfig().get(lameServiceAnno.name()))
          .orElseGet(() -> workerProperties.getServiceConfig().get(invokerType.getSimpleName()));

    }
    return null;
  }


  public EmbeddedRocketMQLameWorkerSite(
      IWorkerSiteService invoker, IClientSiteResponderRegistrar clientSiteResponderRegistrar,
      LameProperties properties, ApplicationContext applicationContext) {
    super(invoker, clientSiteResponderRegistrar);
    this.properties = properties;
    this.setNamespace(properties.getNamespace());
    this.setNameServer(properties.getWorker().getRocketmq().getNameServer());

    configureUsingProperties(properties.getWorker().getDefaultServiceConfig());
    configureUsingProperties(getWorkerInvokerConfiguration(invoker));
  }

  @Override
  protected void buildConsumer(WorkerSiteMQPushConsumer workerSiteMQPushConsumer) {
    workerSiteMQPushConsumer
        .setEnabledAutoCreateTopic(properties.getWorker().getRocketmq().isEnabledAutoCreateTopic());
    workerSiteMQPushConsumer
        .setTopicQueueNums(properties.getWorker().getRocketmq().getTopicQueueNums());
  }
}
