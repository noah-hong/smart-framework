package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventConsumedLogDO;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventConsumedLogCommonRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.mapper.EventConsumedLogCommonMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
* <p>
 * 消息消费日志 服务实现类
 * </p>
*
* @author System
* @since 2022-03-24
*/
@Repository
@Primary
public class EventConsumedLogCommonRepositoryImpl extends ServiceImpl<EventConsumedLogCommonMapper, EventConsumedLogDO> implements
    EventConsumedLogCommonRepository {

}
