package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded;

import org.smartframework.event.driven.core.IEventListenerProxy;
import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
import org.smartframework.event.driven.rocketmq.RocketMQEventReceiver;
import org.smartframework.rocketmq.SmartMQPushConsumer;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.EventDrivenRocketMQProperties;
import org.springframework.context.ApplicationContext;

/**
 * @author noah
 * @create 2021-05-21 11:49
 */
public class EmbeddedRocketMQEventReceiver extends RocketMQEventReceiver {

  private final EventDrivenRocketMQProperties eventDrivenRocketMQProperties;

  public EmbeddedRocketMQEventReceiver(
      IEventListenerProxy eventListenerProxy, ApplicationContext applicationContext) {
    super(eventListenerProxy);
    this.setStoredEventSerializer(applicationContext.getBean(IEventEnvelopeFormatter.class));
    this.setTopicGenerator(applicationContext.getBean(ITopicGenerator.class));
    this.eventDrivenRocketMQProperties = applicationContext
        .getBean(EventDrivenRocketMQProperties.class);
    init();
  }

  @Override
  protected void initConsumer(SmartMQPushConsumer consumer) {
    consumer.setEnabledAutoCreateTopic(
        eventDrivenRocketMQProperties.getConsumer().isEnabledAutoCreateTopic());
    consumer.setTopicQueueNums(eventDrivenRocketMQProperties.getConsumer().getTopicQueueNums());
    super.initConsumer(consumer);

  }

  @Override
  protected String getNamespace() {
    return eventDrivenRocketMQProperties.getNamespace();
  }

  @Override
  protected String getNameServer() {
    return eventDrivenRocketMQProperties.getNameServer();
  }
}
