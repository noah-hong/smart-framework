package org.smartframework.spring.boot.autoconfigure.event.driven;

import org.smartframework.event.driven.core.DefaultTopicGenerator;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.store.DefaultEventEnvelopFormatter;
import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author noah
 * @create 2021-05-24 23:46
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(IEvent.class)
public class EventDrivenAutoConfigure {

  @Bean
  @ConditionalOnMissingBean(ITopicGenerator.class)
  public ITopicGenerator topicGenerator() {
    return new DefaultTopicGenerator();
  }

  @Bean
  @ConditionalOnMissingBean(IEventEnvelopeFormatter.class)
  public IEventEnvelopeFormatter eventEnvelopeSerializer() {
    return new DefaultEventEnvelopFormatter();
  }


}
