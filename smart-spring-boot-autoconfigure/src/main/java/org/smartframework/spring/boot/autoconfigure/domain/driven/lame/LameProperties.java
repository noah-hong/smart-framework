package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author noah
 * @create 2021-06-05 16:14
 */
@ConfigurationProperties(prefix = "smart.domain-driven.lame")
public class LameProperties {

  private String namespace;
  //  private RocketMQProducerProperties rocketmq = new RocketMQProducerProperties();
  private ClientSiteConfiguration client = new ClientSiteConfiguration();
  private WorkerSiteConfiguration worker = new WorkerSiteConfiguration();
  private ResponderSiteConfiguration responder = new ResponderSiteConfiguration();

  public ClientSiteConfiguration getClient() {
    return client;
  }

  public void setClient(
      ClientSiteConfiguration client) {
    this.client = client;
  }

  public WorkerSiteConfiguration getWorker() {
    return worker;
  }

  public void setWorker(
      WorkerSiteConfiguration worker) {
    this.worker = worker;
  }

  public ResponderSiteConfiguration getResponder() {
    return responder;
  }

  public void setResponder(
      ResponderSiteConfiguration responder) {
    this.responder = responder;
  }

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  public static class ResponderSiteConfiguration {

    private RedissonResonderConfiguration redisson = new RedissonResonderConfiguration();

    public RedissonResonderConfiguration getRedisson() {
      return redisson;
    }

    public void setRedisson(
        RedissonResonderConfiguration redisson) {
      this.redisson = redisson;
    }

  }

//  public static class CommandConfiguration {
//
//    public Integer getWaitRespondTimeout() {
//      return waitRespondTimeout;
//    }
//
//    public void setWaitRespondTimeout(Integer waitRespondTimeout) {
//      this.waitRespondTimeout = waitRespondTimeout;
//    }
//
//    private Integer waitRespondTimeout = 60 * 1000;
//
//  }

  public static class ClientSiteProxyConfiguration {


    public Integer getWaitRespondTimeout() {
      return waitRespondTimeout;
    }

    public void setWaitRespondTimeout(Integer waitRespondTimeout) {
      this.waitRespondTimeout = waitRespondTimeout;
    }

    private Integer waitRespondTimeout = 60 * 1000;
    private Long barrierLockPeriod = 0L;

    public Long getBarrierLockPeriod() {
      return barrierLockPeriod;
    }

    public void setBarrierLockPeriod(Long barrierLockPeriod) {
      this.barrierLockPeriod = barrierLockPeriod;
    }

  }


  public static class ClientSiteConfiguration {

    private Integer receiverCount = 3;

    public RocketMQClientSiteConfiguration getRocketmq() {
      return rocketmq;
    }

    public void setRocketmq(
        RocketMQClientSiteConfiguration rocketmq) {
      this.rocketmq = rocketmq;
    }

    private RocketMQClientSiteConfiguration rocketmq;

    public Integer getReceiverCount() {
      return receiverCount;
    }

    public void setReceiverCount(Integer receiverCount) {
      this.receiverCount = receiverCount;
    }

    public ClientSiteProxyConfiguration getDefaultProxyConfig() {
      return defaultProxyConfig;
    }

    public void setDefaultProxyConfig(
        ClientSiteProxyConfiguration defaultProxyConfig) {
      this.defaultProxyConfig = defaultProxyConfig;
    }

    public Map<String, ClientSiteProxyConfiguration> getProxyConfig() {
      return proxyConfig;
    }

    public void setProxyConfig(
        Map<String, ClientSiteProxyConfiguration> proxyConfig) {
      this.proxyConfig = proxyConfig;
    }

    private ClientSiteProxyConfiguration defaultProxyConfig = new ClientSiteProxyConfiguration();
    private Map<String, ClientSiteProxyConfiguration> proxyConfig = new HashMap<>();

  }


  public static class WorkerSiteConfiguration {

    public RocketMQWorkSiteConfiguration getRocketmq() {
      return rocketmq;
    }

    public void setRocketmq(
        RocketMQWorkSiteConfiguration rocketmq) {
      this.rocketmq = rocketmq;
    }

    private RocketMQWorkSiteConfiguration rocketmq;

    public WorkerServiceConfiguration getDefaultServiceConfig() {
      return defaultServiceConfig;
    }

    public void setDefaultServiceConfig(
        WorkerServiceConfiguration defaultServiceConfig) {
      this.defaultServiceConfig = defaultServiceConfig;
    }

    private WorkerServiceConfiguration defaultServiceConfig = new WorkerServiceConfiguration();
    private Map<String, WorkerServiceConfiguration> serviceConfig = new HashMap<>();

    public Map<String, WorkerServiceConfiguration> getServiceConfig() {
      return serviceConfig;
    }

    public void setServiceConfig(
        Map<String, WorkerServiceConfiguration> serviceConfig) {
      this.serviceConfig = serviceConfig;
    }
  }

  public static class WorkerServiceConfiguration {

    public Integer getCommandExecutorMinThread() {
      return commandExecutorMinThread;
    }

    public void setCommandExecutorMinThread(Integer commandExecutorMinThread) {
      this.commandExecutorMinThread = commandExecutorMinThread;
    }

    public Integer getCommandExecutorMaxThread() {
      return commandExecutorMaxThread;
    }

    public void setCommandExecutorMaxThread(Integer commandExecutorMaxThread) {
      this.commandExecutorMaxThread = commandExecutorMaxThread;
    }

    public Integer getCommandBackPressureThresholds() {
      return commandBackPressureThresholds;
    }

    public void setCommandBackPressureThresholds(Integer commandBackPressureThresholds) {
      this.commandBackPressureThresholds = commandBackPressureThresholds;
    }

    public Boolean isEnabledFastFail() {
      return enabledFastFail;
    }

    public void setEnabledFastFail(Boolean enabledFastFail) {
      this.enabledFastFail = enabledFastFail;
    }

    private Integer commandExecutorMinThread = 10;
    private Integer commandExecutorMaxThread = 20;
    private Integer commandBackPressureThresholds = 20;
    private Boolean enabledFastFail = false;
  }


  enum RedissonNodeType {
    Single,
    Sentinel,
    Cluster
  }

  public static class RedissonResonderConfiguration {

    public String getNodes() {
      return nodes;
    }

    public void setNodes(String nodes) {
      this.nodes = nodes;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public RedissonNodeType getNodeType() {
      return nodeType;
    }

    public void setNodeType(
        RedissonNodeType nodeType) {
      this.nodeType = nodeType;
    }

    public String getMasterName() {
      return masterName;
    }

    public void setMasterName(String masterName) {
      this.masterName = masterName;
    }

    private String masterName;
    private String nodes;
    private String password;
    private RedissonNodeType nodeType;
  }

  public static class RocketMQWorkSiteConfiguration {

    public String getNameServer() {
      return nameServer;
    }

    public void setNameServer(String nameServer) {
      this.nameServer = nameServer;
    }

    private String nameServer;

    public boolean isEnabledAutoCreateTopic() {
      return enabledAutoCreateTopic;
    }

    public void setEnabledAutoCreateTopic(boolean enabledAutoCreateTopic) {
      this.enabledAutoCreateTopic = enabledAutoCreateTopic;
    }

    private boolean enabledAutoCreateTopic = true;

    public int getTopicQueueNums() {
      return topicQueueNums;
    }

    public void setTopicQueueNums(int topicQueueNums) {
      this.topicQueueNums = topicQueueNums;
    }

    private int topicQueueNums = 16;
  }

  public static class RocketMQClientSiteConfiguration {

    private String nameServer;
    private String producerGroup;


    public String getProducerGroup() {
      return producerGroup;
    }

    public void setProducerGroup(String producerGroup) {
      this.producerGroup = producerGroup;
    }

    public String getNameServer() {
      return nameServer;
    }

    public void setNameServer(String nameServer) {
      this.nameServer = nameServer;
    }
  }

}
