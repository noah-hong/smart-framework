package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.common.ThreadFactoryImpl;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.IEventDispatcher;
import org.smartframework.event.driven.core.IEventPublisher;
import org.smartframework.event.driven.core.IPublishableEventContextCustomizer;
import org.smartframework.event.driven.core.PublishableEventContext;
import org.smartframework.event.driven.core.store.IEventQueryStore;
import org.smartframework.event.driven.core.store.IPublishedEventTrackerStore;
import org.smartframework.rocketmq.SmartMQPushConsumer;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.EventDrivenJpaStoreAutoConfiguration;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded.EmbeddedRocketMQEventPublisher;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded.EmbeddedRocketMQProducer;
import org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.publish.RocketMQPublishableMessageNotifierAspect;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author noah
 * @create 2021-05-21 17:14
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(EventDrivenRocketMQProperties.class)
@AutoConfigureAfter({RocketMQAutoConfiguration.class, EventDrivenJpaStoreAutoConfiguration.class})
@Import(RocketMQAutoConfiguration.class)
//@EnableRetry
@ConditionalOnBean({RocketMQAutoConfiguration.class})
@ConditionalOnProperty(prefix = "smart.event-driven.rocketmq", value = "publishable-notifier-topic", matchIfMissing = false)
public class RocketMQPublisherAutoConfiguration implements ApplicationRunner,
    ApplicationContextAware {

  private final static Logger log = LoggerFactory
      .getLogger(RocketMQPublisherAutoConfiguration.class);
  @Autowired
  private EventDrivenRocketMQProperties properties;

  @Autowired
  private ObjectProvider<List<IPublishableEventContextCustomizer>> publishableEventContextCustomizers;

  public RocketMQPublisherAutoConfiguration() {
  }

  @Bean
  @ConditionalOnMissingBean(IEventPublisher.class)
  public IEventPublisher eventPublisher(
      ObjectProvider<IEventQueryStore> eventQueryStore,
      ObjectProvider<IPublishedEventTrackerStore> trackerStore,
      ObjectProvider<IEventDispatcher> dispatcher) {
    return new EmbeddedRocketMQEventPublisher(eventQueryStore.getIfAvailable(),
        trackerStore.getIfAvailable(),
        dispatcher.getIfAvailable());
  }

  @Bean
  public RocketMQPublishableMessageNotifierAspect rocketMQPublishableNotifyAspect(
      ObjectProvider<EmbeddedRocketMQProducer> producer) {
    final RocketMQPublishableMessageNotifierAspect rocketMQPublishableMessageNotifierAspect = new RocketMQPublishableMessageNotifierAspect(
        properties.getPublishableNotifierTopic(),
        Objects.requireNonNull(producer.getIfAvailable()).getInstance());
    rocketMQPublishableMessageNotifierAspect.setPublishContextCustomizers(
        publishableEventContextCustomizers.getIfAvailable(ArrayList::new));
    return rocketMQPublishableMessageNotifierAspect;
  }


  @Override
  public void run(ApplicationArguments args) {
    createPublishableMessageConsumer();
//    ForkJoinPool.commonPool().submit(this::createPublishableMessageConsumer);
  }

  public void createPublishableMessageConsumer() {
    try {
      String topic = properties.getPublishableNotifierTopic();
      String group = properties.getPublishableNotifierTopic() + "-group";
      final IEventPublisher eventPublisher = applicationContext.getBean(IEventPublisher.class);

      SmartMQPushConsumer publishableMessageConsumer =
          new SmartMQPushConsumer(properties.getNamespace(), group);
      publishableMessageConsumer.setTopicQueueNums(1);
      ThreadPoolExecutor executor =
          new ThreadPoolExecutor(
              1,
              1,
              1000 * 60,
              TimeUnit.MILLISECONDS,
              new LinkedBlockingQueue<Runnable>(1),
              new ThreadFactoryImpl("EventListener4PublishableThread_"),
              new ThreadPoolExecutor.DiscardPolicy());

      publishableMessageConsumer.subscribe(topic, "*");
      publishableMessageConsumer.setNamesrvAddr(properties.getNameServer());
      publishableMessageConsumer
          .registerMessageListener((MessageListenerOrderly) (list, consumeOrderlyContext) -> {
            for (MessageExt messageExt : list) {
              byte[] body = messageExt.getBody();
              String tagName = new String(body);
              final PublishableEventContext publishableEventContext = new PublishableEventContext();
              final Map<String, String> properties = messageExt.getProperties();
              for (String key : properties.keySet()) {
                publishableEventContext.putPropertyWithPrefix(key, properties.get(key));
              }
              if ("All".equalsIgnoreCase(tagName)) {
                executor.submit(() -> {
                  eventPublisher.publish(publishableEventContext);
                });
              } else {
                executor.submit(() -> eventPublisher.publish(tagName, publishableEventContext));
              }
            }
            return ConsumeOrderlyStatus.SUCCESS;
          });
      publishableMessageConsumer.setMessageModel(MessageModel.CLUSTERING);
      publishableMessageConsumer.setInstanceName(UUID.randomUUID().toString());
      publishableMessageConsumer.start();
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
    }
  }

  private ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

}
