package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.smartframework.event.driven.core.store.IEventStore;
import org.smartframework.event.driven.core.store.IStoredEventCustomizer;
import org.smartframework.event.driven.core.store.StoredEvent;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventSourcingInfo;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventSourcingRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventSourcingInfoDO;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventSourcingInfoCommonRepository;

/**
 * @author noah
 * @create 2021-05-24 17:49
 */
public class MybatisEventStore implements IEventStore {

  public final EventSourcingInfoCommonRepository eventSourcingRepository;
  private IStoredEventCustomizer storedEventCustomizer;

  public IStoredEventCustomizer getStoredEventCustomizer() {
    return storedEventCustomizer;
  }

  public void setStoredEventCustomizer(
      IStoredEventCustomizer storedEventCustomizer) {
    this.storedEventCustomizer = storedEventCustomizer;
  }


  public MybatisEventStore(
      EventSourcingInfoCommonRepository eventSourcingRepository) {

    this.eventSourcingRepository = eventSourcingRepository;
  }


  public StoredEvent toModel(EventSourcingInfo eventSourcing) {
    StoredEvent storedEvent = new StoredEvent();
    storedEvent.setId(eventSourcing.getId());
    storedEvent.setPayload(eventSourcing.getEventBody());

    storedEvent.setTopic(eventSourcing.getTopic());
    storedEvent.setSubTopic(eventSourcing.getSubTopic());
    storedEvent.setTagName(eventSourcing.getTagName());
    storedEvent.setEventId(eventSourcing.getEventId());
    storedEvent.setPersistenceId(eventSourcing.getPersistenceId());
    storedEvent.setLocation(eventSourcing.getLocation());
    storedEvent.setEventType(eventSourcing.getEventType());
    storedEvent.setSenderName(eventSourcing.getSender());
    storedEvent.setSenderId(eventSourcing.getSenderId());
    storedEvent.setSenderScope(eventSourcing.getSenderScope());
    storedEvent.setStoredTime(eventSourcing.getStoredTime());
    storedEvent.setVersion(eventSourcing.getEventVersion());
    storedEvent.setSerializer(eventSourcing.getSerializer());
    if (!StringUtils.isEmpty(eventSourcing.getProperties())) {
      try {
        storedEvent.setProperties(JSON.parseObject(eventSourcing.getProperties(),
            new TypeReference<HashMap<String, String>>() {
            }));
      } catch (Exception ex) {

      }
    }

    return storedEvent;
  }

  @Override
  public void append(StoredEvent event) {
    Objects.requireNonNull(event);
    final IStoredEventCustomizer storedEventCustomizer = getStoredEventCustomizer();
    if (storedEventCustomizer != null) {
      storedEventCustomizer.customize(event);
    }
    EventSourcingInfoDO eventSourcing = toDO(event);
    eventSourcingRepository.save(eventSourcing);
  }

  private EventSourcingInfoDO toDO(StoredEvent event) {
    EventSourcingInfoDO eventSourcing = new EventSourcingInfoDO();
    eventSourcing.setEventId(event.getEventId());
    eventSourcing.setLocation(event.getLocation());
    eventSourcing.setTagName(event.getTagName());
    eventSourcing.setPersistenceId(event.getPersistenceId());
    eventSourcing.setEventType(event.getEventType());
    eventSourcing.setEventBody(event.getPayload());
    eventSourcing.setEventVersion(event.getVersion());
    eventSourcing.setSender(event.getSenderName());
    eventSourcing.setSenderId(event.getSenderId());
    eventSourcing.setSenderScope(event.getSenderScope());
    eventSourcing.setStoredTime(event.getStoredTime());
    eventSourcing.setSerializer(event.getSerializer());
    eventSourcing.setTopic(event.getTopic());
    eventSourcing.setSubTopic(event.getSubTopic());
    if (event.getProperties() != null) {
      eventSourcing.setProperties(JSON.toJSONString(event.getProperties()));
    } else {
      eventSourcing.setProperties("");
    }

    return eventSourcing;
  }

  @Override
  public void appends(StoredEvent... events) {
    List<EventSourcingInfoDO> list = new ArrayList<>(events.length);
    for (StoredEvent event : events) {
      EventSourcingInfoDO eventSourcing = toDO(event);
      list.add(eventSourcing);
    }
    eventSourcingRepository.saveBatch(list);
  }
}
