package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import java.util.List;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.core.communication.ICaller;
import org.smartframework.domain.driven.core.communication.ICallerBarrierFactory;
import org.smartframework.domain.driven.core.communication.ICallerInterceptor;
import org.smartframework.domain.driven.core.communication.IClientSite;
import org.smartframework.domain.driven.core.communication.IClientSiteProcessResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IClientSiteRouter;
import org.smartframework.domain.driven.core.communication.IWorkerSiteDispatcher;
import org.smartframework.domain.driven.lame.client.LameCaller;
import org.smartframework.domain.driven.lame.client.LameCallerBarrierFactory;
import org.smartframework.domain.driven.lame.client.LameClientSiteRouter;
import org.smartframework.domain.driven.lame.client.LameWorkerSiteDispatcher;
import org.smartframework.domain.driven.lame.client.LameClientSiteFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

/**
 * @author noah
 * @create 2021-06-05 20:43
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(LameProperties.class)
@ConditionalOnBean(value = LameResponderAutoConfigure.class, annotation = EnableLameClient.class)
@AutoConfigureAfter(LameResponderAutoConfigure.class)
@Import(LameResponderAutoConfigure.class)
@ConditionalOnClass(IClientSite.class)
//@ConditionalOnProperty(prefix = "smart.domain-driven.lame.client.rocketmq", value = "name-server", matchIfMissing = false)
public class LameClientSiteAutoConfigure implements ApplicationRunner {

  @Autowired
  private LameProperties properties;
  @Autowired
  private ApplicationContext applicationContext;
  @Autowired
  private Environment environment;

  @Autowired(required = false)
  private List<ICallerInterceptor> interceptors;

  @Bean
  @ConditionalOnMissingBean
  public LameClientRocketMQProducer lameClientRocketMQProducer() {
    return new LameClientRocketMQProducer(properties);
  }

  @Bean
  @ConditionalOnMissingBean(IWorkerSiteDispatcher.class)
  public IWorkerSiteDispatcher workerSiteDispatcher(
      LameClientRocketMQProducer lameClientRocketMQProducer,
      IClientSiteProcessResponderRegistrar clientSiteProcessResponderRegistrar) {
    final LameWorkerSiteDispatcher lameWorkerSiteDispatcher = new LameWorkerSiteDispatcher(
        lameClientRocketMQProducer.getInstance(), clientSiteProcessResponderRegistrar);
//    if (properties.getClient().getDefaultProxyConfig() != null) {
//      lameWorkerSiteDispatcher.setWaitRespondTimeout(
//          properties.getClient().getDefaultProxyConfig().getWaitRespondTimeout());
//    }

    return lameWorkerSiteDispatcher;
  }

  @Bean
  @ConditionalOnMissingBean
  public IClientSiteRouter clientSiteRouter() {
    return new LameClientSiteRouter();
  }

  @Bean
  @ConditionalOnMissingBean
  public LameClientSiteFactory redissonLameClientSiteFactory(
      LameResponderRedissonClient redissonClient
      , IClientSiteRouter clientSiteRouter
      , IClientSiteProcessResponderRegistrar clientSiteProcessResponderRegistrar,
      LameProperties lameProperties) {
    return new LameClientSiteFactory(redissonClient.getInstance(), clientSiteRouter,
        clientSiteProcessResponderRegistrar, lameProperties.getNamespace());
  }

  @Bean
  @ConditionalOnMissingBean
  public ICallerBarrierFactory callerBarrierFactory() {
    return new LameCallerBarrierFactory();
  }

  @Bean
  @ConditionalOnMissingBean
  public ICaller domainWorkerCaller(IClientSiteRouter router, IWorkerSiteDispatcher dispatcher,
      ICallerBarrierFactory callerBarrierFactory) {
    final LameCaller lameCaller = new LameCaller(router, dispatcher, callerBarrierFactory);
    lameCaller.addInterceptors(interceptors);
    return lameCaller;
  }

  private static final Logger log = LoggerFactory.getLogger(LameClientSiteAutoConfigure.class);

  @Override
  public void run(ApplicationArguments args) throws Exception {
    final LameClientSiteFactory clientFactory = applicationContext
        .getBean(LameClientSiteFactory.class);
    final Integer receiverCount = properties.getClient().getReceiverCount();
    log.info("服务端响应信息接收者启动数：{}", receiverCount);
    IntStream.range(0, receiverCount).forEach(i -> {
      final IClientSite client = clientFactory.create();
      client.start();
      log.debug("第{}个接收者,LAME-CLIENT-ID: {}", i + 1, client.getClientId());
    });

  }
}
