package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Optional;
import org.smartframework.core.SmartUtils;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.IBaseCommand;
import org.smartframework.domain.driven.core.ICommand;
import org.smartframework.domain.driven.core.communication.ICaller;
import org.smartframework.domain.driven.core.communication.IRoutableCommand;
import org.smartframework.domain.driven.lame.Constants;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.LameProperties.ClientSiteProxyConfiguration;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.annotation.LameClient;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.Assert;

/**
 * @author noah
 * @create 2021-06-06 18:36
 */
public class LameClientFactoryBean implements FactoryBean<Object>, InitializingBean,
    ApplicationContextAware {

  public Class<?> getProxyType() {
    return proxyType;
  }

  public void setProxyType(Class<?> proxyType) {
    this.proxyType = proxyType;
  }

  private Class<?> proxyType;

  public Integer getProxyTimeout() {
    return proxyTimeout;
  }

  public void setProxyTimeout(Integer proxyTimeout) {
    this.proxyTimeout = proxyTimeout;
  }

  private Integer proxyTimeout = 60 * 1000;

  public Long getBarrierLockPeriod() {
    return barrierLockPeriod;
  }

  private Long barrierLockPeriod = 0L;

  public void configureUsingProperties(ClientSiteProxyConfiguration configuration) {
    if (configuration == null) {
      return;
    }
    this.proxyTimeout = SmartUtils.value(configuration
        .getWaitRespondTimeout(), this.getProxyTimeout());
    this.barrierLockPeriod = SmartUtils.value(configuration
        .getBarrierLockPeriod(), this.getBarrierLockPeriod());
  }

  private ClientSiteProxyConfiguration getClientSiteProxyConfiguration(
      LameProperties lameProperties) {
    if (lameProperties.getClient().getProxyConfig() != null) {
      final LameClient annotation = this.proxyType.getAnnotation(LameClient.class);
      return Optional
          .ofNullable(lameProperties.getClient().getProxyConfig().get(annotation.name()))
          .orElseGet(
              () -> lameProperties.getClient().getProxyConfig().get(proxyType.getSimpleName()));
    }
    return null;
  }


  @Override
  public Object getObject() {

    final LameProperties lameProperties = applicationContext.getBean(LameProperties.class);
    configureUsingProperties(lameProperties.getClient().getDefaultProxyConfig());
    configureUsingProperties(getClientSiteProxyConfiguration(lameProperties));
    final ICaller proxy = this.applicationContext
        .getBean(ICaller.class);
    return Proxy.newProxyInstance(this.proxyType.getClassLoader(),
        new Class<?>[]{this.proxyType},
        new LameClientInvocationHandler(proxy, proxyType, this));
  }

  class LameClientInvocationHandler implements InvocationHandler {

    private final ICaller proxy;
    private final Class<?> proxyType;
    private final Integer proxyTimeout;
    private final Long barrierLockPeriod;

    public LameClientInvocationHandler(ICaller proxy, Class<?> proxyType,
        LameClientFactoryBean lameClientFactoryBean) {
      this.proxy = proxy;
      this.proxyType = proxyType;
      this.proxyTimeout = lameClientFactoryBean.getProxyTimeout();
      this.barrierLockPeriod = lameClientFactoryBean.getBarrierLockPeriod();

    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
      if (method.getName().equalsIgnoreCase(Constants.COMMAND_METHOD_NAME)
          && args.length >= 1
          && args[0] instanceof IRoutableCommand
          && method.getReturnType().isAssignableFrom(CommandResponse.class)) {

        return this.proxy
            .call(proxyType, (IRoutableCommand) args[0], this.proxyTimeout, barrierLockPeriod);
      }
      return CommandResponse.error("代理方法参数不合规,请检查后重试");
    }


  }


  @Override
  public Class<?> getObjectType() {
    return this.proxyType;
  }

  @Override
  public void afterPropertiesSet() throws Exception {

    Assert.notNull(this.proxyType, "代理类型必须设置");
  }

  private ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    this.applicationContext = applicationContext;
  }
}
