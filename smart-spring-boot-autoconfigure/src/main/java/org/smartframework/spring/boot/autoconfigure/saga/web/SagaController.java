package org.smartframework.spring.boot.autoconfigure.saga.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.saga.core.SagaInstance;
import org.smartframework.saga.core.SagaInstanceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author noah
 * @create 2021-10-14 22:50
 */
@RestController
@Api(tags = "Saga")
@RequestMapping("${smart.saga.web.root-path:v1/saga}")
public class SagaController {

  Logger log = LoggerFactory.getLogger(SagaController.class);

  @Autowired
  SagaInstanceFactory sagaInstanceFactory;

  @PostMapping("skip/current")
  @ApiOperation("跳过当前步骤")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> skipCurrent(
      @ApiParam(value = "sagaId", required = true) @RequestParam(required = true) String sagaId) {
    final SagaInstance sagaInstance = sagaInstanceFactory.skipCurrent(sagaId);
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }

  @PostMapping("recover")
  @ApiOperation("继续执行")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> recover(
      @ApiParam(value = "sagaId", required = true) @RequestParam(required = true) String sagaId) {
    final SagaInstance sagaInstance = sagaInstanceFactory.recover(sagaId);
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }


  @PostMapping("recreate")
  @ApiOperation("重新创建")
  public ResponseEntity<SagaApiResponseEnvelope<SagaInstanceVO>> recreate(
      @ApiParam(value = "sagaId", required = true) @RequestParam(required = true) String sagaId) {
    final SagaInstance sagaInstance = sagaInstanceFactory.restart(sagaId);
    return ResponseEntity
        .ok(SagaResponseStatus.SUCCESS.toEnvelope(SagaInstanceVO.from(sagaInstance)));
  }


}
