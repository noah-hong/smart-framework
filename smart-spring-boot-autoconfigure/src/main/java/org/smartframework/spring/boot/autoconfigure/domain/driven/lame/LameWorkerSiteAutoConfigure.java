package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import java.util.List;
import org.smartframework.domain.driven.core.communication.CommandHandlerInterceptor;
import org.smartframework.domain.driven.core.communication.ICallerInterceptor;
import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IWorkerSite;
import org.smartframework.domain.driven.core.communication.IWorkerSiteService;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.annotation.LameService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.GenericApplicationContext;

import java.util.Map;

/**
 * @author noah
 * @create 2021-06-03 13:41
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(LameProperties.class)
@ConditionalOnBean(value = LameResponderAutoConfigure.class, annotation = EnableLameService.class)
@AutoConfigureAfter(LameResponderAutoConfigure.class)
@Import(LameResponderAutoConfigure.class)
@ConditionalOnProperty(prefix = "smart.domain-driven.lame.worker.rocketmq", value = "name-server", matchIfMissing = false)
@ConditionalOnClass(IWorkerSite.class)
public class LameWorkerSiteAutoConfigure implements ApplicationRunner, ApplicationContextAware {

  private GenericApplicationContext applicationContext;


  @Bean
  @ConditionalOnMissingBean(LameWokerSiteFactory.class)
  public LameWokerSiteFactory lameWokerSiteFactory(
      IClientSiteResponderRegistrar clientSiteResponderRegistrar, LameProperties properties) {
    return new LameWokerSiteFactory(clientSiteResponderRegistrar, properties, applicationContext);
  }

  @Bean
  @ConditionalOnMissingBean(LameWorkerSiteServiceFactory.class)
  public LameWorkerSiteServiceFactory lameWorkerSiteInvokerFactory(
      IClientSiteResponderRegistrar clientSiteResponderRegistrar) {
    return new LameWorkerSiteServiceFactory(clientSiteResponderRegistrar);
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
    final Map<String, Object> map = applicationContext
        .getBeansWithAnnotation(LameService.class);
    for (Object invoker : map.values()) {
      this.initReceiver(invoker);
    }
  }

  private void initReceiver(Object invoker) {
    final LameWokerSiteFactory lameWokerSiteFactory = applicationContext
            .getBean(LameWokerSiteFactory.class);
    final LameWorkerSiteServiceFactory lameWorkerSiteInvokerFactory = applicationContext
            .getBean(LameWorkerSiteServiceFactory.class);
    final Map<String, CommandHandlerInterceptor> interceptorMap = applicationContext.getBeansOfType(CommandHandlerInterceptor.class);

    final IWorkerSiteService workerSiteInvoker = lameWorkerSiteInvokerFactory.create(invoker, interceptorMap.values());

    final IWorkerSite wokerSite = lameWokerSiteFactory.create(workerSiteInvoker);
    wokerSite.start();

  }


  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = (GenericApplicationContext) applicationContext;
  }
}
