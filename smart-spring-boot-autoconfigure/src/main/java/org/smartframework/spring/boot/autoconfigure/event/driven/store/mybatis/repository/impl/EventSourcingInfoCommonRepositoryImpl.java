package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventSourcingInfoDO;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventSourcingInfoCommonRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.mapper.EventSourcingInfoCommonMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
* <p>
 * 事件溯源 服务实现类
 * </p>
*
* @author System
* @since 2022-03-24
*/
@Repository
@Primary
public class EventSourcingInfoCommonRepositoryImpl extends ServiceImpl<EventSourcingInfoCommonMapper, EventSourcingInfoDO> implements
    EventSourcingInfoCommonRepository {

}
