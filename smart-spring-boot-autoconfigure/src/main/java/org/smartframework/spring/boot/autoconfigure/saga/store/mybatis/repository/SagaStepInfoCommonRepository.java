package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.entity.SagaStepInfoDO;

/**
* <p>
 * saga执行步骤信息 服务类
 * </p>
*
* @author System
* @since 2022-04-05
*/
public interface SagaStepInfoCommonRepository extends IService<SagaStepInfoDO> {

}
