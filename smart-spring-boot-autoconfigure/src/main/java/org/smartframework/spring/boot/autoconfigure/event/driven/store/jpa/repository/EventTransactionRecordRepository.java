package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository;

import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventTransactionRecord;
import org.springframework.data.repository.CrudRepository;

/**
 * @author noah
 * @create 2020-05-21 18:20
 */
public interface EventTransactionRecordRepository
    extends CrudRepository<EventTransactionRecord, Long> {

}
