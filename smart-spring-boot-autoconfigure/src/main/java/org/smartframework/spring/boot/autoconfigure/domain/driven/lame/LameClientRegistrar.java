package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.annotation.LameClient;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

/**
 * @author noah
 * @create 2021-05-12 17:32
 */

public class LameClientRegistrar implements ImportBeanDefinitionRegistrar,
    ResourceLoaderAware, EnvironmentAware {

  private ResourceLoader resourceLoader;

  private Environment environment;

  @Override
  public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
      BeanDefinitionRegistry registry, BeanNameGenerator importBeanNameGenerator) {
    AnnotationAttributes annotationAttributes =
        AnnotationAttributes.fromMap(
            importingClassMetadata
                .getAnnotationAttributes(EnableLameClient.class.getName()));

    Optional.ofNullable(annotationAttributes)
        .ifPresent(annoAttrs -> {
          Set<String> basePackages = new HashSet<>();
          for (String pkg : (String[]) annoAttrs.get("value")) {
            if (StringUtils.hasText(pkg)) {
              basePackages.add(pkg);
            }
          }
          for (Class<?> clazz : (Class[]) annoAttrs.get("basePackageClasses")) {
            basePackages.add(ClassUtils.getPackageName(clazz));
          }
          if (basePackages.isEmpty()) {
            basePackages.add(
                ClassUtils.getPackageName(importingClassMetadata.getClassName()));
          }

          ClassPathScanningCandidateComponentProvider scanner = getScanner();
          scanner.setResourceLoader(this.resourceLoader);
          AnnotationTypeFilter annotationTypeFilter = new AnnotationTypeFilter(
              LameClient.class);
          scanner.addIncludeFilter(annotationTypeFilter);

          int i = 0;
          for (String basePackage : basePackages) {
            Set<BeanDefinition> candidateComponents = scanner
                .findCandidateComponents(basePackage);
            for (BeanDefinition candidateComponent : candidateComponents) {
              if (candidateComponent instanceof AnnotatedBeanDefinition) {
                // verify annotated class is an interface
                AnnotatedBeanDefinition beanDefinition = (AnnotatedBeanDefinition) candidateComponent;
                AnnotationMetadata annotationMetadata = beanDefinition.getMetadata();
                Assert.isTrue(annotationMetadata.isInterface(),
                    "@LameClient can only be specified on an interface");

                Map<String, Object> attributes = annotationMetadata
                    .getAnnotationAttributes(
                        LameClient.class.getCanonicalName());

                boolean primary = (Boolean) attributes.get("primary");
                String alias = getQualifier(attributes);
                String className = annotationMetadata.getClassName();
                if (!StringUtils.hasText(alias)) {
                  alias = annotationMetadata.getClass().getSimpleName() + i++;
                }
                BeanDefinitionBuilder definition = BeanDefinitionBuilder
                    .genericBeanDefinition(LameClientFactoryBean.class);

                definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
                definition.addPropertyValue("proxyType", className);
                AbstractBeanDefinition abstractBeanDefinition = definition.getBeanDefinition();
                abstractBeanDefinition.setAttribute(FactoryBean.OBJECT_TYPE_ATTRIBUTE, className);
                abstractBeanDefinition.setPrimary(primary);
                BeanDefinitionHolder holder = new BeanDefinitionHolder(
                    abstractBeanDefinition, className,
                    new String[]{alias});
                BeanDefinitionReaderUtils.registerBeanDefinition(holder, registry);
              }
            }
          }
        });
  }

  private String getQualifier(Map<String, Object> client) {
    if (client == null) {
      return null;
    }
    String qualifier = (String) client.get("qualifier");
    String name = (String) client.get("value");
    if (StringUtils.hasText(qualifier)) {
      return qualifier;
    } else if (StringUtils.hasText(name)) {
      return name;
    }
    return null;
  }


  protected ClassPathScanningCandidateComponentProvider getScanner() {
    return new ClassPathScanningCandidateComponentProvider(false, this.environment) {
      @Override
      protected boolean isCandidateComponent(
          AnnotatedBeanDefinition beanDefinition) {
        boolean isCandidate = false;
        if (beanDefinition.getMetadata().isIndependent()) {
          if (!beanDefinition.getMetadata().isAnnotation()) {
            isCandidate = true;
          }
        }
        return isCandidate;
      }
    };
  }


  @Override
  public void setEnvironment(Environment environment) {
    this.environment = environment;
  }

  @Override
  public void setResourceLoader(ResourceLoader resourceLoader) {
    this.resourceLoader = resourceLoader;
  }
}

