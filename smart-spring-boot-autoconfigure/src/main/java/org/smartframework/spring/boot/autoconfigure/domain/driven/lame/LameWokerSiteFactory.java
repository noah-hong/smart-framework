package org.smartframework.spring.boot.autoconfigure.domain.driven.lame;

import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.core.communication.IWorkerSite;
import org.smartframework.domain.driven.core.communication.IWorkerSiteService;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.embedded.EmbeddedRocketMQLameWorkerSite;
import org.springframework.context.ApplicationContext;

/**
 * @author noah
 * @create 2021-06-07 11:52
 */
public class LameWokerSiteFactory {

  private final LameProperties properties;
  private final IClientSiteResponderRegistrar clientSiteResponderRegistrar;
  private final ApplicationContext applicationContext;

  public LameWokerSiteFactory(
      IClientSiteResponderRegistrar clientSiteResponderRegistrar, LameProperties properties,
      ApplicationContext applicationContext) {
    this.clientSiteResponderRegistrar = clientSiteResponderRegistrar;
    this.properties = properties;
    this.applicationContext = applicationContext;
  }

  public IWorkerSite create(IWorkerSiteService invoker) {
    return new EmbeddedRocketMQLameWorkerSite(invoker, clientSiteResponderRegistrar, properties,
        applicationContext);
  }

}
