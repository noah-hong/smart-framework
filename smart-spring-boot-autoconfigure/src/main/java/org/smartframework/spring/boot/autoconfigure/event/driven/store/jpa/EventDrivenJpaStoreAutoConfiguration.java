package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa;

import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.store.*;
import org.smartframework.spring.boot.autoconfigure.event.driven.EventDrivenAutoConfigure;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventConsumedLogRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventSourcingRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventTrackerRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventTransactionRecordRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.support.*;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

/**
 * @author noah
 * @create 2021-05-06 11:18
 */
@Configuration(proxyBeanMethods = false)
@EntityScan
@EnableJpaRepositories
@AutoConfigureAfter({EventDrivenAutoConfigure.class, TransactionAutoConfiguration.class,
    JpaRepositoriesAutoConfiguration.class})
@Import({EventDrivenAutoConfigure.class, TransactionAutoConfiguration.class})
@ConditionalOnBean({JpaRepositoriesAutoConfiguration.class, EventDrivenAutoConfigure.class,
        DataSource.class})
public class EventDrivenJpaStoreAutoConfiguration {

  @Bean
  @ConditionalOnMissingBean(IExcatlyOnceSupporter.class)
  public IExcatlyOnceSupporter jpaExcatlyOnceSupport(
      EventTransactionRecordRepository recordRepository) {
    return new JpaExcatlyOnceSupporter(recordRepository);
  }

  @Bean
  @ConditionalOnMissingBean(IConsumedMessageLogger.class)
  public IConsumedMessageLogger jpaComsumedMessageLogger(EventConsumedLogRepository recordRepository) {
    return new JpaComsumedMessageLogger(recordRepository);
  }

  @Bean
  @ConditionalOnMissingBean(IEventStore.class)
  public IEventStore jpaEventStore(EventSourcingRepository eventSourcingRepository, ObjectProvider<IStoredEventCustomizer> storedEventCustomizer) {
    final JpaEventStore jpaEventStore = new JpaEventStore(eventSourcingRepository);
    jpaEventStore.setStoredEventCustomizer(storedEventCustomizer.getIfAvailable(() -> event -> {
    }));
    return jpaEventStore;
  }

  @Bean
  @ConditionalOnMissingBean(IEventQueryStore.class)
  public IEventQueryStore jpaEventQueryStore(EventSourcingRepository eventSourcingRepository) {
    return new JpaEventQueryStore(eventSourcingRepository);
  }

  @Bean
  @ConditionalOnMissingBean(IEventEnvelopStore.class)
  public IEventEnvelopStore jpaEventEnvelopStore(IEventStore eventStore,
      ITopicGenerator topicGenerator,
      IEventEnvelopeFormatter serializer) {
    return new JpaEventEnvelopStoreAdapter(eventStore, topicGenerator, serializer);
  }

  @Bean
  @ConditionalOnMissingBean(ITransactionExecutor.class)
  public ITransactionExecutor jpaTransactionExecutor(TransactionTemplate transactionTemplate) {
    return new JpaTransactionExecutor(transactionTemplate);
  }

  @Bean
  @ConditionalOnMissingBean(IPublishedEventTrackerStore.class)
  public IPublishedEventTrackerStore jpaPublishedEventTrackerStore(
      EventTrackerRepository eventTrackerRepository) {
    return new JpaMessageTrackerPublishedStore(eventTrackerRepository);
  }


}
