package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventTransactionRecordDO;

/**
* <p>
 * 消息队列消费记录，幂等 服务类
 * </p>
*
* @author System
* @since 2022-03-24
*/
public interface EventTransactionRecordCommonRepository extends IService<EventTransactionRecordDO> {

}
