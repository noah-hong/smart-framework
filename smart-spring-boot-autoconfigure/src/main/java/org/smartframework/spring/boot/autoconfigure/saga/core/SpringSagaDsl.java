package org.smartframework.spring.boot.autoconfigure.saga.core;

import org.smartframework.saga.core.DefaultSagaDefinitionBuilder;
import org.smartframework.saga.core.DefaultSagaStepBuilder;
import org.smartframework.saga.core.Saga;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author noah
 * @create 2022-04-14 16:21
 */
public abstract class SpringSagaDsl<Data> implements Saga<Data>, ApplicationContextAware {

  protected SpringSagaStepBuilder<Data> step(String stepName) {
    DefaultSagaDefinitionBuilder<Data> builder = new DefaultSagaDefinitionBuilder<>(this);
    return new SpringSagaStepBuilder<>(stepName, builder, applicationContext);
  }

  ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
}
