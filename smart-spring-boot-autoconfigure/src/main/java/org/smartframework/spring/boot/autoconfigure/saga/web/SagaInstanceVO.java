package org.smartframework.spring.boot.autoconfigure.saga.web;

import java.io.Serializable;
import java.util.Map;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.smartframework.saga.core.SagaInstance;

/**
 * @author noah
 * @create 2021-10-13 18:14
 */
public class SagaInstanceVO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String sagaId;

  private String sagaName;

  private String sagaType;

  private int currentlyExecuting;

  private boolean compensating;

  private boolean endState;

  private String executingException;

  private String compensatingException;

  private String lastMessage;

  private boolean isStop;

  private String currentlyStepName;

  private Object sagaData;

  private Map<String, String> executingMessages;

  private Map<String, String> compensatingMessages;

  public String getSagaId() {
    return sagaId;
  }

  public void setSagaId(String sagaId) {
    this.sagaId = sagaId;
  }

  public String getSagaName() {
    return sagaName;
  }

  public void setSagaName(String sagaName) {
    this.sagaName = sagaName;
  }

  public String getSagaType() {
    return sagaType;
  }

  public void setSagaType(String sagaType) {
    this.sagaType = sagaType;
  }

  public int getCurrentlyExecuting() {
    return currentlyExecuting;
  }

  public void setCurrentlyExecuting(int currentlyExecuting) {
    this.currentlyExecuting = currentlyExecuting;
  }

  public boolean isCompensating() {
    return compensating;
  }

  public void setCompensating(boolean compensating) {
    this.compensating = compensating;
  }

  public boolean isEndState() {
    return endState;
  }

  public void setEndState(boolean endState) {
    this.endState = endState;
  }

  public String getExecutingException() {
    return executingException;
  }

  public void setExecutingException(String executingException) {
    this.executingException = executingException;
  }

  public String getCompensatingException() {
    return compensatingException;
  }

  public void setCompensatingException(String compensatingException) {
    this.compensatingException = compensatingException;
  }

  public String getLastMessage() {
    return lastMessage;
  }

  public void setLastMessage(String lastMessage) {
    this.lastMessage = lastMessage;
  }

  public boolean isStop() {
    return isStop;
  }

  public void setStop(boolean stop) {
    isStop = stop;
  }

  public String getCurrentlyStepName() {
    return currentlyStepName;
  }

  public void setCurrentlyStepName(String currentlyStepName) {
    this.currentlyStepName = currentlyStepName;
  }

  public Object getSagaData() {
    return sagaData;
  }

  public void setSagaData(Object data) {
    this.sagaData = data;
  }

  public Map<String, String> getExecutingMessages() {
    return executingMessages;
  }

  public void setExecutingMessages(Map<String, String> executingMessages) {
    this.executingMessages = executingMessages;
  }

  public Map<String, String> getCompensatingMessages() {
    return compensatingMessages;
  }

  public void setCompensatingMessages(Map<String, String> compensatingMessages) {
    this.compensatingMessages = compensatingMessages;
  }

  private String status;

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStatus() {
    return status;
//    if(isStop() && isCompensating()){
//      return "回滚终止";
//    }
//    if(isEndState() && isCompensating()){
//      return "回滚完成";
//    }
//    if(isEndState() && !isCompensating()){
//      return "执行完成";
//    }
//
//    return "未知";
  }


  public static SagaInstanceVO from(SagaInstance sagaInstance) {
    SagaInstanceVO sagaInstanceVO = new SagaInstanceVO();
    sagaInstanceVO.setStatus(sagaInstance.getSagaStatus().message());
    sagaInstanceVO.setSagaType(sagaInstance.getSagaType().getName());
    sagaInstanceVO.setCurrentlyExecuting(sagaInstance.getCurrentlyExecuting());
    sagaInstanceVO.setCompensating(sagaInstance.isCompensating());
    sagaInstanceVO.setEndState(sagaInstance.isEndState());
    if (sagaInstance.getExecutingException() != null) {
      final Throwable executingRootException = ExceptionUtils
          .getRootCause(sagaInstance.getExecutingException());
      sagaInstanceVO.setExecutingException(executingRootException.toString());
    }
    if (sagaInstance.getCompensatingException() != null) {
      final Throwable compensatingRootException = ExceptionUtils
          .getRootCause(sagaInstance.getCompensatingException());
      sagaInstanceVO.setCompensatingException(compensatingRootException.toString());
    }
    sagaInstanceVO.setLastMessage(sagaInstance.getLastMessage());
    sagaInstanceVO.setStop(sagaInstance.isStop());
    sagaInstanceVO.setCurrentlyStepName(sagaInstance.getCurrentlyStepName());
    sagaInstanceVO.setSagaData(sagaInstance.getSagaData());
    sagaInstanceVO
        .setExecutingMessages(sagaInstance.getExecutingMessages());
    sagaInstanceVO
        .setCompensatingMessages(sagaInstance.getCompensatingMessages());
    sagaInstanceVO.setSagaId(sagaInstance.getSagaId());
    sagaInstanceVO.setSagaName(sagaInstance.getSagaName());
    return sagaInstanceVO;
  }


}
