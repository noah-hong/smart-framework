package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.EventSender;
import org.smartframework.event.driven.core.context.IMessageContext;
import org.smartframework.event.driven.core.context.ISubscribeResult;
import org.smartframework.event.driven.core.context.MessageInfo;
import org.smartframework.event.driven.core.exception.MessageConsumedException;
import org.smartframework.event.driven.core.exception.MessageConsumingException;
import org.smartframework.event.driven.core.store.IConsumedMessageLogger;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventConsumedLog;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventConsumedLogRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventConsumedLogDO;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventConsumedLogCommonRepository;


/**
 * @author noah
 * @create 2021-01-28 14:31
 */
public class MyBatisComsumedMessageLogger implements IConsumedMessageLogger {

  Logger logger = LoggerFactory.getLogger(MyBatisComsumedMessageLogger.class);
  private static final ObjectMapper MESSAGE_OBJECT_MAPPER;

  @JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
  @JsonAutoDetect(fieldVisibility = Visibility.ANY,
      getterVisibility = Visibility.PUBLIC_ONLY,
      setterVisibility = Visibility.NONE,
      isGetterVisibility = Visibility.NONE)
  public static class ThrowableMixIn {

  }

  static {
    MESSAGE_OBJECT_MAPPER = new ObjectMapper();
    MESSAGE_OBJECT_MAPPER.setSerializationInclusion(Include.NON_NULL);
    MESSAGE_OBJECT_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    MESSAGE_OBJECT_MAPPER.registerModule(new JavaTimeModule());
    MESSAGE_OBJECT_MAPPER.setVisibility(MESSAGE_OBJECT_MAPPER.getSerializationConfig()
        .getDefaultVisibilityChecker()
        .withFieldVisibility(Visibility.ANY)
        .withGetterVisibility(Visibility.NONE)
        .withSetterVisibility(Visibility.NONE)
        .withCreatorVisibility(Visibility.NONE));
    MESSAGE_OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    MESSAGE_OBJECT_MAPPER.enable(Feature.WRITE_BIGDECIMAL_AS_PLAIN);
    MESSAGE_OBJECT_MAPPER.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    MESSAGE_OBJECT_MAPPER.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
    MESSAGE_OBJECT_MAPPER.addMixIn(Throwable.class, ThrowableMixIn.class);
    TypeResolverBuilder<?> mapTyper = new DefaultTypeResolverBuilder(DefaultTyping.NON_FINAL,
        LaissezFaireSubTypeValidator.instance) {
      @Override
      public boolean useForType(JavaType t) {
        switch (_appliesFor) {
          case NON_CONCRETE_AND_ARRAYS:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
          case OBJECT_AND_NON_CONCRETE:
            return (t.getRawClass() == Object.class) || !t.isConcrete();
          case NON_FINAL:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
            if (t.getRawClass() == Long.class) {
              return true;
            }
            if (t.getRawClass() == XMLGregorianCalendar.class) {
              return false;
            }
            return !t.isFinal();
          default:
            return t.getRawClass() == Object.class;
        }
      }
    };
    mapTyper.init(JsonTypeInfo.Id.CLASS, null);
    mapTyper.inclusion(JsonTypeInfo.As.PROPERTY);
    MESSAGE_OBJECT_MAPPER.setDefaultTyping(mapTyper);
  }


  private final EventConsumedLogCommonRepository eventConsumedLogRepository;

  public MyBatisComsumedMessageLogger(EventConsumedLogCommonRepository eventConsumedLogRepository) {
    this.eventConsumedLogRepository = eventConsumedLogRepository;
  }

  @Override
  public void log(IMessageContext messageContext, ISubscribeResult subscribeResult,
      Throwable throwable) {
    try {
      EventConsumedLogDO eventConsumedLog = new EventConsumedLogDO();
      MessageInfo message = messageContext.getMessageInfo();
      EventSender sender = messageContext.getEventEnvelop().getSender();
      eventConsumedLog.setConsumerGroup(message.getConsumerGroup());
      eventConsumedLog.setMessageId(message.getMessageId());
      eventConsumedLog.setMessageKey(message.getMessageKey());
      eventConsumedLog.setTopic(message.getTopic());
      eventConsumedLog.setSubTopic(message.getSubTopic());
      eventConsumedLog.setReconsumeTimes(message.getReconsumeTimes());
      eventConsumedLog.setStoredTime(message.getStoreTime());
      eventConsumedLog.setMessageOffset(message.getMessageOffset());
      eventConsumedLog.setProperties(message.getProperties().toString());
      try {
        eventConsumedLog
            .setPayload(MESSAGE_OBJECT_MAPPER.writeValueAsString(messageContext.getEventEnvelop()));
      } catch (Exception e) {
        logger.warn("记录消费日志失败，序列化事件体异常", e);
        eventConsumedLog.setPayload("PAYLOAD-ERROR:" + e.getMessage());
      }

      eventConsumedLog.setBroker(message.getBroker());
      eventConsumedLog.setQueueId(message.getQueueId());
      eventConsumedLog.setBornTimeStamp(message.getBornTimestamp());
      eventConsumedLog.setSender(sender.getName());
      eventConsumedLog.setSenderId(sender.getId());
      eventConsumedLog.setSenderScope(sender.getScope());

      if (subscribeResult.getException() != null) {
        final Throwable rootCause = ExceptionUtils.getRootCause(subscribeResult.getException());
        eventConsumedLog.setExceptionStack(String.format("top-ex:%s@@%s,root-ex:%s@@%s"
            , subscribeResult.getException().getMessage()
            , subscribeResult.getException().getStackTrace()[0].toString()
            , rootCause.getMessage()
            , rootCause.getStackTrace()[0].toString()
        ));
      } else if (throwable != null) {
        eventConsumedLog.setExceptionStack(String.format("sys-ex:%s@@%s"
            , throwable.getMessage()
            , throwable.getStackTrace()[0].toString()
        ));
      }

      eventConsumedLog.setExecStatus(subscribeResult.getExecutionStatus().name());
      eventConsumedLogRepository.save(eventConsumedLog);
    } catch (Exception ex) {
      logger.warn("记录消费日志失败", ex);
    }
  }

  String getRootCause(final Throwable throwable) {
    if (throwable == null) {
      return "";
    }
    if (throwable instanceof MessageConsumedException) {
      return "数据已消费";
    } else if (throwable instanceof MessageConsumingException) {
      return "数据消费中";
    }
    final List<Throwable> list = getThrowableList(throwable);
    return Optional.ofNullable(list.isEmpty() ? null : list.get(list.size() - 1))
        .map(x -> x.getMessage() + "@@" + x.getStackTrace())
        .orElse("");
  }

  List<Throwable> getThrowableList(Throwable throwable) {
    final List<Throwable> list = new ArrayList<>();
    while (throwable != null && !list.contains(throwable)) {
      list.add(throwable);
      throwable = throwable.getCause();
    }
    return list;
  }
}
