package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support;

import java.util.Objects;
import org.smartframework.event.driven.core.EventEnvelope;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.TopicModel;
import org.smartframework.event.driven.core.store.IEventEnvelopStore;
import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
import org.smartframework.event.driven.core.store.IEventStore;
import org.smartframework.event.driven.core.store.StoredEvent;

/**
 * @author noah
 * @create 2021-05-24 16:49
 */
public class MybatisEventEnvelopStoreAdapter implements IEventEnvelopStore {

  private final IEventStore eventStore;
  private final ITopicGenerator topicGenerator;
  private final IEventEnvelopeFormatter eventEnvelopeSerializer;


  public MybatisEventEnvelopStoreAdapter(IEventStore eventStore,
      ITopicGenerator topicGenerator,
      IEventEnvelopeFormatter eventEnvelopeSerializer) {
    this.eventStore = eventStore;
    this.topicGenerator = topicGenerator;
    this.eventEnvelopeSerializer = eventEnvelopeSerializer;
  }

  @Override
  public void append(EventEnvelope eventEnvelope) {
    Objects.requireNonNull(eventEnvelope);
    StoredEvent storedEvent = new StoredEvent();

    IEvent<?> event = eventEnvelope.getEvent();
    storedEvent.setLocation(String
        .format("%s-%s", event.getTagName(), event.getPersistenceId().getValue().toString()));
    storedEvent.setPersistenceId(event.getPersistenceId().getValue().toString());

    final Class<? extends IEvent> aClass = event.getClass();

    TopicModel topicModel = topicGenerator.generate(aClass);
    storedEvent.setPayload(eventEnvelopeSerializer.serialize(eventEnvelope));
    storedEvent.setTopic(topicModel.getTopic());
    storedEvent.setSubTopic(topicModel.getSubTopic());

    storedEvent.setTagName(eventEnvelope.getEvent().getTagName());
    storedEvent.setEventId(eventEnvelope.getEventId());
    storedEvent.setEventType(eventEnvelope.getEvent().getClass().getName());
    storedEvent.setSenderName(eventEnvelope.getSender().getName());
    storedEvent.setSenderId(eventEnvelope.getSender().getId());
    storedEvent.setSenderScope(eventEnvelope.getSender().getScope());
    storedEvent.setStoredTime(eventEnvelope.getOccurredOn());
    storedEvent.setVersion(eventEnvelope.getEvent().getVersion());
    storedEvent.setSerializer(eventEnvelopeSerializer.serializer());
    eventStore.append(storedEvent);
  }

  protected StoredEvent toStoredEvent(EventEnvelope eventEnvelope) {
    StoredEvent storedEvent = new StoredEvent();

    IEvent<?> event = eventEnvelope.getEvent();
    storedEvent.setLocation(String
        .format("%s-%s", event.getTagName(), event.getPersistenceId().getValue().toString()));
    storedEvent.setPersistenceId(event.getPersistenceId().getValue().toString());

    final Class<? extends IEvent> aClass = event.getClass();

    TopicModel topicModel = topicGenerator.generate(aClass);
    storedEvent.setPayload(eventEnvelopeSerializer.serialize(eventEnvelope));
    storedEvent.setTopic(topicModel.getTopic());
    storedEvent.setSubTopic(topicModel.getSubTopic());

    storedEvent.setTagName(eventEnvelope.getEvent().getTagName());
    storedEvent.setEventId(eventEnvelope.getEventId());
    storedEvent.setEventType(eventEnvelope.getEvent().getClass().getName());
    storedEvent.setSenderName(eventEnvelope.getSender().getName());
    storedEvent.setSenderId(eventEnvelope.getSender().getId());
    storedEvent.setSenderScope(eventEnvelope.getSender().getScope());
    storedEvent.setStoredTime(eventEnvelope.getOccurredOn());
    storedEvent.setVersion(eventEnvelope.getEvent().getVersion());
    storedEvent.setSerializer(eventEnvelopeSerializer.serializer());
    return storedEvent;
  }

  @Override
  public void append(EventEnvelope... events) {
    StoredEvent[] storedEvents = new StoredEvent[events.length];
    for (int i = 0; i < events.length; i++) {
      storedEvents[i] = toStoredEvent(events[i]);
    }
    eventStore.appends(storedEvents);
  }
}
