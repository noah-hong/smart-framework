package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.persistence.criteria.CriteriaBuilder.In;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.core.JsonUtils;
import org.smartframework.saga.core.SagaExecutionState;
import org.smartframework.saga.core.SagaInstance;
import org.smartframework.saga.core.SagaInstanceRepository;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.entity.SagaInstanceInfoDO;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.entity.SagaStepInfoDO;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.SagaInstanceInfoCommonRepository;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.SagaStepInfoCommonRepository;

/**
 * @author noah
 * @create 2021-10-13 18:00
 */
public class MybatisSagaInstanceRepository implements SagaInstanceRepository {

  private static final Logger logger = LoggerFactory.getLogger(MybatisSagaInstanceRepository.class);
  private SagaInstanceInfoCommonRepository sagaInstanceInfoRepository;
  private SagaStepInfoCommonRepository sagaStepInfoRepository;
  BlockingQueue<SagaInstanceInfoDO> sagaInstanceInfoDOBlockingQueue = new LinkedBlockingQueue<>();
  BlockingQueue<SagaStepInfoDO> sagaStepInfoDOBlockingQueue = new LinkedBlockingQueue<>();
  Thread sagaInstanceInfoDOConsumeThread;
  Thread sagaStepInfoDOConsumeThread;

  public MybatisSagaInstanceRepository(SagaInstanceInfoCommonRepository sagaInstanceInfoRepository,
      SagaStepInfoCommonRepository sagaStepInfoRepository) {
    this.sagaInstanceInfoRepository = sagaInstanceInfoRepository;
    this.sagaStepInfoRepository = sagaStepInfoRepository;
    sagaInstanceInfoDOConsumeThread = new Thread(new SagaInstanceInfoDOConsumer());
    sagaInstanceInfoDOConsumeThread.setDaemon(true);
    sagaStepInfoDOConsumeThread = new Thread(new SagaStepInfoDOConsumer());
    sagaStepInfoDOConsumeThread.setDaemon(true);
    sagaInstanceInfoDOConsumeThread.start();
    sagaStepInfoDOConsumeThread.start();
  }

  private class SagaInstanceInfoDOConsumer implements Runnable {

    @Override
    public void run() {
      while (true) {
        try {
          final SagaInstanceInfoDO sagaInstanceInfoDO = sagaInstanceInfoDOBlockingQueue.take();
          sagaInstanceInfoRepository.saveOrUpdate(sagaInstanceInfoDO);
        } catch (Exception ex) {
          logger.error("[Saga持久化失败]保存SagaInstanceInfo失败", ex);
        }
      }
    }
  }

  private class SagaStepInfoDOConsumer implements Runnable {

    @Override
    public void run() {
      while (true) {
        try {
          final SagaStepInfoDO sagaStepInfoDO = sagaStepInfoDOBlockingQueue.take();
          sagaStepInfoRepository.save(sagaStepInfoDO);
        } catch (Exception ex) {
          logger.error("[Saga持久化失败]保存SagaStepInfo失败", ex);
        }
      }
    }
  }


  public SagaInstanceInfoDO toSagaInstanceInfoDO(SagaInstance sagaInstance){
    SagaInstanceInfoDO sagaInstanceInfo = new SagaInstanceInfoDO();
    sagaInstanceInfo.setSagaRecoverStrategy(sagaInstance.getSagaRecoverStrategy().name());
    sagaInstanceInfo.setSagaType(sagaInstance.getSagaType().getName());
    sagaInstanceInfo.setCurrentlyExecuting(sagaInstance.getCurrentlyExecuting());
    sagaInstanceInfo.setCompensating(sagaInstance.isCompensating());
    sagaInstanceInfo.setEndState(sagaInstance.isEndState());
    if (sagaInstance.getExecutingException() != null) {
      final Throwable executingRootException = ExceptionUtils
          .getRootCause(sagaInstance.getExecutingException());
//      final String executingRootExceptionStr = StringUtils
//          .substring(executingRootException.toString(), 0, execptionStrLen);
      sagaInstanceInfo.setExecutingException(executingRootException.toString());
    }
    if (sagaInstance.getCompensatingException() != null) {
      final Throwable compensatingRootException = ExceptionUtils
          .getRootCause(sagaInstance.getCompensatingException());
//      final String compensatingRootExceptionStr = StringUtils
//          .substring(compensatingRootException.toString(), 0, execptionStrLen);
      sagaInstanceInfo.setCompensatingException(compensatingRootException.toString());
    }
    sagaInstanceInfo.setLastMessage(sagaInstance.getLastMessage());
    sagaInstanceInfo.setStopState(sagaInstance.isStop());
    sagaInstanceInfo.setCurrentlyStepName(sagaInstance.getCurrentlyStepName());
    sagaInstanceInfo.setSagaData(JsonUtils.toJsonString(sagaInstance.getSagaData()));
    sagaInstanceInfo
        .setExecutingMessages(JsonUtils.toJsonString(sagaInstance.getExecutingMessages()));
    sagaInstanceInfo
        .setCompensatingMessages(JsonUtils.toJsonString(sagaInstance.getCompensatingMessages()));
    sagaInstanceInfo.setSagaId(sagaInstance.getSagaId());
    sagaInstanceInfo.setSagaName(sagaInstance.getSagaName());
    sagaInstanceInfo.setSagaStatus(sagaInstance.getSagaStatus().name());
    return sagaInstanceInfo;
  }

  @Override
  public void save(SagaInstance sagaInstance) {

    final SagaInstanceInfoDO sagaInstanceInfoDO = toSagaInstanceInfoDO(sagaInstance);
    sagaInstanceInfoRepository.saveOrUpdate(sagaInstanceInfoDO);

  }

  @Override
  public void saveAsync(SagaInstance sagaInstance) {
    final SagaInstanceInfoDO sagaInstanceInfoDO = toSagaInstanceInfoDO(sagaInstance);
    try {
      sagaInstanceInfoDOBlockingQueue.put(sagaInstanceInfoDO);
    } catch (InterruptedException ex) {
      Thread.currentThread().interrupt();
      throw new UndeclaredThrowableException(ex);
    }
  }

  private SagaInstance toSagaInstance(SagaInstanceInfoDO sagaInstanceInfo) {
    if (sagaInstanceInfo == null) {
      return null;
    }
    SagaInstance sagaInstance = new SagaInstance();
    sagaInstance.setCurrentlyExecuting(sagaInstanceInfo.getCurrentlyExecuting());
    sagaInstance.setCompensating(sagaInstanceInfo.getCompensating());
    sagaInstance.setEndState(sagaInstanceInfo.getEndState());
    sagaInstance.setStop(sagaInstanceInfo.getStopState());
    sagaInstance.setCurrentlyStepName(sagaInstanceInfo.getCurrentlyStepName());
    sagaInstance.setSagaData(JsonUtils.parseObj(sagaInstanceInfo.getSagaData()));
    sagaInstance.setSagaId(sagaInstanceInfo.getSagaId());
    sagaInstance.setSagaName(sagaInstanceInfo.getSagaName());
    try {
      final Class<?> aClass = Class
          .forName(sagaInstanceInfo.getSagaType(), true,
              Thread.currentThread().getContextClassLoader());
      sagaInstance.setSagaType(aClass);
    } catch (Exception ex) {

      throw new UndeclaredThrowableException(ex, "saga类型获取异常");
    }
    return sagaInstance;
  }

  @Override
  public SagaInstance find(String sagaId) {
    final SagaInstanceInfoDO sagaInstanceInfo = sagaInstanceInfoRepository.getById(sagaId);
    return toSagaInstance(sagaInstanceInfo);
//    return sagaInstanceInfo.map(this::toSagaInstance).orElse(null);
  }
  public SagaStepInfoDO toSagaStepInfoDO(String sagaId, SagaExecutionState currState, SagaExecutionState state,
      Object sagaData, boolean skip) {
    SagaStepInfoDO sagaStepInfo = new SagaStepInfoDO();
    sagaStepInfo.setSagaId(sagaId);
    sagaStepInfo.setCurrentlyExecuting(state.getCurrentlyExecuting());
    sagaStepInfo.setCompensating(currState.isCompensating());
    sagaStepInfo.setEndState(state.isEndState());
    sagaStepInfo.setSkip(skip);
    if (state.getExecutingException() != null) {
      final Throwable executingRootException = ExceptionUtils
          .getRootCause(state.getExecutingException());
      sagaStepInfo.setExecutingException(executingRootException.toString());
    }
    if (state.getCompensatingException() != null) {
      final Throwable compensatingRootException = ExceptionUtils
          .getRootCause(state.getCompensatingException());
      sagaStepInfo.setCompensatingException(compensatingRootException.toString());
    }
    sagaStepInfo.setLastMessage(StringUtils.substring(state.getLastMessage(), 0, 400));
    sagaStepInfo.setStopState(state.isStop());
    sagaStepInfo.setCurrentlyStepName(state.getCurrentlyStepName());
    sagaStepInfo.setSagaData(JsonUtils.toJsonString(sagaData));
    sagaStepInfo
        .setExecutingMessages(JsonUtils.toJsonString(state.getExecutingMessages()));
    sagaStepInfo
        .setCompensatingMessages(JsonUtils.toJsonString(state.getCompensatingMessages()));
    sagaStepInfo.setSagaId(sagaId);
    sagaStepInfo.setSagaStatus(state.getSagaStatus().name());
    return sagaStepInfo;
  }

  @Override
  public void savaStep(String sagaId, SagaExecutionState currState, SagaExecutionState state,
      Object sagaData, boolean skip) {
    SagaStepInfoDO sagaStepInfo = toSagaStepInfoDO(sagaId,currState,state,sagaData,skip);
    sagaStepInfoRepository.save(sagaStepInfo);

    final SagaInstanceInfoDO findSagaInstanceInfo = sagaInstanceInfoRepository
        .getById(sagaId);
    if (findSagaInstanceInfo != null) {

      findSagaInstanceInfo.setSagaRecoverStrategy(currState.getSagaRecoverType().name());
      findSagaInstanceInfo.setCurrentlyExecuting(sagaStepInfo.getCurrentlyExecuting());
      findSagaInstanceInfo.setCompensating(sagaStepInfo.getCompensating());
      findSagaInstanceInfo.setEndState(sagaStepInfo.getEndState());
      findSagaInstanceInfo.setExecutingException(sagaStepInfo.getExecutingException());
      findSagaInstanceInfo.setCompensatingException(sagaStepInfo.getCompensatingException());
      findSagaInstanceInfo
          .setLastMessage(StringUtils.substring(sagaStepInfo.getLastMessage(), 0, 400));
      findSagaInstanceInfo.setStopState(sagaStepInfo.getStopState());
      findSagaInstanceInfo.setCurrentlyStepName(sagaStepInfo.getCurrentlyStepName());
      findSagaInstanceInfo.setSagaData(JsonUtils.toJsonString(sagaData));
      findSagaInstanceInfo
          .setExecutingMessages(JsonUtils.toJsonString(sagaStepInfo.getExecutingMessages()));
      findSagaInstanceInfo
          .setCompensatingMessages(JsonUtils.toJsonString(sagaStepInfo.getCompensatingMessages()));
      findSagaInstanceInfo.setSagaId(sagaId);
      findSagaInstanceInfo.setSagaStatus(sagaStepInfo.getSagaStatus());
      sagaInstanceInfoRepository.saveOrUpdate(findSagaInstanceInfo);
    }


  }

  @Override
  public void savaStepAsync(String sagaId, SagaExecutionState currState, SagaExecutionState state,
      Object sagaData, boolean skip) {
    SagaStepInfoDO sagaStepInfo = toSagaStepInfoDO(sagaId,currState,state,sagaData,skip);
    try {
      sagaStepInfoDOBlockingQueue.put(sagaStepInfo);
    } catch (InterruptedException ex) {
      Thread.currentThread().interrupt();
      throw new UndeclaredThrowableException(ex);
    }

    final SagaInstanceInfoDO findSagaInstanceInfo = sagaInstanceInfoRepository
        .getById(sagaId);
    if (findSagaInstanceInfo != null) {

      findSagaInstanceInfo.setSagaRecoverStrategy(currState.getSagaRecoverType().name());
      findSagaInstanceInfo.setCurrentlyExecuting(sagaStepInfo.getCurrentlyExecuting());
      findSagaInstanceInfo.setCompensating(sagaStepInfo.getCompensating());
      findSagaInstanceInfo.setEndState(sagaStepInfo.getEndState());
      findSagaInstanceInfo.setExecutingException(sagaStepInfo.getExecutingException());
      findSagaInstanceInfo.setCompensatingException(sagaStepInfo.getCompensatingException());
      findSagaInstanceInfo
          .setLastMessage(StringUtils.substring(sagaStepInfo.getLastMessage(), 0, 400));
      findSagaInstanceInfo.setStopState(sagaStepInfo.getStopState());
      findSagaInstanceInfo.setCurrentlyStepName(sagaStepInfo.getCurrentlyStepName());
      findSagaInstanceInfo.setSagaData(JsonUtils.toJsonString(sagaData));
      findSagaInstanceInfo
          .setExecutingMessages(JsonUtils.toJsonString(sagaStepInfo.getExecutingMessages()));
      findSagaInstanceInfo
          .setCompensatingMessages(JsonUtils.toJsonString(sagaStepInfo.getCompensatingMessages()));
      findSagaInstanceInfo.setSagaId(sagaId);
      findSagaInstanceInfo.setSagaStatus(sagaStepInfo.getSagaStatus());
      try {
        sagaInstanceInfoDOBlockingQueue.put(findSagaInstanceInfo);
      } catch (InterruptedException ex) {
        Thread.currentThread().interrupt();
        throw new UndeclaredThrowableException(ex);
      }
//      sagaInstanceInfoRepository.saveOrUpdate(sagaInstanceInfo);
    }

  }
}
