package org.smartframework.spring.boot.autoconfigure.saga.store.jpa.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author noah
 * @create 2021-10-13 18:14
 */
@Entity(name = "saga_step_info")
public class SagaStepInfo implements Serializable {

  private static final long serialVersionUID = 1L;

  //    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public boolean isSkip() {
    return skip;
  }

  public void setSkip(boolean skip) {
    this.skip = skip;
  }

  @Column(name = "saga_id")
  private String sagaId;
  private boolean skip;

  @Column(name = "saga_status")
  private String sagaStatus;
  @Column(name = "currently_executing")
  private int currentlyExecuting;
  @Column(name = "compensating")
  private boolean compensating;
  @Column(name = "end_state")
  private boolean endState;
  @Column(name = "executing_exception")
  private String executingException;
  @Column(name = "compensating_exception")
  private String compensatingException;
  @Column(name = "last_message")
  private String lastMessage;
  @Column(name = "stop_state")
  private boolean isStop;
  @Column(name = "currently_step_name")
  private String currentlyStepName;
  @Column(name = "saga_data")
  private String sagaData;
  @Column(name = "executing_messages")
  private String executingMessages;
  @Column(name = "compensating_messages")
  private String compensatingMessages;

  public String getSagaId() {
    return sagaId;
  }

  public void setSagaId(String sagaId) {
    this.sagaId = sagaId;
  }


  public String getSagaStatus() {
    return sagaStatus;
  }

  public void setSagaStatus(String sagaStatus) {
    this.sagaStatus = sagaStatus;
  }

  public int getCurrentlyExecuting() {
    return currentlyExecuting;
  }

  public void setCurrentlyExecuting(int currentlyExecuting) {
    this.currentlyExecuting = currentlyExecuting;
  }

  public boolean isCompensating() {
    return compensating;
  }

  public void setCompensating(boolean compensating) {
    this.compensating = compensating;
  }

  public boolean isEndState() {
    return endState;
  }

  public void setEndState(boolean endState) {
    this.endState = endState;
  }

  public String getExecutingException() {
    return executingException;
  }

  public void setExecutingException(String executingException) {
    this.executingException = executingException;
  }

  public String getCompensatingException() {
    return compensatingException;
  }

  public void setCompensatingException(String compensatingException) {
    this.compensatingException = compensatingException;
  }

  public String getLastMessage() {
    return lastMessage;
  }

  public void setLastMessage(String lastMessage) {
    this.lastMessage = lastMessage;
  }

  public boolean isStop() {
    return isStop;
  }

  public void setStop(boolean stop) {
    isStop = stop;
  }

  public String getCurrentlyStepName() {
    return currentlyStepName;
  }

  public void setCurrentlyStepName(String currentlyStepName) {
    this.currentlyStepName = currentlyStepName;
  }

  public String getSagaData() {
    return sagaData;
  }

  public void setSagaData(String data) {
    this.sagaData = data;
  }

  public String getExecutingMessages() {
    return executingMessages;
  }

  public void setExecutingMessages(String executingMessages) {
    this.executingMessages = executingMessages;
  }

  public String getCompensatingMessages() {
    return compensatingMessages;
  }

  public void setCompensatingMessages(String compensatingMessages) {
    this.compensatingMessages = compensatingMessages;
  }
}
