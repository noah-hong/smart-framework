package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis;

import org.smartframework.saga.core.Saga;
import org.smartframework.saga.core.SagaInstanceRepository;
import org.smartframework.saga.core.SagaStepCompletedObserver;
import org.smartframework.spring.boot.autoconfigure.saga.SagaAutoConfiguration;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.MybatisRepositorySagaConfiguration;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.SagaInstanceInfoCommonRepository;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.SagaStepInfoCommonRepository;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author noah
 * @create 2021-10-13 18:02
 */
@Configuration
@AutoConfigureAfter(SagaAutoConfiguration.class)
@ConditionalOnClass(Saga.class)
@Import(MybatisRepositorySagaConfiguration.class)
@ConditionalOnBean(SagaAutoConfiguration.class)
public class MybatisSagaStoreAutoConfiguration {

  @Bean
  @ConditionalOnMissingBean
  public SagaInstanceRepository mybatisSagaInstanceRepository(
      SagaInstanceInfoCommonRepository sagaInstanceInfoRepository,
      SagaStepInfoCommonRepository sagaStepInfoRepository) {
    return new MybatisSagaInstanceRepository(sagaInstanceInfoRepository, sagaStepInfoRepository);
  }

  @Bean
  public SagaStepCompletedObserver mybatisSaveSagaStepCompletedObserver(
      SagaInstanceRepository sagaInstanceRepository) {
    return new MybatisSaveSagaStepCompletedObserver(sagaInstanceRepository);
  }


}
