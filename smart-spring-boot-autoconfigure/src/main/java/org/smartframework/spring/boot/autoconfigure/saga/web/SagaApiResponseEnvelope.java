package org.smartframework.spring.boot.autoconfigure.saga.web;

/**
 * @author noah
 * @create 2021-03-23 16:48
 */
public class SagaApiResponseEnvelope<T> {

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public T getResult() {
    return result;
  }

  public void setResult(T result) {
    this.result = result;
  }

  private String code;
  private String message;
  private T result;

  public static ResponseBuilder failure() {
    return new ResponseBuilder(SagaResponseStatus.ERROR_BIZ_EXCEPTION);
  }

  public static <T> SagaApiResponseEnvelope<T> failure(String message) {
    return new ResponseBuilder(SagaResponseStatus.ERROR_BIZ_EXCEPTION).message(message).build();
  }

  public static <T> SagaApiResponseEnvelope<T> failure(String code, String message) {
    return new ResponseBuilder(SagaResponseStatus.ERROR_BIZ_EXCEPTION).code(code).message(message)
        .build();
  }

  public static ResponseBuilder success() {
    return new ResponseBuilder(SagaResponseStatus.SUCCESS);
  }

  public static <T> SagaApiResponseEnvelope<T> success(T t) {
    return new ResponseBuilder(SagaResponseStatus.SUCCESS).build(t);
  }

  public static ResponseBuilder status(String code) {
    return new ResponseBuilder(code);
  }

  public static ResponseBuilder status(SagaResponseStatus code) {
    return new ResponseBuilder(code);
  }

  public static ResponseBuilder error() {
    return new ResponseBuilder(SagaResponseStatus.ERROR_INTERNAL_SERVER_EXCEPTION);
  }

  public static class ResponseBuilder {

    private String code;
    private String message;

    public ResponseBuilder(String code) {
      this.code = code;
    }


    public ResponseBuilder(SagaResponseStatus code) {
      this.code = code.getCode();
      this.message = code.getMessage();

    }

    public ResponseBuilder message(String message) {
      this.message = message;
      return this;
    }

    public ResponseBuilder code(String code) {
      this.code = code;
      return this;
    }


    public <T> SagaApiResponseEnvelope<T> build() {
      return this.build(null);
    }

    public <T> SagaApiResponseEnvelope<T> build(T t) {
      SagaApiResponseEnvelope<T> responseEnvelope = new SagaApiResponseEnvelope<T>();
      responseEnvelope.setMessage(message);
      responseEnvelope.setCode(code);
      responseEnvelope.setResult(t);
      return responseEnvelope;
    }



  }

}
