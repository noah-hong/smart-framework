package org.smartframework.spring.boot.autoconfigure.saga.store.jpa;

import org.smartframework.saga.core.Saga;
import org.smartframework.saga.core.SagaExecutionState;
import org.smartframework.saga.core.SagaInstanceRepository;
import org.smartframework.saga.core.SagaStep;
import org.smartframework.saga.core.SagaStepCompletedObserver;

/**
 * @author noah
 * @create 2021-11-16 15:37
 */
public class JpaSaveSagaStepCompletedObserver implements SagaStepCompletedObserver {

  SagaInstanceRepository sagaInstanceRepository;

  public JpaSaveSagaStepCompletedObserver(SagaInstanceRepository sagaInstanceRepository) {
    this.sagaInstanceRepository = sagaInstanceRepository;
  }

  @Override
  public void apply(String sagaId, Saga<Object> saga, SagaStep<Object> sagaStep,
      SagaExecutionState currState,
      SagaExecutionState state, Object sagaData,
      boolean skip) {
    sagaInstanceRepository.savaStepAsync(sagaId, currState, state, sagaData, skip);
  }
}
