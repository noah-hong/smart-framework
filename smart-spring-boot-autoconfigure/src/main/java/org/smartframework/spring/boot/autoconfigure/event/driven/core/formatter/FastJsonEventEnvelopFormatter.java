//package org.smartframework.spring.boot.autoconfigure.event.driven.core.formatter;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.parser.DefaultJSONParser;
//import com.alibaba.fastjson.parser.JSONToken;
//import com.alibaba.fastjson.parser.ParserConfig;
//import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
//import com.alibaba.fastjson.util.IOUtils;
//import java.lang.reflect.Type;
//import java.nio.charset.Charset;
//import org.smartframework.event.driven.core.EventEnvelope;
//import org.smartframework.event.driven.core.IEvent;
//import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
//
///**
// * @author noah
// * @create 2021-01-21 13:41
// */
//public class FastJsonEventEnvelopFormatter implements IEventEnvelopeFormatter {
//
//  @Override
//  public byte[] serialize(EventEnvelope target) {
//    return JSON.toJSONBytes(target);
//  }
//
//  @Override
//  public EventEnvelope deserialize(byte[] target,
//      Class<IEvent<?>> eventType) {
//    ParserConfig parserConfig = new ParserConfig();
//    parserConfig.putDeserializer(IEvent.class, new EventBodyDeserializer(eventType));
//    return JSON.parseObject(target, (Charset) IOUtils.UTF8, EventEnvelope.class, parserConfig, null,
//        JSON.DEFAULT_PARSER_FEATURE);
////    ParameterizedTypeImpl targetType =
////        new ParameterizedTypeImpl(
////            new Type[]{
////                eventType
////            },
////            null,
////            EventEnvelope.class);
////    return JSON.parseObject(target, targetType);
//  }
//
//  @Override
//  public String serializer() {
//    return "FAST-JSON";
//  }
//
//  private static class EventBodyDeserializer implements ObjectDeserializer {
//
//    private final Class<?> clazz;
//
//    public EventBodyDeserializer(Class<?> clazz) {
//      this.clazz = clazz;
//    }
//
//    @SuppressWarnings("unchecked")
//    @Override
//    public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
//      IEvent<?> event = (IEvent<?>) parser.parseObject(clazz);
//      return (T) event;
//    }
//
//    @Override
//    public int getFastMatchToken() {
//      return JSONToken.LITERAL_INT;
//    }
//
//  }
//}
