/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq;

import org.springframework.boot.context.properties.ConfigurationProperties;

@SuppressWarnings("WeakerAccess")
@ConfigurationProperties(prefix = "smart.event-driven.rocketmq")
public class EventDrivenRocketMQProperties {

  /**
   * 消息队列服务器地址, 格式为: `host:port;host:port`
   */
  private String nameServer;

  private String publishableNotifierTopic;

  private String namespace = "LOCAL";

  private Producer producer = new Producer();

  private Consumer consumer = new Consumer();

  public String getPublishableNotifierTopic() {
    return publishableNotifierTopic;
  }

  public void setPublishableNotifierTopic(String publishableNotifierTopic) {
    this.publishableNotifierTopic = publishableNotifierTopic;
  }

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  public String getNameServer() {
    return nameServer;
  }

  public void setNameServer(String nameServer) {
    this.nameServer = nameServer;
  }

  public Producer getProducer() {
    return producer;
  }

  public void setProducer(Producer producer) {
    this.producer = producer;
  }

  public Consumer getConsumer() {
    return consumer;
  }

  public void setConsumer(Consumer consumer) {
    this.consumer = consumer;
  }

  public static class Producer {

    /**
     * 消费者组
     */
    private String group;
    /**
     * 发送消息的超时时间，单位：毫秒
     */
    private int sendMessageTimeout = 3000;

    /**
     * 消息压缩的阈值，默认4K
     */
    private int compressMessageBodyThreshold = 1024 * 4;

    /**
     * 同步消息发送失败后的最大重试次数，有可能导致发送重复消息
     */
    private int retryTimesWhenSendFailed = 2;

    /**
     * 异步消息发送失败后的最大重试次数，有可能导致发送重复消息
     */
    private int retryTimesWhenSendAsyncFailed = 2;

    /**
     * 发生失败时，是否尝试使用下一个BROKER进行发送
     */
    private boolean retryNextServer = false;

    /**
     * 最大能够允许的消息大小
     */
    private int maxMessageSize = 1024 * 1024 * 4;

    private boolean enabledAutoCreateTopic = true;

    public boolean isEnabledAutoCreateTopic() {
      return enabledAutoCreateTopic;
    }

    public void setEnabledAutoCreateTopic(boolean enabledAutoCreateTopic) {
      this.enabledAutoCreateTopic = enabledAutoCreateTopic;
    }

    public int getDefaultTopicQueueNums() {
      return defaultTopicQueueNums;
    }

    public void setDefaultTopicQueueNums(int defaultTopicQueueNums) {
      this.defaultTopicQueueNums = defaultTopicQueueNums;
    }

    private int defaultTopicQueueNums = 16;

    public String getGroup() {
      return group;
    }

    public void setGroup(String group) {
      this.group = group;
    }

    public int getSendMessageTimeout() {
      return sendMessageTimeout;
    }

    public void setSendMessageTimeout(int sendMessageTimeout) {
      this.sendMessageTimeout = sendMessageTimeout;
    }

    public int getCompressMessageBodyThreshold() {
      return compressMessageBodyThreshold;
    }

    public void setCompressMessageBodyThreshold(int compressMessageBodyThreshold) {
      this.compressMessageBodyThreshold = compressMessageBodyThreshold;
    }

    public int getRetryTimesWhenSendFailed() {
      return retryTimesWhenSendFailed;
    }

    public void setRetryTimesWhenSendFailed(int retryTimesWhenSendFailed) {
      this.retryTimesWhenSendFailed = retryTimesWhenSendFailed;
    }

    public int getRetryTimesWhenSendAsyncFailed() {
      return retryTimesWhenSendAsyncFailed;
    }

    public void setRetryTimesWhenSendAsyncFailed(int retryTimesWhenSendAsyncFailed) {
      this.retryTimesWhenSendAsyncFailed = retryTimesWhenSendAsyncFailed;
    }

    public boolean isRetryNextServer() {
      return retryNextServer;
    }

    public void setRetryNextServer(boolean retryNextServer) {
      this.retryNextServer = retryNextServer;
    }

    public int getMaxMessageSize() {
      return maxMessageSize;
    }

    public void setMaxMessageSize(int maxMessageSize) {
      this.maxMessageSize = maxMessageSize;
    }
  }

  public static final class Consumer {

    private boolean enabled = true;

    public boolean isEnabledAutoCreateTopic() {
      return enabledAutoCreateTopic;
    }

    public void setEnabledAutoCreateTopic(boolean enabledAutoCreateTopic) {
      this.enabledAutoCreateTopic = enabledAutoCreateTopic;
    }

    private boolean enabledAutoCreateTopic = true;

    public int getTopicQueueNums() {
      return topicQueueNums;
    }

    public void setTopicQueueNums(int topicQueueNums) {
      this.topicQueueNums = topicQueueNums;
    }

    private int topicQueueNums = 16;

    public boolean isEnabled() {
      return enabled;
    }

    public void setEnabled(boolean enabled) {
      this.enabled = enabled;
    }

  }

}
