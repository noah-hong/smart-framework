package org.smartframework.spring.boot.autoconfigure.saga.core;

import org.smartframework.saga.core.Saga;
import org.smartframework.saga.core.SagaInstance;
import org.smartframework.saga.core.SagaInstanceFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author noah
 * @create 2022-04-18 09:33
 */
public class SpringSagaInstanceFactory implements ApplicationContextAware {

  SagaInstanceFactory factory;

  public SpringSagaInstanceFactory(SagaInstanceFactory factory) {
    this.factory = factory;
  }

  public <SagaData> SagaInstance create(Class<? extends Saga<SagaData>> sagaClazz,
      SagaData sagaData, String sagaName) {
    Saga<SagaData> saga = applicationContext.getBean(sagaClazz);
    return factory.create(saga, sagaData, sagaName);
  }

  public <SagaData> SagaInstance create(Class<? extends Saga<SagaData>> sagaClazz,
      SagaData sagaData) {
    Saga<SagaData> saga = applicationContext.getBean(sagaClazz);
    return factory.create(saga, sagaData);
  }

  public <SagaData> SagaInstance create(Saga<SagaData> saga, SagaData sagaData, String sagaName) {

    return factory.create(saga, sagaData, sagaName);
  }

  public <SagaData> SagaInstance create(Saga<SagaData> saga, SagaData sagaData) {
    return factory.create(saga, sagaData);
  }

  public <SagaData> SagaInstance create(Class<? extends Saga<SagaData>> sagaClazz, String sagaId, SagaData sagaData) {
    Saga<SagaData> saga = applicationContext.getBean(sagaClazz);
    return factory.create(saga, sagaId, sagaData);
  }

  public <SagaData> SagaInstance create(Class<? extends Saga<SagaData>> sagaClazz, String sagaId, String sagaName,
      SagaData sagaData) {
    Saga<SagaData> saga = applicationContext.getBean(sagaClazz);
    return factory.create(saga, sagaId, sagaName, sagaData);
  }

  public <SagaData> SagaInstance create(Saga<SagaData> saga, String sagaId, SagaData sagaData) {
    return factory.create(saga, sagaId, sagaData);
  }

  public <SagaData> SagaInstance create(Saga<SagaData> saga, String sagaId, String sagaName,
      SagaData sagaData) {

    return factory.create(saga, sagaId, sagaName, sagaData);
  }

  public SagaInstance recover(String sagaId) {
    return factory.recover(sagaId);
  }

  public <SagaData> SagaInstance recover(String sagaId, SagaData sagaData) {
    return factory.recover(sagaId, sagaData);
  }

  public SagaInstance recoverToReStart(String sagaId) {
    return factory.restart(sagaId);
  }

  public SagaInstance recoverToNextStep(String sagaId) {
    return factory.skipCurrent(sagaId);
  }

  ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
}
