package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository;

import org.mybatis.spring.annotation.MapperScan;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.EventDrivenMybatisStoreAutoConfiguration;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.mapper.EventConsumedLogCommonMapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author noah
 * @create 2022-04-05 11:22
 */
@ComponentScan(basePackages = "org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository")
@MapperScan(basePackageClasses = {EventConsumedLogCommonMapper.class})
@ConditionalOnClass({IEvent.class})
//@ConditionalOnBean(EventConsumedLogCommonMapper.class)
public class MybatisRepositoryEventDrivenConfiguration {

}
