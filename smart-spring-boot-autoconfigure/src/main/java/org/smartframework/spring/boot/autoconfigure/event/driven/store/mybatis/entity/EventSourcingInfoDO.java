package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Blob;
import java.time.LocalDateTime;

/**
* <p>
  * 事件溯源
  * </p>
*
* @author System
* @since 2022-03-24
*/
@TableName("event_sourcing_info")
public class EventSourcingInfoDO implements Serializable {

private static final long serialVersionUID=1L;

        /**
        * 主键
        */
                @TableId(value = "id", type = IdType.AUTO)
                private Long id;

        /**
        * 事件标识
        */
        private String eventId;

        /**
        * 标记名称-一般为表名或者领域名称
        */
        private String tagName;

        /**
        * 持久化编号-每张表的主键或者领域标识
        */
        private String persistenceId;

        private String location;

        /**
        * 事件类型
        */
        private String eventType;

        /**
        * 事件实体
        */
        private byte[] eventBody;

        /**
        * 事件版本
        */
        private Integer eventVersion;

        private String sender;

        private String senderId;

        private String senderScope;

        private LocalDateTime storedTime;

        private String serializer;

        /**
        * 主题
        */
        private String topic;

        /**
        * 子主题
        */
        private String subTopic;

        private LocalDateTime createdTime;

        /**
        * 用户属性
        */
        private String properties;


    public Long getId() {
    return id;
    }

        public void setId(Long id) {
    this.id = id;
    }

    public String getEventId() {
    return eventId;
    }

        public void setEventId(String eventId) {
    this.eventId = eventId;
    }

    public String getTagName() {
    return tagName;
    }

        public void setTagName(String tagName) {
    this.tagName = tagName;
    }

    public String getPersistenceId() {
    return persistenceId;
    }

        public void setPersistenceId(String persistenceId) {
    this.persistenceId = persistenceId;
    }

    public String getLocation() {
    return location;
    }

        public void setLocation(String location) {
    this.location = location;
    }

    public String getEventType() {
    return eventType;
    }

        public void setEventType(String eventType) {
    this.eventType = eventType;
    }

    public byte[] getEventBody() {
    return eventBody;
    }

        public void setEventBody(byte[] eventBody) {
    this.eventBody = eventBody;
    }

    public Integer getEventVersion() {
    return eventVersion;
    }

        public void setEventVersion(Integer eventVersion) {
    this.eventVersion = eventVersion;
    }

    public String getSender() {
    return sender;
    }

        public void setSender(String sender) {
    this.sender = sender;
    }

    public String getSenderId() {
    return senderId;
    }

        public void setSenderId(String senderId) {
    this.senderId = senderId;
    }

    public String getSenderScope() {
    return senderScope;
    }

        public void setSenderScope(String senderScope) {
    this.senderScope = senderScope;
    }

    public LocalDateTime getStoredTime() {
    return storedTime;
    }

        public void setStoredTime(LocalDateTime storedTime) {
    this.storedTime = storedTime;
    }

    public String getSerializer() {
    return serializer;
    }

        public void setSerializer(String serializer) {
    this.serializer = serializer;
    }

    public String getTopic() {
    return topic;
    }

        public void setTopic(String topic) {
    this.topic = topic;
    }

    public String getSubTopic() {
    return subTopic;
    }

        public void setSubTopic(String subTopic) {
    this.subTopic = subTopic;
    }

    public LocalDateTime getCreatedTime() {
    return createdTime;
    }

        public void setCreatedTime(LocalDateTime createdTime) {
    this.createdTime = createdTime;
    }

    public String getProperties() {
    return properties;
    }

        public void setProperties(String properties) {
    this.properties = properties;
    }
    
    public static final String ID = "id";

    public static final String EVENT_ID = "event_id";

    public static final String TAG_NAME = "tag_name";

    public static final String PERSISTENCE_ID = "persistence_id";

    public static final String LOCATION = "location";

    public static final String EVENT_TYPE = "event_type";

    public static final String EVENT_BODY = "event_body";

    public static final String EVENT_VERSION = "event_version";

    public static final String SENDER = "sender";

    public static final String SENDER_ID = "sender_id";

    public static final String SENDER_SCOPE = "sender_scope";

    public static final String STORED_TIME = "stored_time";

    public static final String SERIALIZER = "serializer";

    public static final String TOPIC = "topic";

    public static final String SUB_TOPIC = "sub_topic";

    public static final String CREATED_TIME = "created_time";

    public static final String PROPERTIES = "properties";

@Override
public String toString() {
return "EventSourcingInfoDO{" +
        "id=" + id +
        ", eventId=" + eventId +
        ", tagName=" + tagName +
        ", persistenceId=" + persistenceId +
        ", location=" + location +
        ", eventType=" + eventType +
        ", eventBody=" + eventBody +
        ", eventVersion=" + eventVersion +
        ", sender=" + sender +
        ", senderId=" + senderId +
        ", senderScope=" + senderScope +
        ", storedTime=" + storedTime +
        ", serializer=" + serializer +
        ", topic=" + topic +
        ", subTopic=" + subTopic +
        ", createdTime=" + createdTime +
        ", properties=" + properties +
"}";
}
}
