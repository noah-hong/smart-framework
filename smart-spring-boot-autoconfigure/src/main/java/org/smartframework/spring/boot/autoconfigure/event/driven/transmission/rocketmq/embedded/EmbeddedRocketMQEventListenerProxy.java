package org.smartframework.spring.boot.autoconfigure.event.driven.transmission.rocketmq.embedded;

import java.util.Objects;
import org.smartframework.event.driven.rocketmq.annotation.RocketMQEventListener;
import org.smartframework.event.driven.rocketmq.support.RocketMQEventListenerProxy;
import org.springframework.aop.framework.AopProxyUtils;

/**
 * @author noah
 * @create 2021-05-30 21:55
 */
public class EmbeddedRocketMQEventListenerProxy extends RocketMQEventListenerProxy {

  private Class<?> proxyTargetType;

  public EmbeddedRocketMQEventListenerProxy(Object proxyTarget) {
    super(proxyTarget);
    Objects.requireNonNull(proxyTarget);
    proxyTargetType = AopProxyUtils.ultimateTargetClass(proxyTarget);
    Objects.requireNonNull(proxyTargetType.getAnnotation(RocketMQEventListener.class));
  }

  @Override
  public Class<?> getProxyTargetType() {
    return proxyTargetType;
  }
}
