package org.smartframework.spring.boot.autoconfigure.domain.driven.lame.embedded;

import org.smartframework.domain.driven.core.communication.IClientSiteResponderRegistrar;
import org.smartframework.domain.driven.lame.woker.LameWorkerSiteService;
import org.springframework.aop.framework.AopProxyUtils;

/**
 * @author noah
 * @create 2021-06-06 23:42
 */
public class EmbeddedLameWorkerSiteService extends LameWorkerSiteService {


  public EmbeddedLameWorkerSiteService(Object invoker,
      IClientSiteResponderRegistrar rpcClientResponderRegistrar) {
    super(invoker, AopProxyUtils.ultimateTargetClass(invoker), rpcClientResponderRegistrar);

  }
}
