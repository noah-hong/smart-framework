package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.support;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import javax.persistence.LockModeType;
import org.apache.commons.lang3.StringUtils;
import org.smartframework.event.driven.core.store.IEventQueryStore;
import org.smartframework.event.driven.core.store.StoredEvent;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventSourcingInfo;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventSourcingRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author noah
 * @create 2021-05-24 17:49
 */
public class JpaEventQueryStore implements IEventQueryStore {

  public final EventSourcingRepository eventSourcingRepository;
  @PersistenceContext
  private EntityManager entityManager;

  public JpaEventQueryStore(
          EventSourcingRepository eventSourcingRepository) {

    this.eventSourcingRepository = eventSourcingRepository;
  }

  private StoredEvent toModel(EventSourcingInfo eventSourcing) {
    StoredEvent storedEvent = new StoredEvent();
    storedEvent.setId(eventSourcing.getId());
    storedEvent.setPayload(eventSourcing.getEventBody());

    storedEvent.setTopic(eventSourcing.getTopic());
    storedEvent.setSubTopic(eventSourcing.getSubTopic());
    storedEvent.setTagName(eventSourcing.getTagName());
    storedEvent.setEventId(eventSourcing.getEventId());
    storedEvent.setPersistenceId(eventSourcing.getPersistenceId());
    storedEvent.setLocation(eventSourcing.getLocation());
    storedEvent.setEventType(eventSourcing.getEventType());
    storedEvent.setSenderName(eventSourcing.getSender());
    storedEvent.setSenderId(eventSourcing.getSenderId());
    storedEvent.setSenderScope(eventSourcing.getSenderScope());
    storedEvent.setStoredTime(eventSourcing.getStoredTime());
    storedEvent.setVersion(eventSourcing.getEventVersion());
    storedEvent.setSerializer(eventSourcing.getSerializer());
    if(!StringUtils.isEmpty(eventSourcing.getProperties())){
      try {
        storedEvent.setProperties(JSON.parseObject(eventSourcing.getProperties(),
            new TypeReference<HashMap<String, String>>() {
            }));
      } catch (Exception ex) {

      }
    }
    return storedEvent;
  }

  @Override
  public List<StoredEvent> allStoredEventsSinceByTag(String tagName, Long aStoredId) {

//    ExampleMatcher matcher = ExampleMatcher.matching()
//        .withMatcher("name", match->match.)
//        .withMatcher("company", match -> match.startsWith().ignoreCase());
//    new Specifications<StoredEvent>()
//        .eq(StringUtils.isNotBlank(even.getId()), "id", even.getId())
//        .gt(Objects.nonNull(even.getStatus()), "status", 0)
//        .between("registerTime", new Range<>(new Date()-1, new Date()))
//        .like("eventTitle", "%"+even.getEventTitle+"%")
//        .build();

//    Specification<EventSourcingInfo> spec = new Specification<EventSourcingInfo>() {
//      @Override
//      public Predicate toPredicate(Root<EventSourcingInfo> root, CriteriaQuery<?> criteriaQuery,
//          CriteriaBuilder criteriaBuilder) {
//        root.get()
////        criteriaBuilder.
//        return null;
//      }
//    };

    final List<EventSourcingInfo> eventSourcing =
            eventSourcingRepository.findByTagNameEqualsAndIdGreaterThan(tagName, aStoredId);

    if (CollectionUtils.isEmpty(eventSourcing)) {
      return new ArrayList<>(0);
    }

    return eventSourcing.stream().map(this::toModel).collect(Collectors.toList());
  }

  @Override
  @Transactional(readOnly = true)
  public List<StoredEvent> allStoredEventsSinceByTag(String tagName, Long aStoredId, Integer limit) {
//    entityManager.createNativeQuery()
//    entityManager.lock(EventSourcingInfo.class, LockModeType.PESSIMISTIC_READ);
    final List<EventSourcingInfo> resultList = entityManager.createQuery("SELECT p FROM event_sourcing_info p WHERE tag_name=:tagName AND id>:aStoredId ORDER BY p.id",
            EventSourcingInfo.class).setLockMode(LockModeType.PESSIMISTIC_READ).setParameter("tagName", tagName).setParameter("aStoredId", aStoredId).setMaxResults(limit).getResultList();

//    Pageable pageable = PageRequest.of(0, limit, Sort.by(Sort.Direction.ASC, "id"));
//    Specification<EventSourcingInfo> spec = (Specification<EventSourcingInfo>) (root, criteriaQuery, criteriaBuilder) -> {
//      final Path<Object> tagNamePath = root.get("tagName");
//      final Path<Object> idPath = root.get("id");
//
//      return criteriaBuilder.and(
//              criteriaBuilder.equal(tagNamePath, tagName),
//              criteriaBuilder.gt(idPath.as(Long.class), aStoredId)
//      );
//    };
//
//    final List<EventSourcingInfo> list = eventSourcingRepository.findAll(spec, pageable).getContent();

    return resultList.stream().map(this::toModel).collect(Collectors.toList());
  }

  private EventSourcingInfo toDO(StoredEvent event) {
    EventSourcingInfo eventSourcing = new EventSourcingInfo();
    eventSourcing.setEventId(event.getEventId());
    eventSourcing.setLocation(event.getLocation());
    eventSourcing.setTagName(event.getTagName());
    eventSourcing.setPersistenceId(event.getPersistenceId());
    eventSourcing.setEventType(event.getEventType());
    eventSourcing.setEventBody(event.getPayload());
    eventSourcing.setEventVersion(event.getVersion());
    eventSourcing.setSender(event.getSenderName());
    eventSourcing.setSenderId(event.getSenderId());
    eventSourcing.setSenderScope(event.getSenderScope());
    eventSourcing.setStoredTime(event.getStoredTime());
    eventSourcing.setSerializer(event.getSerializer());
    eventSourcing.setTopic(event.getTopic());
    eventSourcing.setSubTopic(event.getSubTopic());
    return eventSourcing;
  }

}
