package org.smartframework.spring.boot.autoconfigure.saga.core;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import javax.xml.crypto.Data;
import org.smartframework.saga.core.DefaultParticipantInvocationBuilder;
import org.smartframework.saga.core.DefaultSagaDefinitionBuilder;
import org.smartframework.saga.core.DefaultSagaStepBuilder;
import org.smartframework.saga.core.ParticipantInvocation;
import org.smartframework.saga.core.SagaCommandResult;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author noah
 * @create 2022-04-14 16:12
 */
public class SpringSagaStepBuilder<Data> {

  DefaultSagaDefinitionBuilder<Data> parent;
  ApplicationContext applicationContext;

  public String getName() {
    return name;
  }

  private String name;

  public SpringSagaStepBuilder(String name,
      DefaultSagaDefinitionBuilder<Data> parent) {
    this.name = name;
    this.parent = parent;
  }

  public SpringSagaStepBuilder(String name,
      DefaultSagaDefinitionBuilder<Data> parent, ApplicationContext applicationContext) {
    this.name = name;
    this.parent = parent;
    this.applicationContext = applicationContext;
  }

  public SpringSagaStepBuilder<Data> context(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
    return this;
  }

  public SpringParticipantInvocationBuilder<Data> withCompensation(
      Class<? extends ParticipantInvocation<Data>> clazz) {
    return new SpringParticipantInvocationBuilder<>(this, parent, applicationContext)
        .withCompensation(clazz);
  }


  public SpringParticipantInvocationBuilder<Data> invokeParticipant(
      Class<? extends ParticipantInvocation<Data>> clazz) {
    return new SpringParticipantInvocationBuilder<>(this, parent, applicationContext)
        .invokeParticipant(clazz);
  }

  public SpringParticipantInvocationBuilder<Data> withCompensation(
      ParticipantInvocation<Data> compensation) {
    return new SpringParticipantInvocationBuilder<>(this, parent, applicationContext)
        .withCompensation(Optional.of(compensation));
  }

  public SpringParticipantInvocationBuilder<Data> withCompensation(
      Function<Data, SagaCommandResult> compensation) {
    return new SpringParticipantInvocationBuilder<>(this, parent, applicationContext)
        .withCompensation(compensation);
  }

  public SpringParticipantInvocationBuilder<Data> withCompensation(
      Predicate<Data> compensationPredicate,
      Function<Data, SagaCommandResult> compensation) {
    return new SpringParticipantInvocationBuilder<>(this, parent, applicationContext)
        .withCompensation(compensationPredicate, compensation);
  }

  public SpringParticipantInvocationBuilder<Data> invokeOriginParticipant(
      ParticipantInvocation<Data> action) {
    return new SpringParticipantInvocationBuilder<>(this, parent, applicationContext)
        .invokeParticipant(Optional.of(action));
  }

  public SpringParticipantInvocationBuilder<Data> invokeParticipant(
      Function<Data, SagaCommandResult> action) {
    return new SpringParticipantInvocationBuilder<>(this, parent, applicationContext)
        .invokeParticipant(action);
  }

  public SpringParticipantInvocationBuilder<Data> invokeParticipant(
      Predicate<Data> invokeParticipantPredicate,
      Function<Data, SagaCommandResult> action) {

    return new SpringParticipantInvocationBuilder<>(this, parent, applicationContext)
        .invokeParticipant(invokeParticipantPredicate, action);
  }

}
