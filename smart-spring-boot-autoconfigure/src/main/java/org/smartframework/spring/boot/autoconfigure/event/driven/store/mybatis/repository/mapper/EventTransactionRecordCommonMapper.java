package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventTransactionRecordDO;

/**
* <p>
 * 消息队列消费记录，幂等 Mapper 接口
 * </p>
*
* @author System
* @since 2022-03-24
*/
 @Mapper
public interface EventTransactionRecordCommonMapper extends BaseMapper<EventTransactionRecordDO> {

}
