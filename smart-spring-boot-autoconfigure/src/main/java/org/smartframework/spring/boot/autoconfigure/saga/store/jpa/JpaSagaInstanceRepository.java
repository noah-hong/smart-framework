package org.smartframework.spring.boot.autoconfigure.saga.store.jpa;

import java.lang.reflect.UndeclaredThrowableException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.smartframework.core.JsonUtils;
import org.smartframework.saga.core.SagaExecutionState;
import org.smartframework.saga.core.SagaInstance;
import org.smartframework.saga.core.SagaInstanceRepository;
import org.smartframework.spring.boot.autoconfigure.saga.store.jpa.entity.SagaInstanceInfo;
import org.smartframework.spring.boot.autoconfigure.saga.store.jpa.entity.SagaStepInfo;
import org.smartframework.spring.boot.autoconfigure.saga.store.jpa.repository.SagaInstanceInfoRepository;

import java.util.Optional;
import org.smartframework.spring.boot.autoconfigure.saga.store.jpa.repository.SagaStepInfoRepository;

/**
 * @author noah
 * @create 2021-10-13 18:00
 */
public class JpaSagaInstanceRepository implements SagaInstanceRepository {

  private SagaInstanceInfoRepository sagaInstanceInfoRepository;
  private SagaStepInfoRepository sagaStepInfoRepository;

  public JpaSagaInstanceRepository(SagaInstanceInfoRepository sagaInstanceInfoRepository,
      SagaStepInfoRepository sagaStepInfoRepository) {
    this.sagaInstanceInfoRepository = sagaInstanceInfoRepository;
    this.sagaStepInfoRepository = sagaStepInfoRepository;
  }


  @Override
  public void save(SagaInstance sagaInstance) {
    SagaInstanceInfo sagaInstanceInfo = new SagaInstanceInfo();
    sagaInstanceInfo.setSagaRecoverStrategy(sagaInstance.getSagaRecoverStrategy().name());
    sagaInstanceInfo.setSagaType(sagaInstance.getSagaType().getName());
    sagaInstanceInfo.setCurrentlyExecuting(sagaInstance.getCurrentlyExecuting());
    sagaInstanceInfo.setCompensating(sagaInstance.isCompensating());
    sagaInstanceInfo.setEndState(sagaInstance.isEndState());
    if (sagaInstance.getExecutingException() != null) {
      final Throwable executingRootException = ExceptionUtils
          .getRootCause(sagaInstance.getExecutingException());
      sagaInstanceInfo.setExecutingException(executingRootException.toString());
    }
    if (sagaInstance.getCompensatingException() != null) {
      final Throwable compensatingRootException = ExceptionUtils
          .getRootCause(sagaInstance.getCompensatingException());
      sagaInstanceInfo.setCompensatingException(compensatingRootException.toString());
    }
    sagaInstanceInfo.setLastMessage(sagaInstance.getLastMessage());
    sagaInstanceInfo.setStop(sagaInstance.isStop());
    sagaInstanceInfo.setCurrentlyStepName(sagaInstance.getCurrentlyStepName());
    sagaInstanceInfo.setSagaData(JsonUtils.toJsonString(sagaInstance.getSagaData()));
    sagaInstanceInfo
        .setExecutingMessages(JsonUtils.toJsonString(sagaInstance.getExecutingMessages()));
    sagaInstanceInfo
        .setCompensatingMessages(JsonUtils.toJsonString(sagaInstance.getCompensatingMessages()));
    sagaInstanceInfo.setSagaId(sagaInstance.getSagaId());
    sagaInstanceInfo.setSagaName(sagaInstance.getSagaName());
    sagaInstanceInfo.setSagaStatus(sagaInstance.getSagaStatus().name());

    sagaInstanceInfoRepository.save(sagaInstanceInfo);

  }

  @Override
  public void saveAsync(SagaInstance sagaInstance) {

  }

  private SagaInstance toSagaInstance(SagaInstanceInfo sagaInstanceInfo) {
    SagaInstance sagaInstance = new SagaInstance();
    sagaInstance.setCurrentlyExecuting(sagaInstanceInfo.getCurrentlyExecuting());
    sagaInstance.setCompensating(sagaInstanceInfo.isCompensating());
    sagaInstance.setEndState(sagaInstanceInfo.isEndState());
    sagaInstance.setStop(sagaInstanceInfo.isStop());
    sagaInstance.setCurrentlyStepName(sagaInstanceInfo.getCurrentlyStepName());
    sagaInstance.setSagaData(JsonUtils.parseObj(sagaInstanceInfo.getSagaData()));
    sagaInstance.setSagaId(sagaInstanceInfo.getSagaId());
    sagaInstance.setSagaName(sagaInstanceInfo.getSagaName());
    try {
      final Class<?> aClass = Class
          .forName(sagaInstanceInfo.getSagaType(), true,
              Thread.currentThread().getContextClassLoader());
      sagaInstance.setSagaType(aClass);
    } catch (Exception ex) {
      throw new UndeclaredThrowableException(ex, "saga类型获取异常");
    }
    return sagaInstance;
  }

  @Override
  public SagaInstance find(String sagaId) {
    final Optional<SagaInstanceInfo> sagaInstanceInfo = sagaInstanceInfoRepository.findById(sagaId);

    return sagaInstanceInfo.map(this::toSagaInstance).orElse(null);
  }

  @Override
  public void savaStep(String sagaId,SagaExecutionState currState, SagaExecutionState state, Object sagaData, boolean skip) {
    SagaStepInfo sagaStepInfo = new SagaStepInfo();
    sagaStepInfo.setSagaId(sagaId);
    sagaStepInfo.setCurrentlyExecuting(state.getCurrentlyExecuting());
    sagaStepInfo.setCompensating(currState.isCompensating());
    sagaStepInfo.setEndState(state.isEndState());
    sagaStepInfo.setSkip(skip);
    if (state.getExecutingException() != null) {
      final Throwable executingRootException = ExceptionUtils
          .getRootCause(state.getExecutingException());
      sagaStepInfo.setExecutingException(executingRootException.toString());
    }
    if (state.getCompensatingException() != null) {
      final Throwable compensatingRootException = ExceptionUtils
          .getRootCause(state.getCompensatingException());
      sagaStepInfo.setCompensatingException(compensatingRootException.toString());
    }
    sagaStepInfo.setLastMessage(state.getLastMessage());
    sagaStepInfo.setStop(state.isStop());
    sagaStepInfo.setCurrentlyStepName(state.getCurrentlyStepName());
    sagaStepInfo.setSagaData(JsonUtils.toJsonString(sagaData));
    sagaStepInfo
        .setExecutingMessages(JsonUtils.toJsonString(state.getExecutingMessages()));
    sagaStepInfo
        .setCompensatingMessages(JsonUtils.toJsonString(state.getCompensatingMessages()));
    sagaStepInfo.setSagaId(sagaId);
    sagaStepInfo.setSagaStatus(state.getSagaStatus().name());

    sagaStepInfoRepository.save(sagaStepInfo);

    final Optional<SagaInstanceInfo> findSagaInstanceInfo = sagaInstanceInfoRepository
        .findById(sagaId);
    if (findSagaInstanceInfo.isPresent()) {

      final SagaInstanceInfo sagaInstanceInfo = findSagaInstanceInfo.get();
      sagaInstanceInfo.setSagaRecoverStrategy(currState.getSagaRecoverType().name());
      sagaInstanceInfo.setCurrentlyExecuting(sagaStepInfo.getCurrentlyExecuting());
      sagaInstanceInfo.setCompensating(sagaStepInfo.isCompensating());
      sagaInstanceInfo.setEndState(sagaStepInfo.isEndState());
      sagaInstanceInfo.setExecutingException(sagaStepInfo.getExecutingException());
      sagaInstanceInfo.setCompensatingException(sagaStepInfo.getCompensatingException());
      sagaInstanceInfo.setLastMessage(sagaStepInfo.getLastMessage());
      sagaInstanceInfo.setStop(sagaStepInfo.isStop());
      sagaInstanceInfo.setCurrentlyStepName(sagaStepInfo.getCurrentlyStepName());
      sagaInstanceInfo.setSagaData(JsonUtils.toJsonString(sagaData));
      sagaInstanceInfo
          .setExecutingMessages(JsonUtils.toJsonString(sagaStepInfo.getExecutingMessages()));
      sagaInstanceInfo
          .setCompensatingMessages(JsonUtils.toJsonString(sagaStepInfo.getCompensatingMessages()));
      sagaInstanceInfo.setSagaId(sagaId);
      sagaInstanceInfo.setSagaStatus(sagaStepInfo.getSagaStatus());
      sagaInstanceInfoRepository.save(sagaInstanceInfo);
    }


  }

  @Override
  public void savaStepAsync(String sagaId, SagaExecutionState currstate, SagaExecutionState state,
      Object sagaData, boolean skip) {

  }
}
