package org.smartframework.spring.boot.autoconfigure.saga.store.jpa;

import org.smartframework.saga.core.Saga;
import org.smartframework.saga.core.SagaInstanceRepository;
import org.smartframework.saga.core.SagaStepCompletedObserver;
import org.smartframework.spring.boot.autoconfigure.saga.store.jpa.repository.SagaInstanceInfoRepository;
import org.smartframework.spring.boot.autoconfigure.saga.store.jpa.repository.SagaStepInfoRepository;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author noah
 * @create 2021-10-13 18:02
 */
@Configuration(proxyBeanMethods = false)
@EntityScan
@EnableJpaRepositories
@AutoConfigureAfter(JpaRepositoriesAutoConfiguration.class)
@ConditionalOnBean(JpaRepositoriesAutoConfiguration.class)
@ConditionalOnClass(Saga.class)
public class JpaSagaStoreAutoConfiguration {

  @Bean
  @ConditionalOnMissingBean
  public SagaInstanceRepository jpaSagaInstanceRepository(
      SagaInstanceInfoRepository sagaInstanceInfoRepository,
      SagaStepInfoRepository sagaStepInfoRepository) {
    return new JpaSagaInstanceRepository(sagaInstanceInfoRepository, sagaStepInfoRepository);
  }

  @Bean
  public SagaStepCompletedObserver defaultSaveSagaStepCompletedObserver(
      SagaInstanceRepository sagaInstanceRepository) {
    return new JpaSaveSagaStepCompletedObserver(sagaInstanceRepository);
  }


}
