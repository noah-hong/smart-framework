package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.entity.SagaInstanceInfoDO;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.SagaInstanceInfoCommonRepository;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.mapper.SagaInstanceInfoCommonMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
* <p>
 * saga执行结果信息 服务实现类
 * </p>
*
* @author System
* @since 2022-04-05
*/
@Repository
@Primary
public class SagaInstanceInfoCommonRepositoryImpl extends ServiceImpl<SagaInstanceInfoCommonMapper, SagaInstanceInfoDO> implements
    SagaInstanceInfoCommonRepository {

}
