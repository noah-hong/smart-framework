package org.smartframework.spring.boot.autoconfigure.saga.annotation;

import org.springframework.stereotype.Service;

import java.lang.annotation.*;

/**
 * @author noah
 * @create 2021-10-14 12:07
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Service
public @interface Saga {
}
