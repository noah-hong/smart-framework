package org.smartframework.spring.boot.autoconfigure.saga.core;

import java.util.List;
import org.smartframework.saga.core.DefaultSagaManager;
import org.smartframework.saga.core.Saga;
import org.smartframework.saga.core.SagaInstanceRepository;
import org.smartframework.saga.core.SagaStepCompletedObserver;
import org.springframework.context.ApplicationContext;

/**
 * @author noah
 * @create 2021-10-18 11:11
 */
public class SpringSagaManager<Data> extends DefaultSagaManager<Data> {

  @Override
  protected Saga<Data> restoreSaga(Class<?> aSagaType) {
    return (Saga<Data>) applicationContext.getBean(aSagaType);
  }

  public SpringSagaManager(ApplicationContext applicationContext,
      SagaInstanceRepository sagaInstanceRepository,
      List<SagaStepCompletedObserver> observers) {
    super(sagaInstanceRepository, observers);
    this.applicationContext = applicationContext;
  }

  public SpringSagaManager(Saga<Data> saga,
      SagaInstanceRepository sagaInstanceRepository,
      List<SagaStepCompletedObserver> observers) {
    super(saga, sagaInstanceRepository, observers);
  }

  private ApplicationContext applicationContext;


}
