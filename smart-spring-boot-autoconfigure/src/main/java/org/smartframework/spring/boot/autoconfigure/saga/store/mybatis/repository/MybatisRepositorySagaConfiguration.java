package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository;

import org.mybatis.spring.annotation.MapperScan;
import org.smartframework.saga.core.Saga;
import org.smartframework.saga.core.SagaDsl;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.MybatisSagaStoreAutoConfiguration;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.mapper.SagaInstanceInfoCommonMapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author noah
 * @create 2022-04-05 11:24
 */
@ComponentScan(basePackages = "org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository")
@MapperScan(basePackageClasses = {SagaInstanceInfoCommonMapper.class})
@ConditionalOnClass(Saga.class)
//@ConditionalOnBean(MybatisSagaStoreAutoConfiguration.class)
public class MybatisRepositorySagaConfiguration {

}
