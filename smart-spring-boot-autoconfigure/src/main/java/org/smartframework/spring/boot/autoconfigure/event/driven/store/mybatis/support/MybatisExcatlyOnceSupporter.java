package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.support;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.ConsumeStatusEnum;
import org.smartframework.event.driven.core.EventSender;
import org.smartframework.event.driven.core.context.IMessageContext;
import org.smartframework.event.driven.core.context.MessageInfo;
import org.smartframework.event.driven.core.store.IExcatlyOnceSupporter;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventTransactionRecord;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventTransactionRecordRepository;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventTransactionRecordDO;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.EventTransactionRecordCommonRepository;
import org.springframework.dao.DataIntegrityViolationException;

/**
 * @author noah
 * @create 2020-05-21 18:16
 */
public class MybatisExcatlyOnceSupporter implements IExcatlyOnceSupporter {

  private final EventTransactionRecordCommonRepository repository;
  Logger log = LoggerFactory.getLogger(MybatisExcatlyOnceSupporter.class);

  public MybatisExcatlyOnceSupporter(EventTransactionRecordCommonRepository repository) {
    this.repository = repository;
  }

  @Override
  public ConsumeStatusEnum support(IMessageContext messageContext) {
//    return ConsumeStatusEnum.UNCONSUMED;
    try {
      MessageInfo messageInfo = messageContext.getMessageInfo();
      EventSender sender = messageContext.getEventEnvelop().getSender();
      EventTransactionRecordDO recordDO = new EventTransactionRecordDO();
      recordDO.setMessageId(messageInfo.getMessageId());
      recordDO.setMessageKey(messageInfo.getMessageKey());
      recordDO.setConsumerGroup(messageInfo.getConsumerGroup());
      recordDO.setReconsumeTimes(messageInfo.getReconsumeTimes());
      recordDO.setTopic(messageInfo.getTopic());
      recordDO.setSubTopic(messageInfo.getSubTopic());
      recordDO.setStoredTime(messageInfo.getStoreTime());
      recordDO.setSender(sender.getName());
      recordDO.setSenderId(sender.getId());
      recordDO.setSenderScope(sender.getScope());
      repository.save(recordDO);
      return ConsumeStatusEnum.UNCONSUMED;
    } catch (DataIntegrityViolationException e2) {
      return ConsumeStatusEnum.CONSUMED;
    } catch (Exception ex) {
      if (ex.getMessage().contains("uk_message_key") || ex.getMessage().contains("uk_message_id")) {
        return ConsumeStatusEnum.CONSUMED;
      }
      log.error(ex.getMessage(), ex);
      return ConsumeStatusEnum.CONSUMING;
    }
  }
}
