package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.support;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.smartframework.event.driven.core.store.IPublishedEventTrackerStore;
import org.smartframework.event.driven.core.store.PublishedEventTracker;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity.EventTrackerInfo;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.repository.EventTrackerRepository;


/**
 * @author noah
 * @create 2020-05-10 20:51
 */
public class JpaMessageTrackerPublishedStore implements IPublishedEventTrackerStore {

  private final EventTrackerRepository eventTrackerRepository;

  public JpaMessageTrackerPublishedStore(EventTrackerRepository eventTrackerRepository) {
    this.eventTrackerRepository = eventTrackerRepository;
  }

  private PublishedEventTracker toModel(EventTrackerInfo eventTrackerDO) {
    PublishedEventTracker tracker = new PublishedEventTracker();
    tracker.setMostRecentStoredId(eventTrackerDO.getMostRecentStoredId());
    tracker.setTagName(eventTrackerDO.getTagName());
    return tracker;
  }

  private EventTrackerInfo toDO(PublishedEventTracker tracker) {
    EventTrackerInfo eventTrackerDO = new EventTrackerInfo();
    eventTrackerDO.setMostRecentStoredId(tracker.getMostRecentStoredId());
    eventTrackerDO.setTagName(tracker.getTagName());
    return eventTrackerDO;
  }

  @Override
  public List<PublishedEventTracker> getTrackers() {
    final Iterator<EventTrackerInfo> iterator = eventTrackerRepository.findAll().iterator();
    List<PublishedEventTracker> list = new ArrayList<>();
    while (iterator.hasNext()) {
      final EventTrackerInfo next = iterator.next();
      list.add(toModel(next));
    }
    return list;
  }

  @Override
  public PublishedEventTracker getTrackerByTagName(String aTagName) {
    final EventTrackerInfo eventTrackerDO = eventTrackerRepository.findByTagName(aTagName);

    return toModel(eventTrackerDO);
  }

  @Override
  public void track(PublishedEventTracker tracker) {
    final EventTrackerInfo eventTrackerDO = toDO(tracker);
    eventTrackerRepository.save(eventTrackerDO);
  }
}
