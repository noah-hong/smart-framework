package org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.entity.SagaStepInfoDO;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.SagaStepInfoCommonRepository;
import org.smartframework.spring.boot.autoconfigure.saga.store.mybatis.repository.mapper.SagaStepInfoCommonMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

/**
* <p>
 * saga执行步骤信息 服务实现类
 * </p>
*
* @author System
* @since 2022-04-05
*/
@Repository
@Primary
public class SagaStepInfoCommonRepositoryImpl extends ServiceImpl<SagaStepInfoCommonMapper, SagaStepInfoDO> implements
    SagaStepInfoCommonRepository {

}
