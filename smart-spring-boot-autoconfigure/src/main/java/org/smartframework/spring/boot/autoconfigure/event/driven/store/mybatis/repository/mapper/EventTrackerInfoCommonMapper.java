package org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.smartframework.spring.boot.autoconfigure.event.driven.store.mybatis.entity.EventTrackerInfoDO;

/**
* <p>
 * 事件发布追踪者 Mapper 接口
 * </p>
*
* @author System
* @since 2022-03-24
*/
 @Mapper
public interface EventTrackerInfoCommonMapper extends BaseMapper<EventTrackerInfoDO> {

}
