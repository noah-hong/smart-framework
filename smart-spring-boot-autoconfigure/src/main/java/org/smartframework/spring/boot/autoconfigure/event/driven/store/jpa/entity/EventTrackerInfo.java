package org.smartframework.spring.boot.autoconfigure.event.driven.store.jpa.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author noah
 * @create 2020-05-10 16:33
 */
@Entity(name = "event_tracker_info")
public class EventTrackerInfo implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @Column(name = "tag_name")
  private String tagName;

  @Column(name = "most_recent_stored_id")
  private long mostRecentStoredId;

  public long getMostRecentStoredId() {
    return mostRecentStoredId;
  }

  public void setMostRecentStoredId(long mostRecentStoredId) {
    this.mostRecentStoredId = mostRecentStoredId;
  }


  public String getTagName() {
    return tagName;
  }

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }
}
