package org.smartframework.core;

/**
 * @author noah
 * @create 2021-08-18 10:31
 */
public final class SmartUtils {

  public static <T> T value(T value, T defaultValue) {
    return value == null ? defaultValue : value;
  }

}
