package org.smartframework.event.driven.rocketmq;

import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.deserializer.ExtraProcessor;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeWriter;
import org.smartframework.core.IPersistenceId;
import org.smartframework.event.driven.core.IEvent;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * @author noah
 * @create 2021-05-08 14:49
 */
public class TestApp {

  public static void main(String[] args) {
    int i=0;
    i++;
    i++;
    i++;
    System.out.println(i);
//    ExtraProcessor processor = new TestApp.MyExtraProcessor();

//    Model2 model2 = new Model2();
//    DefaultEvent event = new DefaultEvent();
//    event.setPersistenceId(new DefaultId());
//    event.setTagName("test");
//    model2.setEvent(event);
//    model2.setId(1);
//    System.out.println(JSON.toJSON(model2));
//    final byte[] o = JSON.toJSONBytes(model2);
//
////    final Model2 model21 = JSON.parseObject(o.toString(), Model2.class);
////    System.out.println(JSON.toJSON(model21));
//    ParserConfig parserConfig = new ParserConfig();
//    parserConfig.putDeserializer(IEvent.class, new EventDeser(DefaultEvent.class));
////    Model2 vo = JSON
////        .parseObject(o.toString(), Model2.class, parserConfig, JSON.DEFAULT_PARSER_FEATURE);
//    Model2 vo =  JSON.parseObject(o, (Charset) IOUtils.UTF8, Model2.class, parserConfig, null,
//        JSON.DEFAULT_PARSER_FEATURE);
//
//    System.out.println(JSON.toJSON(vo));
////    VO vo = JSON.parseObject("{\"id\":123,\"value\":\"123456\"}", VO.class, processor);
////    System.out.println(JSON.toJSON(vo));
//
//    ParserConfig.getGlobalInstance()
//        .putDeserializer(OrderActionEnum.class, new OrderActionEnumDeser());
//
//    Msg msg = JSON.parseObject("{\"actionEnum\":0,\"body\":\"B\"}", Msg.class);
//    System.out.println(JSON.toJSON(msg));
//    Model model = new Model();
//    model.id = 1001;
//    String text = JSON.toJSONString(model);
//    System.out.println(text);
//    Map<String, String> map = new HashMap<>();
//    System.out.println(map.put("1", "2"));
//    System.out.println(map.put("1", "2"));
  }

  public static class Model2 {

    public int id;
    private IEvent<?> event;

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public IEvent<?> getEvent() {
      return event;
    }

    public void setEvent(IEvent<?> event) {
      this.event = event;
    }
  }

  public static class DefaultId implements IPersistenceId<Integer> {

    @Override
    public Integer getValue() {
      return 1;
    }
  }

  public static class DefaultEvent implements IEvent<DefaultId> {

    private DefaultId persistenceId;
    private String tagName;

    public void setPersistenceId(DefaultId persistenceId) {
      this.persistenceId = persistenceId;
    }

    public void setTagName(String tagName) {
      this.tagName = tagName;
    }

    @Override
    public DefaultId getPersistenceId() {
      return persistenceId;
    }

    @Override
    public String getTagName() {
      return tagName;
    }
  }

  @JSONType(serializer = ModelSerializer.class)
  public static class Model {

    public int id;
  }

  public static class ModelSerializer implements ObjectSerializer {

    @Override
    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType,
        int features) throws IOException {
      Model model = (Model) object;
      SerializeWriter out = serializer.getWriter();
      out.writeFieldValue('{', "ID", model.id);
      out.write('}');
    }

  }

  public static enum OrderActionEnum {
    FAIL(1), SUCC(0);

    private int code;

    OrderActionEnum(int code) {
      this.code = code;
    }
  }

  public static class Msg {

    public OrderActionEnum actionEnum;
    public String body;
  }

  public static class OrderActionEnumDeser implements ObjectDeserializer {

    @SuppressWarnings("unchecked")
    @Override
    public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
      Integer intValue = parser.parseObject(int.class);
      if (intValue == 1) {
        return (T) OrderActionEnum.FAIL;
      } else if (intValue == 0) {
        return (T) OrderActionEnum.SUCC;
      }
      throw new IllegalStateException();
    }

    @Override
    public int getFastMatchToken() {
      return JSONToken.LITERAL_INT;
    }

  }

  public static class EventDeser implements ObjectDeserializer {

    private Class<?> clazz;

    public EventDeser(Class<?> clazz) {
      this.clazz = clazz;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
      IEvent intValue = (IEvent) parser.parseObject(clazz);
      return (T) intValue;
//      throw new IllegalStateException();
    }

    @Override
    public int getFastMatchToken() {
      return JSONToken.LITERAL_INT;
    }

  }

  public static class VO {

    private int id;
    private Map<String, Object> attributes = new HashMap<String, Object>();


    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public Map<String, Object> getAttributes() {
      return attributes;
    }
  }

  static class MyExtraProcessor implements ExtraProcessor {

    private Class<?> clazz;

    public MyExtraProcessor(Class<?> clazz) {
      this.clazz = clazz;
    }

    public void processExtra(Object object, String key, Object value) {
      Model2 vo = (Model2) object;
      vo.setId(222);
    }

    public Type getExtraType(Object object, String key) {
      if ("value".equals(key)) {
        return int.class;
      }
      return null;
    }
  }

  ;
}
