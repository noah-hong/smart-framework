package org.smartframework.event.driven.rocketmq.support;

import org.apache.commons.collections4.MapUtils;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.NamespaceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.EventEnvelope;
import org.smartframework.event.driven.core.IEventListenerProxy;
import org.smartframework.event.driven.core.SubscribeResult;
import org.smartframework.event.driven.core.context.ExecutionStatus;
import org.smartframework.event.driven.core.context.ISubscribeResult;
import org.smartframework.event.driven.core.context.MessageInfo;
import org.smartframework.event.driven.core.exception.MessageConsumedException;
import org.smartframework.event.driven.core.exception.MessageConsumingException;
import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
import org.smartframework.event.driven.rocketmq.RocketMQMessageContext;
import org.smartframework.event.driven.rocketmq.exceptions.LossOfSubscriberException;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;

/**
 * @author noah
 * @create 2021-04-30 11:44
 */
public class DefaultMessageListenerOrderly implements MessageListenerOrderly {

    private static final Logger log = LoggerFactory.getLogger(DefaultMessageListenerOrderly.class);
    private final static long SUSPEND_CURRENT_QUEUE_TIME_MILLIS = 30000;
    private final IEventListenerProxy eventListenerProxy;
    private final IEventEnvelopeFormatter storedEventSerializer;

    private final DefaultMQPushConsumer consumer;

    public DefaultMessageListenerOrderly(DefaultMQPushConsumer consumer,
                                         IEventListenerProxy eventListenerProxy, IEventEnvelopeFormatter storedEventSerializer) {
        this.eventListenerProxy = eventListenerProxy;
        this.consumer = consumer;
        this.storedEventSerializer = storedEventSerializer;
    }

    private EventEnvelope getEventEnvelop(MessageExt messageExt) {
        return this.storedEventSerializer.deserialize(messageExt.getBody());
    }

//  public Class<IEvent<?>> getEventClazz(MessageExt messageExt) {
//    String className = messageExt.getProperty(Properties4RocketMQ.PAYLOAD_CLASS_NAME);
//    if (StringUtils.isBlank(className)) {
//      throw new LossOfSubscriberException(
//          "订阅处理类丢失，TOPIC：" + messageExt.getTopic()
//              + " TAGs:" + messageExt.getTags());
//    }
//    try {
//      final Class<IEvent<?>> clazz = LegcySubscribeEventTypeFactory.get(className);
//      if (clazz == null) {
//        LegcySubscribeEventTypeFactory
//            .add(className, (Class<IEvent<?>>) Class.forName(className));
//        return LegcySubscribeEventTypeFactory.get(className);
//      }
//      return clazz;
//    } catch (Exception exception) {
//      throw new RuntimeException("无法找到订阅类:" + className);
//    }
//  }

    private ISubscribeResult handleMessage(MessageExt messageExt)
            throws MessageConsumingException, MessageConsumedException {
        Objects.requireNonNull(eventListenerProxy);
        MessageInfo messageInfo = new MessageInfo();
        String consumerGroup = NamespaceUtil
                .wrapNamespace(consumer.getNamespace(), consumer.getConsumerGroup());
        String topic = NamespaceUtil.wrapNamespace(consumer.getNamespace(), messageExt.getTopic());
        messageInfo.setConsumerGroup(consumerGroup);
        messageInfo.setTopic(topic);
        messageInfo.setSubTopic(messageExt.getTags());
        messageInfo.setMessageId(messageExt.getMsgId());
        messageInfo.setMessageKey(messageExt.getKeys());
        messageInfo.setQueueId(messageExt.getQueueId());
        messageInfo.setMessageOffset(messageExt.getCommitLogOffset());
        messageInfo.setReconsumeTimes(messageExt.getReconsumeTimes());
        messageInfo
                .setBroker(Optional.ofNullable(messageExt.getBrokerName()).orElseGet(() -> "unknown"));
        messageInfo.setBornTimestamp(messageExt.getBornTimestamp());
        Properties properties = MapUtils.toProperties(messageExt.getProperties());
        messageInfo.setProperties(properties);
        Instant instant = Instant.ofEpochMilli(messageExt.getStoreTimestamp());
        ZoneId zone = ZoneId.systemDefault();
        messageInfo.setStoreTime(LocalDateTime.ofInstant(instant, zone));
        EventEnvelope eventEnvelop = getEventEnvelop(messageExt);
        if (eventEnvelop == null) {
            log.error("消息消费失败，无法获取转换事件报文，topic:{},messageKey:{}", messageExt.getTopic(),
                    messageExt.getKeys());
            return SubscribeResult.SUCCESS;
        }
        return eventListenerProxy.subscribe(new RocketMQMessageContext(messageInfo, eventEnvelop));
    }

    private static final String FAILURE_COUNT_NAME = "failure_count";
    private static final Integer MAX_FAILURE_COUNT = 3;

    //ConsumeMessageOrderlyService
    @Override
    public ConsumeOrderlyStatus consumeMessage(
            List<MessageExt> msgs, ConsumeOrderlyContext context) {

        context.setSuspendCurrentQueueTimeMillis(SUSPEND_CURRENT_QUEUE_TIME_MILLIS);
        for (MessageExt messageExt : msgs) {
            int failureCount = messageExt.getReconsumeTimes();
            try {
                ISubscribeResult subscribeResult = handleMessage(messageExt);
                if (subscribeResult.getExecutionStatus() == ExecutionStatus.FALIURE
                        || subscribeResult.getExecutionStatus() == ExecutionStatus.UNKNOWN) {
                    log.warn("消费失败，失败次数：{}，consumer group：{}，topic：{}，tags:{}，key:{}，message：{}"
                            , failureCount
                            , consumer.getConsumerGroup()
                            , messageExt.getTopic()
                            , messageExt.getTags()
                            , messageExt.getKeys()
                            , subscribeResult.getException() == null ? "" : subscribeResult.getException().getMessage());
                    return ConsumeOrderlyStatus.SUSPEND_CURRENT_QUEUE_A_MOMENT;
                }
            } catch (LossOfSubscriberException ex) {
                log.warn("消费失败，订阅者丢失", ex);
                return ConsumeOrderlyStatus.SUCCESS;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                log.warn("消费失败，失败次数：{}，消费者组：{}，TOPIC：{}，TAGS:{}，KEY:{}"
                        , failureCount
                        , consumer.getConsumerGroup()
                        , messageExt.getTopic()
                        , messageExt.getTags()
                        , messageExt.getKeys());
                return ConsumeOrderlyStatus.SUSPEND_CURRENT_QUEUE_A_MOMENT;
            }

        }
        return ConsumeOrderlyStatus.SUCCESS;
    }
}
