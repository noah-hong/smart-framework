package org.smartframework.event.driven.rocketmq;

/**
 * @author noah
 * @create 2021-01-21 16:13
 */
public final class Properties4RocketMQ {

  public final static String PAYLOAD_CLASS_NAME = "PAYLOAD_CLASS_NAME";
  public final static String PAYLOAD_SERIALIZER = "PAYLOAD_SERIALIZER";
  public final static String SUBSCRIBE_METHOD = "subscribe";
}
