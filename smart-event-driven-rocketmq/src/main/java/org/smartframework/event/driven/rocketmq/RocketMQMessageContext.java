package org.smartframework.event.driven.rocketmq;

import org.smartframework.event.driven.core.EventEnvelope;
import org.smartframework.event.driven.core.context.IMessageContext;
import org.smartframework.event.driven.core.context.MessageInfo;

/**
 * @author noah
 * @create 2021-04-30 17:00
 */
public class RocketMQMessageContext implements IMessageContext {

  public RocketMQMessageContext(MessageInfo messageInfo,
      EventEnvelope eventEnvelope) {
    this.messageInfo = messageInfo;
    this.eventEnvelope = eventEnvelope;
  }

  public void setMessageInfo(MessageInfo messageInfo) {
    this.messageInfo = messageInfo;
  }

  public void setEventEnvelope(EventEnvelope eventEnvelope) {
    this.eventEnvelope = eventEnvelope;
  }

  private MessageInfo messageInfo;
  private EventEnvelope eventEnvelope;

  @Override
  public MessageInfo getMessageInfo() {
    return messageInfo;
  }

  @Override
  public EventEnvelope getEventEnvelop() {
    return eventEnvelope;
  }
}
