package org.smartframework.event.driven.rocketmq.exceptions;

/**
 * @author noah
 * @create 2021-05-30 19:48
 */
public class LossOfSubscriberException extends RuntimeException {


  public LossOfSubscriberException(String message) {
    super(message);
  }
}
