package org.smartframework.event.driven.rocketmq;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.consumer.listener.MessageListener;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.impl.MQClientManager;
import org.apache.rocketmq.client.impl.factory.MQClientInstance;
import org.apache.rocketmq.common.UtilAll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.IEventListenerProxy;
import org.smartframework.event.driven.core.IEventReceiver;
import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.TopicModel;
import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
import org.smartframework.event.driven.rocketmq.annotation.RocketMQEventListener;
import org.smartframework.event.driven.rocketmq.support.CustomRemoteBrokerOffsetStore;
import org.smartframework.event.driven.rocketmq.support.DefaultMessageListenerConcurrently;
import org.smartframework.event.driven.rocketmq.support.DefaultMessageListenerOrderly;
import org.smartframework.rocketmq.SmartMQPushConsumer;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * @author noah
 * @create 2021-04-29 13:56
 */
public class RocketMQEventReceiver implements IEventReceiver {

  private final static Logger log = LoggerFactory.getLogger(RocketMQEventReceiver.class);
  private final Class<?> eventListenerType;
  private final RocketMQEventListener eventListenerAnnotation;

  private SmartMQPushConsumer consumer;
  private final IEventListenerProxy eventListenerProxy;

  public IEventEnvelopeFormatter getStoredEventSerializer() {
    return storedEventSerializer;
  }

  public void setStoredEventSerializer(
      IEventEnvelopeFormatter storedEventSerializer) {
    this.storedEventSerializer = storedEventSerializer;
  }

  private IEventEnvelopeFormatter storedEventSerializer;

  public ITopicGenerator getTopicGenerator() {
    return topicGenerator;
  }

  public void setTopicGenerator(ITopicGenerator topicGenerator) {
    this.topicGenerator = topicGenerator;
  }

  private ITopicGenerator topicGenerator;

  public RocketMQEventReceiver(IEventListenerProxy eventListenerProxy) {
    this.eventListenerProxy = eventListenerProxy;
    this.eventListenerType = eventListenerProxy.getProxyTargetType();
    this.eventListenerAnnotation = eventListenerType
        .getAnnotation(RocketMQEventListener.class);

  }

  public void init() {
    String consumerGroup = getInternalConsumerGroup();
    this.consumer = new SmartMQPushConsumer(consumerGroup);

    initConsumer(consumer);
  }

  public RocketMQEventReceiver(IEventListenerProxy eventListenerProxy,
      ITopicGenerator topicGenerator, IEventEnvelopeFormatter storedEventSerializer) {
    this(eventListenerProxy);
    this.storedEventSerializer = storedEventSerializer;
    this.topicGenerator = topicGenerator;
  }

  protected void initConsumer(SmartMQPushConsumer consumer) {
    String nameServer = getInternalNameServer();
    String namespace = getInternalNameSpace();
    if (consumer.getTopicQueueNums() <= 0) {
      consumer.setTopicQueueNums(16);
    }
    consumer.setNamespace(namespace);
    consumer.setNamesrvAddr(nameServer);
    consumer.setConsumeThreadMax(getInternalConsumeThreadMax());
    if (consumer.getConsumeThreadMax() < consumer.getConsumeThreadMin()) {
      consumer.setConsumeThreadMin(consumer.getConsumeThreadMax());
    }
    consumer.setConsumeTimeout(eventListenerAnnotation.consumeTimeout());
    consumer.setInstanceName(UUID.randomUUID().toString());
    consumer.setConsumeFromWhere(eventListenerAnnotation.consumeFromWhere());
    MQClientInstance mqClientInstance = MQClientManager
        .getInstance().getOrCreateMQClientInstance(consumer, null);
    consumer.setOffsetStore(
        new CustomRemoteBrokerOffsetStore(mqClientInstance, consumer.getConsumerGroup()));
    consumer.setConsumeTimestamp(
        UtilAll.timeMillisToHumanString3(
            System.currentTimeMillis() + (eventListenerAnnotation.consumeTimestamp())));
    consumer.setConsumeMessageBatchMaxSize(1);
    consumer.setMessageModel(eventListenerAnnotation.messageModel());
    consumer.setMessageListener(getMessageListener());
    consumer.setConsumeFromWhere(eventListenerAnnotation.consumeFromWhere());

    //后面有需要将订阅方法数据抽取解耦单独出去，可以根据自己需要更改订阅方法，如加注解等方式
    Method[] subscribeMethods =
        Arrays.stream(eventListenerType.getDeclaredMethods())
            .filter(x -> x.getName().equals(Properties4RocketMQ.SUBSCRIBE_METHOD))
            .toArray(Method[]::new);

    Map<String, List<String>> topics = new HashMap<>();
    for (Method subscribeMethod : subscribeMethods) {

      final Class<?>[] parameterTypes = subscribeMethod.getParameterTypes();

      if (parameterTypes.length > 0) {
        if (!IEvent.class.isAssignableFrom(parameterTypes[0])) {
          continue;
        }
        final Class<IEvent<?>> parameterType = (Class<IEvent<?>>) parameterTypes[0];
        TopicModel topicModel =
            topicGenerator.generate(parameterType);
        topics.putIfAbsent(topicModel.getTopic(), new ArrayList<>());
        topics.get(topicModel.getTopic()).add(topicModel.getSubTopic());
//        LegcySubscribeEventTypeFactory
//            .add(parameterType.getName(),
//                parameterType);
      }
    }
    topics.forEach((k, v) -> {
      try {
        final Optional<String> isAllSubscribe = v.stream().filter(x -> x.equals("*")).findAny();
        if (isAllSubscribe.isPresent()) {
          consumer.subscribe(k, "*");
        } else {
          consumer.subscribe(k, String.join(
              " || ",
              CollectionUtils.emptyIfNull(v)));
        }

      } catch (MQClientException e) {
        e.printStackTrace();
      }
    });
  }

  @Override
  public void start() {
    try {
      consumer.start();
    } catch (MQClientException e) {
      log.error(e.getErrorMessage(), e);
    }
  }

  private int getInternalConsumeThreadMax() {
    return eventListenerAnnotation.consumeThreadMax();
  }


  private String getInternalConsumerGroup() {
    return Optional.of(eventListenerAnnotation.consumerGroup()).filter(StringUtils::isNotEmpty)
        .orElseGet(this::getConsumerGroup);
  }


  private String getInternalNameServer() {
    return Optional.of(eventListenerAnnotation.nameServer()).filter(StringUtils::isNotEmpty)
        .orElseGet(this::getNameServer);
  }

  private String getInternalNameSpace() {
    return Optional.of(eventListenerAnnotation.namespace()).filter(StringUtils::isNotEmpty)
        .orElseGet(this::getNamespace);
  }

  protected MessageListener getMessageListener() {
    if (eventListenerAnnotation.listenerMode() == ListenerMode.Concurrently) {
      return new DefaultMessageListenerConcurrently(consumer, eventListenerProxy,
          storedEventSerializer);
    }
    return new DefaultMessageListenerOrderly(consumer, eventListenerProxy, storedEventSerializer);
  }

  protected String getNamespace() {
    return "LOCAL";
  }

  protected String getNameServer() {
    return "localhost:9876";
  }

  protected String getConsumerGroup() {
    return eventListenerType.getName().replace(".", "-").replace("$", "-");
  }
}
