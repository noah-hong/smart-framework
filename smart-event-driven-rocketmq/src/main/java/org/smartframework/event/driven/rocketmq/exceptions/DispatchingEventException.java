package org.smartframework.event.driven.rocketmq.exceptions;

/**
 * @author noah
 * @create 2021-05-30 22:57
 */
public class DispatchingEventException extends RuntimeException {

  public DispatchingEventException(String message) {
    super(message);
  }
}
