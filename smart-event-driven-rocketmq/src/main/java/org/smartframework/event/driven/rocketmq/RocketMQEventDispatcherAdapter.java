package org.smartframework.event.driven.rocketmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.EventDispatchResult;
import org.smartframework.event.driven.core.EventEnvelope;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.IEventDispatcher;
import org.smartframework.event.driven.core.IEventDispatcherAdapter;
import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.TopicModel;
import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
import org.smartframework.event.driven.core.store.StoredEvent;

import java.util.Objects;

/**
 * @author noah
 * @create 2022-03-21 15:23
 */
public class RocketMQEventDispatcherAdapter implements IEventDispatcherAdapter {

  private final ITopicGenerator topicGenerator;
  private final IEventEnvelopeFormatter eventEnvelopeSerializer;
  private final IEventDispatcher dispatcher;
  private static final Logger log = LoggerFactory.getLogger(RocketMQEventDispatcherAdapter.class);

  public RocketMQEventDispatcherAdapter(
      ITopicGenerator topicGenerator,
      IEventEnvelopeFormatter eventEnvelopeSerializer,
      IEventDispatcher dispatcher) {
    this.topicGenerator = topicGenerator;
    this.eventEnvelopeSerializer = eventEnvelopeSerializer;
    this.dispatcher = dispatcher;
  }


  @Override
  public EventDispatchResult dispatch(EventEnvelope eventEnvelope) {
    Objects.requireNonNull(eventEnvelope);
    StoredEvent storedEvent = new StoredEvent();

    IEvent<?> event = eventEnvelope.getEvent();
    storedEvent.setLocation(String
        .format("%s-%s", event.getTagName(), event.getPersistenceId().getValue().toString()));
    storedEvent.setPersistenceId(event.getPersistenceId().getValue().toString());

    final Class<? extends IEvent> aClass = event.getClass();

    TopicModel topicModel = topicGenerator.generate(aClass);
    storedEvent.setPayload(eventEnvelopeSerializer.serialize(eventEnvelope));
    storedEvent.setTopic(topicModel.getTopic());
    storedEvent.setSubTopic(topicModel.getSubTopic());

    storedEvent.setTagName(eventEnvelope.getEvent().getTagName());
    storedEvent.setEventId(eventEnvelope.getEventId());
    storedEvent.setEventType(eventEnvelope.getEvent().getClass().getName());
    storedEvent.setSenderName(eventEnvelope.getSender().getName());
    storedEvent.setSenderId(eventEnvelope.getSender().getId());
    storedEvent.setSenderScope(eventEnvelope.getSender().getScope());
    storedEvent.setStoredTime(eventEnvelope.getOccurredOn());
    storedEvent.setVersion(eventEnvelope.getEvent().getVersion());
    storedEvent.setSerializer(eventEnvelopeSerializer.serializer());

    return dispatcher.dispatch(storedEvent);
  }
}
