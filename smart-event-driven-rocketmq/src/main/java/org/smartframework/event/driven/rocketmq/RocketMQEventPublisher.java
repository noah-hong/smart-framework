package org.smartframework.event.driven.rocketmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.DispatchStatus;
import org.smartframework.event.driven.core.EventDispatchResult;
import org.smartframework.event.driven.core.IEventDispatcher;
import org.smartframework.event.driven.core.IEventPublisher;
import org.smartframework.event.driven.core.PublishableEventContext;
import org.smartframework.event.driven.core.store.IEventQueryStore;
import org.smartframework.event.driven.core.store.IPublishedEventTrackerStore;
import org.smartframework.event.driven.core.store.PublishedEventTracker;
import org.smartframework.event.driven.core.store.StoredEvent;
import org.smartframework.event.driven.rocketmq.exceptions.PublishingEventException;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author noah
 * @create 2020-05-04 23:22
 */
public class RocketMQEventPublisher implements IEventPublisher {

  Logger log = LoggerFactory.getLogger(RocketMQEventPublisher.class);
  private final IEventQueryStore eventQueryStore;
  private final IPublishedEventTrackerStore trackerStore;
  private final IEventDispatcher dispatcher;

  public RocketMQEventPublisher(
      IEventQueryStore eventQueryStore,
      IPublishedEventTrackerStore trackerStore,
      IEventDispatcher dispatcher) {

    this.eventQueryStore = Objects.requireNonNull(eventQueryStore);
    this.trackerStore = Objects.requireNonNull(trackerStore);
    this.dispatcher = Objects.requireNonNull(dispatcher);
  }

  @Override
  public synchronized void publish(PublishableEventContext context) {
    doPublish(context);
  }

  protected void doPublish(PublishableEventContext context) {
    long now = System.currentTimeMillis();
    List<PublishedEventTracker> publishedNotificationTrackers =
        trackerStore.getTrackers();
    AtomicBoolean isPublishingException = new AtomicBoolean(false);
    publishedNotificationTrackers.parallelStream()
        .forEach(
            tracker -> {
              try {
                publishGroupByTag(tracker, context);
              } catch (Exception ex) {
                log.error("发送消息异常,消费方注意幂等处理", ex);
                isPublishingException.set(true);
              }
            });
    if (isPublishingException.get()) {
      throw new PublishingEventException();
    }
    long costTime = System.currentTimeMillis() - now;
    log.debug("发送消息花费时间: {} ms", costTime);
  }

  @Override
  public void publish(String tagName, PublishableEventContext context) {
    long now = System.currentTimeMillis();
    PublishedEventTracker publishedEventTracker = trackerStore.getTrackerByTagName(tagName);
    try {
      publishGroupByTag(publishedEventTracker, context);
    } catch (Exception ex) {
      log.error("消息发送失败", ex);
      throw new PublishingEventException();
    }
    long costTime = System.currentTimeMillis() - now;
    log.debug("发送消息花费时间: {} ms", costTime);
  }

  private int publish(PublishedEventTracker tracker, List<StoredEvent> storedEvents) {
    return publishWithOrderly(storedEvents);
  }

  protected void publishGroupByTag(PublishedEventTracker tracker, PublishableEventContext context) {
    int takeCount = 50;
    List<StoredEvent> storedEvents = eventQueryStore
        .allStoredEventsSinceByTag(tracker.getTagName(), tracker.getMostRecentStoredId(),
            takeCount);
    int totalPublishCount = 0;
    while (storedEvents != null && storedEvents.size() > 0) {
//      if (log.isDebugEnabled()) {
//        for (StoredEvent storedEvent : storedEvents) {
//          log.debug("准备发送事件,TOPIC:{},事件编码：{},From-TrackerId:{}", storedEvent.getTopic(),
//              storedEvent.getEventId(), tracker.getMostRecentStoredId());
//        }
//      }
//      storedEvents.stream().map(x->x.getEventId()).collect(Collectors.toList());
      int publishCount = publish(tracker, storedEvents);
      totalPublishCount += publishCount;
      if (publishCount > 0) {
        StoredEvent lastStoredEvent = storedEvents.get(publishCount - 1);
        PublishedEventTracker newPublishedNotificationTracker = new PublishedEventTracker();
        newPublishedNotificationTracker.setTagName(tracker.getTagName());
        newPublishedNotificationTracker.setMostRecentStoredId(lastStoredEvent.getId());
        trackerStore.track(newPublishedNotificationTracker);
        storedEvents = eventQueryStore
            .allStoredEventsSinceByTag(tracker.getTagName(), lastStoredEvent.getId(), takeCount);
        tracker = newPublishedNotificationTracker;
      } else {
        try {
          //发送失败后3秒后重试
          Thread.sleep(3 * 1000);
          storedEvents = eventQueryStore
              .allStoredEventsSinceByTag(tracker.getTagName(), tracker.getMostRecentStoredId(),
                  takeCount);
        } catch (InterruptedException e) {
          log.warn("消息发送失败", e);
        }
      }
    }
    log.info("领域[{}]本次发送事件数量:{}", tracker.getTagName(), totalPublishCount);
  }

  private int publishWithOrderly(List<StoredEvent> storedEvents) {
    int i = 0;
    try {
      for (StoredEvent storedEvent : storedEvents) {
        final EventDispatchResult dispatchResult = dispatcher.dispatch(storedEvent);
        if (dispatchResult.getDispatchStatus() == DispatchStatus.FALIURE) {
          return i;
          //throw new DispatchingEventException("发送消息失败");
        }
        i++;
      }
    } catch (Exception ex) {
      log.error("消息发送失败，错误消息：{}，消息编码：{}", ex.getMessage()
          , storedEvents.get(i).getEventId());
      return i;
    }
    return storedEvents.size();
  }
}
