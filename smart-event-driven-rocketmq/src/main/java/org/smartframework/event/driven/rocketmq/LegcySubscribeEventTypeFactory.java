package org.smartframework.event.driven.rocketmq;

import org.smartframework.event.driven.core.IEvent;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author noah
 * @create 2021-01-21 20:38
 */
@Deprecated
public final class LegcySubscribeEventTypeFactory {

  private static final Map<String, Class<IEvent<?>>> EVENT_TYPES = new ConcurrentHashMap<>();

  public static void add(String className, Class<IEvent<?>> clazz) {
    EVENT_TYPES.putIfAbsent(className, clazz);
  }

  public static Class<IEvent<?>> get(String className) {
    return EVENT_TYPES.get(className);
  }
}
