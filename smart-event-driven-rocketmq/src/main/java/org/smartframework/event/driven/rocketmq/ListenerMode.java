package org.smartframework.event.driven.rocketmq;

/**
 * @author noah
 * @create 2022-03-20 16:13
 */
public enum ListenerMode {
  Orderly,
  Concurrently

}
