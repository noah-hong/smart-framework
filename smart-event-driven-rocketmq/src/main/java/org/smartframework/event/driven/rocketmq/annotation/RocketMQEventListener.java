package org.smartframework.event.driven.rocketmq.annotation;

import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.smartframework.event.driven.rocketmq.ListenerMode;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author noah
 * @create 2020-03-02 11:03
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RocketMQEventListener {

  String consumerGroup() default "";

  String namespace() default "";

  ConsumeFromWhere consumeFromWhere() default ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET;

  int consumeTimestamp() default 0;


  MessageModel messageModel() default MessageModel.CLUSTERING;

  /**
   * 最大消费线程数
   */
  int consumeThreadMax() default 64;

  /**
   * 最大消费超时时间,默认30秒
   */
  long consumeTimeout() default 30000L;

  /**
   * ROCKETMQ服务端地址
   */
  String nameServer() default "";

  ListenerMode listenerMode() default ListenerMode.Orderly;
}
