package org.smartframework.event.driven.rocketmq;

import org.apache.commons.lang3.StringUtils;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.TopicModel;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author noah
 * @create 2021-01-21 20:38
 */
public final class SubscribeEventTypeFactory {

  private static Map<String, Class<IEvent<?>>> EVENT_TYPES = new ConcurrentHashMap<>();

  public static void add(TopicModel key, Class<IEvent<?>> value) {
    String topic = key.getTopic();
    String tags = key.getSubTopic().equals("*") ? "" : key.getSubTopic();
    if (StringUtils.isNotBlank(topic) && StringUtils.isNotBlank(tags)) {
      EVENT_TYPES.putIfAbsent(String.format("%s-%s", topic, tags), value);
    } else if (StringUtils.isNotBlank(topic) && StringUtils.isEmpty(tags)) {
      EVENT_TYPES.putIfAbsent(topic, value);
    }
  }

  public static Class<IEvent<?>> get(TopicModel key) {
    String topic = key.getTopic();
    String tags = key.getSubTopic().equals("*") ? "" : key.getSubTopic();
    return Optional.ofNullable(EVENT_TYPES.get(String.format("%s-%s", topic, tags)))
        .orElseGet(() -> EVENT_TYPES.get(topic));
  }

}
