package org.smartframework.event.driven.rocketmq;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.client.producer.selector.SelectMessageQueueByHash;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.DispatchStatus;
import org.smartframework.event.driven.core.EventDispatchResult;
import org.smartframework.event.driven.core.IEventDispatcher;
import org.smartframework.event.driven.core.store.StoredEvent;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author noah
 * @create 2021-04-29 09:17
 */
public class RocketMQEventDispatcher implements IEventDispatcher {

  private static final Logger log = LoggerFactory.getLogger(RocketMQEventDispatcher.class);

  protected final DefaultMQProducer producer;
  protected final MessageQueueSelector messageQueueSelector = new SelectMessageQueueByHash();

  public RocketMQEventDispatcher(DefaultMQProducer producer) {
    this.producer = producer;
  }

  private Message createMessage(String topic, String tag, byte[] payload) {
    if (Objects.isNull(tag)) {
      tag = "";
    }
    return new Message(topic, tag, payload);
  }

  @Override
  public EventDispatchResult dispatch(StoredEvent event) {
    Objects.requireNonNull(event.getPayload());
    Objects.requireNonNull(event.getTopic());
    Objects.requireNonNull(event.getLocation());
    Objects.requireNonNull(event.getEventType());
    Objects.requireNonNull(event.getEventId());
    String hashKey = event.getLocation();
    SendResult sendResult = null;
    long now = System.currentTimeMillis();
    try {
      Message rocketMsg =
          createMessage(
              event.getTopic(),
              event.getSubTopic(),
              event.getPayload());
      if (event.getProperties() != null) {
        final Map<String, String> properties = event.getProperties();
        for (String key : properties.keySet()) {
          rocketMsg.getProperties().put(key, properties.get(key));
        }
      }
//      rocketMsg.getProperties().put(Properties4RocketMQ.PAYLOAD_CLASS_NAME, event.getEventType());
      rocketMsg.getProperties().put(Properties4RocketMQ.PAYLOAD_SERIALIZER, event.getSerializer());
      rocketMsg.setKeys(event.getEventId());
      rocketMsg.setWaitStoreMsgOK(true);
      log.debug("消息即将发送. 主题:{}, 消息编码:{}", event.getTopic(), event.getEventId());
      sendResult =
          producer.send(rocketMsg, messageQueueSelector, hashKey, producer.getSendMsgTimeout());
    } catch (Exception e) {
      final String sendStatus = Optional.ofNullable(sendResult)
          .map(x -> x.getSendStatus().name())
          .orElse("NONE");
      log.error("消息发送失败:{}, 主题:{},消息编码:{},发送状态:{} ", e.getMessage(), event.getTopic(),
          event.getEventId(),
          sendStatus);
    }
    long costTime = System.currentTimeMillis() - now;
    if (Objects.nonNull(sendResult) && sendResult.getSendStatus().equals(SendStatus.SEND_OK)) {
      log.debug("消息发送成功, 花费:{} 毫秒, 消息编号:{}, 消息编码：{}", costTime, sendResult.getMsgId(),
          event.getEventId());
      EventDispatchResult eventDispatchResult = new EventDispatchResult();
      eventDispatchResult.setDispatchStatus(DispatchStatus.SUCCESS);
      return eventDispatchResult;
    } else {
      log.warn("消息发送失败, 花费:{} 毫秒, 消息编码：{}", costTime,
          event.getEventId());
      EventDispatchResult eventDispatchResult = new EventDispatchResult();
      eventDispatchResult.setDispatchStatus(DispatchStatus.FALIURE);
      return eventDispatchResult;
    }
  }
}
