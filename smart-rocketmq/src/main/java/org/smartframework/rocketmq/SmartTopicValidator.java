package org.smartframework.rocketmq;

import org.apache.rocketmq.common.topic.TopicValidator;

/**
 * @author noah
 * @create 2021-08-17 10:35
 */
public class SmartTopicValidator {

  public static final String AUTO_CREATE_TOPIC_KEY_TOPIC_BY_VERSION4_3 = "AUTO_CREATE_TOPIC_KEY";
  public static final String AUTO_CREATE_TOPIC_KEY_TOPIC = TopicValidator.AUTO_CREATE_TOPIC_KEY_TOPIC;
}
