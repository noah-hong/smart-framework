package org.smartframework.rocketmq;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.impl.MQClientAPIImpl;
import org.apache.rocketmq.client.impl.producer.TopicPublishInfo;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.TopicConfig;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.protocol.RequestCode;
import org.apache.rocketmq.common.protocol.ResponseCode;
import org.apache.rocketmq.common.protocol.body.ClusterInfo;
import org.apache.rocketmq.common.protocol.header.namesrv.GetRouteInfoRequestHeader;
import org.apache.rocketmq.common.protocol.route.BrokerData;
import org.apache.rocketmq.remoting.RemotingClient;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.apache.rocketmq.remoting.protocol.RemotingCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;


/**
 * @author noah
 * @create 2021-08-17 11:25
 */
public class SmartMQProducer extends DefaultMQProducer {

  private String test2;
  private static final Logger logger = LoggerFactory.getLogger(SmartMQProducer.class);

  private boolean enabledAutoCreateTopic = true;

  public boolean isEnabledAutoCreateTopic() {
    return enabledAutoCreateTopic;
  }

  public void setEnabledAutoCreateTopic(boolean enabledAutoCreateTopic) {
    this.enabledAutoCreateTopic = enabledAutoCreateTopic;
  }

  public SmartMQProducer(String producerGroup) {
    super(producerGroup);
  }

  public SmartMQProducer(String namespace, String producerGroup) {
    super(namespace, producerGroup);
  }

  @Override
  public SendResult send(Message msg, MessageQueueSelector selector, Object arg, long timeout)
      throws MQClientException, RemotingException, MQBrokerException, InterruptedException {
    final String topic = withNamespace(msg.getTopic());
    autoCreateTopic(topic);
    msg.setTopic(topic);
    return this.defaultMQProducerImpl.send(msg, selector, arg, timeout);
  }


  protected void autoCreateTopic(String topicName) {
    if (!enabledAutoCreateTopic) {
      return;
    }
    final TopicPublishInfo topicPublishInfo = this.defaultMQProducerImpl.getTopicPublishInfoTable()
        .get(topicName);
    if (null == topicPublishInfo || !topicPublishInfo.ok()) {
      final MQClientAPIImpl mqClientAPIImpl = this.defaultMQProducerImpl.getmQClientFactory()
          .getMQClientAPIImpl();

      final RemotingClient remotingClient = mqClientAPIImpl.getRemotingClient();
      GetRouteInfoRequestHeader requestHeader = new GetRouteInfoRequestHeader();
      requestHeader.setTopic(topicName);
      RemotingCommand request = RemotingCommand
          .createRequestCommand(RequestCode.GET_ROUTEINFO_BY_TOPIC, requestHeader);
      try {
        RemotingCommand response = remotingClient.invokeSync(null, request, 3 * 1000);
        if (response.getCode() == ResponseCode.TOPIC_NOT_EXIST) {
          RemotingCommand cluster_request = RemotingCommand
              .createRequestCommand(RequestCode.GET_BROKER_CLUSTER_INFO, null);
          RemotingCommand cluster_response = remotingClient
              .invokeSync(null, cluster_request, 3 * 1000);
          if (cluster_response.getCode() == ResponseCode.SUCCESS) {
            ClusterInfo clusterInfo = ClusterInfo
                .decode(cluster_response.getBody(), ClusterInfo.class);

            final HashMap<String, BrokerData> brokerAddrTable = clusterInfo.getBrokerAddrTable();
            for (BrokerData brokerData : brokerAddrTable.values()) {
              TopicConfig topicConfig = new TopicConfig();
              topicConfig.setWriteQueueNums(this.getDefaultTopicQueueNums());
              topicConfig.setReadQueueNums(this.getDefaultTopicQueueNums());
              topicConfig.setTopicName(topicName);
              mqClientAPIImpl
                  .createTopic(brokerData.selectBrokerAddr(),
                      SmartTopicValidator.AUTO_CREATE_TOPIC_KEY_TOPIC_BY_VERSION4_3, topicConfig,
                      3 * 1000);
              logger
                  .info("自动创建主题成功,cluster:{},broker:{},topic:{}"
                      , brokerData.getCluster()
                      , brokerData.getBrokerName()
                      , topicName);
            }
          }
        }

      } catch (Exception e) {
        logger.error("自动创建主题失败", e);
      }
    }

  }

}
