package org.smartframework.domain.driven.core.communication;

import org.smartframework.domain.driven.core.CommandResponse;

/**
 * @author noah
 * @create 2021-09-10 15:06
 */
public interface CommandHandlerInterceptor {

  default boolean preHandle(CommandRequestEnvelope<?> request, CommandResponse<?> response, Object handler)
      throws Exception {

    return true;
  }


  default void postHandle(CommandRequestEnvelope<?> request, CommandResponse<?> response, Object handler)
      throws Exception {

  }


  default void afterCompletion(CommandRequestEnvelope<?> request, CommandResponse<?> response, Object handler,
      Exception ex) throws Exception {
  }

}
