package org.smartframework.domain.driven.core;

import org.smartframework.core.IAccount;

/**
 * @author noah
 * @create 2021-06-23 18:28
 */
public class Commander implements IAccount<String> {

  public Commander() {
  }

  public Commander(IAccount account) {
    this.name = account.getName();
    this.id = account.getId().toString();
    this.scope = account.getScope();
    this.workerNumber = account.getWorkerNumber();
    this.mobilePhone = account.getMobilePhone();
  }

  public Commander(String name, String id, String scope) {
    this.name = name;
    this.id = id;
    this.scope = scope;
  }


  public void setName(String name) {
    this.name = name;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  private String name;
  private String id;
  private String scope;
  private String workerNumber;
  private String mobilePhone;

  @Override
  public String getWorkerNumber() {
    return workerNumber;
  }

  public void setWorkerNumber(String workerNumber) {
    this.workerNumber = workerNumber;
  }

  @Override
  public String getMobilePhone() {
    return mobilePhone;
  }

  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }


  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getScope() {
    return scope;
  }

  public final static Commander ANONYMOUS = new Commander("anonymous", "", "");
}
