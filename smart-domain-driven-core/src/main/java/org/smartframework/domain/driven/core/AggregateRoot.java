package org.smartframework.domain.driven.core;

import org.smartframework.core.IPersistenceId;

/**
 * @author noah
 * @create 2021-06-02 13:54
 */
public abstract class AggregateRoot<PersistenceId extends IPersistenceId<?>> implements IAggregateRoot<PersistenceId> {

  private PersistenceId persistenceId;
  private Integer version;

  public void setPersistenceId(PersistenceId persistenceId) {
    this.persistenceId = persistenceId;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  @Override
  public PersistenceId getPersistenceId() {
    return persistenceId;
  }

  @Override
  public Integer getVersion() {
    return version;
  }
}
