package org.smartframework.domain.driven.core;

/**
 * @author noah
 * @create 2020-10-15 22:09
 */
public enum CommandStatus {
  NONE,
  SUCCESS,
  FAILURE,
  TIME_OUT,
  EXCEED_BACK_PRESSURE_THRESHOLD,
  ERROR
}
