package org.smartframework.domain.driven.core;

/**
 * @author noah
 * @create 2021-01-10 0:28
 */
public enum CommandBehavior {
    NONE,
    NEW,
    UPDATE,
    DElETE,
}
