package org.smartframework.domain.driven.core.communication;

/**
 * @author noah
 * @create 2021-02-20 16:13
 */
public interface IClientSiteProcessResponderRegistrar {

  IClientSiteProcessResponder register(String correlationId, IClientSiteProcessResponder responder);

  void remove(String correlationId);

  IClientSiteProcessResponder get(String corrlationId);

}
