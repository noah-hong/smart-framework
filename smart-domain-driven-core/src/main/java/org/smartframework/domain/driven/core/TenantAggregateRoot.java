package org.smartframework.domain.driven.core;

import org.smartframework.core.IPersistenceId;
import org.smartframework.core.ITenant;

/**
 * @author noah
 * @create 2021-07-04 17:21
 */
public class TenantAggregateRoot<PersistenceId extends IPersistenceId<?>> extends
        AggregateRoot<PersistenceId> implements ITenant {

  private String tenantId;

  @Override
  public String getTenantId() {
    return tenantId;
  }

  @Override
  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }
}
