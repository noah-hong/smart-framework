package org.smartframework.domain.driven.core.communication;

/**
 * @author noah
 * @create 2021-07-21 16:56
 */
public interface ICallerBarrierFactory {

  ICallerBarrier getBrarrier(Class<?> serviceType);
}
