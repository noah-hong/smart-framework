package org.smartframework.domain.driven.core;

import org.smartframework.core.IPersistenceId;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author noah
 * @create 2021-06-02 14:55
 */
public interface IBaseCommand<PersistenceId extends IPersistenceId<?>> extends ICommand,
    Serializable {

  PersistenceId getPersistenceId();

  default LocalDateTime getOccurredOn() {
    return LocalDateTime.now();
  }

  Commander getCommander();
}
