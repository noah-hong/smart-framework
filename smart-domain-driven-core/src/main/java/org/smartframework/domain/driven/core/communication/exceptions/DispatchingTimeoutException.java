package org.smartframework.domain.driven.core.communication.exceptions;

/**
 * @author noah
 * @create 2021-02-20 15:57
 */
public class DispatchingTimeoutException extends DispatchingException {

  public DispatchingTimeoutException() {
    super("执行超时");
  }
}
