package org.smartframework.domain.driven.core;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.Serializable;

/**
 * @author noah
 * @create 2021-06-02 17:33
 */
public class CommandResponse<T> implements Serializable {

  public CommandStatus getCommandStatus() {
    return commandStatus;
  }

  public void setCommandStatus(CommandStatus commandStatus) {
    this.commandStatus = commandStatus;
  }

  public CommandMessageEnvelope[] getMessages() {
    return messages;
  }

  public void setMessages(CommandMessageEnvelope[] messages) {
    this.messages = messages;
  }

  public Long getReponsedTime() {
    return reponsedTime;
  }

  public void setReponsedTime(Long reponsedTime) {
    this.reponsedTime = reponsedTime;
  }

  private Long reponsedTime = System.currentTimeMillis();
  private CommandStatus commandStatus;

  private CommandMessageEnvelope[] messages = new CommandMessageEnvelope[0];

  private T body;

  public T getBody() {
    return body;
  }

  public void setBody(T body) {
    this.body = body;
  }

  public static ResponseBuilder failure() {
    return builder(CommandStatus.FAILURE);
  }

  public static <T> CommandResponse<T> failure(String... messages) {
    return builder(CommandStatus.FAILURE).withMessages(messages).build();
  }

  public static <T> CommandResponse<T> timeout(String... messages) {
    return builder(CommandStatus.TIME_OUT).withMessages(messages).build();
  }

  public static <T> CommandResponse<T> exceedBackPressureThreshold(String message) {
    return builder(CommandStatus.EXCEED_BACK_PRESSURE_THRESHOLD).withMessages(message).build();
  }

  public static <T> CommandResponse<T> error(Throwable... throwables) {
    return builder(CommandStatus.ERROR).withMessages(throwables).build();
  }

  public static <T> CommandResponse<T> error(String... messages) {
    return builder(CommandStatus.ERROR).withMessages(messages).build();
  }

  public static <T> CommandResponse<T> failure(CommandMessageEnvelope... messageEnvelopes) {
    return builder(CommandStatus.FAILURE).withMessages(messageEnvelopes).build();
  }

  public static <T> CommandResponse<T> failure(Throwable... throwables) {
    return builder(CommandStatus.FAILURE).withMessages(throwables).build();
  }

  public static ResponseBuilder success() {
    return builder(CommandStatus.SUCCESS);
  }

  public static <T> CommandResponse<T> success(T body) {
    return builder(CommandStatus.SUCCESS).build(body);
  }

  public static ResponseBuilder builder(CommandStatus commandStatus) {
    return new ResponseBuilder(commandStatus);
  }


  public static class ResponseBuilder {

    private CommandMessageEnvelope[] commandMessageEnvelopes;
    private final CommandStatus commandStatus;

    public ResponseBuilder(CommandStatus commandStatus) {
      this.commandStatus = commandStatus;
    }

    public ResponseBuilder withMessages(String... messages) {
      commandMessageEnvelopes = new CommandMessageEnvelope[messages.length];
      int i = 0;
      for (String message : messages) {
        final CommandMessageEnvelope commandMessageEnvelope = commandMessageEnvelopes[i++] = new CommandMessageEnvelope();
        commandMessageEnvelope.setCode("");
        commandMessageEnvelope.setMessage(message);
      }
      return this;
    }

    public ResponseBuilder withMessages(CommandMessageEnvelope... exceptions) {
      commandMessageEnvelopes = exceptions;
      return this;
    }

    public ResponseBuilder withMessages(Throwable... throwables) {
      commandMessageEnvelopes = new CommandMessageEnvelope[throwables.length];
      int i = 0;
      for (Throwable ex : throwables) {
        final CommandMessageEnvelope commandMessageEnvelope = commandMessageEnvelopes[i++] = new CommandMessageEnvelope();
        final Throwable rootCause = ExceptionUtils.getRootCause(ex);
        if (rootCause instanceof DomainException) {
          DomainException domainException = (DomainException) rootCause;
          commandMessageEnvelope.setCode(domainException.getCode());
        }
        commandMessageEnvelope.setMessage(rootCause.getMessage());
      }
      return this;
    }

    public <T> CommandResponse<T> build(T body) {
      final CommandResponse<T> commandResponse = new CommandResponse<>();
      commandResponse.setCommandStatus(commandStatus);
      commandResponse.setMessages(commandMessageEnvelopes);
      commandResponse.setBody(body);
      return commandResponse;
    }

    public <T> CommandResponse<T> build() {
      return this.build(null);
    }


  }

}
