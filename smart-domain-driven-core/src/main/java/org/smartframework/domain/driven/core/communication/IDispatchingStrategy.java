package org.smartframework.domain.driven.core.communication;

/**
 * @author noah
 * @create 2021-08-24 16:10
 */
public interface IDispatchingStrategy {

  String getRouteKey();

  Integer getTimeout();
}