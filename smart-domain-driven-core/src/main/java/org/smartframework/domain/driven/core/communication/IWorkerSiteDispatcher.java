package org.smartframework.domain.driven.core.communication;

import org.smartframework.domain.driven.core.CommandResponse;

/**
 * @author noah
 * @create 2021-02-19 22:38
 */
public interface IWorkerSiteDispatcher {

  CommandResponse<?> dispatch(Class<?> serviceProxyType,
      CommandRequestEnvelope<?> envelope, IDispatchingStrategy dispatchingStrategy);
}
