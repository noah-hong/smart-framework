package org.smartframework.domain.driven.core;

/**
 * @author noah
 * @create 2021-06-02 17:24
 */
public class DomainException extends RuntimeException {

  public DomainException() {
  }

  public DomainException(String message) {
    super(message);
    this.message = message;
  }

  public DomainException(String code, String message) {
    this(message);
    this.code = code;
  }

  private String message;
  private String code;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
