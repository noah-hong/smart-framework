package org.smartframework.domain.driven.core.communication;

import org.smartframework.domain.driven.core.CommandResponse;

import java.io.Serializable;

/**
 * @author noah
 * @create 2021-02-19 10:17
 */
public class CommandResponseEnvelope implements Serializable {

  private static final long serialVersionUID = 5190038650932101370L;

  public CommandResponse<?> getCommandResult() {
    return commandResult;
  }

  public void setCommandResult(CommandResponse<?> commandResult) {
    this.commandResult = commandResult;
  }

  public String getCorrelationId() {
    return correlationId;
  }

  public void setCorrelationId(String correlationId) {
    this.correlationId = correlationId;
  }

  private CommandResponse<?> commandResult;
  private String correlationId;
}
