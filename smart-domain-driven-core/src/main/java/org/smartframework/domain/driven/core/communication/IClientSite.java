package org.smartframework.domain.driven.core.communication;

/**
 * @author noah
 * @create 2021-02-19 22:48
 */
public interface IClientSite {

  void start();

//  void stop();

  String getClientId();
}
