package org.smartframework.domain.driven.core;

import org.smartframework.core.IPersistenceId;
import org.smartframework.core.ITenant;
import org.smartframework.domain.driven.core.communication.IRoutableCommand;

import java.util.Optional;
import java.util.UUID;

/**
 * @author noah
 * @create 2021-07-02 17:18
 */
public class TenantCommand<PersistenceId extends IPersistenceId<?>> extends
        BaseCommand<PersistenceId> implements IRoutableCommand, ITenant {

  private String tenantId;

  @Override
  public String getTenantId() {
    return tenantId;
  }

  @Override
  public void setTenantId(String tenantId) {
    this.tenantId = tenantId;
  }

  @Override
  public String getRouteKey() {
    if (this.getPersistenceId() != null) {
      return Optional.ofNullable(this.getPersistenceId())
          .map(x -> x.getValue().toString())
          .orElseGet(IPersistenceId.UNKNOWN::getValue);
    }
    return UUID.randomUUID().toString();
  }
}
