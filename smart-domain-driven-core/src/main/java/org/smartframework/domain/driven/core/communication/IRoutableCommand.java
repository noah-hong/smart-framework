package org.smartframework.domain.driven.core.communication;

import org.smartframework.domain.driven.core.ICommand;

/**
 * @author noah
 * @create 2021-08-24 17:34
 */
public interface IRoutableCommand extends ICommand {

  String getRouteKey();
//
//  default String getRouteKey() {
//    if (this.getPersistenceId() != null) {
//      return Optional.ofNullable(this.getPersistenceId())
//          .map(x -> x.getValue().toString())
//          .orElseGet(IPersistenceId.UNKNOWN::getValue);
//    }
//    return "";
//  }
//
//  ;

}
