package org.smartframework.domain.driven.core.communication.exceptions;

/**
 * @author noah
 * @create 2021-02-20 15:58
 */
public class DispatchingException extends RuntimeException {

  public DispatchingException(String message) {
    super(message);
  }

  public DispatchingException(String message, Throwable cause) {
    super(message, cause);
  }

  public DispatchingException(Throwable cause) {
    super(cause);
  }

  protected DispatchingException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
