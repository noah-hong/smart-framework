package org.smartframework.domain.driven.core;

import org.smartframework.core.IPersistenceId;

import java.time.LocalDateTime;

/**
 * @author noah
 * @create 2021-06-27 17:36
 */
public class BaseCommand<PersistenceId extends IPersistenceId<?>> implements
    IBaseCommand<PersistenceId> {

  private PersistenceId persistenceId;
  private LocalDateTime occurredOn = LocalDateTime.now();
  private Commander commander = Commander.ANONYMOUS;

  public void setPersistenceId(PersistenceId persistenceId) {
    this.persistenceId = persistenceId;
  }

  public void setOccurredOn(LocalDateTime occurredOn) {
    this.occurredOn = occurredOn;
  }

  public void setCommander(Commander commander) {
    this.commander = commander;
  }

  @Override
  public PersistenceId getPersistenceId() {
    return persistenceId;
  }

  @Override
  public LocalDateTime getOccurredOn() {
    return occurredOn;
  }

  @Override
  public Commander getCommander() {
    return commander;
  }


}
