package org.smartframework.domain.driven.core.communication;


import org.smartframework.domain.driven.core.CommandResponse;

/**
 * @author noah
 * @create 2021-02-19 22:42
 */
public interface IWorkerSiteService {

  CommandResponse<?> invoke(CommandRequestEnvelope<?> envelope);

  Class<?> getHandlerType();

}
