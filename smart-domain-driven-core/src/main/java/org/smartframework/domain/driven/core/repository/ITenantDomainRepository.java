package org.smartframework.domain.driven.core.repository;

import org.smartframework.core.IPersistenceId;
import org.smartframework.domain.driven.core.TenantAggregateRoot;

import java.util.Optional;

/**
 * @author noah
 * @create 2021-09-07 09:59
 */
public interface ITenantDomainRepository<State extends TenantAggregateRoot<PersistenceId>, PersistenceId extends IPersistenceId<?>> extends
    ITenantPersistenceIdGenerator<PersistenceId> {

  State save(ISavedContext<State> context);

  Optional<State> get(PersistenceId id);
}
