package org.smartframework.domain.driven.core.communication;

/**
 * @author noah
 * @create 2021-06-04 15:32
 */
public interface IClientSiteRouter {

  void register(IClientSite client);

  IClientSite route();
}
