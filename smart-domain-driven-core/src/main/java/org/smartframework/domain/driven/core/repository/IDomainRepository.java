package org.smartframework.domain.driven.core.repository;

import org.smartframework.core.IPersistenceId;
import org.smartframework.domain.driven.core.IAggregateRoot;

import java.util.Optional;

/**
 * @author noah
 * @create 2021-06-02 15:36
 */
public interface IDomainRepository<State extends IAggregateRoot<PersistenceId>, PersistenceId extends IPersistenceId<?>> extends
    IPersistenceIdGenerator<PersistenceId> {

  State save(ISavedContext<State> context);

  Optional<State> get(PersistenceId id);

}
