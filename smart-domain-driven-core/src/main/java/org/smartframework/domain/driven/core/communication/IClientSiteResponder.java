package org.smartframework.domain.driven.core.communication;


/**
 * @author noah
 * @create 2021-02-22 11:20
 */
public interface IClientSiteResponder {

  void respond(CommandResponseEnvelope envelope);
}
