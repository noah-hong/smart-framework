package org.smartframework.domain.driven.core.repository;

import org.smartframework.core.IPersistenceId;

/**
 * @author noah
 * @create 2021-09-07 10:16
 */
public interface ITenantPersistenceIdGenerator<PersistenceId extends IPersistenceId<?>> {

  <T> TenantPersistenceIdEnvelope<PersistenceId, T> nextPersistenceId(String tenantId);
}

