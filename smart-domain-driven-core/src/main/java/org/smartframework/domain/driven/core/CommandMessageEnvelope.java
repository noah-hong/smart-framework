package org.smartframework.domain.driven.core;

import java.io.Serializable;

/**
 * @author noah
 * @create 2021-06-02 17:24
 */
public class CommandMessageEnvelope implements Serializable {

  public CommandMessageEnvelope() {

  }

  public CommandMessageEnvelope(String message) {
    this.message = message;
  }

  public CommandMessageEnvelope(String code, String message) {
    this.code = code;
    this.message = message;
  }

  private String message;
  private String code;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
