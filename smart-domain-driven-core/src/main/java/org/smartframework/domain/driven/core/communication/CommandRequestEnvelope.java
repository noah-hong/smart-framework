package org.smartframework.domain.driven.core.communication;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author noah
 * @create 2021-02-10 18:04
 */
public class CommandRequestEnvelope<Command extends IRoutableCommand> implements Serializable {

  public Command getCommand() {
    return command;
  }

  public void setCommand(Command command) {
    this.command = command;
  }

  public String getCorrelationId() {
    return correlationId;
  }

  public void setCorrelationId(String correlationId) {
    this.correlationId = correlationId;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public Map<String, String> getProperties() {
    return this.properties;
  }

  void setProperties(Map<String, String> properties) {
    this.properties = properties;
  }

  private Command command;

  private String correlationId;
  private String clientId;
  private Map<String, String> properties;

  public void putUserProperty(String name, String value) {
    this.putProperty(name, value);
  }

  void putProperty(String name, String value) {
    if (null == this.properties) {
      this.properties = new HashMap();
    }

    this.properties.put(name, value);
  }

  public String getProperty(String name) {
    if (null == this.properties) {
      this.properties = new HashMap();
    }

    return (String) this.properties.get(name);
  }
}
