package org.smartframework.domain.driven.core;

import org.smartframework.core.IPersistenceId;
import org.smartframework.core.IVersion;

import java.io.Serializable;

/**
 * @author noah
 * @create 2021-06-02 13:54
 */
public interface IAggregateRoot<PersistenceId extends IPersistenceId<?>> extends Serializable, IVersion {

  PersistenceId getPersistenceId();
}
