package org.smartframework.domain.driven.core.communication;


/**
 * @author noah
 * @create 2021-02-19 22:39
 */
public interface IWorkerSite {

//  Class<? extends AbstractDomainService> getWorkClazz();
//
//  AbstractDomainService getWorker();
//
//  IRpcWorkerInvoker getInvoker();

  void start();
}
