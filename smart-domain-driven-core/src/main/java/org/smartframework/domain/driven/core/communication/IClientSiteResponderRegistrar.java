package org.smartframework.domain.driven.core.communication;

/**
 * @author noah
 * @create 2021-02-22 11:30
 */
public interface IClientSiteResponderRegistrar {

  IClientSiteResponder register(String clientId, IClientSiteResponder responder);

  IClientSiteResponder get(String clientId);
}
