package org.smartframework.domain.driven.core.communication;

import org.smartframework.domain.driven.core.CommandResponse;

/**
 * @author noah
 * @create 2021-09-23 10:27
 */
public interface ICallerInterceptor {
  default boolean preCall(CommandRequest request, CommandResponse<?> response, Object handler)
      throws Exception {

    return true;
  }


  default void postCall(CommandRequest request, CommandResponse<?> response, Object handler)
      throws Exception {

  }


  default void afterCompletion(CommandRequest request, CommandResponse<?> response, Object handler,
      Exception ex) throws Exception {
  }
}
