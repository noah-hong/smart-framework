package org.smartframework.domain.driven.core.repository;

import org.smartframework.core.IAccount;
import org.smartframework.domain.driven.core.CommandBehavior;
import org.smartframework.domain.driven.core.IAggregateRoot;

/**
 * @author noah
 * @create 2021-06-02 15:54
 */
public interface ISavedContext<State extends IAggregateRoot<?>> {

  IAccount<?> getOperator();

  State getState();

  CommandBehavior getBehavior();
}
