package org.smartframework.domain.driven.core.communication;


import org.smartframework.domain.driven.core.CommandResponse;

/**
 * @author noah
 * @create 2021-02-19 22:58
 */
public interface IClientSiteProcessResponder {


 <T> CommandResponse<T> waitRespond(int timeout) throws InterruptedException;

  <T> void respond(CommandResponse<T> commandResult);

}
