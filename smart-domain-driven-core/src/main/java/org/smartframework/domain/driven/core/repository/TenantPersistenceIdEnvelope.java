package org.smartframework.domain.driven.core.repository;

import org.smartframework.core.IPersistenceId;

/**
 * @author noah
 * @create 2021-09-07 11:22
 */
public class TenantPersistenceIdEnvelope<PersistenceId extends IPersistenceId<?>, T> {

  private PersistenceId persistenceId;
  private T data;

  public PersistenceId getPersistenceId() {
    return persistenceId;
  }

  public void setPersistenceId(PersistenceId persistenceId) {
    this.persistenceId = persistenceId;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }
}
