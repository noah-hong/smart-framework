package org.smartframework.domain.driven.core.repository;

import org.smartframework.core.IAccount;
import org.smartframework.domain.driven.core.CommandBehavior;
import org.smartframework.domain.driven.core.IAggregateRoot;

/**
 * @author noah
 * @create 2021-06-02 15:39
 */
public class SavedContext implements ISavedContext {

  private IAccount<?> operator;
  private IAggregateRoot<?> state;
  private CommandBehavior behavior;

  public void setOperator(IAccount<?> operator) {
    this.operator = operator;
  }

  public void setBehavior(CommandBehavior behavior) {
    this.behavior = behavior;
  }

  @Override
  public IAggregateRoot<?> getState() {
    return state;
  }

  public void setState(IAggregateRoot<?> state) {
    this.state = state;
  }


  @Override
  public CommandBehavior getBehavior() {
    return behavior;
  }

  @Override
  public IAccount<?> getOperator() {
    return operator;
  }

}
