package org.smartframework.domain.driven.core.communication;

import org.smartframework.domain.driven.core.CommandResponse;

/**
 * @author noah
 * @create 2021-06-05 0:32
 */
public interface ICaller {

  CommandResponse<?> call(Class<?> serviceType, IRoutableCommand command);

  CommandResponse<?> call(Class<?> serviceType, IRoutableCommand command, Integer timeout);

  CommandResponse<?> call(Class<?> serviceType, IRoutableCommand command, Integer timeout,
      Long barrierLockPeriod);

  CommandResponse<?> call(CommandRequest commandRequest);
//  CommandResponse<?> call(Class<?> serviceType, ICommand<?> command,booleanInteger timeout);
}
