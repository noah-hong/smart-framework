package org.smartframework.domain.driven.core.communication;

/**
 * @author noah
 * @create 2021-09-23 10:30
 */
public class CommandRequest {

  private Class<?> serviceType;
  private IRoutableCommand command;
  private Integer timeout;
  private Long barrierLockPeriod;

  public Class<?> getServiceType() {
    return serviceType;
  }

  public void setServiceType(Class<?> serviceType) {
    this.serviceType = serviceType;
  }

  public IRoutableCommand getCommand() {
    return command;
  }

  public void setCommand(IRoutableCommand command) {
    this.command = command;
  }

  public Integer getTimeout() {
    return timeout;
  }

  public void setTimeout(Integer timeout) {
    this.timeout = timeout;
  }

  public Long getBarrierLockPeriod() {
    return barrierLockPeriod;
  }

  public void setBarrierLockPeriod(Long barrierLockPeriod) {
    this.barrierLockPeriod = barrierLockPeriod;
  }
}
