package org.smartframework.domain.driven.core.communication;

/**
 * @author noah
 * @create 2021-07-21 15:09
 */
public interface ICallerBarrier {

  boolean tryEntry();

  void tryLock(Long period);

}
