package org.smartframework.domain.driven.core.repository;

import org.smartframework.core.IPersistenceId;

/**
 * @author noah
 * @create 2021-09-07 10:16
 */
public interface IPersistenceIdGenerator<PersistenceId extends IPersistenceId<?>> {

  PersistenceId nextPersistenceId();
}
