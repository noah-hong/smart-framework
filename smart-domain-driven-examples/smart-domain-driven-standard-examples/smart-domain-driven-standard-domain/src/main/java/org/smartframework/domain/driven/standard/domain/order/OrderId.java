package org.smartframework.domain.driven.standard.domain.order;

import org.smartframework.core.IPersistenceId;

/**
 * 定义领域标识
 * @author noah
 * @create 2021-06-07 20:44
 */
public class OrderId implements IPersistenceId<Long> {

  public OrderId() {

  }

  public OrderId(Long value) {
    this.value = value;
  }

  private Long value;

  public void setValue(Long value) {
    this.value = value;
  }

  @Override
  public Long getValue() {
    return value;
  }
}
