package org.smartframework.domain.driven.standard.domain.order.command;

import org.smartframework.domain.driven.core.Commander;
import org.smartframework.domain.driven.core.IBaseCommand;
import org.smartframework.domain.driven.standard.domain.order.OrderId;

/**
 * 定义领域命令
 * @author noah
 * @create 2021-06-07 21:56
 */
public class OrderCreateCommand implements IBaseCommand<OrderId> {

  private OrderId persistenceId;
  private Commander commander;
  private String address;

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public void setCommander(Commander commander) {
    this.commander = commander;
  }

  public void setPersistenceId(
      OrderId persistenceId) {
    this.persistenceId = persistenceId;
  }


  @Override
  public OrderId getPersistenceId() {
    return persistenceId;
  }

  @Override
  public Commander getCommander() {
    return commander;
  }
}
