package org.smartframework.domain.driven.standard.domain.order;

import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.standard.domain.order.command.OrderCreateCommand;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.annotation.LameClient;

/**
 * 定义领域服务
 * @author noah
 * @create 2021-06-07 21:19
 */
@LameClient
public interface OrderService {

  CommandResponse<Void> when(OrderCreateCommand command);
}
