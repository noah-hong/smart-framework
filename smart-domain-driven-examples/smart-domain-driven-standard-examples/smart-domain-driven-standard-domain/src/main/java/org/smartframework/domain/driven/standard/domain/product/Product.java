package org.smartframework.domain.driven.standard.domain.product;

import org.smartframework.domain.driven.core.IAggregateRoot;

/**
 * @author noah
 * @create 2021-06-11 22:35
 */
public final class Product {

  public static class State implements IAggregateRoot<ProductId> {

    ProductId  persistenceId;

    public void setPersistenceId(
        ProductId persistenceId) {
      this.persistenceId = persistenceId;
    }

    @Override
    public ProductId getPersistenceId() {
      return persistenceId;
    }
  }
}
