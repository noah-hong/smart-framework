package org.smartframework.domain.driven.standard.domain.order;

import org.smartframework.domain.driven.standard.domain.kernel.Currency;

/**
 * 定义领域实体
 * @author noah
 * @create 2021-06-07 21:05
 */
public class OrderDetail {

  private String productCode;
  private Integer count;
  private Currency currency;

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }
}
