package org.smartframework.domain.driven.standard.domain.order.event;

import org.smartframework.domain.driven.standard.domain.order.Order;
import org.smartframework.domain.driven.standard.domain.order.OrderId;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.annotation.Topic;

/**
 * 定义领域根事件
 * @author noah
 * @create 2021-06-07 22:00
 */
@Topic
public class OrderEvent implements IEvent<OrderId> {

  private OrderId persistenceId;

  public void setPersistenceId(
      OrderId persistenceId) {
    this.persistenceId = persistenceId;
  }


  @Override
  public OrderId getPersistenceId() {
    return persistenceId;
  }

  @Override
  public String getTagName() {
    return Order.TAG;
  }
}
