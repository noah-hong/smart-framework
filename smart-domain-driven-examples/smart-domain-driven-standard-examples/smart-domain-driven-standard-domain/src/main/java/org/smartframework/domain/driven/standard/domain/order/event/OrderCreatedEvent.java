package org.smartframework.domain.driven.standard.domain.order.event;

import org.smartframework.event.driven.core.annotation.SubTopic;

/**
 * 定义领域事件
 * @author noah
 * @create 2021-06-07 22:00
 */
@SubTopic
public class OrderCreatedEvent extends OrderEvent {

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String address;
}
