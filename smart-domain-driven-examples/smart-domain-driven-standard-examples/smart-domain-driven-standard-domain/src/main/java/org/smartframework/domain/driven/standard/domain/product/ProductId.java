package org.smartframework.domain.driven.standard.domain.product;

import org.smartframework.core.IPersistenceId;

/**
 * @author noah
 * @create 2021-06-11 22:36
 */
public class ProductId implements IPersistenceId<String> {

  String value;

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String getValue() {
    return null;
  }
}
