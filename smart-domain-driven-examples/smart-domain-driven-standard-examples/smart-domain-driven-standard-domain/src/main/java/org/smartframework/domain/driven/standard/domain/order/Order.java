package org.smartframework.domain.driven.standard.domain.order;

import com.google.common.collect.Lists;
import java.util.List;
import org.smartframework.domain.driven.core.CommandMessageEnvelope;
import org.smartframework.domain.driven.core.IAggregateRoot;
import org.smartframework.domain.driven.core.repository.IDomainRepository;
import org.smartframework.domain.driven.standard.domain.kernel.Currency;

/**
 * 领域聚合对象
 *
 * @author noah
 * @create 2021-06-07 20:45
 */
public final class Order {

  //region 定义领域名称
  public final static String TAG = "ORDER";
  //endregion

  // region 定义聚合根

  public static class State implements IAggregateRoot<OrderId> {

    private OrderId persistenceId;
    private String address;
    private Currency currency;
    private String orderType;
    private List<OrderDetail> orderDetails;

    public String getOrderType() {
      return orderType;
    }

    public void setOrderType(String orderType) {
      this.orderType = orderType;
    }


    public List<OrderDetail> getOrderDetails() {
      return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
      this.orderDetails = orderDetails;
    }

    public void setPersistenceId(OrderId persistenceId) {
      this.persistenceId = persistenceId;
    }

    @Override
    public OrderId getPersistenceId() {
      return persistenceId;
    }

    public String getAddress() {
      return address;
    }

    public void setAddress(String address) {
      this.address = address;
    }

    public Currency getCurrency() {
      return currency;
    }

    public void setCurrency(Currency currency) {
      this.currency = currency;
    }
  }

  // endregion

  // region 定义聚合根创建行为

  public static State init() {
    final State state = new State();
    state.setOrderDetails(Lists.newArrayList());
    state.setPersistenceId(new OrderId());
    state.setAddress("");
    state.setCurrency(new Currency());
    return state;
  }

  public static State normalOrder(OrderId orderId) {
    final State state = new State();
    state.setPersistenceId(orderId);
    state.setAddress("1");
    state.setOrderType("普通订单");
    return state;
  }

  public static State abnormalOrder() {
    final State state = new State();
    state.setAddress("2");
    state.setOrderType("补发订单");
    return state;
  }
  //endregion

  //region 定于领域仓储接口

  public interface Repository extends IDomainRepository<State, OrderId> {

    String getX();

    String getY();
  }

  //endregion

  //region 定义领域异常
  public static class CreateOrderFailureException extends CommandMessageEnvelope {

    public CreateOrderFailureException() {
      super("订单创建失败");
    }
  }
  //endregion
}
