package org.smartframework.domain.driven.standard.adapter.rpc.activator;

import org.smartframework.core.IAccount;

/**
 * @author noah
 * @create 2021-06-10 11:26
 */
public class DefaultAccount implements IAccount<String> {

  public void setName(String name) {
    this.name = name;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  private String name;
  private String id;
  private String scope;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getScope() {
    return scope;
  }

  @Override
  public String getWorkerNumber() {
    return null;
  }

  @Override
  public String getMobilePhone() {
    return null;
  }
}
