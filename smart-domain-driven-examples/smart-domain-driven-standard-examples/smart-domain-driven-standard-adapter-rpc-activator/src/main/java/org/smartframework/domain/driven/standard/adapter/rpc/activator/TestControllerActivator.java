package org.smartframework.domain.driven.standard.adapter.rpc.activator;

import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.Commander;
import org.smartframework.domain.driven.standard.adapter.rpc.TestController;
import org.smartframework.domain.driven.standard.domain.order.OrderId;
import org.smartframework.domain.driven.standard.domain.order.OrderService;
import org.smartframework.domain.driven.standard.domain.order.command.OrderCreateCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author noah
 * @create 2021-06-11 18:18
 */
@RestController
public class TestControllerActivator implements TestController {
  @Autowired
  private OrderService orderService;


  @Override
  public ResponseEntity<String> test() {
    final OrderCreateCommand createOrderCommand = new OrderCreateCommand();
    final Commander defaultAccount = new Commander();
    defaultAccount.setId("11");
    defaultAccount.setScope("sys");
    defaultAccount.setName("test");
    createOrderCommand.setCommander(defaultAccount);
    createOrderCommand.setAddress("湖头街");
    createOrderCommand.setPersistenceId(new OrderId(1L));
    final CommandResponse<Void> commandResponse = orderService.when(createOrderCommand);
    return ResponseEntity.ok("success");
  }

}
