package org.smartframework.domain.driven.standard.repository;

import java.util.Optional;
import org.smartframework.domain.driven.core.repository.ISavedContext;
import org.smartframework.domain.driven.standard.domain.order.Order;
import org.smartframework.domain.driven.standard.domain.order.OrderId;
import org.springframework.stereotype.Repository;
import org.smartframework.domain.driven.standard.domain.order.Order.State;

/**
 * @author noah
 * @create 2021-06-07 22:07
 */
@Repository
public class OrderRepository implements Order.Repository {

  @Override
  public State save(ISavedContext<State> context) {
    //TODO State=>表模型
    //TODO 根据命令行为，增删改，进行判断，对指定属性进行赋值，如时间
    //TODO 保存数据到数据库
    return context.getState();
  }

  @Override
  public Optional<State> get(OrderId id) {
    //TODO 表模型=>State
    //TODO 返回State
    return Optional.empty();
  }

  @Override
  public OrderId nextPersistenceId() {
    //TODO 获取领域下一个标识
    //例1：单独创建领域标识生成表进行标识获取
    // update review_seq set nextval= LAST_INSERT_ID(nextval+1);select LAST_INSERT_ID();（普通场景推荐）
    //例2：UUID
    //例3：雪花算法
    //例4：其他能保证唯一标识获取的方法，如基于雪花算法的百度Uid生成帮助类
    //例5：不使用该方法，使用save方法保存数据后，通过数据库自增主键并返回该主键并赋值给State返回（不推荐）
    return new OrderId(5L);
  }

  @Override
  public String getX() {
    //TODO 其他操作数据库的操作，数据库包括Mysql,Redis,ES等
    return null;
  }

  @Override
  public String getY() {
    //TODO 其他操作数据库的操作，数据库包括Mysql,Redis,ES等
    return null;
  }
}
