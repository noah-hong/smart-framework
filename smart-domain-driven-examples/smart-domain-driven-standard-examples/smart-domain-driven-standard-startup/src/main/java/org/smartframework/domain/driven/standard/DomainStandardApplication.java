package org.smartframework.domain.driven.standard;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author noah
 * @create 2021-06-11 18:22
 */
@SpringBootApplication
@EnableSwagger2Doc
public class DomainStandardApplication {

  public static void main(String[] args) {
    SpringApplication.run(DomainStandardApplication.class, args);
  }
}
