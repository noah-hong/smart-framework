package org.smartframework.domain.driven.standard.adapter.rpc;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.smartframework.domain.driven.core.CommandResponse;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author noah
 * @create 2021-06-10 11:17
 */
@Api(value = "领域样例")
@RequestMapping("/domain-driven-worker/examples")
public interface TestController {

  @GetMapping("command-test")
  @ApiOperation("test")
  ResponseEntity<String> test();


}
