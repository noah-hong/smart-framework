package org.smartframework.domain.driven.standard.service.interpreter;

import java.util.ArrayList;
import java.util.Optional;
import org.smartframework.domain.driven.core.CommandBehavior;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.repository.SavedContext;
import org.smartframework.domain.driven.standard.domain.kernel.Currency;
import org.smartframework.domain.driven.standard.domain.order.Order;
import org.smartframework.domain.driven.standard.domain.order.Order.State;
import org.smartframework.domain.driven.standard.domain.order.OrderId;
import org.smartframework.domain.driven.standard.domain.order.OrderService;
import org.smartframework.domain.driven.standard.domain.order.command.OrderCreateCommand;
import org.smartframework.domain.driven.standard.domain.order.event.OrderCreatedEvent;
import org.smartframework.event.driven.core.EventEnvelope;
import org.smartframework.event.driven.core.annotation.Publishable;
import org.smartframework.event.driven.core.store.IEventEnvelopStore;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.annotation.LameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author noah
 * @create 2021-06-07 22:05
 */
@LameService
public class OrderServiceInterpreter implements OrderService {

  @Autowired
  private Order.Repository repository;
  @Autowired
  private IEventEnvelopStore eventStore;

  /**
   * 执行领域命令
   *
   * @param command 命令请求实体
   * @return 命令执行响应结果
   */
  @Override
  @Transactional
  @Publishable
  public CommandResponse<Void> when(OrderCreateCommand command) {
    final Optional<State> state = Optional.ofNullable(command.getPersistenceId())
        .map(repository::get).get();
    if (!state.isPresent()) {
      //TODO 新增订单逻辑
      //如果没有领域标识则自己获取一个
      final OrderId orderId = Optional.ofNullable(command.getPersistenceId())
          .orElseGet(() -> repository.nextPersistenceId());
      //构建初始化领域对象
      State newState = Order.normalOrder(orderId);
      newState.setAddress(command.getAddress());
      newState.setPersistenceId(command.getPersistenceId());
      newState.setCurrency(Currency.createRMB(5L));
      newState.setOrderDetails(new ArrayList<>());
      //创建保存领域对象上下文
      final SavedContext savedContext = new SavedContext();
      savedContext.setOperator(command.getCommander());
      savedContext.setBehavior(CommandBehavior.NEW);
      savedContext.setState(newState);

      //保存领域对象
      repository.save(savedContext);
      //创建领域事件
      OrderCreatedEvent event = new OrderCreatedEvent();
      event.setPersistenceId(newState.getPersistenceId());
      event.setAddress(newState.getAddress());
      //保存领域事件
      eventStore.append(EventEnvelope.builder(event).build());
      return CommandResponse.success().build();
    }
    return CommandResponse.failure(new Order.CreateOrderFailureException());
  }
}
