package org.smartframework.domain.driven.lame.client.examples.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.Commander;
import org.smartframework.domain.driven.lame.worker.examples.domain.OrderId;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.OrderService22;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.CreateOrderCommand;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author noah
 * @create 2021-06-10 16:55
 */
@Api(value = "领域样例")
@RestController
@RequestMapping("/domain-driven-client/examples")
public class TestController {

  @Autowired
  private ObjectProvider<OrderService22> orderService;

  @GetMapping("command-test")
  @ApiOperation("test")
  public ResponseEntity<CommandResponse> test() {
    final CreateOrderCommand createOrderCommand = new CreateOrderCommand();
    final Commander defaultAccount = new Commander();
    defaultAccount.setId("11");
    defaultAccount.setScope("sys");
    defaultAccount.setName("test");
    createOrderCommand.setCommander(defaultAccount);
    createOrderCommand.setAddress("湖头街222");
    createOrderCommand.setPersistenceId(new OrderId(1L));
    final CommandResponse commandResponse = orderService.getIfAvailable().when(createOrderCommand);
    return ResponseEntity.ok(commandResponse);
  }

}
