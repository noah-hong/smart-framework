package org.smartframework.domain.driven.lame.client.examples;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.EnableLameClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author noah
 * @create 2021-06-07 20:36
 */
@SpringBootApplication
@EnableSwagger2Doc
@EnableLameClient(value = "org.smartframework.domain.driven.lame.worker.examples.domain.service")
public class LameClientApplication {

  public static void main(String[] args) {
//    System.setProperty("rocketmq.client.logUseSlf4j", "true");
    SpringApplication.run(LameClientApplication.class, args);
  }
}
