# 领域驱动框架-LAME

## 前言

领域驱动框架-LAME，提供了在实行领域驱动设计时的相关规范和请求通信时的功能组件。
其中通信组件帮助开发人员解决分布式应用服务中常见的问题，如解决请求并发时常见的问题，进行可靠的有状态服务编程、保证服务整体可用性等，并且通信组件支持一键插拔，让研发人员轻松决定使用与否。

## 相关样例
### 开发规范样例
- 项目工程结构与领域对象定义
[smart-domain-driven-standard-examples](smart-domain-driven-standard-examples).
  
### 使用样例
- 工作区定义
[smart-domain-driven-lame-worker-examples](smart-domain-driven-lame-worker-examples). 
- 客户区定义
[smart-domain-driven-lame-client-examples](smart-domain-driven-lame-client-examples).
    
## 通信组件

### 特性说明

- [x] 请求顺序性：同一标识请求会顺序执行，结合其单调性，可进行有状态编程，提升应用服务吞吐量
- [x] 请求单调性
    1. 应用服务未扩容时，同一标识请求会落在同一工作区中执行
    2. 应用服务扩容时，请求会重新分配工作区，分配工作区后，同一标识请求会落在同一工作区执行
- [x] 请求平衡性：所有请求会均匀的分配到所有工作区中
- [x] 请求背压机制：当工作区请求数量到达设定上限时，会快速返回失败结果告知客户区已无法容纳执行请求
- [x] 请求透明执行：所有请求在调用与执行时研发人员无感知，与本地开发模式一致

### 架构图

![架构图.png](docs/架构图.png)

### 所需组件

- [x] RocketMQ
- [x] Redisson

### 快速开始

- 在POM文件中引入如下依赖

``` maven
<dependency>
    <groupId>org.smartframework</groupId>
    <artifactId>smart-domain-driven-lame-spring-boot-starter</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

### 如何使用

- 在application.yml添加相关配置信息，样例配置如下:

```yml
smart:
  domain-driven:
    lame:
      namespace: noah-ns
      # 应用服务为客户区时需配置
      client:
        receiver-count: 1
        rocketmq:
          name-server: 10.251.76.2:9876;10.251.76.3:9876;10.251.76.4:9876;10.251.76.5:9876
          producer-group: ${spring.application.name}-lame-producer-group
        # 默认代理配置
        default-proxy-config:
          # 5秒内未得到响应则超时
          wait-respond-timeout: 5000
        proxy-config:
          # LameClient名称，如果不指定则为类名
          OrderService:
            wait-respond-timeout: 7000
      # 应用服务无论为客户区还是工作区都需配置
      responder:
        redisson:
          nodes: redis://10.0.91.249:7001
          password: 123456
          node-type: single
          #nodes: redis://10.0.71.50:7001;redis://10.0.71.50:7006;redis://10.0.71.51:7004
          #node-type: cluster
      # 应用服务为工作区时需配置
      worker:
        # 工作区执行者默认配置
        default-service-config:
          # 请求背压阀值，超过阀值请求将直接返回失败
          command-back-pressure-thresholds: 1
          # 请求执行者最大数量，建议不超过设置的数据库连接池大小
          command-executor-max-thread: 1
          # 请求执行者最小数量
          command-executor-min-thread: 1
        service-config:
          # Lame服务名称，如果不指定则为类名
          OrderServiceInterpreter:
            # 请求背压阀值，超过阀值请求将直接返回失败
            command-back-pressure-thresholds: 10
            # 请求执行者最大数量，建议不超过设置的数据库连接池大小
            command-executor-max-thread: 2
            # 请求执行者最小数量
            command-executor-min-thread: 2

```

- 在Spring Boot Application的上添加`@EnableLameService`，表示启用工作区

``` java
@SpringBootApplication
@EnableLameService
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
```

- 在Spring Boot Application的上添加`@EnableLameClient`，表示启用客户区

``` java
@SpringBootApplication
@EnableLameClient
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
```

- 定义领域服务，启用@EnableLameClient时，领域服务加上@LameClient注解后在调用方法时开启请求透明传输至工作区

``` java
@LameClient
public interface OrderService {
  CommandResponse when(CreateOrderCommand command);
}
```

- 定义领域服务解释者，启用@EnableLameService时，每个加上@LameService的领域服务解释者都为一个工作区，负责接收客户区的请求

``` java
@LameService
public class OrderServiceInterpreter implements OrderService {

  @Autowired
  private Order.Repository repository;
  @Autowired
  private IEventEnvelopStore eventStore;

  /**
   * 执行领域命令
   * @param command 命令请求实体
   * @return 命令执行响应结果
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  @Publishable
  public CommandResponse<Void> when(OrderCreateCommand command) {
    final Optional<State> state = Optional.ofNullable(command.getPersistenceId())
        .map(repository::get).get();
    if (!state.isPresent()) {
      //TODO 新增订单逻辑
      //如果没有领域标识则自己获取一个
      final OrderId orderId = Optional.ofNullable(command.getPersistenceId()).orElseGet(() -> repository.nextPersistenceId());
      //构建初始化领域对象
      State newState = Order.normalOrder(orderId);
      newState.setAddress(command.getAddress());
      newState.setPersistenceId(command.getPersistenceId());
      newState.setCurrency(Currency.createRMB(5L));
      newState.setOrderDetails(new ArrayList<>());
      //创建保存领域对象上下文
      final SavedContext savedContext = new SavedContext();
      savedContext.setOperator(command.getCommander());
      savedContext.setBehavior(CommandBehavior.NEW);
      savedContext.setState(newState);
      //保存领域对象
      repository.save(savedContext);
      //创建领域事件
      OrderCreatedEvent event = new OrderCreatedEvent();
      event.setPersistenceId(newState.getPersistenceId());
      event.setAddress(newState.getAddress());
      //保存领域事件
      eventStore.append(EventEnvelope.builder(event).build());
      return CommandResponse.success().build();
    }
    return CommandResponse.failure(new Order.CreateOrderFailureException());
  }
}


```

- 领域仓储实现样例
```java
@Repository
public class OrderRepository implements Order.Repository {

  @Override
  public State save(ISavedContext<State> context) {
    //TODO State=>表模型
    //TODO 根据命令行为，增删改，进行判断，对指定属性进行赋值，如时间
    //TODO 保存数据到数据库
    return context.getState();
  }

  @Override
  public Optional<State> get(OrderId id) {
    //TODO 表模型=>State
    //TODO 返回State
    return Optional.empty();
  }

  @Override
  public OrderId nextPersistenceId() {
    //TODO 获取领域下一个标识
    //例1：单独创建领域标识生成表进行标识获取
    // update review_seq set nextval= LAST_INSERT_ID(nextval+1);select LAST_INSERT_ID();（普通场景推荐）
    //例2：UUID
    //例3：雪花算法
    //例4：其他能保证唯一标识获取的方法，如基于雪花算法的百度Uid生成帮助类
    //例5：不使用该方法，使用save方法保存数据后，通过数据库自增主键并返回该主键并赋值给State返回（不推荐）
    return new OrderId(5L);
  }

  @Override
  public String getX() {
    //TODO 其他操作数据库的操作，数据库包括Mysql,Redis,ES,本地内存等
    return null;
  }

  @Override
  public String getY() {
    return null;
  }
}

```
- 如果RocketMQ未开启自动创建TOPIC，则需要将控制台上显示的lame-work-topic加入到RocketMQ中，否则无法正常通信，例子如下
  
![例子.png](docs/topic.png)