package org.smartframework.domain.driven.lame.worker.examples.domain.service.command;

import org.smartframework.domain.driven.core.BaseCommand;
import org.smartframework.domain.driven.core.Commander;
import org.smartframework.domain.driven.core.TenantCommand;
import org.smartframework.domain.driven.core.communication.IRoutableCommand;
import org.smartframework.domain.driven.core.communication.TenantRoutableCommand;
import org.smartframework.domain.driven.lame.worker.examples.domain.OrderId;

/**
 * @author noah
 * @create 2021-06-07 21:56
 */
public class CreateOrderCommand extends TenantCommand<OrderId> {

//  @Override
//  public String getRouteKey() {
//    return address;
//  }

  private OrderId persistenceId;
  private Commander commander;
  private String address;

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  @Override
  public void setCommander(Commander commander) {
    this.commander = commander;
  }

  @Override
  public void setPersistenceId(
      OrderId persistenceId) {
    this.persistenceId = persistenceId;
  }


  @Override
  public OrderId getPersistenceId() {
    return persistenceId;
  }

  @Override
  public Commander getCommander() {
    return commander;
  }
}
