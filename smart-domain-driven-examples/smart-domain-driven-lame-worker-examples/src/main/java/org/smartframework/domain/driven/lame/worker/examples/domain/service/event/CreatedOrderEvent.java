package org.smartframework.domain.driven.lame.worker.examples.domain.service.event;

import org.smartframework.event.driven.core.annotation.SubTopic;

/**
 * @author noah
 * @create 2021-06-07 22:00
 */
@SubTopic
public class CreatedOrderEvent extends OrderEvent {

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String address;
}
