package org.smartframework.domain.driven.lame.worker.examples.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author noah
 * @create 2021-06-07 21:05
 */
public class OrderDetail implements Serializable {

  private String productCode;
  private Integer count;
  private Currency currency;
  private LocalDateTime date;

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }
}
