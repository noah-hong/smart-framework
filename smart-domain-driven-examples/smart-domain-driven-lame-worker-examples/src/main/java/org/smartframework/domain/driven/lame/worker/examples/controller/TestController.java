package org.smartframework.domain.driven.lame.worker.examples.controller;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.stream.LongStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.core.IPersistenceId;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.CommandStatus;
import org.smartframework.domain.driven.core.Commander;
import org.smartframework.domain.driven.lame.worker.examples.domain.OrderDetail;
import org.smartframework.domain.driven.lame.worker.examples.domain.OrderId;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.OrderService22;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.CreateOrderCommand;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.CreateOrderWithUnknownIdCommand;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.TestRouteCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author noah
 * @create 2021-06-10 11:17
 */
@Api(value = "领域样例")
@RestController
@RequestMapping("/domain-driven-worker/examples")
public class TestController {

  private final static Logger log = LoggerFactory.getLogger(TestController.class);
  @Autowired
  private OrderService22 orderService;

  @GetMapping("normal-test")
  @ApiOperation("normal-test")
  public ResponseEntity<String> normaltest() {
    return ResponseEntity.ok("11");
  }


  @GetMapping("route-test")
  @ApiOperation("route-test")
  public ResponseEntity<String> routetest() {
    TestRouteCommand testRouteCommand = new TestRouteCommand();
    orderService
        .when(testRouteCommand);
    return ResponseEntity.ok("11");
  }

  @GetMapping("command-test")
  @ApiOperation("test")
  public ResponseEntity<CommandResponse<OrderDetail>> test(Long i) {
    final CreateOrderCommand createOrderCommand = new CreateOrderCommand();
    final Commander defaultAccount = new Commander();
    defaultAccount.setId("11");
    defaultAccount.setScope("sys");
    defaultAccount.setName("test");
    createOrderCommand.setCommander(defaultAccount);
    createOrderCommand.setAddress("湖头街");
    createOrderCommand.setPersistenceId(new OrderId(i));
    final CommandResponse<OrderDetail> commandResponse = orderService
        .when(createOrderCommand);
    if(commandResponse.getCommandStatus() == CommandStatus.SUCCESS){
      return ResponseEntity.ok(commandResponse);
    }else{
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


  }

  @GetMapping("command-test-new2")
  @ApiOperation("test")
  public ResponseEntity<CommandResponse<OrderDetail>> test_new2() {
    final CreateOrderCommand createOrderCommand = new CreateOrderCommand();
    final Commander defaultAccount = new Commander();
    defaultAccount.setId("11");
    defaultAccount.setScope("sys");
    defaultAccount.setName("test");
    createOrderCommand.setCommander(defaultAccount);
    createOrderCommand.setAddress("湖头街");
    createOrderCommand.setPersistenceId(null);
    final CommandResponse<OrderDetail> commandResponse = orderService
        .when(createOrderCommand);

    return ResponseEntity.ok(commandResponse);
  }

  @GetMapping("command-test-new")
  @ApiOperation("test")
  public ResponseEntity<CommandResponse<String>> test_new() {
    final CreateOrderWithUnknownIdCommand createOrderCommand = new CreateOrderWithUnknownIdCommand();
    final Commander defaultAccount = new Commander();
    defaultAccount.setId("11");
    defaultAccount.setScope("sys");
    defaultAccount.setName("test");
    createOrderCommand.setCommander(defaultAccount);
    createOrderCommand.setAddress("湖头街");
    createOrderCommand.setPersistenceId(IPersistenceId.UNKNOWN);
    final CommandResponse<String> commandResponse = orderService
        .when(createOrderCommand);

    return ResponseEntity.ok(commandResponse);
  }

  @GetMapping("command-test-output")
  @ApiOperation("test")
  public ResponseEntity<OrderDetail> test(long count) {
    LongStream.range(0, count)
        .parallel()
        .forEach(
            i -> {
              final CreateOrderCommand createOrderCommand = new CreateOrderCommand();
              final Commander defaultAccount = new Commander();
              defaultAccount.setId("11");
              defaultAccount.setScope("sys");
              defaultAccount.setName("test");
              createOrderCommand.setCommander(defaultAccount);
              createOrderCommand.setAddress("湖头街");
              createOrderCommand.setPersistenceId(new OrderId(i));
              final CommandResponse<OrderDetail> commandResponse = orderService.when(createOrderCommand);
              log.info("请求：{}，返回结果：{}", JSON.toJSON(createOrderCommand),JSON.toJSON(commandResponse));
            });

    return ResponseEntity.ok().build();
  }

}
