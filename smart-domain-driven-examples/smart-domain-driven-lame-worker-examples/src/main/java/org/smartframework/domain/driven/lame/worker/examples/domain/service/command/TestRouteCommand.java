package org.smartframework.domain.driven.lame.worker.examples.domain.service.command;

import org.smartframework.domain.driven.core.Commander;
import org.smartframework.domain.driven.core.communication.IRoutableCommand;
import org.smartframework.domain.driven.core.communication.TenantRoutableCommand;
import org.smartframework.domain.driven.lame.worker.examples.domain.OrderId;

/**
 * @author noah
 * @create 2021-06-07 21:56
 */
public class TestRouteCommand implements IRoutableCommand {

  @Override
  public String getRouteKey() {
    return "test";
  }

}
