package org.smartframework.domain.driven.lame.worker.examples.domain.service.command;

import org.smartframework.core.UnknownId;
import org.smartframework.domain.driven.core.Commander;
import org.smartframework.domain.driven.core.IBaseCommand;

/**
 * @author noah
 * @create 2021-06-07 21:56
 */
public class CreateOrderWithUnknownIdCommand implements IBaseCommand<UnknownId> {

  private UnknownId persistenceId;
  private Commander commander;
  private String address;

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }
  
  public void setCommander(Commander commander) {
    this.commander = commander;
  }

  public void setPersistenceId(
      UnknownId persistenceId) {
    this.persistenceId = persistenceId;
  }


  @Override
  public UnknownId getPersistenceId() {
    return persistenceId;
  }

  @Override
  public Commander getCommander() {
    return commander;
  }
}
