package org.smartframework.domain.driven.lame.worker.examples;

import com.alibaba.fastjson.JSON;
import com.spring4all.swagger.EnableSwagger2Doc;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.rocketmq.client.log.ClientLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.domain.driven.lame.woker.ChannelClosedEvent;
import org.smartframework.domain.driven.lame.woker.InClosedChannelPredicate;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.OrderService22;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.EnableLameClient;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.EnableLameService;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.embedded.ChannelClosedApplicationEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;

/**
 * @author noah
 * @create 2021-06-07 20:36
 */
@SpringBootApplication
@EnableSwagger2Doc
@EnableLameService
@EnableLameClient
public class LameWorkerApplication implements
    ApplicationListener<ChannelClosedApplicationEvent> {

  private final static Logger logger = LoggerFactory.getLogger(LameWorkerApplication.class);
  //rocketmq-client LoggerName

  //NOTE:使用StopWatch替换计时()
  //NOTE:增加失败返回，比如broker不可用(√)
  public static void main(String[] args) {
    //TBW102
    System.setProperty(ClientLogger.CLIENT_LOG_USESLF4J, "true");
    SpringApplication.run(LameWorkerApplication.class, args);
  }

  Map<String,String> a =new HashMap<>();

  @Override
  public void onApplicationEvent(
      ChannelClosedApplicationEvent appEvent) {
    ChannelClosedEvent event = (ChannelClosedEvent) appEvent.getSource();
    final Set<String> strings = a.keySet();
    for(String s:strings){
      if (InClosedChannelPredicate.test(s, OrderService22.class, event)) {
        a.remove(s);
        logger.info("编码在已经被移除的通道中");
      } else {
        logger.info("编码未在被移除的通道中");
      }
    }


    logger.info(JSON.toJSONString(appEvent.getSource()));
  }
}
