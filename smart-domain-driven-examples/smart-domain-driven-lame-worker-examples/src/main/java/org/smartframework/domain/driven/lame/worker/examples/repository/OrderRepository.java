package org.smartframework.domain.driven.lame.worker.examples.repository;

import java.util.Optional;
import org.smartframework.domain.driven.core.repository.ISavedContext;
import org.smartframework.domain.driven.lame.worker.examples.domain.Order;
import org.smartframework.domain.driven.lame.worker.examples.domain.Order.State;
import org.smartframework.domain.driven.lame.worker.examples.domain.OrderId;
import org.springframework.stereotype.Repository;

/**
 * @author noah
 * @create 2021-06-07 22:07
 */
@Repository
public class OrderRepository implements Order.Repository {

  @Override
  public State save(ISavedContext<State> context) {
//    State state = new State();
//    state.setOrderDetails(Lists.newArrayList());
//    state.setPersistenceId(new OrderId(5L));
//    state.setAddress("福州大鱼丸");
//    state.setCurrency(Currency.create("$", 500L));
    return context.getState();
  }

  @Override
  public Optional<State> get(OrderId id) {

    return Optional.empty();
  }

  @Override
  public OrderId nextPersistenceId() {
    return new OrderId(5L);
  }

  @Override
  public String getX() {
    return null;
  }

  @Override
  public String getY() {
    return null;
  }
}
