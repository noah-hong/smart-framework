package org.smartframework.domain.driven.lame.worker.examples.domain;

import org.smartframework.core.IPersistenceId;

/**
 * @author noah
 * @create 2021-06-07 20:44
 */
public class OrderId implements IPersistenceId<Long> {

  public OrderId() {

  }

  public OrderId(Long value) {
    this.value = value;
  }

  private Long value;

  public void setValue(Long value) {
    this.value = value;
  }

  @Override
  public Long getValue() {
    return value;
  }
}
