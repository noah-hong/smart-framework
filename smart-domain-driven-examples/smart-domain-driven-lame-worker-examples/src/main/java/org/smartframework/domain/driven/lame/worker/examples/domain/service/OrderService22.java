package org.smartframework.domain.driven.lame.worker.examples.domain.service;

import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.lame.worker.examples.domain.OrderDetail;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.CreateOrderCommand;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.CreateOrderWithUnknownIdCommand;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.TestRouteCommand;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.annotation.LameClient;

/**
 * @author noah
 * @create 2021-06-07 21:19
 */
@LameClient
public interface OrderService22 {

  CommandResponse<OrderDetail> when(CreateOrderCommand command);

  CommandResponse<String> when(CreateOrderWithUnknownIdCommand command);

  CommandResponse<String> when(TestRouteCommand command);
}
