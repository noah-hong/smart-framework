package org.smartframework.domain.driven.lame.worker.examples.domain.service.interpreter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import org.smartframework.domain.driven.core.CommandBehavior;
import org.smartframework.domain.driven.core.CommandResponse;
import org.smartframework.domain.driven.core.repository.SavedContext;
import org.smartframework.domain.driven.lame.worker.examples.domain.Currency;
import org.smartframework.domain.driven.lame.worker.examples.domain.Order;
import org.smartframework.domain.driven.lame.worker.examples.domain.Order.CreateOrderFailureException;
import org.smartframework.domain.driven.lame.worker.examples.domain.Order.State;
import org.smartframework.domain.driven.lame.worker.examples.domain.OrderDetail;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.OrderService22;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.CreateOrderCommand;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.CreateOrderWithUnknownIdCommand;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.command.TestRouteCommand;
import org.smartframework.domain.driven.lame.worker.examples.domain.service.event.CreatedOrderEvent;
import org.smartframework.event.driven.core.store.IEventEnvelopStore;
import org.smartframework.spring.boot.autoconfigure.domain.driven.lame.annotation.LameService;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author noah
 * @create 2021-06-07 22:05
 */
@LameService
public class OrderServiceInterpreter implements OrderService22 {

  @Autowired
  private Order.Repository repository;
  @Autowired
  private IEventEnvelopStore eventStore;

  @Override
//  @Transactional
  public CommandResponse<String> when(TestRouteCommand command) {
    return CommandResponse.success("test");
  }

  @Override
//  @Transactional
  public CommandResponse<OrderDetail> when(CreateOrderCommand command) {
    System.out.println("curr-thread-name:" + Thread.currentThread().getName());
    final Optional<State> state = repository.get(command.getPersistenceId());
    if (!state.isPresent()) {
      if (command.getPersistenceId().getValue() == 33) {
        throw new CreateOrderFailureException();
      }
      //TODO 新增订单逻辑
      State newState = new State();
      newState.setAddress(command.getAddress());
      newState.setPersistenceId(command.getPersistenceId());
      newState.setCurrency(Currency.createRMB(5L));
      newState.setOrderDetails(new ArrayList<>());
      OrderDetail orderDetail = new OrderDetail();
      orderDetail.setProductCode("test");
      orderDetail.setCount(1);
      orderDetail.setCurrency(Currency.createRMB(123L));
      orderDetail.setDate(LocalDateTime.now());
      newState.getOrderDetails().add(orderDetail);
      final SavedContext savedContext = new SavedContext();
      savedContext.setOperator(command.getCommander());
      savedContext.setBehavior(CommandBehavior.NEW);
      savedContext.setState(newState);
      repository.save(savedContext);
      CreatedOrderEvent event = new CreatedOrderEvent();
      event.setPersistenceId(newState.getPersistenceId());
      event.setAddress(newState.getAddress());
//      eventStore.append(EventEnvelope.builder(event).build());
      return CommandResponse.success(orderDetail);
    }
//    CommandResponse.failure("");
//    CommandResponse.failure(new RuntimeException());
//    CommandResponse.failure(new CommandMessageEnvelope());
    return CommandResponse.failure(new Order.CreateOrderFailureException());
  }

  @Override
  public CommandResponse<String> when(CreateOrderWithUnknownIdCommand command) {
    System.out.println("curr-thread-name:" + Thread.currentThread().getName());
    final State state = new State();
    //TODO 新增订单逻辑
    State newState = new State();
    newState.setAddress(command.getAddress());
    newState.setPersistenceId(repository.nextPersistenceId());
    newState.setCurrency(Currency.createRMB(5L));
    newState.setOrderDetails(new ArrayList<>());
    final SavedContext savedContext = new SavedContext();
    savedContext.setOperator(command.getCommander());
    savedContext.setBehavior(CommandBehavior.NEW);
    savedContext.setState(newState);
    repository.save(savedContext);
    CreatedOrderEvent event = new CreatedOrderEvent();
    event.setPersistenceId(newState.getPersistenceId());
    event.setAddress(newState.getAddress());

//      eventStore.append(EventEnvelope.builder(event).build());
    return CommandResponse.success().build();


  }
}
