package org.smartframework.domain.driven.lame.worker.examples.domain;

import java.io.Serializable;
import org.smartframework.domain.driven.core.annotation.ValueObject;

/**
 * @author noah
 * @create 2021-06-07 20:47
 */
@ValueObject
public class Currency implements Serializable {

  private String symbol;

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  private Long amount;

  public static Currency createRMB( Long amount) {
    Currency currency = new Currency();
    currency.setAmount(amount);
    currency.setSymbol("￥");
    return currency;
  }

}
