package org.smartframework.domain.driven.lame.worker.examples.domain;

import java.util.List;
import org.smartframework.domain.driven.core.DomainException;
import org.smartframework.domain.driven.core.IAggregateRoot;
import org.smartframework.domain.driven.core.repository.IDomainRepository;

/**
 * @author noah
 * @create 2021-06-07 20:45
 */
public final class Order {

  public static class State implements IAggregateRoot<OrderId> {

    private OrderId persistenceId;
    private String address;
    private Currency currency;
    private List<OrderDetail> orderDetails;


    public List<OrderDetail> getOrderDetails() {
      return orderDetails;
    }

    public void setOrderDetails(
        List<OrderDetail> orderDetails) {
      this.orderDetails = orderDetails;
    }


    public void setPersistenceId(
        OrderId persistenceId) {
      this.persistenceId = persistenceId;
    }

    @Override
    public OrderId getPersistenceId() {
      return persistenceId;
    }

    public String getAddress() {
      return address;
    }

    public void setAddress(String address) {
      this.address = address;
    }

    public Currency getCurrency() {
      return currency;
    }

    public void setCurrency(Currency currency) {
      this.currency = currency;
    }

  }

  public static State create() {
    return new State();
  }

  public interface Repository extends IDomainRepository<State, OrderId> {

    String getX();

    String getY();
  }

  public static class CreateOrderFailureException extends DomainException {

    public CreateOrderFailureException() {
      super("44", "订单创建失败");
    }
  }
}
