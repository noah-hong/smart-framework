# SMART框架

## 前言

略

## 事件驱动框架

使用说明请查看 [smart-event-driven-examples](smart-event-driven-examples).

## 领域驱动框架

使用说明请查看 [smart-domain-driven-examples](smart-domain-driven-examples).

## 能力编排框架

使用说明请查看 [smart-saga-examples](smart-saga-examples).
