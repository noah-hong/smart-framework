package org.smartframework.streaming.spring.boot.starter.flink;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.configuration.Configuration;
import org.smartframework.streaming.core.MappingProcessor;
import org.smartframework.streaming.core.SourceProcessor;
import org.smartframework.streaming.core.SourceProcessorNode;
import org.smartframework.streaming.core.flink.SourceProcessorNodeBootstrap;
import org.smartframework.streaming.spring.boot.starter.SpringApplicationFactory;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Map;

/**
 * @author noah
 * @create 2022-10-27 11:21
 */
public class SpringSourceProcessorNodeBootstrap extends SourceProcessorNodeBootstrap {

    private Class<?> primarySource;

    public SpringSourceProcessorNodeBootstrap(SourceProcessorNode sourceProcessorNode, int nodeLevel, Class<?> primarySource) {
        super(sourceProcessorNode, nodeLevel);
        this.primarySource = primarySource;
    }

    @Override
    protected SourceProcessor createProcessor(Class<? extends SourceProcessor> processorType) throws IllegalAccessException, InstantiationException {
        return applicationContext.getBean(processorType);
    }

    ConfigurableApplicationContext applicationContext;

    @Override
    public void open(Configuration parameters) throws Exception {
        final ExecutionConfig.GlobalJobParameters globalJobParameters =
                getRuntimeContext().getExecutionConfig().getGlobalJobParameters();
        final Map<String, String> params = globalJobParameters.toMap();
        final String args = params.get("args");

        applicationContext = SpringApplicationFactory
                .create(primarySource, args.split(" "));
        super.open(parameters);
    }

    @Override
    public void close() throws Exception {
        super.close();
        SpringApplicationFactory.shutdown();
    }
}
