package org.smartframework.streaming.spring.boot.starter.flink;

import org.smartframework.streaming.core.MappingProcessorNode;
import org.smartframework.streaming.core.SinkProcessorNode;
import org.smartframework.streaming.core.SourceProcessorNode;
import org.smartframework.streaming.core.flink.MappingProcessorNodeExecuteFunction;
import org.smartframework.streaming.core.flink.SinkProcessorNodeExecuteFunction;
import org.smartframework.streaming.core.flink.SourceProcessorNodeBootstrap;
import org.smartframework.streaming.core.flink.StreamingGenerator;

/**
 * @author noah
 * @create 2022-10-27 11:26
 */
public class SpringStreamingGenerator extends StreamingGenerator {

    Class<?> springEntryType;

    public SpringStreamingGenerator(Class<?> springEntryType) {
        this.springEntryType = springEntryType;
    }

    @Override
    protected SourceProcessorNodeBootstrap createSourceProcessorNodeBootstrap(SourceProcessorNode sourceProcessorNode, int nodeLevel) {
        return new SpringSourceProcessorNodeBootstrap(sourceProcessorNode, nodeLevel, springEntryType);
    }

    @Override
    protected MappingProcessorNodeExecuteFunction createMappingProcessorNodeExecuteFunction(MappingProcessorNode<?, ?> mappingProcessorNode) {
        return new SpringMappingProcessorNodeExecuteFunction(mappingProcessorNode, springEntryType);
    }

    @Override
    protected SinkProcessorNodeExecuteFunction createSinkProcessorNodeExecuteFunction(SinkProcessorNode sinkProcessorNode) {
        return new SpringSinkProcessorNodeExecuteFunction(sinkProcessorNode, springEntryType);
    }
}
