package org.smartframework.streaming.spring.boot.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author noah
 * @create 2022-10-26 21:15
 */
public class SpringApplicationFactory {

    static transient ConfigurableApplicationContext applicationContext;
    static ThreadGroup threadGroup = new ThreadGroup("test");

    public static synchronized void shutdown() {
        applicationContext.close();
        threadGroup.interrupt();
//        threadGroup.stop();
    }

    public static synchronized ConfigurableApplicationContext create(Class<?> primarySource, String[] args) throws ExecutionException, InterruptedException {
        if (applicationContext == null) {
            CompletableFuture<ConfigurableApplicationContext> future = new CompletableFuture<>();

//            ThreadFactory threadFactory = Executors.defaultThreadFactory();
//            ThreadPoolExecutor executorPool = new ThreadPoolExecutor(3, 10, 5, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(2), threadFactory);
//            executorPool.execute(() -> {

//                future.complete(applicationContext);
//            });
            final Thread thread = new Thread(threadGroup, () -> {
                applicationContext = SpringApplication
                        .run(primarySource, args);
                future.complete(applicationContext);
            });
            thread.setName("application-context-thread");
            thread.setDaemon(true);
            thread.start();

            return future.get();
        }

        return applicationContext;


    }


}
