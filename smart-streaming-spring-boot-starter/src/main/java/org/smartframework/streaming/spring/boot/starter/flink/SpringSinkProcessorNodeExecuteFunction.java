package org.smartframework.streaming.spring.boot.starter.flink;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.configuration.Configuration;
import org.smartframework.streaming.core.SinkProcessor;
import org.smartframework.streaming.core.SinkProcessorNode;
import org.smartframework.streaming.core.flink.SinkProcessorNodeExecuteFunction;
import org.smartframework.streaming.spring.boot.starter.SpringApplicationFactory;
import org.smartframework.streaming.spring.boot.starter.SpringSinkProcessor;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Map;

/**
 * @author noah
 * @create 2022-10-26 17:10
 */
public class SpringSinkProcessorNodeExecuteFunction extends SinkProcessorNodeExecuteFunction {

    Class<?> primarySource;
    SinkProcessorNode sinkProcessorNode;

    public SpringSinkProcessorNodeExecuteFunction(SinkProcessorNode sinkProcessorNode, Class<?> primarySource) {
        super(sinkProcessorNode);
        this.sinkProcessorNode = sinkProcessorNode;
        this.primarySource = primarySource;
    }

    @Override
    protected SinkProcessor createProcessor(Class<? extends SinkProcessor> processorType) throws IllegalAccessException, InstantiationException {
        return applicationContext.getBean(processorType);
    }

    ConfigurableApplicationContext applicationContext;

    @Override
    public void open(Configuration parameters) throws Exception {
        final ExecutionConfig.GlobalJobParameters globalJobParameters =
                getRuntimeContext().getExecutionConfig().getGlobalJobParameters();
        final Map<String, String> params = globalJobParameters.toMap();
        final String args = params.get("args");

        applicationContext = SpringApplicationFactory
                .create(primarySource, args.split(" "));
        super.open(parameters);
    }

    @Override
    public void close() throws Exception {
//        SpringApplicationFactory.shutdown();
    }
}
