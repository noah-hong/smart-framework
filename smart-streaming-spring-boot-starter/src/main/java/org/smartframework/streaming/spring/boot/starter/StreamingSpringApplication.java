package org.smartframework.streaming.spring.boot.starter;

import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.core.execution.JobClient;
import org.apache.flink.core.execution.JobListener;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.streaming.core.NoneProcessorNodeDsl;
import org.smartframework.streaming.core.ProcessorNodeDsl;
import org.smartframework.streaming.core.flink.ExtParameterTool;
import org.smartframework.streaming.core.flink.StreamingGenerator;
import org.smartframework.streaming.spring.boot.starter.flink.SpringSinkProcessorNodeExecuteFunctionFactory;
import org.smartframework.streaming.spring.boot.starter.flink.SpringStreamingGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import javax.annotation.Nullable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author noah
 * @create 2022-10-24 11:26
 */
public class StreamingSpringApplication {

    //mvn clean -pl smart-streaming-spring-boot-starter -am install
    private static final Logger LOG = LoggerFactory.getLogger(StreamingSpringApplication.class);


    public static ConfigurableApplicationContext run(Class<?> primarySource, BiConsumer<StreamExecutionEnvironment, ConfigurableApplicationContext> streamEnvOptions, String... args) throws Exception {
        ExtParameterTool parameterTool = ExtParameterTool.fromArgs2(args);
        final String streamingCore = parameterTool.get("streaming.core");

        if (StringUtils.isEmpty(streamingCore)) {
            throw new RuntimeException("执行DSL参数未设置");
        }
        ConfigurableApplicationContext applicationContext = SpringApplicationFactory.create(primarySource, args);
        ProcessorNodeDsl processorNodeDsl = applicationContext
                .getBeansOfType(ProcessorNodeDsl.class).values()
                .stream()
                .filter(node -> node.getStreamingDefinition().getCode().equals(streamingCore))
                .findFirst().orElseGet(NoneProcessorNodeDsl::new);

        if (processorNodeDsl instanceof NoneProcessorNodeDsl) {
            throw new RuntimeException("执行DSL未发现：" + streamingCore);
        }
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);
        env.getConfig().disableClosureCleaner();
        //异常后尝试重启3次，每次重启间隔10秒
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3, 10 * 1000L));
        env.setStateBackend(new HashMapStateBackend());
        //每3秒存盘快照1次
        env.enableCheckpointing(3L * 1000L, CheckpointingMode.AT_LEAST_ONCE);
        env.getCheckpointConfig().setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        env.getConfig().setGlobalJobParameters(parameterTool);

        final StreamingGenerator streamingGenerator = new SpringStreamingGenerator(primarySource);
        processorNodeDsl.getStreamingDefinition().getSources().all().forEach(node -> {
            streamingGenerator.generate(env, node, 1);

        });

        streamEnvOptions.accept(env, applicationContext);
        try {
            //显示数据流执行计划,可将打印出来的执行计划在该地址（https://flink.apache.org/visualizer/）执行显示数据流图
            final String executionPlan = env.getExecutionPlan();
            LOG.info("数据流执行计划：\n{}", executionPlan);
            env.executeAsync(streamingCore);
//            applicationContext.close();
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

        return applicationContext;
    }


}
