package org.smartframework.streaming.spring.boot.starter;

import org.smartframework.streaming.core.SinkProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author noah
 * @create 2022-10-26 17:08
 */
public abstract class SpringSinkProcessor<T> implements SinkProcessor<T> {

    private ConfigurableApplicationContext applicationContext;

    public ConfigurableApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ConfigurableApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
