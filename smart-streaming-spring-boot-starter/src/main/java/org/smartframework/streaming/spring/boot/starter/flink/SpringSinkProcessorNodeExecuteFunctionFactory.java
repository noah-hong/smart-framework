package org.smartframework.streaming.spring.boot.starter.flink;

import org.smartframework.streaming.core.SinkProcessorNode;
import org.smartframework.streaming.core.flink.SinkProcessorNodeExecuteFunction;
import org.smartframework.streaming.core.flink.SinkProcessorNodeExecuteFunctionFactory;

/**
 * @author noah
 * @create 2022-10-26 17:23
 */
public class SpringSinkProcessorNodeExecuteFunctionFactory implements SinkProcessorNodeExecuteFunctionFactory {

    Class<?> primarySource;

    public SpringSinkProcessorNodeExecuteFunctionFactory(Class<?> primarySource) {
        this.primarySource = primarySource;
    }

    @Override
    public SinkProcessorNodeExecuteFunction create(SinkProcessorNode sinkProcessorNode) {
        return new SpringSinkProcessorNodeExecuteFunction(sinkProcessorNode, primarySource);
    }
}
