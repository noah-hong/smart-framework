package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.DefaultMappingProcessorNode;
import org.smartframework.streaming.core.MappingProcessor;
import org.smartframework.streaming.core.MappingProcessorNode;
import org.smartframework.streaming.core.ShardingNode;

import java.util.Objects;

/**
 * @author noah
 * @create 2022-10-20 23:27
 */
public class DefaultMappingProcessorNodeBuilder<I extends DataElement, O extends DataElement> implements
    MappingProcessorNodeBuilder<I, O> {

  private String code;
  private String name;
  private final Class<I> inputType;
  private final Class<O> outputType;
  private Class<? extends MappingProcessor<I, O>> mappingProcessorType;
  private int shardingNum;
  private ShardingNode.ShardingKeySelector<?> shardingKeySelector;


  public DefaultMappingProcessorNodeBuilder(Class<I> inputType, Class<O> outputType) {
    this.inputType = inputType;
    this.outputType = outputType;
    shardingNum = 1;
    shardingKeySelector = new ShardingNode.NoShardingKeySelector<I>() {
    };
  }

  @Override
  public MappingProcessorNodeBuilder<I, O> shardingNumber(int num) {
    this.shardingNum = num;
    return this;
  }

  @Override
  public MappingProcessorNodeBuilder<I, O> shardingKey(
      ShardingNode.ShardingKeySelector<I> shardingKeySelector) {
    this.shardingKeySelector = shardingKeySelector;
    return this;
  }


  @Override
  public MappingProcessorNodeBuilder<I, O> code(String code) {
    this.code = code;
    return this;
  }

  @Override
  public MappingProcessorNodeBuilder<I, O> name(String name) {
    this.name = name;
    return this;
  }

  @Override
  public MappingProcessorNodeBuilder<I, O> processor(Class<? extends MappingProcessor<I, O>> mappingProcessorType) {
    this.mappingProcessorType = mappingProcessorType;
    return this;
  }


  @Override
  public MappingProcessorNode<I, O> build() {
    Objects.requireNonNull(mappingProcessorType);
    DefaultMappingProcessorNode<I, O> defaultMappingProcessorNode = new DefaultMappingProcessorNode<>();
    defaultMappingProcessorNode.setMappingProcessorType(mappingProcessorType);
    defaultMappingProcessorNode.setCode(code);
    defaultMappingProcessorNode.setName(name);
    defaultMappingProcessorNode.setInputType(inputType);
    defaultMappingProcessorNode.setOutputType(outputType);
    defaultMappingProcessorNode.setShardingNumber(shardingNum);
    defaultMappingProcessorNode.setShardingKeySelector(shardingKeySelector);
    if (code == null) {
      defaultMappingProcessorNode.setCode(
          "from " + inputType.getSimpleName() + " to " + outputType.getSimpleName());
    }
    if (name == null) {
      defaultMappingProcessorNode.setName(
          "from " + inputType.getSimpleName() + " to " + outputType.getSimpleName());
    }
    return defaultMappingProcessorNode;
  }
}
