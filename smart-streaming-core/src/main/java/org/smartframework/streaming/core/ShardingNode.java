package org.smartframework.streaming.core;

import java.io.Serializable;

/**
 * @author noah
 * @create 2022-09-29 16:26
 */
public interface ShardingNode extends Node {

    default int shardingNumber() {
        return 1;
    }

    default ShardingKeySelector<?> shardingKeySelector() {
        return new NoShardingKeySelector<Void>() {
        };
    }

    interface ShardingKeySelector<IN> extends Serializable {
        String key(IN value) throws Exception;
    }


    interface NoShardingKeySelector<IN> extends ShardingKeySelector<IN> {
        @Override
        default String key(IN value) throws Exception {
            return "";
        }
    }
}
