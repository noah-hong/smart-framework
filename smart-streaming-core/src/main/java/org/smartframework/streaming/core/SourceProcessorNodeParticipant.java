package org.smartframework.streaming.core;

import java.io.Serializable;
import java.util.List;

/**
 * @author noah
 * @create 2022-09-14 13:00
 */
public interface SourceProcessorNodeParticipant extends Serializable {

    void add(SourceProcessorNode processor);

    List<SourceProcessorNode> all();
}
