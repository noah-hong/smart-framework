package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.ShardingNode;
import org.smartframework.streaming.core.SinkProcessor;
import org.smartframework.streaming.core.SinkProcessorNode;

import java.io.Serializable;

/**
 * @author noah
 * @create 2022-10-20 23:12
 */
public interface SinkProcessorNodeBuilder<T> extends Serializable {

  SinkProcessorNodeBuilder<T> code(String code);

  SinkProcessorNodeBuilder<T> name(String name);

  SinkProcessorNodeBuilder<T> shardingNumber(int num);

  SinkProcessorNodeBuilder<T> shardingKey(
      ShardingNode.ShardingKeySelector<T> shardingKeySelector);

  SinkProcessorNodeBuilder<T> processor(Class<? extends SinkProcessor<T>> processorType);

  SinkProcessorNode<T> build();
}