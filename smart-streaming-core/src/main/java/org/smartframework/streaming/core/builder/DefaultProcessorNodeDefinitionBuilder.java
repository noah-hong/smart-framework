package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.DefaultProcessorNodeDefinition;
import org.smartframework.streaming.core.DefaultSourceProcessorNodeParticipant;
import org.smartframework.streaming.core.SourceProcessorNode;
import org.smartframework.streaming.core.SourceProcessorNodeParticipant;
import org.smartframework.streaming.core.StreamingDefinition;

import java.util.Objects;

/**
 * @author noah
 * @create 2022-10-20 23:25
 */
public  class DefaultProcessorNodeDefinitionBuilder implements
    ProcessorNodeDefinitionBuilder {

  private SourceProcessorNodeParticipant sources;
  private String code;


  public DefaultProcessorNodeDefinitionBuilder() {
    this.sources = new DefaultSourceProcessorNodeParticipant();
  }


  @Override
  public ProcessorNodeDefinitionBuilder code(String code) {
    this.code = code;
    return this;
  }


  @Override
  public ProcessorNodeDefinitionBuilder source(SourceProcessorNodeBuilderFunction func) {
    final SourceProcessorNode node = func.apply(new DefaultSourceProcessorNodeBuilder());
    sources.add(node);
    return this;
  }

  @Override
  public StreamingDefinition build() {
    Objects.requireNonNull(this.code);
    DefaultProcessorNodeDefinition defaultProcessorNodeDefinition = new DefaultProcessorNodeDefinition();
    defaultProcessorNodeDefinition.setCode(code);
    defaultProcessorNodeDefinition.setSources(sources);
    return defaultProcessorNodeDefinition;
  }
}
