package org.smartframework.streaming.core;

import java.io.Serializable;

/**
 * @author noah
 * @create 2022-09-12 19:53
 */
public interface SourceProcessor extends Processor {

    void run(Context context) throws Exception;

    default State initializeState() {
        return null;
    }

    default void snapshotStateComplete() {
    }

    interface State extends Serializable {
    }

    interface Context {
        void collect(Element element);

        int getTaskNumber();

        int getTaskIndex();

        <T extends State> T getState();

        void updateState(State state);
    }



}
