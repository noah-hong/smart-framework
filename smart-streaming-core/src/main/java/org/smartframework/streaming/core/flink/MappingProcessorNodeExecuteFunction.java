package org.smartframework.streaming.core.flink;


import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.configuration.Configuration;
import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.MappingProcessor;
import org.smartframework.streaming.core.MappingProcessorNode;
import org.smartframework.streaming.core.SourceProcessor;

/**
 * @author noah
 * @create 2022-09-15 16:20
 */
public class MappingProcessorNodeExecuteFunction
        extends RichMapFunction<DataElement, DataElement> {

    MappingProcessorNode<?, ?> mappingProcessorNode;

    public MappingProcessorNodeExecuteFunction(MappingProcessorNode<?, ?> mappingProcessorNode) {
        this.mappingProcessorNode = mappingProcessorNode;

    }

    protected MappingProcessor createProcessor(Class<? extends MappingProcessor> processorType) throws IllegalAccessException, InstantiationException {
        return processorType.newInstance();
    }

    MappingProcessor processor;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);

        processor = createProcessor(mappingProcessorNode.processorType());
    }

    @Override
    public DataElement map(DataElement value) throws Exception {
        return (DataElement) processor.map(value);
    }
}
