package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.DefaultSinkProcessorNode;
import org.smartframework.streaming.core.ShardingNode;
import org.smartframework.streaming.core.SinkProcessor;
import org.smartframework.streaming.core.SinkProcessorNode;

import java.util.Objects;

/**
 * @author noah
 * @create 2022-10-20 23:26
 */

public class DefaultSinkProcessorNodeBuilder<T extends DataElement> implements
        SinkProcessorNodeBuilder<T> {

    private String code;
    private String name;
    private Class<? extends SinkProcessor<T>> processorType;
    private int shardingNum;
    private ShardingNode.ShardingKeySelector<T> shardingKeySelector;
    private final Class<T> processElementType;

    public DefaultSinkProcessorNodeBuilder(Class<T> processEleType) {
        this.processElementType = processEleType;
        this.shardingNum = 1;
        this.shardingKeySelector = new ShardingNode.NoShardingKeySelector<T>() {
        };

    }

    @Override
    public SinkProcessorNodeBuilder<T> code(String code) {
        this.code = code;
        return this;
    }

    @Override
    public SinkProcessorNodeBuilder<T> name(String name) {
        this.name = name;
        return this;
    }

    @Override
    public SinkProcessorNodeBuilder<T> shardingNumber(int num) {
        this.shardingNum = num;
        return this;
    }

    @Override
    public SinkProcessorNodeBuilder<T> shardingKey(
            ShardingNode.ShardingKeySelector<T> shardingKeySelector) {
        this.shardingKeySelector = shardingKeySelector;
        return this;
    }

    @Override
    public SinkProcessorNodeBuilder<T> processor(Class<? extends SinkProcessor<T>> processorType) {
        this.processorType = processorType;
        return this;
    }

    @Override
    public SinkProcessorNode<T> build() {
        Objects.requireNonNull(processorType);
        Objects.requireNonNull(processElementType);
        DefaultSinkProcessorNode<T> sinkProcessorNode = new DefaultSinkProcessorNode<>();
        sinkProcessorNode.setProcessorType(processorType);
        sinkProcessorNode.setShardingNumber(shardingNum);
        sinkProcessorNode.setShardingKeySelector(shardingKeySelector);
        sinkProcessorNode.setCode(code);
        sinkProcessorNode.setName(name);
        if (code == null) {
            sinkProcessorNode.setCode(processorType.getSimpleName());
        }
        if (name == null) {
            sinkProcessorNode.setName(
                    processElementType.getSimpleName() + " by " + processorType
                            .getSimpleName());
        }
        sinkProcessorNode.setProcessElementType(processElementType);
        return sinkProcessorNode;
    }
}
