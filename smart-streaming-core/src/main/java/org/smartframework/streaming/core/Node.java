package org.smartframework.streaming.core;

import java.io.Serializable;

/**
 * @author noah
 * @create 2022-09-14 11:18
 */
public interface Node extends Serializable {
    String getName();
    String getCode();
}
