package org.smartframework.streaming.core.flink;


import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.MappingProcessor;
import org.smartframework.streaming.core.SinkProcessor;
import org.smartframework.streaming.core.SinkProcessorNode;

/**
 * @author noah
 * @create 2022-09-16 15:02
 */
public class SinkProcessorNodeExecuteFunction extends RichSinkFunction<DataElement> implements CheckpointedFunction {


    SinkProcessorNode sinkProcessorNode;

    public SinkProcessorNodeExecuteFunction(SinkProcessorNode sinkProcessorNode) {
        this.sinkProcessorNode = sinkProcessorNode;
    }

    protected SinkProcessor createProcessor(Class<? extends SinkProcessor> processorType) throws IllegalAccessException, InstantiationException {
        return processorType.newInstance();
    }
    SinkProcessor sinkProcessor;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        this.sinkProcessor = createProcessor(sinkProcessorNode.processorType());
    }

    @Override
    public void invoke(DataElement value, Context context) throws Exception {
        sinkProcessor.processElement(value);
    }

    @Override
    public void snapshotState(FunctionSnapshotContext context) throws Exception {

    }

    @Override
    public void initializeState(FunctionInitializationContext context) throws Exception {

    }
}
