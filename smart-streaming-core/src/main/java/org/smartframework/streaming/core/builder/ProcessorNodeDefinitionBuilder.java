package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.StreamingDefinition;

import java.io.Serializable;

/**
 * @author noah
 * @create 2022-10-20 22:55
 */
public interface ProcessorNodeDefinitionBuilder extends Serializable {

  ProcessorNodeDefinitionBuilder code(String code);

  ProcessorNodeDefinitionBuilder source(SourceProcessorNodeBuilderFunction func);

  StreamingDefinition build();
}