package org.smartframework.streaming.core;

/**
 * @author noah
 * @create 2022-09-14 11:17
 */
public interface SinkProcessorNode<T> extends ShardingNode {

    Class<T> processElementType();
    Class<? extends SinkProcessor<T>> processorType();

}
