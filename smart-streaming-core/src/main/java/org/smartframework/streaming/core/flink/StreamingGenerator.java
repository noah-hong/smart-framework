package org.smartframework.streaming.core.flink;

import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSink;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.Element;
import org.smartframework.streaming.core.MappingProcessorNode;
import org.smartframework.streaming.core.MappingProcessorNodeParticipant;
import org.smartframework.streaming.core.ShardingNode;
import org.smartframework.streaming.core.SinkProcessorNode;
import org.smartframework.streaming.core.SinkProcessorNodeParticipant;
import org.smartframework.streaming.core.SourceProcessor;
import org.smartframework.streaming.core.SourceProcessorNode;
import org.smartframework.streaming.core.SourceProcessorNodeParticipant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author noah
 * @create 2022-10-24 14:20
 */
public class StreamingGenerator {

    protected SourceProcessorNodeBootstrap createSourceProcessorNodeBootstrap(SourceProcessorNode sourceProcessorNode, int nodeLevel) {
        return new SourceProcessorNodeBootstrap(sourceProcessorNode, nodeLevel);
    }

    protected MappingProcessorNodeExecuteFunction createMappingProcessorNodeExecuteFunction(MappingProcessorNode<?, ?> mappingProcessorNode) {
        return new MappingProcessorNodeExecuteFunction(mappingProcessorNode);
    }

    protected SinkProcessorNodeExecuteFunction createSinkProcessorNodeExecuteFunction(SinkProcessorNode sinkProcessorNode) {
        return new SinkProcessorNodeExecuteFunction(sinkProcessorNode);
    }

    public void generate(StreamExecutionEnvironment env, SourceProcessorNode sourceProcessorNode, int nodeLevel) {
        final SourceProcessorNodeBootstrap sourceProcessorProcessFunction = createSourceProcessorNodeBootstrap(sourceProcessorNode, nodeLevel);
        final SingleOutputStreamOperator<Element> sourceStreamOperator = env.addSource(sourceProcessorProcessFunction)
                .name(sourceProcessorNode.getName())
                .setDescription(nodeLevel + "级节点:" + sourceProcessorNode.getName())
                .uid(sourceProcessorNode.getCode())
                .startNewChain();
        sourceStreamOperator.setParallelism(sourceProcessorNode.shardingNumber());

        final MappingProcessorNodeParticipant mappingProcessorNodeParticipant = sourceProcessorNode.mappings();
        final SinkProcessorNodeParticipant sinks = sourceProcessorNode.sinks();

        final List<MappingProcessorNode<?, ?>> mappingProcessorNodes = mappingProcessorNodeParticipant.getAll();


        MappingProcessorNodeBuildingFactory mappingProcessorNodeOutputFactory = new MappingProcessorNodeBuildingFactory(sinks);

        for (MappingProcessorNode<?, ?> node : mappingProcessorNodes) {
            mappingProcessorNodeOutputFactory.add(node);
        }

        final List<SinkProcessorNode<?>> sinkList = sinks.all();
        UndistributedSinkProcessorNodeBuildingFactory undistributedSinbkProcessorNodeBuildingFactory = new UndistributedSinkProcessorNodeBuildingFactory();
        final Set<SinkProcessorNode<?>> holdingDataProcessorNodes = mappingProcessorNodeOutputFactory.allHoldingDataProcessorNode();
        for (SinkProcessorNode<?> sink : sinkList) {
            if (!holdingDataProcessorNodes.contains(sink)) {
                undistributedSinbkProcessorNodeBuildingFactory.add(sink);
            }
        }

        final SingleOutputStreamOperator<Element> distributeStreamOp = sourceStreamOperator
                .process(new ProcessorNodeDistributeFunction(mappingProcessorNodeOutputFactory, undistributedSinbkProcessorNodeBuildingFactory))
                .uid(sourceProcessorNode.getCode() + ":distribute-processor-node")
                .name("分发节点");

        distributeStreamOp.setParallelism(sourceProcessorNode.shardingNumber());

        distributeStreamOp.addSink(new SourceProcessorNodeEndFunction(sourceProcessorNode))
                .name("结束节点")
                .uid(sourceProcessorNode.getCode() + ":end-processor-node")
                .setParallelism(1);

        final List<MappingProcessorNodeBuildingFactory.MappingProcessorNodeBuildingInfo> mappingProcessorNodeBuildingInfos = mappingProcessorNodeOutputFactory.getAll();
        Map<SinkProcessorNode<?>, List<SingleOutputStreamOperator<DataElement>>> unionMappingMap = new HashMap<>();
        for (MappingProcessorNodeBuildingFactory.MappingProcessorNodeBuildingInfo mappingProcessorNodeBuildingInfo : mappingProcessorNodeBuildingInfos) {
            final MappingProcessorNode<?, ?> mapping = mappingProcessorNodeBuildingInfo.getMapping();
            if (mapping.shardingKeySelector() instanceof ShardingNode.NoShardingKeySelector) {
                final SingleOutputStreamOperator<DataElement> mappingStreamOp = distributeStreamOp.getSideOutput(mappingProcessorNodeBuildingInfo.getOutputTag())
                        .map(createMappingProcessorNodeExecuteFunction(mappingProcessorNodeBuildingInfo.getMapping()))
                        .name("Mapping:" + mappingProcessorNodeBuildingInfo.getMapping().getName())
                        .uid(sourceProcessorNode.getCode() + ":" + mappingProcessorNodeBuildingInfo.getMapping().getCode())
                        .disableChaining();
                mappingStreamOp.setParallelism(mappingProcessorNodeBuildingInfo.getMapping().shardingNumber());
                unionMappingMap.putIfAbsent(mappingProcessorNodeBuildingInfo.getSink(), new ArrayList<>());
                unionMappingMap.get(mappingProcessorNodeBuildingInfo.getSink()).add(mappingStreamOp);
            } else {
                final SingleOutputStreamOperator<DataElement> mappingStreamOp = distributeStreamOp.getSideOutput(mappingProcessorNodeBuildingInfo.getOutputTag())
                        .keyBy((KeySelector<DataElement, String>) value -> {
                            final ShardingNode.ShardingKeySelector shardingKeySelector = mapping.shardingKeySelector();
                            return shardingKeySelector.key(value);
                        })
                        .map(createMappingProcessorNodeExecuteFunction(mappingProcessorNodeBuildingInfo.getMapping()))
                        .name("Mapping:" + mappingProcessorNodeBuildingInfo.getMapping().getName())
                        .uid(sourceProcessorNode.getCode() + ":" + mappingProcessorNodeBuildingInfo.getMapping().getCode())
                        .disableChaining();
                mappingStreamOp.setParallelism(mappingProcessorNodeBuildingInfo.getMapping().shardingNumber());
                unionMappingMap.putIfAbsent(mappingProcessorNodeBuildingInfo.getSink(), new ArrayList<>());
                unionMappingMap.get(mappingProcessorNodeBuildingInfo.getSink()).add(mappingStreamOp);
            }


        }
        for (SinkProcessorNode<?> sinkNode : unionMappingMap.keySet()) {
            final List<SingleOutputStreamOperator<DataElement>> singleOutputStreamOperators = unionMappingMap.get(sinkNode);
            DataStream<DataElement> currentSingleOutputStreamOperator = singleOutputStreamOperators.get(0);

            for (int i = 1; i < singleOutputStreamOperators.size(); i++) {
                currentSingleOutputStreamOperator = currentSingleOutputStreamOperator.union(singleOutputStreamOperators.get(i));
            }
            if (sinkNode.shardingKeySelector() == null
                    || sinkNode.shardingKeySelector() instanceof ShardingNode.NoShardingKeySelector) {
                final DataStreamSink<DataElement> dataElementDataStreamSink = currentSingleOutputStreamOperator
                        .addSink(createSinkProcessorNodeExecuteFunction(sinkNode))
                        .name(sinkNode.getName())
                        .uid(sourceProcessorNode.getCode() + ":" + sinkNode.getCode())
                        .disableChaining();

                dataElementDataStreamSink.setParallelism(sinkNode.shardingNumber());
            } else {
                final DataStreamSink<DataElement> dataElementDataStreamSink = currentSingleOutputStreamOperator
                        .keyBy((KeySelector<DataElement, String>) value -> {

                            final ShardingNode.ShardingKeySelector shardingKeySelector = sinkNode.shardingKeySelector();
                            return shardingKeySelector.key(value);
                        })
                        .addSink(createSinkProcessorNodeExecuteFunction(sinkNode))
                        .name(sinkNode.getName())
                        .uid(sourceProcessorNode.getCode() + ":" + sinkNode.getCode())
                        .disableChaining();

                dataElementDataStreamSink.setParallelism(sinkNode.shardingNumber());
            }


        }


        final List<UndistributedSinkProcessorNodeBuildingFactory.Data> undistributedDataProcessorNodeBuildingDatas = undistributedSinbkProcessorNodeBuildingFactory.getAll();


        for (UndistributedSinkProcessorNodeBuildingFactory.Data data : undistributedDataProcessorNodeBuildingDatas) {
            final SinkProcessorNode<?> sinkNode = data.getSinkProcessorNode();
            if (sinkNode.shardingKeySelector() == null
                    || sinkNode.shardingKeySelector() instanceof ShardingNode.NoShardingKeySelector) {
                final DataStreamSink<DataElement> dataElementDataStreamSink =
                        distributeStreamOp
                                .getSideOutput(data.getOutputTag())
                                .addSink(createSinkProcessorNodeExecuteFunction(data.getSinkProcessorNode()))
                                .name(data.getSinkProcessorNode().getName())
                                .uid(sourceProcessorNode.getCode() + ":" + data.getSinkProcessorNode().getCode())
                                .disableChaining();
                dataElementDataStreamSink.setParallelism(data.getSinkProcessorNode().shardingNumber());
            } else {
                final DataStreamSink<DataElement> dataElementDataStreamSink =
                        distributeStreamOp
                                .getSideOutput(data.getOutputTag())
                                .keyBy(new KeySelector<DataElement, Object>() {
                                    @Override
                                    public Object getKey(DataElement value) throws Exception {
                                        final ShardingNode.ShardingKeySelector shardingKeySelector = sinkNode.shardingKeySelector();
                                        return shardingKeySelector.key(value);
                                    }
                                })
                                .addSink(createSinkProcessorNodeExecuteFunction(data.getSinkProcessorNode()))
                                .name(data.getSinkProcessorNode().getName())
                                .uid(sourceProcessorNode.getCode() + ":" + data.getSinkProcessorNode().getCode())
                                .disableChaining();
                dataElementDataStreamSink.setParallelism(data.getSinkProcessorNode().shardingNumber());
            }
        }

        final SourceProcessorNodeParticipant sourceProcessorNodeChildren = sourceProcessorNode.sources();
        if (sourceProcessorNodeChildren == null) {
            return;
        }
        final List<SourceProcessorNode> sourceProcessorNodes = sourceProcessorNodeChildren.all();
        if (!sourceProcessorNodes.isEmpty()) {
            for (SourceProcessorNode branchSourceProcessorNode : sourceProcessorNodes) {
                generate(env, branchSourceProcessorNode, nodeLevel + 1);
            }
        }
    }
}
