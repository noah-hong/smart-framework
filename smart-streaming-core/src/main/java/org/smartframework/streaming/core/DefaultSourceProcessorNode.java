package org.smartframework.streaming.core;

/**
 * @author noah
 * @create 2022-10-20 23:29
 */
public class DefaultSourceProcessorNode implements SourceProcessorNode {

  private String name;
  private String code;
  private Class<? extends SourceProcessor> processorType;
  private SinkProcessorNodeParticipant sinks;
  private SourceProcessorNodeParticipant sources;
  private MappingProcessorNodeParticipant mappings;

  private int shardingNumber = 1;

  @Override
  public int shardingNumber() {
    return shardingNumber;
  }

  public void setShardingNumber(int shardingNumber) {
    this.shardingNumber = shardingNumber;
  }

  @Override
  public ShardingKeySelector<?> shardingKeySelector() {
    return new NoShardingKeySelector<Void>() {
    };
  }


  @Override
  public Class<? extends SourceProcessor> processorType() {
    return processorType;
  }

  @Override
  public MappingProcessorNodeParticipant mappings() {
    return mappings;
  }

  public void setMappings(
      MappingProcessorNodeParticipant mappings) {
    this.mappings = mappings;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setProcessorType(Class<? extends SourceProcessor> processorType) {
    this.processorType = processorType;
  }

  public void setSinks(
      SinkProcessorNodeParticipant sinks) {
    this.sinks = sinks;
  }

  public void setSources(
      SourceProcessorNodeParticipant sources) {
    this.sources = sources;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getCode() {
    return code;
  }


  @Override
  public SinkProcessorNodeParticipant sinks() {
    return sinks;
  }

  @Override
  public SourceProcessorNodeParticipant sources() {
    return sources;
  }
}