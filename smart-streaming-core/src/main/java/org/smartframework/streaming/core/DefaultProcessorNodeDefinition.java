package org.smartframework.streaming.core;

import org.smartframework.streaming.core.builder.DefaultProcessorNodeDefinitionBuilder;
import org.smartframework.streaming.core.builder.ProcessorNodeDefinitionBuilder;

/**
 * @author noah
 * @create 2022-09-21 11:20
 */
public class DefaultProcessorNodeDefinition implements StreamingDefinition {

    private String code;
    private SourceProcessorNodeParticipant sources;


    public void setCode(String code) {
        this.code = code;
    }

    public void setSources(SourceProcessorNodeParticipant sources) {
        this.sources = sources;
    }

    @Override
    public String getCode() {
        return code;
    }


    @Override
    public SourceProcessorNodeParticipant getSources() {
        return sources;
    }

    public static ProcessorNodeDefinitionBuilder builder() {
        return new DefaultProcessorNodeDefinitionBuilder();
    }

}
