package org.smartframework.streaming.core;

/**
 * @author noah
 * @create 2022-10-24 14:07
 */
public  class NoneProcessorNodeDsl implements ProcessorNodeDsl {

    @Override
    public StreamingDefinition getStreamingDefinition() {
        throw  new UnsupportedOperationException();
    }
}
