package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.MappingProcessorNode;

import java.util.function.Function;

/**
 * @author noah
 * @create 2022-10-20 23:14
 */

@FunctionalInterface
public interface MappingProcessorNodeBuilderFunction<IN, OUT> extends
    Function<MappingProcessorNodeBuilder<IN, OUT>, MappingProcessorNode<IN, OUT>> {

}