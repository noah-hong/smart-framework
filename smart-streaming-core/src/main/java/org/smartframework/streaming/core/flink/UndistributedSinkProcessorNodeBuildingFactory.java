package org.smartframework.streaming.core.flink;


import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.util.OutputTag;
import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.SinkProcessorNode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author noah
 * @create 2022-09-16 14:29
 */
public class UndistributedSinkProcessorNodeBuildingFactory implements Serializable {

    public static class Data implements Serializable {
        public Data(OutputTag<DataElement> outputTag, SinkProcessorNode<?> sinkProcessorNode) {

            this.outputTag = outputTag;
            this.sinkProcessorNode = sinkProcessorNode;
        }

        public OutputTag<DataElement> getOutputTag() {
            return outputTag;
        }

        public SinkProcessorNode<?> getSinkProcessorNode() {
            return sinkProcessorNode;
        }

        OutputTag<DataElement> outputTag;
        SinkProcessorNode<?> sinkProcessorNode;
    }

    private final Map<String, Data> map = new HashMap<>();

    public void add(SinkProcessorNode<?> sinkProcessorNode) {
        final Data data = new Data(new OutputTag<>(UUID.randomUUID().toString(), TypeInformation.of(DataElement.class)), sinkProcessorNode);
        map.put(sinkProcessorNode.processElementType().getName(), data);
    }

    public OutputTag<DataElement> getOutputTag(Class<?> dataType) {
        final Data data = map.get(dataType.getName());
        if (data != null) {
            return data.getOutputTag();
        }
        return null;
    }

    public Data get(Class<?> dataType) {
        return map.get(dataType.getName());
    }

    public List<Data> getAll() {
        return new ArrayList<>(map.values());
    }

}
