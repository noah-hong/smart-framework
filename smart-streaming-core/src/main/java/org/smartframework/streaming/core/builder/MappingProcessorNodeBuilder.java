package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.MappingProcessor;
import org.smartframework.streaming.core.MappingProcessorNode;
import org.smartframework.streaming.core.ShardingNode;

import java.io.Serializable;

/**
 * @author noah
 * @create 2022-10-20 23:12
 */
public interface MappingProcessorNodeBuilder<I, O> extends Serializable {

    MappingProcessorNodeBuilder<I, O> code(String code);

    MappingProcessorNodeBuilder<I, O> name(String name);

    MappingProcessorNodeBuilder<I, O> shardingNumber(int num);

    MappingProcessorNodeBuilder<I, O> shardingKey(
            ShardingNode.ShardingKeySelector<I> shardingKeySelector);

    MappingProcessorNodeBuilder<I, O> processor(Class<? extends MappingProcessor<I, O>> mappingType);

    MappingProcessorNode<I, O> build();
}
