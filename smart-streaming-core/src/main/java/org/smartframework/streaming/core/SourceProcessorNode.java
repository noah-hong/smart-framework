package org.smartframework.streaming.core;

/**
 * @author noah
 * @create 2022-09-14 10:53
 */
public interface SourceProcessorNode extends ShardingNode {

//    default NodeSwitch getNodeSwitch(){};

    Class<? extends SourceProcessor> processorType();

    default MappingProcessorNodeParticipant mappings() {
        return null;
    }

    SinkProcessorNodeParticipant sinks();

    default SourceProcessorNodeParticipant sources() {
        return null;
    }

    ;
}
