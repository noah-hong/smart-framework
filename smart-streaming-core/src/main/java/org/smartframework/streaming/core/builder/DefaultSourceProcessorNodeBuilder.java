package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.DefaultMappingProcessorNodeParticipant;
import org.smartframework.streaming.core.DefaultSinkProcessorParticipant;
import org.smartframework.streaming.core.DefaultSourceProcessorNode;
import org.smartframework.streaming.core.DefaultSourceProcessorNodeParticipant;
import org.smartframework.streaming.core.MappingProcessorNode;
import org.smartframework.streaming.core.MappingProcessorNodeParticipant;
import org.smartframework.streaming.core.SinkProcessorNode;
import org.smartframework.streaming.core.SinkProcessorNodeParticipant;
import org.smartframework.streaming.core.SourceProcessor;
import org.smartframework.streaming.core.SourceProcessorNode;
import org.smartframework.streaming.core.SourceProcessorNodeParticipant;

import java.util.Objects;

/**
 * @author noah
 * @create 2022-10-20 23:26
 */
public class DefaultSourceProcessorNodeBuilder implements SourceProcessorNodeBuilder {

    private String code;
    private String name;
    private int shardingNumber;
    private Class<? extends SourceProcessor> sourceProcessorType;
    private SinkProcessorNodeParticipant sinkProcessorNodeParticipant;
    private SourceProcessorNodeParticipant sourceProcessorNodeParticipant;
    private MappingProcessorNodeParticipant mappingProcessorNodeParticipant;

    public DefaultSourceProcessorNodeBuilder() {
        this.sinkProcessorNodeParticipant = new DefaultSinkProcessorParticipant();
        this.sourceProcessorNodeParticipant = new DefaultSourceProcessorNodeParticipant();
        this.mappingProcessorNodeParticipant = new DefaultMappingProcessorNodeParticipant();
        this.shardingNumber = 1;
    }

    @Override
    public SourceProcessorNodeBuilder code(String code) {
        this.code = code;
        return this;
    }

    @Override
    public SourceProcessorNodeBuilder name(String name) {
        this.name = name;
        return this;
    }

    @Override
    public SourceProcessorNodeBuilder shardingNumber(int num) {
        this.shardingNumber = num;
        return this;
    }


    @Override
    public SourceProcessorNodeBuilder processor(Class<? extends SourceProcessor> sourceProcessorType) {
        this.sourceProcessorType = sourceProcessorType;
        return this;
    }

    @Override
    public <I extends DataElement, O extends DataElement> SourceProcessorNodeBuilder map(
            Class<I> inputType, Class<O> outputType,
            MappingProcessorNodeBuilderFunction<I, O> func) {
        final MappingProcessorNode<I, O> node = func
                .apply(new DefaultMappingProcessorNodeBuilder<>(inputType, outputType));
        mappingProcessorNodeParticipant.add(node);
        return this;
    }


    @Override
    public <T extends DataElement> SourceProcessorNodeBuilder sink(
            Class<T> processElementType, SinkProcessorNodeBuilderFunction<T> func) {

        final SinkProcessorNode<T> node = func
                .apply(new DefaultSinkProcessorNodeBuilder<>(processElementType));
        sinkProcessorNodeParticipant.add(node);
        return this;
    }

    @Override
    public SourceProcessorNodeBuilder child(SourceProcessorNodeBuilderFunction func) {
        final SourceProcessorNode node = func.apply(new DefaultSourceProcessorNodeBuilder());
        sourceProcessorNodeParticipant.add(node);
        return this;
    }


    @Override
    public SourceProcessorNode build() {
        Objects.requireNonNull(this.sourceProcessorType);
        Objects.requireNonNull(this.sinkProcessorNodeParticipant);

        DefaultSourceProcessorNode sourceProcessorNode = new DefaultSourceProcessorNode();
        sourceProcessorNode.setProcessorType(sourceProcessorType);
        sourceProcessorNode.setSinks(sinkProcessorNodeParticipant);
        sourceProcessorNode.setSources(sourceProcessorNodeParticipant);
        sourceProcessorNode.setMappings(mappingProcessorNodeParticipant);
        sourceProcessorNode.setCode(code);
        sourceProcessorNode.setName(name);
        sourceProcessorNode.setShardingNumber(shardingNumber);

        if (code == null) {
            sourceProcessorNode.setCode(sourceProcessorType.getSimpleName());
        }
        if (name == null) {
            sourceProcessorNode.setName(sourceProcessorType.getSimpleName());
        }
        return sourceProcessorNode;
    }
}

