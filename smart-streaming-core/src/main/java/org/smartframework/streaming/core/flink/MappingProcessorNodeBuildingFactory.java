package org.smartframework.streaming.core.flink;


import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.util.OutputTag;
import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.MappingProcessorNode;
import org.smartframework.streaming.core.SinkProcessorNode;
import org.smartframework.streaming.core.SinkProcessorNodeParticipant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author noah
 * @create 2022-09-27 16:00
 */
public class MappingProcessorNodeBuildingFactory implements Serializable {

    public static class MappingProcessorNodeBuildingInfo implements Serializable {

        public MappingProcessorNodeBuildingInfo(OutputTag<DataElement> outputTag, MappingProcessorNode<?, ?> mapping, SinkProcessorNode sink) {
            Objects.requireNonNull(outputTag);
            Objects.requireNonNull(mapping);
            Objects.requireNonNull(sink);
            this.outputTag = outputTag;
            this.mapping = mapping;
            this.sink = sink;
        }

        public OutputTag<DataElement> getOutputTag() {
            return outputTag;
        }

        public MappingProcessorNode<?, ?> getMapping() {
            return mapping;
        }

        public SinkProcessorNode<?> getSink() {
            return sink;
        }

        OutputTag<DataElement> outputTag;
        MappingProcessorNode<?, ?> mapping;
        SinkProcessorNode<?> sink;

    }

    private Map<String, List<MappingProcessorNodeBuildingInfo>> map = new HashMap<>();
    private SinkProcessorNodeParticipant dataProcessorParticipant;

    public MappingProcessorNodeBuildingFactory(SinkProcessorNodeParticipant dataProcessorParticipant) {
        this.dataProcessorParticipant = dataProcessorParticipant;
    }

    public void add(MappingProcessorNode<?, ?> targetNode) {
        map.putIfAbsent(targetNode.inputType().getName(), new ArrayList<>());
        final MappingProcessorNodeBuildingInfo mappingProcessorNodeBuildingInfo = new MappingProcessorNodeBuildingInfo(new OutputTag<>(UUID.randomUUID().toString()
                , TypeInformation.of(DataElement.class)), targetNode
                , dataProcessorParticipant.get(targetNode.outputType()));

        map.get(targetNode.inputType().getName()).add(mappingProcessorNodeBuildingInfo);
    }

    public List<MappingProcessorNodeBuildingInfo> get(Class<?> intputType) {
        return map.get(intputType.getName());
    }

    public List<OutputTag<DataElement>> getOutputTags(Class<?> intputType) {
        final List<MappingProcessorNodeBuildingInfo> outputTags = map.get(intputType.getName());
        if (outputTags == null) {
            return new ArrayList<>();
        }
        return outputTags.stream().map(MappingProcessorNodeBuildingInfo::getOutputTag).collect(Collectors.toList());
    }

    public List<MappingProcessorNodeBuildingInfo> getAll() {
        return map.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    public Set<SinkProcessorNode<?>> allHoldingDataProcessorNode() {
        return map.values().stream().flatMap(Collection::stream).map(
            MappingProcessorNodeBuildingInfo::getSink).collect(Collectors.toSet());
    }

}
