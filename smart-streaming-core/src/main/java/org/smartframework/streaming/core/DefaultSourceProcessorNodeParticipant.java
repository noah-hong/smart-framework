package org.smartframework.streaming.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author noah
 * @create 2022-09-13 11:32
 */
public class DefaultSourceProcessorNodeParticipant implements SourceProcessorNodeParticipant {
    private Map<String, SourceProcessorNode> map = new HashMap<>();


    @Override
    public void add(SourceProcessorNode processor) {
        map.put(processor.getCode(), processor);
    }


    @Override
    public List<SourceProcessorNode> all() {
        return new ArrayList<>(map.values());
    }


}

