package org.smartframework.streaming.core;

/**
 * @author noah
 * @create 2022-10-20 23:27
 */
public  class DefaultSinkProcessorNode<T> implements SinkProcessorNode<T> {

  private String name;
  private String code;
  private  Class<? extends SinkProcessor<T>> processorType;
  private int shardingNumber = 1;
  private Class<T> processElementType;
  private ShardingKeySelector<T> shardingKeySelector = new NoShardingKeySelector<T>() {
  };

  @Override
  public int shardingNumber() {
    return shardingNumber;
  }

  public void setShardingNumber(int shardingNumber) {
    this.shardingNumber = shardingNumber;
  }

  public void setShardingKeySelector(ShardingKeySelector<T> shardingKeySelector) {
    this.shardingKeySelector = shardingKeySelector;
  }

  @Override
  public ShardingKeySelector<?> shardingKeySelector() {
    return shardingKeySelector;
  }

  @Override
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setProcessElementType(Class<T> processElementType) {
    this.processElementType = processElementType;
  }

  @Override
  public Class<T> processElementType() {
    return processElementType;
  }

  @Override
  public  Class<? extends SinkProcessor<T>> processorType() {
    return processorType;
  }

  public void setProcessorType( Class<? extends SinkProcessor<T>> processorType) {
    this.processorType = processorType;
  }
}
