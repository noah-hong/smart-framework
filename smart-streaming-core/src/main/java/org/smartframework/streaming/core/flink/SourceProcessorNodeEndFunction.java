package org.smartframework.streaming.core.flink;


import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.smartframework.streaming.core.Element;
import org.smartframework.streaming.core.EndElement;
import org.smartframework.streaming.core.SourceProcessorNode;
import org.smartframework.streaming.core.SourceProcessorNodeParticipant;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author noah
 * @create 2022-10-11 19:02
 */
public class SourceProcessorNodeEndFunction extends RichSinkFunction<Element> {

    private SourceProcessorNode sourceProcessorNode;

    public SourceProcessorNodeEndFunction(SourceProcessorNode sourceProcessorNode) {
        this.sourceProcessorNode = sourceProcessorNode;
    }
    public static boolean isActivated(String jobId, String code){
        String parentFile = "E:\\data\\" + jobId;
        String fileName = code + ".activated";
        return Files.exists(Paths.get(parentFile, fileName));
    }

    public static void activate(String jobId, String code) throws Exception {
        String parentFile = "E:\\data\\" + jobId;
        String fileName = code + ".activated";
        final Path directory = Files.createDirectory(Paths.get(parentFile));
        Files.createFile(Paths.get(directory.toString(), fileName));
//        File dir = new File(parentFile);
//        File file = new File(dir, fileName);
//        dir.mkdirs();
//        try {
//            file.createNewFile();
//            System.out.println("文件创建成功");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void invoke(Element value, Context context) throws Exception {
        if (value instanceof EndElement) {
            final SourceProcessorNodeParticipant sourceProcessorNodeChildren = sourceProcessorNode.sources();
            final List<SourceProcessorNode> sources = sourceProcessorNodeChildren.all();
            if (sources != null && sources.size() > 0) {
                for (SourceProcessorNode processorNode : sources) {
                    activate(getRuntimeContext().getJobId().toString(), processorNode.getCode());
                }
            }

        }

    }
}
