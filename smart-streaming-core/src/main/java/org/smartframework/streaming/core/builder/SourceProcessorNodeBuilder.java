package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.SourceProcessor;
import org.smartframework.streaming.core.SourceProcessorNode;

import java.io.Serializable;

/**
 * @author noah
 * @create 2022-10-20 22:55
 */
public interface SourceProcessorNodeBuilder extends Serializable {

  SourceProcessorNodeBuilder code(String code);

  SourceProcessorNodeBuilder name(String name);

  SourceProcessorNodeBuilder shardingNumber(int num);

  SourceProcessorNodeBuilder processor(Class<? extends SourceProcessor> sourceProcessorType);

  <I extends DataElement, O extends DataElement> SourceProcessorNodeBuilder map(
      Class<I> inputType, Class<O> outputType, MappingProcessorNodeBuilderFunction<I, O> func);

  <T extends DataElement> SourceProcessorNodeBuilder sink(
      Class<T> processElementType, SinkProcessorNodeBuilderFunction<T> func);

  SourceProcessorNodeBuilder child(SourceProcessorNodeBuilderFunction func);

  SourceProcessorNode build();

}