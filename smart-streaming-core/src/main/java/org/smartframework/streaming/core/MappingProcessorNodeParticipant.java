package org.smartframework.streaming.core;

import java.io.Serializable;
import java.util.List;

/**
 * @author noah
 * @create 2022-09-25 21:14
 */
public interface MappingProcessorNodeParticipant extends Serializable {

    void add(MappingProcessorNode<?, ?> processor);

    List<MappingProcessorNode<?, ?>> getAll();
}
