package org.smartframework.streaming.core;

/**
 * @author noah
 * @create 2022-09-13 10:59
 */
public interface StreamingDefinition {

    String getCode();
    SourceProcessorNodeParticipant getSources();

}
