package org.smartframework.streaming.core.flink;

import org.smartframework.streaming.core.SinkProcessorNode;

/**
 * @author noah
 * @create 2022-10-26 17:20
 */
public interface SinkProcessorNodeExecuteFunctionFactory {

    SinkProcessorNodeExecuteFunction create(SinkProcessorNode sinkProcessorNode);

    class Default implements SinkProcessorNodeExecuteFunctionFactory {

        @Override
        public SinkProcessorNodeExecuteFunction create(SinkProcessorNode sinkProcessorNode) {
            return new SinkProcessorNodeExecuteFunction(sinkProcessorNode);
        }
    }
}
