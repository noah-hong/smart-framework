package org.smartframework.streaming.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author noah
 * @create 2022-09-13 11:32
 */
public class DefaultSinkProcessorParticipant implements SinkProcessorNodeParticipant {

    private Map<Class<?>, SinkProcessorNode<?>> map = new HashMap<>();


    @Override
    public void add(SinkProcessorNode<?> value) {
        map.put(value.processElementType(), value);
    }

    @Override
    public SinkProcessorNode<?> get(Class<?> sinkProcessorNodeType) {
        return map.get(sinkProcessorNodeType);
    }

    @Override
    public List<SinkProcessorNode<?>> all() {
        return new ArrayList<>(map.values());
    }
}
