package org.smartframework.streaming.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author noah
 * @create 2022-09-13 11:32
 */
public class DefaultMappingProcessorNodeParticipant implements MappingProcessorNodeParticipant {

    private Map<String, MappingProcessorNode<?, ?>> map = new HashMap<>();

    @Override
    public void add(MappingProcessorNode<?, ?> processor) {
        map.put(processor.getCode(), processor);
    }

    @Override
    public List<MappingProcessorNode<?, ?>> getAll() {
        return new ArrayList<>(map.values());
    }


}

