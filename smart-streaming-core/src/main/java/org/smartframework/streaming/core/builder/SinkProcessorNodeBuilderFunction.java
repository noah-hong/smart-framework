package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.SinkProcessorNode;

import java.util.function.Function;

/**
 * @author noah
 * @create 2022-10-20 23:13
 */
@FunctionalInterface
public interface SinkProcessorNodeBuilderFunction<T> extends
    Function<SinkProcessorNodeBuilder<T>, SinkProcessorNode<T>> {

}