package org.smartframework.streaming.core.flink;


import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.state.CheckpointListener;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.streaming.core.Element;
import org.smartframework.streaming.core.SourceProcessor;
import org.smartframework.streaming.core.SourceProcessorNode;

import java.text.MessageFormat;

/**
 * @author noah
 * @create 2022-09-27 14:40
 */
public class SourceProcessorNodeBootstrap extends RichParallelSourceFunction<Element>
        implements CheckpointedFunction, CheckpointListener {

    private static Logger logger = LoggerFactory.getLogger(SourceProcessorNodeBootstrap.class);
    private final SourceProcessorNode sourceProcessorNode;

//    private transient ExecutorService executorService;

    private transient SourceProcessor.State state;
    private transient ListState<SourceProcessor.State> states;
    //    private transient volatile boolean isRunning;
    private int nodeLevel;

    public SourceProcessorNodeBootstrap(SourceProcessorNode sourceProcessorNode, int nodeLevel) {
        this.nodeLevel = nodeLevel;
        this.sourceProcessorNode = sourceProcessorNode;
    }

    protected SourceProcessor createProcessor(Class<? extends SourceProcessor> processorType) throws IllegalAccessException, InstantiationException {
        return processorType.newInstance();
    }

    private SourceProcessor sourceProcessor;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        final Class<? extends SourceProcessor> processorType = sourceProcessorNode.processorType();
        sourceProcessor = createProcessor(processorType);
        state = sourceProcessor.initializeState();
//        isRunning = true;

    }

    @Override
    public void snapshotState(FunctionSnapshotContext context) throws Exception {
        states.clear();
        if (state != null) {
            states.add(state);
        }
    }

    @Override
    public void initializeState(FunctionInitializationContext context) throws Exception {
        states = context.getOperatorStateStore()
                .getListState(new ListStateDescriptor<>("source-state", SourceProcessor.State.class));
        if (context.isRestored() && states.get() != null) {
            state = states.get().iterator().next();
        }
    }

    private Boolean checkOpenFire() {
        return SourceProcessorNodeEndFunction
                .isActivated(getRuntimeContext().getJobId().toString(), sourceProcessorNode.getCode());
    }

    @Override
    public void run(SourceContext<Element> ctx) throws Exception {
        final RuntimeContext runtimeContext = getRuntimeContext();
        while (nodeLevel != 1 && !checkOpenFire()) {
            try {
                logger.info(MessageFormat.format("{0}({1})-{2}/{3}，未检测到开始信号，10秒后重试"
                        , sourceProcessorNode.getCode()
                        , sourceProcessorNode.getName()
                        , runtimeContext.getIndexOfThisSubtask() + 1
                        , runtimeContext.getNumberOfParallelSubtasks()));
                Thread.sleep(10 * 1000L);
            } catch (Exception ignored) {

            }
        }
        logger.info(MessageFormat.format("{0}({1})-{2}/{3}，开始获取数据"
                , sourceProcessorNode.getCode()
                , sourceProcessorNode.getName()
                , runtimeContext.getIndexOfThisSubtask() + 1
                , runtimeContext.getNumberOfParallelSubtasks()));

        try {

            sourceProcessor.run(
                    StreamingAdapterContexts.getSourceContext(ctx, runtimeContext, state));
        } catch (Exception ex) {

        }

        logger.info("任务：" + sourceProcessorNode.getCode() + "结束");
    }


    @Override
    public void cancel() {

    }

    @Override
    public void close() throws Exception {

    }

    @Override
    public void notifyCheckpointComplete(long checkpointId) throws Exception {
        sourceProcessor.snapshotStateComplete();
    }
}
