package org.smartframework.streaming.core;


/**
 * @author noah
 * @create 2022-09-13 10:08
 */
public interface SinkProcessor<T> extends Processor {

    void processElement(T element);

}
