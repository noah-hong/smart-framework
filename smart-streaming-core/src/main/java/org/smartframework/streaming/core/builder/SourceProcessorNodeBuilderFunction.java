package org.smartframework.streaming.core.builder;

import org.smartframework.streaming.core.SourceProcessorNode;

import java.util.function.Function;

/**
 * @author noah
 * @create 2022-10-20 23:13
 */
@FunctionalInterface
public interface SourceProcessorNodeBuilderFunction extends
    Function<SourceProcessorNodeBuilder, SourceProcessorNode> {

}