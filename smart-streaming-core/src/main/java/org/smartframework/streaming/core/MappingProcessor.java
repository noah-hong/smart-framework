package org.smartframework.streaming.core;

/**
 * @author noah
 * @create 2022-09-25 17:30
 */
public interface MappingProcessor<IN, OUT> extends Processor {

    OUT map(IN dataElement);

}
