package org.smartframework.streaming.core.flink;

import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.streaming.api.functions.source.SourceFunction.SourceContext;
import org.smartframework.streaming.core.Element;
import org.smartframework.streaming.core.SourceProcessor.Context;
import org.smartframework.streaming.core.SourceProcessor.State;

/**
 * @author noah
 * @create 2022-10-21 23:44
 */
public final class StreamingAdapterContexts {

    private StreamingAdapterContexts() {
    }

    public static Context getSourceContext(SourceContext<Element> sourceContext,
                                           RuntimeContext runtimeContext,
                                           State state) {
        return new NormalContext(sourceContext, runtimeContext, state);
    }


    private static class NormalContext implements Context {

        SourceContext<Element> sourceContext;
        RuntimeContext runtimeContext;
        State state;

        public NormalContext(SourceContext<Element> sourceContext, RuntimeContext runtimeContext,
                             State state) {
            this.sourceContext = sourceContext;
            this.runtimeContext = runtimeContext;
            this.state = state;
        }

        @Override
        public void collect(Element element) {
            sourceContext.collect(element);
        }

        @Override
        public int getTaskNumber() {
            return runtimeContext.getNumberOfParallelSubtasks();
        }

        @Override
        public int getTaskIndex() {
            return runtimeContext.getIndexOfThisSubtask();
        }

        @Override
        public <T extends State> T getState() {
            return (T) state;
        }

        @Override
        public void updateState(State state) {
            this.state = state;
        }
    }
}
