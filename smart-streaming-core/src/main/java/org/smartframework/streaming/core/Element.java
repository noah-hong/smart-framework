package org.smartframework.streaming.core;

import java.io.Serializable;

/**
 * @author noah
 * @create 2022-10-21 23:19
 */
public  interface Element extends Serializable {

}