package org.smartframework.streaming.core;

/**
 * @author noah
 * @create 2022-10-20 23:29
 */
public class DefaultMappingProcessorNode<I extends DataElement, O extends DataElement> implements MappingProcessorNode<I, O> {

  private String name;
  private String code;
  private Class<I> inputType;
  private Class<O> outputType;
  private Class<? extends MappingProcessor<I, O>> mappingProcessorType;
  private int shardingNumber = 1;
  private ShardingKeySelector<?> shardingKeySelector;

  public void setShardingKeySelector(ShardingKeySelector<?> shardingKeySelector) {
    this.shardingKeySelector = shardingKeySelector;
  }

  @Override
  public int shardingNumber() {
    return shardingNumber;
  }

  public void setShardingNumber(int shardingNumber) {
    this.shardingNumber = shardingNumber;
  }

  @Override
  public ShardingKeySelector<?> shardingKeySelector() {
    return this.shardingKeySelector;
  }


  public void setName(String name) {
    this.name = name;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setMappingProcessorType(Class<? extends MappingProcessor<I, O>> mappingProcessorType) {
    this.mappingProcessorType = mappingProcessorType;
  }

  public void setInputType(Class<I> inputType) {
    this.inputType = inputType;
  }

  public void setOutputType(Class<O> outputType) {
    this.outputType = outputType;
  }

  @Override
  public Class<I> inputType() {
    return inputType;
  }

  @Override
  public Class<O> outputType() {
    return outputType;
  }

  @Override
  public Class<? extends MappingProcessor<I, O>> processorType() {
    return mappingProcessorType;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getCode() {
    return code;
  }
}
