package org.smartframework.streaming.core;

/**
 * @author noah
 * @create 2022-09-25 18:00
 */
public interface MappingProcessorNode<I, O> extends ShardingNode {

    Class<I> inputType();

    Class<O> outputType();

    Class<? extends MappingProcessor<I, O>> processorType();
}
