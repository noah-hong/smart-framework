package org.smartframework.streaming.core;

import org.smartframework.streaming.core.builder.ProcessorNodeDefinitionBuilder;

import java.io.Serializable;

/**
 * @author noah
 * @create 2022-09-21 15:22
 */
public interface ProcessorNodeDsl extends Serializable {

    default ProcessorNodeDefinitionBuilder builder() {
        return DefaultProcessorNodeDefinition.builder();
    }

    StreamingDefinition getStreamingDefinition();
}
