package org.smartframework.streaming.core.flink;

import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.smartframework.streaming.core.DataElement;
import org.smartframework.streaming.core.Element;

import java.util.List;

/**
 * @author noah
 * @create 2022-09-15 16:20
 */
public class ProcessorNodeDistributeFunction
        extends ProcessFunction<Element, Element>
        implements CheckpointedFunction {

    MappingProcessorNodeBuildingFactory mappingProcessorNodeOutputFactory;
    UndistributedSinkProcessorNodeBuildingFactory undistributedDataProcessorNodeBuildingFactory;

    public ProcessorNodeDistributeFunction(MappingProcessorNodeBuildingFactory mappingProcessorNodeOutputFactory, UndistributedSinkProcessorNodeBuildingFactory undistributedDataProcessorNodeBuildingFactory) {
        this.mappingProcessorNodeOutputFactory = mappingProcessorNodeOutputFactory;
        this.undistributedDataProcessorNodeBuildingFactory = undistributedDataProcessorNodeBuildingFactory;

    }

    @Override
    public void processElement(Element value, Context context, Collector<Element> out) throws Exception {

//        Map<String, String> map = new HashMap<>();
//        map.put("test22-key", "test22-value");
//        ParameterTool parameterTool = ParameterTool.fromMap(map);
//
//        getRuntimeContext().getExecutionConfig().setGlobalJobParameters(parameterTool);

        if (value instanceof DataElement) {
            final List<OutputTag<DataElement>> outputTags = mappingProcessorNodeOutputFactory.getOutputTags(value.getClass());
            if (outputTags != null && outputTags.size() > 0) {
                for (OutputTag<DataElement> outputTag : outputTags) {
                    context.output(outputTag, (DataElement) value);
                }
            } else {
                final OutputTag<DataElement> outputTag = undistributedDataProcessorNodeBuildingFactory.getOutputTag(value.getClass());
                if (outputTag != null) {
                    context.output(outputTag, (DataElement) value);
                } else {
                    System.out.println("数据未有任何处理");
                }
            }

        } else {
            out.collect(value);
        }
    }

    @Override
    public void snapshotState(FunctionSnapshotContext context) throws Exception {

    }

    @Override
    public void initializeState(FunctionInitializationContext context) throws Exception {

    }
}
