package org.smartframework.streaming.core;

import java.io.Serializable;
import java.util.List;

/**
 * @author noah
 * @create 2022-09-14 13:01
 */
public interface SinkProcessorNodeParticipant extends Serializable {

    void add(SinkProcessorNode<?> value);

    SinkProcessorNode<?> get(Class<?> sinkProcessorNodeType);

    List<SinkProcessorNode<?>> all();
}
