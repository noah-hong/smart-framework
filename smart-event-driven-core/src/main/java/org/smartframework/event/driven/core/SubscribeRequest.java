package org.smartframework.event.driven.core;

import org.smartframework.event.driven.core.context.IMessageContext;

/**
 * @author noah
 * @create 2021-05-25 15:48
 */
public class SubscribeRequest implements ISubscribeRequest {

  public void setMessageContext(
      IMessageContext messageContext) {
    this.messageContext = messageContext;
  }

  public void setHandler(Object handler) {
    this.handler = handler;
  }

  private IMessageContext messageContext;
  private Object handler;

  @Override
  public IMessageContext getMessageContext() {
    return messageContext;
  }

  @Override
  public Object getHandler() {
    return handler;
  }
}
