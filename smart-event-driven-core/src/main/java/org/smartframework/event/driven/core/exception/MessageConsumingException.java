package org.smartframework.event.driven.core.exception;

/**
 * @author noah
 * @create 2020-05-04 13:46
 */
public class MessageConsumingException extends RuntimeException {

  public MessageConsumingException() {
    this("消息消费中");
  }

  public MessageConsumingException(String message) {
    super(message);
  }
}
