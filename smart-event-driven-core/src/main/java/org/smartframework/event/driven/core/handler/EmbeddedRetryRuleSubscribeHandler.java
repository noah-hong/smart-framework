package org.smartframework.event.driven.core.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.ISubscribeHandler;
import org.smartframework.event.driven.core.ISubscribeHandlerChain;
import org.smartframework.event.driven.core.ISubscribeRequest;
import org.smartframework.event.driven.core.ISubscribeResponse;
import org.smartframework.event.driven.core.annotation.RetryRule;
import org.smartframework.event.driven.core.annotation.RetryType;
import org.smartframework.event.driven.core.context.ExecutionStatus;
import org.smartframework.event.driven.core.context.ISubscribeResult;
import org.smartframework.event.driven.core.context.MessageInfo;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author noah
 * @create 2021-05-25 11:22
 */
public class EmbeddedRetryRuleSubscribeHandler implements ISubscribeHandler {


  private IRetryRuleResolver retryRuleResolver;

  public EmbeddedRetryRuleSubscribeHandler(IRetryRuleResolver retryRuleResolver) {
    this.retryRuleResolver = retryRuleResolver;
  }

  public EmbeddedRetryRuleSubscribeHandler() {
    retryRuleResolver = new DefaultRetryRuleResolver();
  }

  private final static Logger log = LoggerFactory
          .getLogger(EmbeddedRetryRuleSubscribeHandler.class);

  private static Map<String, RetryRecord> RETRY_RECORDS = new ConcurrentHashMap<>();

//  public static void main(String[] args) {
//    RetryRecord retryRecord = new RetryRecord(1, 1, RetryType.CUSTOM);
//    retryRecord.addRetryCount();
//    System.out.println(retryRecord);
//    retryRecord.addRetryCount();
//    System.out.println(retryRecord);
//    retryRecord.addRetryCount();
//    System.out.println(retryRecord);
//    retryRecord.addRetryCount();
//    System.out.println(retryRecord);
//    retryRecord.initRetryCount();
//    System.out.println(retryRecord);
//  }

  public static class RetryRecord {
    @Override
    public String toString() {
      return "RetryRecord{" +
              "retryCount=" + retryCount +
              ", retryInterval=" + retryInterval +
              ", currRetryCount=" + currRetryCount +
              ", currRetryInterval=" + currRetryInterval +
              ", retryType=" + retryType +
              ", canRetry=" + canRetry() +
              '}';
    }

    public RetryRecord(int retryCount, int retryInterval, RetryType retryType) {
      this.retryCount = retryCount;
      this.retryInterval = retryInterval;
      this.retryType = retryType;
    }

    public boolean canRetry() {
      return (retryType == RetryType.ALWAYS
              || (retryType == RetryType.CUSTOM && retryCount >= currRetryCount))
              && currRetryInterval % retryInterval == 0;
    }

    public void addRetryCount() {
      currRetryInterval += 1;
      if (currRetryInterval % retryInterval == 0) {
        currRetryCount += 1;
      }
    }

    public void initRetryCount() {
      currRetryCount = 0;
      currRetryInterval = 0;
    }

    private int retryCount;
    private int retryInterval;
    private int currRetryCount = 0;
    private int currRetryInterval = 0;
    private RetryType retryType;

  }


  @Override
  public void handle(ISubscribeRequest request, ISubscribeResponse response,
                     ISubscribeHandlerChain chain) {
    final RetryRule retryRule = request.getHandler().getClass()
            .getAnnotation(RetryRule.class);
    if (Objects.isNull(retryRule)) {
      chain.handle(request, response);
      return;
    }
    final RetryRuleInfo retryRuleInfo = retryRuleResolver.resolve(retryRule);
    final RetryType retryType = retryRuleInfo.getRetryType();
    final int retryInterval = retryRuleInfo.getRetryInterval();
    final int retryCount = retryRuleInfo.getRetryCount();
    final MessageInfo messageInfo = request.getMessageContext().getMessageInfo();
    final String consumerGroup = messageInfo.getConsumerGroup();
    final String topic = messageInfo.getTopic();
    final String subTopic = messageInfo.getSubTopic();
    String retryKey = String.format("%s-%s-%s", consumerGroup, topic, subTopic);
    RetryRecord retryRecord = RETRY_RECORDS.putIfAbsent(retryKey, new RetryRecord(retryCount, retryInterval, retryType));
    if (retryRecord == null || retryRecord.canRetry()) {
      retryRecord = RETRY_RECORDS.get(retryKey);
      retryRecord.addRetryCount();

      chain.handle(request, response);
      final ISubscribeResult subscribeResult = response.getSubscribeResult();
      if (subscribeResult.getExecutionStatus() == ExecutionStatus.SUCCESS
              || subscribeResult.getExecutionStatus() == ExecutionStatus.FALIURE_NOT_RECONSUME) {
        retryRecord.initRetryCount();
      }
    } else {
      log.warn("消息重试已达最大上限, 请检查程序后再试, consumer group：{}, topic：{}, tag：{}, key：{}"
              , consumerGroup, topic, subTopic, messageInfo.getMessageKey());
    }

  }
}
