package org.smartframework.event.driven.core.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.ISubscribeHandlerChain;
import org.smartframework.event.driven.core.ISubscribeRequest;
import org.smartframework.event.driven.core.ISubscribeResponse;
import org.smartframework.event.driven.core.SubscribeResult;
import org.smartframework.event.driven.core.context.IMessageContext;
import org.smartframework.event.driven.core.context.ISubscribeResult;

import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author noah
 * @create 2021-05-25 13:21
 */
public class EmbeddedOriginalSubscribeHandlerChain implements ISubscribeHandlerChain {

//  public EmbeddedOriginalSubscribeHandlerChain()


  private static final Logger log = LoggerFactory
      .getLogger(EmbeddedOriginalSubscribeHandlerChain.class);
  private static final Map<String, Optional<Method>> subscribeMethods = new ConcurrentHashMap<>();
  private final static String SUBSCRIBE_METHOD_NAME = "subscribe";

  public Optional<Method> cacheSubscribeMethodFor(Object handler,
      ISubscribeRequest subscribeRequest) {
    final IEvent<?> event = subscribeRequest.getMessageContext().getEventEnvelop().getEvent();
    final Class<?> handlerType = handler.getClass();

    Class<?> eventType = event.getClass();
    String key = handlerType.getName() + ":" + eventType.getName();

     if (subscribeMethods.containsKey(key)) {
      return subscribeMethods.get(key);
    }
    synchronized (subscribeMethods) {
      if (subscribeMethods.containsKey(key)) {
        return subscribeMethods.get(key);
      }
      final Optional<Method> subscribeMethod =
          Arrays.stream(handlerType.getDeclaredMethods())
              .filter(m -> m.getName().equals(SUBSCRIBE_METHOD_NAME))
              .filter(
                  m ->
                      m.getParameterCount() == 2
                          && m.getParameterTypes()[0].isAssignableFrom(event.getClass())
                          && m.getParameterTypes()[1].isAssignableFrom(IMessageContext.class))
              .findFirst();

      if (subscribeMethod.isPresent()) {
        Method method = subscribeMethod.get();
        method.setAccessible(true);
        subscribeMethods.put(key, Optional.of(method));
      } else {
        subscribeMethods.put(key, Optional.empty());
      }
      return subscribeMethods.get(key);
    }


  }

  @Override
  public void handle(ISubscribeRequest request, ISubscribeResponse response) {
    final Optional<Method> subscribeMethod = cacheSubscribeMethodFor(request.getHandler(), request);
    ISubscribeResult result;
    try {
      if (subscribeMethod.isPresent()) {
        result = (ISubscribeResult) subscribeMethod.get()
            .invoke(request.getHandler(), request.getMessageContext().getEventEnvelop().getEvent(),
                request.getMessageContext());
      } else {
        result = SubscribeResult.FALIURE_NOT_RECONSUME;
      }
    } catch (Exception ex) {
//      return response.setSubscribeResult(SubscribeResult.FALIURE(ex));
      throw new UndeclaredThrowableException(ex);
    }
    response.setSubscribeResult(result);
  }
}
