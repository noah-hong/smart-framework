package org.smartframework.event.driven.core.exception;

/**
 * @author noah
 * @create 2021-01-08 15:50
 */
public class MessageConsumeFailedException extends RuntimeException {

    public MessageConsumeFailedException() {
        this("消息消费失败");
    }

    public MessageConsumeFailedException(String message) {
        super(message);
    }

    public MessageConsumeFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public MessageConsumeFailedException(Throwable cause) {
        super(cause);
    }

    protected MessageConsumeFailedException(
            String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
