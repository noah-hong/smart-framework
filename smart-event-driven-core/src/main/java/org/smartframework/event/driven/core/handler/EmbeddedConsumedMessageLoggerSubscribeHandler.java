package org.smartframework.event.driven.core.handler;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.core.LogDefine;
import org.smartframework.event.driven.core.ISubscribeHandler;
import org.smartframework.event.driven.core.ISubscribeHandlerChain;
import org.smartframework.event.driven.core.ISubscribeRequest;
import org.smartframework.event.driven.core.ISubscribeResponse;
import org.smartframework.event.driven.core.annotation.ConsumedLogger;
import org.smartframework.event.driven.core.context.ExecutionStatus;
import org.smartframework.event.driven.core.context.MessageInfo;
import org.smartframework.event.driven.core.store.IConsumedMessageLogger;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author noah
 * @create 2021-05-25 16:29
 */
public class EmbeddedConsumedMessageLoggerSubscribeHandler implements ISubscribeHandler {

  private final static Logger log = LoggerFactory
      .getLogger(EmbeddedConsumedMessageLoggerSubscribeHandler.class);
  private final IConsumedMessageLogger logger;
  //  private final  static
  private final static ThreadPoolExecutor THREAD_POLL =
      new ThreadPoolExecutor(
          1,
          2,
          1000 * 6,
          TimeUnit.MILLISECONDS,
          new LinkedBlockingQueue<>(100),
          new ThreadPoolExecutor.DiscardPolicy());

  public EmbeddedConsumedMessageLoggerSubscribeHandler(
      IConsumedMessageLogger logger) {
    this.logger = logger;
  }


  @Override
  public void handle(ISubscribeRequest request, ISubscribeResponse response,
      ISubscribeHandlerChain chain) {
    final ConsumedLogger annotation = request.getHandler().getClass()
        .getAnnotation(ConsumedLogger.class);
    if (Objects.isNull(annotation)) {
      chain.handle(request, response);
      return;
    }
    try {
      chain.handle(request, response);
      logRecord(request, response, null);
    } catch (Exception ex) {
      logRecord(request, response, ex);
      throw ex;
    }
  }

  ConcurrentHashMap<String, AtomicInteger> repeatErrorCount = new ConcurrentHashMap<>();

  private void logRecord(ISubscribeRequest request, ISubscribeResponse response,
      Throwable throwable) {
    if (response.getSubscribeResult() != null
        && response.getSubscribeResult().getExecutionStatus() == ExecutionStatus.SUCCESS) {
      //优化为自定义线程池
      THREAD_POLL
          .execute(() -> logger
              .log(request.getMessageContext(), response.getSubscribeResult(), throwable));
      return;
    }
    final MessageInfo messageInfo = request.getMessageContext().getMessageInfo();
    String message = "";
    if (response.getSubscribeResult() != null
        && response.getSubscribeResult().getException() != null) {
      message = response.getSubscribeResult().getException().getMessage();
    } else {
      message = Optional.ofNullable(throwable).map(x -> ExceptionUtils.getRootCause(x).getMessage())
          .orElse("");
    }
    String key = String
        .format("%s-%s-%s", messageInfo.getConsumerGroup().hashCode(),
            messageInfo.getMessageKey().hashCode(), message.hashCode());
    repeatErrorCount.putIfAbsent(key,
        new AtomicInteger(0));
    final int i = repeatErrorCount.get(key).incrementAndGet();
    if (i <= 2) {
      THREAD_POLL
          .execute(() -> logger
              .log(request.getMessageContext(), response.getSubscribeResult(), throwable));
    } else {
      log.error("{}存在多次重复消费错误，请检查，consumer group：{}，topic：{}，tags:{}，key:{}，message：{}"
          , LogDefine.ALARM
          , messageInfo.getConsumerGroup()
          , messageInfo.getTopic()
          , messageInfo.getSubTopic()
          , messageInfo.getMessageKey()
          , message);
    }
  }
}
