package org.smartframework.event.driven.core.exception;

/**
 * @author noah
 * @create 2020-04-23 17:40
 */
public class SendMessageException extends RuntimeException {

  private static final long serialVersionUID = -562780754069965215L;

  public SendMessageException() {
    this("发送消息失败");
  }

  public SendMessageException(String msg) {
    super(msg);
  }

  public SendMessageException(String msg, Throwable cause) {
    super(msg, cause);
  }
}
