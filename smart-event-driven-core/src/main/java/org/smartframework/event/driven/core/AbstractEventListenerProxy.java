package org.smartframework.event.driven.core;

import org.smartframework.event.driven.core.context.IMessageContext;
import org.smartframework.event.driven.core.context.ISubscribeResult;
import org.smartframework.event.driven.core.handler.EmbeddedMainSubscribeHandler;
import org.smartframework.event.driven.core.handler.EmbeddedOriginalSubscribeHandlerChain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author noah
 * @create 2021-05-25 11:04
 */
public class AbstractEventListenerProxy implements IEventListenerProxy {

  public List<ISubscribeHandler> getAdditionalHandlers() {
    return additionalHandlers;
  }

  private List<ISubscribeHandler> additionalHandlers = new ArrayList<>();

  private Object proxyTarget;
  public ISubscribeHandler getMainHandler() {
    return mainHandler;
  }

  public void setMainHandler(ISubscribeHandler mainHandler) {
    this.mainHandler = mainHandler;
  }

  public ISubscribeHandlerChain getOriginalChain() {
    return originalChain;
  }

  public void setOriginalChain(ISubscribeHandlerChain originalChain) {
    this.originalChain = originalChain;
  }

  private ISubscribeHandler mainHandler;
  private ISubscribeHandlerChain originalChain;


  public AbstractEventListenerProxy(Object proxyTarget) {
    this.proxyTarget = proxyTarget;
    final EmbeddedMainSubscribeHandler embeddedSubscribeHandler = new EmbeddedMainSubscribeHandler();
    embeddedSubscribeHandler.setAdditionalHandlers(additionalHandlers);
    mainHandler = embeddedSubscribeHandler;
    originalChain = new EmbeddedOriginalSubscribeHandlerChain();
  }


  @Override
  public ISubscribeResult subscribe(IMessageContext messageContext) {

    SubscribeRequest subscribeRequest = new SubscribeRequest();
    subscribeRequest.setHandler(proxyTarget);
    subscribeRequest.setMessageContext(messageContext);
    SubscribeResponse subscribeResponse = new SubscribeResponse();
    subscribeResponse.setSubscribeResult(SubscribeResult.UNKNOWN);
    mainHandler.handle(subscribeRequest, subscribeResponse, originalChain);
    return subscribeResponse.getSubscribeResult();
  }

  @Override
  public Class<?> getProxyTargetType() {
    return this.proxyTarget.getClass();
  }


}
