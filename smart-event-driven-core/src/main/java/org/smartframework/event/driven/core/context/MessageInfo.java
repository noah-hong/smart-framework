package org.smartframework.event.driven.core.context;

import java.time.LocalDateTime;
import java.util.Properties;

/**
 * @author noah
 * @create 2020-03-13 11:30
 */
public class MessageInfo {

  private String messageId;
  private String messageKey;
  private String topic;

  public String getSubTopic() {
    return subTopic;
  }

  public void setSubTopic(String subTopic) {
    this.subTopic = subTopic;
  }

  private String subTopic;

  public String getBroker() {
    return broker;
  }

  public void setBroker(String broker) {
    this.broker = broker;
  }

  private String broker;
  private Integer queueId;
  private String consumerGroup;
  private Integer reconsumeTimes;
  private LocalDateTime storeTime;
  private Properties properties;
  private Long messageOffset;

  public Long getBornTimestamp() {
    return bornTimestamp;
  }

  public void setBornTimestamp(Long bornTimestamp) {
    this.bornTimestamp = bornTimestamp;
  }

  private Long bornTimestamp;

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }

  public String getMessageKey() {
    return messageKey;
  }

  public void setMessageKey(String messageKey) {
    this.messageKey = messageKey;
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public Integer getQueueId() {
    return queueId;
  }

  public void setQueueId(Integer queueId) {
    this.queueId = queueId;
  }

  public String getConsumerGroup() {
    return consumerGroup;
  }

  public void setConsumerGroup(String consumerGroup) {
    this.consumerGroup = consumerGroup;
  }

  public Integer getReconsumeTimes() {
    return reconsumeTimes;
  }

  public void setReconsumeTimes(Integer reconsumeTimes) {
    this.reconsumeTimes = reconsumeTimes;
  }

  public LocalDateTime getStoreTime() {
    return storeTime;
  }

  public void setStoreTime(LocalDateTime storeTime) {
    this.storeTime = storeTime;
  }

  public Properties getProperties() {
    return properties;
  }

  public void setProperties(Properties properties) {
    this.properties = properties;
  }

  public Long getMessageOffset() {
    return messageOffset;
  }

  public void setMessageOffset(Long messageOffset) {
    this.messageOffset = messageOffset;
  }


}
