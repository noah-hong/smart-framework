package org.smartframework.event.driven.core.handler;

import org.smartframework.event.driven.core.annotation.RetryType;

/**
 * @author noah
 * @create 2021-10-22 23:16
 */
public class RetryRuleInfo {
    private int retryCount;
    private RetryType retryType;
    private int retryInterval;

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public RetryType getRetryType() {
        return retryType;
    }

    public void setRetryType(RetryType retryType) {
        this.retryType = retryType;
    }

    public int getRetryInterval() {
        return retryInterval;
    }

    public void setRetryInterval(int retryInterval) {
        this.retryInterval = retryInterval;
    }
}
