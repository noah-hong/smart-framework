package org.smartframework.event.driven.core;

import org.smartframework.event.driven.core.context.ExecutionStatus;
import org.smartframework.event.driven.core.context.ISubscribeResult;

/**
 * @author noah
 * @create 2021-01-28 15:33
 */
public class SubscribeResult implements ISubscribeResult {

  @Override
  public ExecutionStatus getExecutionStatus() {
    return executionStatus;
  }

  @Override
  public Throwable getException() {
    return exception;
  }

  public void setExecutionStatus(
      ExecutionStatus executionStatus) {
    this.executionStatus = executionStatus;
  }

  public void setException(Throwable exception) {
    this.exception = exception;
  }

  private ExecutionStatus executionStatus;
  private Throwable exception;

  public SubscribeResult() {
    this.executionStatus = ExecutionStatus.SUCCESS;
  }

  private SubscribeResult(ExecutionStatus status, Throwable exception) {
    this.executionStatus = status;
    this.exception = exception;
  }

  public final static SubscribeResult SUCCESS = new SubscribeResult();

  public final static SubscribeResult UNKNOWN = new SubscribeResult(
      ExecutionStatus.UNKNOWN, null);

  public final static SubscribeResult FALIURE_NOT_RECONSUME = new SubscribeResult(
      ExecutionStatus.FALIURE_NOT_RECONSUME, null);

  public static SubscribeResult FALIURE_NOT_RECONSUME(Throwable ex) {
    return new SubscribeResult(
        ExecutionStatus.FALIURE_NOT_RECONSUME, ex);
  }

  public static SubscribeResult FALIURE(Throwable ex) {
    return new SubscribeResult(
        ExecutionStatus.FALIURE, ex);
  }


}
