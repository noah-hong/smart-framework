package org.smartframework.event.driven.core.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.ConsumeStatusEnum;
import org.smartframework.event.driven.core.ISubscribeHandler;
import org.smartframework.event.driven.core.ISubscribeHandlerChain;
import org.smartframework.event.driven.core.ISubscribeRequest;
import org.smartframework.event.driven.core.ISubscribeResponse;
import org.smartframework.event.driven.core.SubscribeResult;
import org.smartframework.event.driven.core.annotation.ExactlyOnce;
import org.smartframework.event.driven.core.context.ExecutionStatus;
import org.smartframework.event.driven.core.exception.MessageConsumedException;
import org.smartframework.event.driven.core.exception.MessageConsumingException;
import org.smartframework.event.driven.core.store.IExcatlyOnceSupporter;
import org.smartframework.event.driven.core.store.ITransactionExecutor;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.Objects;

/**
 * @author noah
 * @create 2021-05-25 11:22
 */
public class EmbeddedExcatlyOnceSubscribeHandler implements ISubscribeHandler {

  private final static Logger log = LoggerFactory
      .getLogger(EmbeddedExcatlyOnceSubscribeHandler.class);
  private final IExcatlyOnceSupporter excatlyOnceSupporter;
  private final ITransactionExecutor transactionExecutor;

  public EmbeddedExcatlyOnceSubscribeHandler(IExcatlyOnceSupporter excatlyOnceSupporter,
      ITransactionExecutor transactionExecutor) {
    this.excatlyOnceSupporter = excatlyOnceSupporter;
    this.transactionExecutor = transactionExecutor;
  }

  @Override
  public void handle(ISubscribeRequest request, ISubscribeResponse response,
      ISubscribeHandlerChain chain) {
    final ExactlyOnce annotation = request.getHandler().getClass()
        .getAnnotation(ExactlyOnce.class);
    if (Objects.isNull(annotation)) {
      chain.handle(request, response);
      return;
    }

    transactionExecutor.execute(() -> {
      final ConsumeStatusEnum consumeStatusEnum = excatlyOnceSupporter
          .support(request.getMessageContext());
      switch (consumeStatusEnum) {
        case CONSUMING:
          MessageConsumingException messageConsumingException = new MessageConsumingException();
          response.setSubscribeResult(SubscribeResult.FALIURE(messageConsumingException));
          throw messageConsumingException;
        case CONSUMED:
          final MessageConsumedException messageConsumedException = new MessageConsumedException();
          response.setSubscribeResult(
              SubscribeResult.FALIURE_NOT_RECONSUME(messageConsumedException));
          throw messageConsumedException;
        default:
          chain.handle(request, response);
          if (response.getSubscribeResult().getExecutionStatus()
              .equals(ExecutionStatus.FALIURE)) {
            final Throwable exception = response.getSubscribeResult().getException();
            throw new UndeclaredThrowableException(exception);
          }
          break;
      }
    });


  }
}
