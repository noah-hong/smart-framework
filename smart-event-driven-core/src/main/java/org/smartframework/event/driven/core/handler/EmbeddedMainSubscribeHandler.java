package org.smartframework.event.driven.core.handler;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.ISubscribeHandler;
import org.smartframework.event.driven.core.ISubscribeHandlerChain;
import org.smartframework.event.driven.core.ISubscribeRequest;
import org.smartframework.event.driven.core.ISubscribeResponse;
import org.smartframework.event.driven.core.SubscribeResult;
import org.smartframework.event.driven.core.context.ExecutionStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * @author noah
 * @create 2021-05-25 13:11
 */
public class EmbeddedMainSubscribeHandler implements ISubscribeHandler {

  private final static Logger log = LoggerFactory.getLogger(EmbeddedMainSubscribeHandler.class);

  public void setAdditionalHandlers(
      List<? extends ISubscribeHandler> additionalHandlers) {
    this.additionalHandlers = additionalHandlers;
  }

  private List<? extends ISubscribeHandler> additionalHandlers = new ArrayList<>();


  @Override
  public void handle(ISubscribeRequest request, ISubscribeResponse response,
      ISubscribeHandlerChain chain) {
    try {
      new VirtualSubscribeHandlerChain(additionalHandlers, chain).handle(request, response);
    } catch (Exception ex) {
      Throwable rootEx = ExceptionUtils.getRootCause(ex);
      log.error(rootEx.getMessage(), rootEx);
      if (response.getSubscribeResult() == null
          || response.getSubscribeResult().getExecutionStatus() == ExecutionStatus.UNKNOWN) {
        response.setSubscribeResult(SubscribeResult.FALIURE(rootEx));
      }
    }
  }

  private static class VirtualSubscribeHandlerChain implements ISubscribeHandlerChain {

    private final List<? extends ISubscribeHandler> additionalHandlers;
    private final ISubscribeHandlerChain originalChain;
    private int currentPosition = 0;

    private VirtualSubscribeHandlerChain(
        List<? extends ISubscribeHandler> additionalHandlers,
        ISubscribeHandlerChain originalChain) {
      this.additionalHandlers = additionalHandlers;
      this.originalChain = originalChain;
    }

    @Override
    public void handle(ISubscribeRequest request, ISubscribeResponse response) {

      if (this.currentPosition == this.additionalHandlers.size()) {
        this.originalChain.handle(request, response);
      } else {
        this.currentPosition++;
        ISubscribeHandler nextHandler = this.additionalHandlers.get(this.currentPosition - 1);
        nextHandler.handle(request, response, this);
      }

    }

  }
}
