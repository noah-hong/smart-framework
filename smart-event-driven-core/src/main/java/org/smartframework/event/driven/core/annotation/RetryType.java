package org.smartframework.event.driven.core.annotation;

/**
 * @author noah
 * @create 2021-10-22 16:21
 */
public enum RetryType {
    ALWAYS,
    CUSTOM
}
