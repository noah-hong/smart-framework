package org.smartframework.event.driven.core.handler;

import org.smartframework.event.driven.core.annotation.RetryRule;

/**
 * @author noah
 * @create 2021-10-23 14:28
 */
public class DefaultRetryRuleResolver implements IRetryRuleResolver {

    @Override
    public RetryRuleInfo resolve(RetryRule retryRule) {
        RetryRuleInfo retryRuleInfo = new RetryRuleInfo();
        retryRuleInfo.setRetryCount(retryRule.retryCount());
        retryRuleInfo.setRetryInterval(retryRule.retryInterval());
        retryRuleInfo.setRetryType(retryRule.retryType());
        return retryRuleInfo;
    }
}
