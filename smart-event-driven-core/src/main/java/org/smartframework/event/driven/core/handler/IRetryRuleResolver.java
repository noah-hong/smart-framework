package org.smartframework.event.driven.core.handler;

import org.smartframework.event.driven.core.annotation.RetryRule;

/**
 * @author noah
 * @create 2021-10-22 23:14
 */
public interface IRetryRuleResolver {

    RetryRuleInfo resolve(RetryRule retryRule);
}
