package org.smartframework.event.driven.core.exception;

/**
 * @author noah
 * @create 2020-03-13 23:37
 */
public class MessageConsumedException extends RuntimeException {

  static final long serialVersionUID = -7034897190745766939L;

  public MessageConsumedException() {
    this("消息已消费");
  }

  public MessageConsumedException(String message) {
    super(message);
  }

  public MessageConsumedException(String message, Throwable cause) {
    super(message, cause);
  }

  public MessageConsumedException(Throwable cause) {
    super(cause);
  }

  protected MessageConsumedException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
