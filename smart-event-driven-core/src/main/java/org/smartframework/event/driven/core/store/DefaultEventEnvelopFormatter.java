package org.smartframework.event.driven.core.store;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.EventEnvelope;

import javax.xml.datatype.XMLGregorianCalendar;
/**
 * @author noah
 * @create 2021-01-21 13:41
 */
public class DefaultEventEnvelopFormatter implements IEventEnvelopeFormatter {

  private static ObjectMapper mapper;

  @JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
  @JsonAutoDetect(fieldVisibility = Visibility.ANY,
      getterVisibility = Visibility.PUBLIC_ONLY,
      setterVisibility = Visibility.NONE,
      isGetterVisibility = Visibility.NONE)
  public static class ThrowableMixIn {

  }

  static {
    mapper = new ObjectMapper();
    mapper.setSerializationInclusion(Include.NON_NULL);
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    mapper.registerModule(new JavaTimeModule());
    mapper.setVisibility(mapper.getSerializationConfig()
        .getDefaultVisibilityChecker()
        .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
        .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
        .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
        .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
    mapper.enable(Feature.WRITE_BIGDECIMAL_AS_PLAIN);
    mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    mapper.disable(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE);
    mapper.disable(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES);
    mapper.disable(DeserializationFeature.FAIL_ON_MISSING_EXTERNAL_TYPE_ID_PROPERTY);
    mapper.disable(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS);
    mapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL);
    mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    mapper.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);

    mapper.addMixIn(Throwable.class, ThrowableMixIn.class);
    TypeResolverBuilder<?> mapTyper = new DefaultTypeResolverBuilder(DefaultTyping.NON_FINAL) {
      @Override
      public boolean useForType(JavaType t) {
        switch (_appliesFor) {
          case NON_CONCRETE_AND_ARRAYS:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
            // fall through
          case OBJECT_AND_NON_CONCRETE:
            return (t.getRawClass() == Object.class) || !t.isConcrete();
          case NON_FINAL:
            while (t.isArrayType()) {
              t = t.getContentType();
            }
            // to fix problem with wrong long to int conversion
            if (t.getRawClass() == Long.class) {
              return true;
            }
            if (t.getRawClass() == XMLGregorianCalendar.class) {
              return false;
            }
            return !t.isFinal(); // includes Object.class
          default:
            // case JAVA_LANG_OBJECT:
            return t.getRawClass() == Object.class;
        }
      }
    };
    mapTyper.init(JsonTypeInfo.Id.CLASS, null);
    mapTyper.inclusion(JsonTypeInfo.As.PROPERTY);
    mapper.setDefaultTyping(mapTyper);
  }

  private static final Logger log = LoggerFactory.getLogger(DefaultEventEnvelopFormatter.class);

  @Override
  public byte[] serialize(EventEnvelope target) {
    try {
      return mapper.writeValueAsBytes(target);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return null;
    }
  }

  @Override
  public EventEnvelope deserialize(byte[] target) {
    try {
      return mapper.readValue(target, EventEnvelope.class);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return null;
    }
  }

  @Override
  public String serializer() {
    return "Jackson";
  }

}
