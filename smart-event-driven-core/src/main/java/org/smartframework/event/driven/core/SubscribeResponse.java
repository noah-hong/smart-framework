package org.smartframework.event.driven.core;

import org.smartframework.event.driven.core.context.ISubscribeResult;

/**
 * @author noah
 * @create 2021-05-25 15:47
 */
public class SubscribeResponse implements ISubscribeResponse {

  @Override
  public ISubscribeResult getSubscribeResult() {
    return subscribeResult;
  }

  private ISubscribeResult subscribeResult;

  @Override
  public void setSubscribeResult(ISubscribeResult subscribeResult) {
    this.subscribeResult = subscribeResult;
  }
}
