package org.smartframework.core;

/**
 * @author noah
 * @create 2021-06-24 13:06
 */
public abstract class LongPersistenceId implements IPersistenceId<Long> {

  private Long value;

  public void setValue(Long value) {
    this.value = value;
  }

  @Override
  public Long getValue() {
    return value;
  }
}
