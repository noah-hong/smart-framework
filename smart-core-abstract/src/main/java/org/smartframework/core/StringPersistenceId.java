package org.smartframework.core;

/**
 * @author noah
 * @create 2021-06-24 13:04
 */
public  abstract class StringPersistenceId implements IPersistenceId<String> {

  private String value;

  public void setValue(String value) {
    this.value = value;
  }

  @Override
  public String getValue() {
    return value;
  }
}
