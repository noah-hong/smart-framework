package org.smartframework.core;

import java.io.Serializable;

/**
 * @author noah
 * @create 2020-04-19 13:33
 */
public interface IVersion extends Serializable {

  default Integer getVersion() {
    return 1;
  }
}
