package org.smartframework.core;

import java.util.UUID;

/**
 * @author noah
 * @create 2021-06-24 13:14
    */
public final class UnknownId extends StringPersistenceId {

  public UnknownId() {
    this.setValue("unknown:" + UUID.randomUUID().toString());
  }


}
