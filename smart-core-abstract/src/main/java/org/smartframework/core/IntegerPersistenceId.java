package org.smartframework.core;

/**
 * @author noah
 * @create 2021-06-24 13:06
 */
public abstract class IntegerPersistenceId implements IPersistenceId<Integer> {

  private Integer value;

  public void setValue(Integer value) {
    this.value = value;
  }

  @Override
  public Integer getValue() {
    return value;
  }
}
