package org.smartframework.core;

import java.io.Serializable;

/**
 * @author noah
 * @create 2020-09-07 13:57
 */
public interface IPersistenceId<IdentityType> extends Serializable {

  IdentityType getValue();

  UnknownId UNKNOWN = new UnknownId();

}
