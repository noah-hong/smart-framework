package org.smartframework.core;

import java.io.Serializable;

/**
 * @author noah
 * @create 2020-08-20 14:56
 */
public interface IAccount<AccountId> extends Serializable {

  String getName();

  AccountId getId();

  String getScope();

  String getWorkerNumber();

  String getMobilePhone();

}
