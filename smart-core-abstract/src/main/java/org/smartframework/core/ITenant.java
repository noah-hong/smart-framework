package org.smartframework.core;

/**
 * @author noah
 * @create 2021-09-13 10:56
 */
public interface ITenant {

  String getTenantId();

  void setTenantId(String tenantId);

}
