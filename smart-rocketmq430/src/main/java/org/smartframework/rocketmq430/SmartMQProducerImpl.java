package org.smartframework.rocketmq430;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.impl.producer.DefaultMQProducerImpl;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.RPCHook;
import org.apache.rocketmq.remoting.exception.RemotingException;

/**
 * @author noah
 * @create 2021-08-17 11:27
 */
public class SmartMQProducerImpl extends DefaultMQProducerImpl {

  public SmartMQProducerImpl(
      DefaultMQProducer defaultMQProducer) {
    super(defaultMQProducer);
  }

  public SmartMQProducerImpl(
      DefaultMQProducer defaultMQProducer, RPCHook rpcHook) {
    super(defaultMQProducer, rpcHook);
  }

  @Override
  public void send(Message msg, SendCallback sendCallback)
      throws MQClientException, RemotingException, InterruptedException {
    super.send(msg, sendCallback);
  }

}
