package org.smartframework.rocketmq430;

/**
 * @author noah
 * @create 2022-03-20 10:59
 */
public class SmartClientConfig {

  protected String namespace;

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }
}
