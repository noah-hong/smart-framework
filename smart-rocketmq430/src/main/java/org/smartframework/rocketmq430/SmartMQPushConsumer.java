package org.smartframework.rocketmq430;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.MessageSelector;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.impl.MQClientAPIImpl;
import org.apache.rocketmq.common.MixAll;
import org.apache.rocketmq.common.TopicConfig;
import org.apache.rocketmq.common.protocol.RequestCode;
import org.apache.rocketmq.common.protocol.ResponseCode;
import org.apache.rocketmq.common.protocol.body.ClusterInfo;
import org.apache.rocketmq.common.protocol.header.namesrv.GetRouteInfoRequestHeader;
import org.apache.rocketmq.common.protocol.heartbeat.SubscriptionData;
import org.apache.rocketmq.common.protocol.route.BrokerData;
import org.apache.rocketmq.remoting.RemotingClient;
import org.apache.rocketmq.remoting.protocol.RemotingCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * @author noah
 * @create 2021-08-11 20:59
 */
public class SmartMQPushConsumer extends DefaultMQPushConsumer {

  Logger logger = LoggerFactory.getLogger(SmartMQPushConsumer.class);
  private SmartClientConfig smartClientConfig = new SmartClientConfig();

  public int getTopicQueueNums() {
    return topicQueueNums;
  }

  public void setTopicQueueNums(int topicQueueNums) {
    this.topicQueueNums = topicQueueNums;
  }

  private int topicQueueNums;

  public SmartMQPushConsumer() {
    super();
  }

  public boolean isEnabledAutoCreateTopic() {
    return enabledAutoCreateTopic;
  }

  public void setEnabledAutoCreateTopic(boolean enabledAutoCreateTopic) {
    this.enabledAutoCreateTopic = enabledAutoCreateTopic;
  }

  private boolean enabledAutoCreateTopic = true;

  public SmartMQPushConsumer(String consumerGroup) {
    this(null, consumerGroup);

  }

  public SmartMQPushConsumer(String namespace, String consumerGroup) {
    super(consumerGroup);
    this.smartClientConfig.setNamespace(namespace);

  }


  protected void autoCreateTopic() {
    if (!enabledAutoCreateTopic) {
      return;
    }
    final MQClientAPIImpl mqClientAPIImpl = this.getDefaultMQPushConsumerImpl().getmQClientFactory()
        .getMQClientAPIImpl();
    final ConcurrentMap<String, SubscriptionData> subscriptionInner = this
        .getDefaultMQPushConsumerImpl()
        .getRebalanceImpl().getSubscriptionInner();
    for (String topicName : subscriptionInner.keySet()) {
      if (topicName.startsWith(MixAll.RETRY_GROUP_TOPIC_PREFIX)) {
        continue;
      }
      final RemotingClient remotingClient = mqClientAPIImpl.getRemotingClient();
      GetRouteInfoRequestHeader requestHeader = new GetRouteInfoRequestHeader();
      requestHeader.setTopic(topicName);
      RemotingCommand request = RemotingCommand
          .createRequestCommand(RequestCode.GET_ROUTEINTO_BY_TOPIC, requestHeader);

      try {
        RemotingCommand response = remotingClient.invokeSync(null, request, 3 * 1000);
        if (response.getCode() == ResponseCode.TOPIC_NOT_EXIST) {
          RemotingCommand cluster_request = RemotingCommand
              .createRequestCommand(RequestCode.GET_BROKER_CLUSTER_INFO, null);

          RemotingCommand cluster_response = remotingClient
              .invokeSync(null, cluster_request, 3 * 1000);
          if (cluster_response.getCode() == ResponseCode.SUCCESS) {
            ClusterInfo clusterInfo = ClusterInfo
                .decode(cluster_response.getBody(), ClusterInfo.class);

            final HashMap<String, BrokerData> brokerAddrTable = clusterInfo.getBrokerAddrTable();
            for (BrokerData brokerData : brokerAddrTable.values()) {
              TopicConfig topicConfig = new TopicConfig();
              topicConfig.setWriteQueueNums(topicQueueNums);
              topicConfig.setReadQueueNums(topicQueueNums);
              topicConfig.setTopicName(topicName);
              mqClientAPIImpl
                  .createTopic(brokerData.selectBrokerAddr(),
                      SmartTopicValidator.AUTO_CREATE_TOPIC_KEY_TOPIC_BY_VERSION4_3, topicConfig,
                      3 * 1000);
              logger
                  .info("主题创建成功,cluster:{},broker:{},topic:{}"
                      , brokerData.getCluster()
                      , brokerData.getBrokerName()
                      , topicName);
            }
          }
        }

      } catch (Exception e) {
        logger.error("主题创建失败", e);
      }
    }
  }

  @Override
  public void start() throws MQClientException {
    super.start();
    autoCreateTopic();

  }

  @Override
  public void subscribe(String topic, String subExpression) throws MQClientException {

//    mqClientAPIImpl.createTopic();
//    this.defaultMQPushConsumerImpl.getmQClientFactory().getMQClientAPIImpl().createTopic();
    super.subscribe(withNamespace(topic), subExpression);
  }

  @Override
  public void subscribe(String topic, String fullClassName, String filterClassSource)
      throws MQClientException {
    super.subscribe(withNamespace(topic), fullClassName, filterClassSource);
  }

  @Override
  public void subscribe(String topic, MessageSelector messageSelector) throws MQClientException {
    super.subscribe(withNamespace(topic), messageSelector);
  }



  public String withNamespace(String resource) {
    return NamespaceUtil.wrapNamespace(smartClientConfig.getNamespace(), resource);
  }



}
