package org.smartframework.rocketmq430;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.impl.MQClientAPIImpl;
import org.apache.rocketmq.client.impl.producer.TopicPublishInfo;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.TopicConfig;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.common.protocol.RequestCode;
import org.apache.rocketmq.common.protocol.ResponseCode;
import org.apache.rocketmq.common.protocol.body.ClusterInfo;
import org.apache.rocketmq.common.protocol.header.namesrv.GetRouteInfoRequestHeader;
import org.apache.rocketmq.common.protocol.route.BrokerData;
import org.apache.rocketmq.remoting.RemotingClient;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.apache.rocketmq.remoting.protocol.RemotingCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;


/**
 * @author noah
 * @create 2021-08-17 11:25
 */
public class SmartMQProducer extends DefaultMQProducer {


  private SmartClientConfig smartClientConfig = new SmartClientConfig();
  private static final Logger logger = LoggerFactory.getLogger(SmartMQProducer.class);

  private boolean enabledAutoCreateTopic = true;

  public boolean isEnabledAutoCreateTopic() {
    return enabledAutoCreateTopic;
  }

  public void setEnabledAutoCreateTopic(boolean enabledAutoCreateTopic) {
    this.enabledAutoCreateTopic = enabledAutoCreateTopic;
  }

  public SmartMQProducer(String producerGroup) {
    super(producerGroup);
  }

  public SmartMQProducer(String namespace, String producerGroup) {
    super(producerGroup);
  }

  private Message rebuildMsg(Message msg) {
    final String topic = withNamespace(msg.getTopic());
    autoCreateTopic(topic);
    msg.setTopic(topic);
    return msg;
  }

  @Override
  public SendResult send(Message msg, MessageQueueSelector selector, Object arg, long timeout)
      throws MQClientException, RemotingException, MQBrokerException, InterruptedException {

    return this.defaultMQProducerImpl.send(rebuildMsg(msg), selector, arg, timeout);
  }

  @Override
  public SendResult send(Message msg)
      throws MQClientException, RemotingException, MQBrokerException, InterruptedException {
    return this.defaultMQProducerImpl.send(rebuildMsg(msg));
  }

  @Override
  public SendResult send(Message msg, long timeout)
      throws MQClientException, RemotingException, MQBrokerException, InterruptedException {
    return this.defaultMQProducerImpl.send(rebuildMsg(msg), timeout);
  }

  @Override
  public void send(Message msg, SendCallback sendCallback)
      throws MQClientException, RemotingException, InterruptedException {
    this.defaultMQProducerImpl.send(rebuildMsg(msg), sendCallback);
  }

  @Override
  public void send(Message msg, SendCallback sendCallback, long timeout)
      throws MQClientException, RemotingException, InterruptedException {
    this.defaultMQProducerImpl.send(rebuildMsg(msg), sendCallback, timeout);
  }

  @Override
  public void sendOneway(Message msg)
      throws MQClientException, RemotingException, InterruptedException {
    this.defaultMQProducerImpl.sendOneway(rebuildMsg(msg));
  }

  @Override
  public SendResult send(Message msg, MessageQueue mq)
      throws MQClientException, RemotingException, MQBrokerException, InterruptedException {
    return this.defaultMQProducerImpl.send(rebuildMsg(msg), mq);
  }

  @Override
  public SendResult send(Message msg, MessageQueue mq, long timeout)
      throws MQClientException, RemotingException, MQBrokerException, InterruptedException {
    return this.defaultMQProducerImpl.send(rebuildMsg(msg), mq, timeout);
  }

  @Override
  public void send(Message msg, MessageQueue mq, SendCallback sendCallback)
      throws MQClientException, RemotingException, InterruptedException {
    this.defaultMQProducerImpl.send(rebuildMsg(msg), sendCallback);
  }

  @Override
  public void send(Message msg, MessageQueue mq, SendCallback sendCallback, long timeout)
      throws MQClientException, RemotingException, InterruptedException {
    this.defaultMQProducerImpl.send(rebuildMsg(msg), mq, sendCallback, timeout);
  }

  @Override
  public void sendOneway(Message msg, MessageQueue mq)
      throws MQClientException, RemotingException, InterruptedException {
    this.defaultMQProducerImpl.sendOneway(rebuildMsg(msg), mq);
  }

  @Override
  public SendResult send(Message msg, MessageQueueSelector selector, Object arg)
      throws MQClientException, RemotingException, MQBrokerException, InterruptedException {
    return this.defaultMQProducerImpl.send(rebuildMsg(msg), selector, arg);
  }

  @Override
  public void send(Message msg, MessageQueueSelector selector, Object arg,
      SendCallback sendCallback) throws MQClientException, RemotingException, InterruptedException {
    this.defaultMQProducerImpl.send(rebuildMsg(msg), selector, arg, sendCallback);

  }

  @Override
  public void send(Message msg, MessageQueueSelector selector, Object arg,
      SendCallback sendCallback, long timeout)
      throws MQClientException, RemotingException, InterruptedException {

    this.defaultMQProducerImpl.send(rebuildMsg(msg), selector, arg, sendCallback, timeout);
  }

  @Override
  public void sendOneway(Message msg, MessageQueueSelector selector, Object arg)
      throws MQClientException, RemotingException, InterruptedException {
    super.sendOneway(msg, selector, arg);
    this.defaultMQProducerImpl.sendOneway(rebuildMsg(msg), selector, arg);
  }

//  @Override
//  public TransactionSendResult sendMessageInTransaction(Message msg, Object arg)
//      throws MQClientException {
//    return super.sendMessageInTransaction(msg, arg);
//    this.defaultMQProducerImpl.sendMessageInTransaction(rebuildMsg(msg), arg);
//  }

  protected void autoCreateTopic(String topicName) {
    if (!enabledAutoCreateTopic) {
      return;
    }
    final TopicPublishInfo topicPublishInfo = this.defaultMQProducerImpl.getTopicPublishInfoTable()
        .get(topicName);
    if (null == topicPublishInfo || !topicPublishInfo.ok()) {
      final MQClientAPIImpl mqClientAPIImpl = this.defaultMQProducerImpl.getmQClientFactory()
          .getMQClientAPIImpl();

      final RemotingClient remotingClient = mqClientAPIImpl.getRemotingClient();
      GetRouteInfoRequestHeader requestHeader = new GetRouteInfoRequestHeader();
      requestHeader.setTopic(topicName);
      RemotingCommand request = RemotingCommand
          .createRequestCommand(RequestCode.GET_ROUTEINTO_BY_TOPIC, requestHeader);
      try {
        RemotingCommand response = remotingClient.invokeSync(null, request, 3 * 1000);
        if (response.getCode() == ResponseCode.TOPIC_NOT_EXIST) {
          RemotingCommand cluster_request = RemotingCommand
              .createRequestCommand(RequestCode.GET_BROKER_CLUSTER_INFO, null);
          RemotingCommand cluster_response = remotingClient
              .invokeSync(null, cluster_request, 3 * 1000);
          if (cluster_response.getCode() == ResponseCode.SUCCESS) {
            ClusterInfo clusterInfo = ClusterInfo
                .decode(cluster_response.getBody(), ClusterInfo.class);

            final HashMap<String, BrokerData> brokerAddrTable = clusterInfo.getBrokerAddrTable();
            for (BrokerData brokerData : brokerAddrTable.values()) {
              TopicConfig topicConfig = new TopicConfig();
              topicConfig.setWriteQueueNums(this.getDefaultTopicQueueNums());
              topicConfig.setReadQueueNums(this.getDefaultTopicQueueNums());
              topicConfig.setTopicName(topicName);
              mqClientAPIImpl
                  .createTopic(brokerData.selectBrokerAddr(),
                      SmartTopicValidator.AUTO_CREATE_TOPIC_KEY_TOPIC_BY_VERSION4_3, topicConfig,
                      3 * 1000);
              logger
                  .info("自动创建主题成功,cluster:{},broker:{},topic:{}"
                      , brokerData.getCluster()
                      , brokerData.getBrokerName()
                      , topicName);
            }
          }
        }

      } catch (Exception e) {
        logger.error("自动创建主题失败", e);
      }
    }

  }

  public String withNamespace(String resource) {
    return NamespaceUtil.wrapNamespace(smartClientConfig.getNamespace(), resource);
  }

}
