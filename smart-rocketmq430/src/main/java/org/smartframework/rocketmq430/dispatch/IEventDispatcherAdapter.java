package org.smartframework.rocketmq430.dispatch;

import org.smartframework.event.driven.core.EventDispatchResult;
import org.smartframework.event.driven.core.EventEnvelope;
import org.smartframework.event.driven.core.IEventDispatcher;

/**
 * @author noah
 * @create 2022-03-21 13:43
 */
public interface IEventDispatcherAdapter extends IEventDispatcher {
  EventDispatchResult dispatch(EventEnvelope storedEvent);

}
