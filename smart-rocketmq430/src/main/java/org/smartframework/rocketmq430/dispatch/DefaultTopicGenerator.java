package org.smartframework.rocketmq430.dispatch;

import org.apache.commons.lang3.StringUtils;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.TopicModel;
import org.smartframework.event.driven.core.annotation.SubTopic;
import org.smartframework.event.driven.core.annotation.Topic;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;


/**
 * @author noah
 * @create 2020-09-13 16:31
 */
public class DefaultTopicGenerator implements ITopicGenerator {

  @Override
  public TopicModel generate(Class<? extends IEvent> aClass) {
    final String topic = getTopicName(aClass);
    final String subTopic = getSubTopicName(aClass);
    final TopicModel topicModel = new TopicModel();
    topicModel.setTopic(topic);
    topicModel.setSubTopic(subTopic);
    Objects.requireNonNull(topicModel.getTopic(), "事件未发现@Topic注解");

    if (StringUtils.isEmpty(subTopic)) {
      topicModel.setSubTopic("*");
    }
    return topicModel;
  }

//  @Override
//  public TopicModel generateTopic(IEvent event) {
//    return generateTopic(IEvent.class);
//  }

  private String getTopicName(Class<?> aClass) {
    if (aClass == null) {
      return null;
    }
    final Optional<Annotation> anno =
        Arrays.stream(aClass.getAnnotations()).filter(x -> x instanceof Topic).findFirst();

    if (anno.isPresent()) {
      Topic topic = (Topic) anno.get();
      if (StringUtils.isEmpty(topic.value())) {
        return aClass.getName().replace(".", "-");
      }
      return topic.value();
    }

    return getTopicName(aClass.getSuperclass());
  }

  private String getSubTopicName(Class<?> aClass) {
    if (aClass == null) {
      return null;
    }
    final Optional<Annotation> anno =
        Arrays.stream(aClass.getAnnotations()).filter(x -> x instanceof SubTopic).findFirst();

    if (anno.isPresent()) {
      SubTopic topic = (SubTopic) anno.get();
      if (StringUtils.isEmpty(topic.value())) {
        return aClass.getName().replace(".", "-");
      }
      return topic.value();
    }

    return getSubTopicName(aClass.getSuperclass());
  }
}
