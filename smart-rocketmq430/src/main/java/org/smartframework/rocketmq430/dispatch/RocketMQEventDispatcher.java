package org.smartframework.rocketmq430.dispatch;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.client.producer.selector.SelectMessageQueueByHash;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.smartframework.event.driven.core.DispatchStatus;
import org.smartframework.event.driven.core.EventDispatchResult;
import org.smartframework.event.driven.core.EventEnvelope;
import org.smartframework.event.driven.core.IEvent;
import org.smartframework.event.driven.core.ITopicGenerator;
import org.smartframework.event.driven.core.TopicModel;
import org.smartframework.event.driven.core.store.IEventEnvelopeFormatter;
import org.smartframework.event.driven.core.store.StoredEvent;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;


/**
 * @author noah
 * @create 2021-04-29 09:17
 */
public class RocketMQEventDispatcher implements IEventDispatcherAdapter {

  private static final ITopicGenerator topicGenerator = new DefaultTopicGenerator();
  private static final IEventEnvelopeFormatter eventEnvelopeSerializer = new DefaultEventEnvelopFormatter();
  private static final Logger log = LoggerFactory.getLogger(RocketMQEventDispatcher.class);

  protected final DefaultMQProducer producer;
  protected final MessageQueueSelector messageQueueSelector = new SelectMessageQueueByHash();

  public RocketMQEventDispatcher(DefaultMQProducer producer) {
    this.producer = producer;
  }

  private Message createMessage(String topic, String tag, byte[] payload) {
    if (Objects.isNull(tag)) {
      tag = "";
    }
    return new Message(topic, tag, payload);
  }

  @Override
  public EventDispatchResult dispatch(StoredEvent event) {
    Objects.requireNonNull(event.getPayload());
    Objects.requireNonNull(event.getTopic());
    Objects.requireNonNull(event.getLocation());
    Objects.requireNonNull(event.getEventType());
    Objects.requireNonNull(event.getEventId());
    String hashKey = event.getLocation();
    SendResult sendResult = null;
    long now = System.currentTimeMillis();
    try {
      Message rocketMsg =
          createMessage(
              event.getTopic(),
              event.getSubTopic(),
              event.getPayload());
      if (event.getProperties() != null) {
        final Map<String, String> properties = event.getProperties();
        for (String key : properties.keySet()) {
          rocketMsg.getProperties().put(key, properties.get(key));
        }
      }
//      rocketMsg.getProperties().put(Properties4RocketMQ.PAYLOAD_CLASS_NAME, event.getEventType());
      rocketMsg.getProperties().put(Properties4RocketMQ.PAYLOAD_SERIALIZER, event.getSerializer());
      rocketMsg.setKeys(event.getEventId());
      rocketMsg.setWaitStoreMsgOK(true);
      log.debug("消息即将发送. 主题:{}, 消息编码:{}", event.getTopic(), event.getEventId());
      sendResult =
          producer.send(rocketMsg, messageQueueSelector, hashKey, producer.getSendMsgTimeout());
    } catch (Exception e) {
      final String sendStatus = Optional.ofNullable(sendResult)
          .map(x -> x.getSendStatus().name())
          .orElse("NONE");
      log.error("消息发送失败:{}, 主题:{},消息编码:{},发送状态:{} ", e.getMessage(), event.getTopic(),
          event.getEventId(),
          sendStatus);
    }
    long costTime = System.currentTimeMillis() - now;
    if (Objects.nonNull(sendResult) && sendResult.getSendStatus().equals(SendStatus.SEND_OK)) {
      log.debug("消息发送成功, 花费:{} 毫秒, 消息编号:{}, 消息编码：{}", costTime, sendResult.getMsgId(),
          event.getEventId());
      EventDispatchResult eventDispatchResult = new EventDispatchResult();
      eventDispatchResult.setDispatchStatus(DispatchStatus.SUCCESS);
      return eventDispatchResult;
    } else {
      log.warn("消息发送失败, 花费:{} 毫秒, 消息编码：{}", costTime,
          event.getEventId());
      EventDispatchResult eventDispatchResult = new EventDispatchResult();
      eventDispatchResult.setDispatchStatus(DispatchStatus.FALIURE);
      return eventDispatchResult;
    }
  }

  @Override
  public EventDispatchResult dispatch(EventEnvelope eventEnvelope) {

    Objects.requireNonNull(eventEnvelope);
    StoredEvent storedEvent = new StoredEvent();

    IEvent<?> event = eventEnvelope.getEvent();
    storedEvent.setLocation(String
        .format("%s-%s", event.getTagName(), event.getPersistenceId().getValue().toString()));
    storedEvent.setPersistenceId(event.getPersistenceId().getValue().toString());

    final Class<? extends IEvent> aClass = event.getClass();

    TopicModel topicModel = topicGenerator.generate(aClass);
    storedEvent.setPayload(eventEnvelopeSerializer.serialize(eventEnvelope));
    storedEvent.setTopic(topicModel.getTopic());
    storedEvent.setSubTopic(topicModel.getSubTopic());

    storedEvent.setTagName(eventEnvelope.getEvent().getTagName());
    storedEvent.setEventId(eventEnvelope.getEventId());
    storedEvent.setEventType(eventEnvelope.getEvent().getClass().getName());
    storedEvent.setSenderName(eventEnvelope.getSender().getName());
    storedEvent.setSenderId(eventEnvelope.getSender().getId());
    storedEvent.setSenderScope(eventEnvelope.getSender().getScope());
    storedEvent.setStoredTime(eventEnvelope.getOccurredOn());
    storedEvent.setVersion(eventEnvelope.getEvent().getVersion());
    storedEvent.setSerializer(eventEnvelopeSerializer.serializer());

    return dispatch(storedEvent);
  }
}
